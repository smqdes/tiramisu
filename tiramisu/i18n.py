# -*- coding: UTF-8 -*-
# Copyright (C) 2012-2019 Team tiramisu (see AUTHORS for all contributors)
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# The original `Config` design model is unproudly borrowed from
# the rough gus of pypy: pypy: http://codespeak.net/svn/pypy/dist/pypy/config/
# the whole pypy projet is under MIT licence
"internationalisation utilities"
import gettext
import os
import sys
import locale

# Application Name
APP_NAME = 'tiramisu'

# Traduction dir
APP_DIR = os.path.join(sys.prefix, 'share')
LOCALE_DIR = os.path.join(APP_DIR, 'locale')

# Default Lanugage
DEFAULT_LANG = os.environ.get('LANG', '').split(':')
DEFAULT_LANG += ['en_US']

languages = []
lc, encoding = locale.getdefaultlocale()
if lc:
    languages = [lc]

languages += DEFAULT_LANG
mo_location = LOCALE_DIR

gettext.find(APP_NAME, mo_location)
gettext.textdomain(APP_NAME)
gettext.bind_textdomain_codeset(APP_NAME, "UTF-8")

t = gettext.translation(APP_NAME, fallback=True)

_ = t.gettext
