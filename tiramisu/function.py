# Copyright (C) 2018-2019 Team tiramisu (see AUTHORS for all contributors)
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from .i18n import _


class Params:
    __slots__ = ('args', 'kwargs')
    def __init__(self, args=None, kwargs=None, **kwgs):
        if args is None:
            args = tuple()
        if kwargs is None:
            kwargs = {}
        if kwgs:
            kwargs.update(kwgs)
        if isinstance(args, Param):
            args = (args,)
        else:
            if not isinstance(args, tuple):
                raise ValueError(_('args in params must be a tuple'))
            for arg in args:
                if not isinstance(arg, Param):
                    raise ValueError(_('arg in params must be a Param'))
        if not isinstance(kwargs, dict):
            raise ValueError(_('kwargs in params must be a dict'))
        for arg in kwargs.values():
            if not isinstance(arg, Param):
                raise ValueError(_('arg in params must be a Param'))
        self.args = args
        self.kwargs = kwargs


class Param:
    pass


class ParamOption(Param):
    __slots__ = ('option', 'notraisepropertyerror')
    def __init__(self, option, notraisepropertyerror=False):
        if not hasattr(option, 'impl_is_symlinkoption'):
            raise ValueError(_('paramoption needs an option not {}').format(type(option)))
        if option.impl_is_symlinkoption():
            cur_opt = option.impl_getopt()
        else:
            cur_opt = option
        if not isinstance(notraisepropertyerror, bool):
            raise ValueError(_('param must have a boolean'
                               ' not a {} for notraisepropertyerror'
                              ).format(type(notraisepropertyerror)))

        self.option = cur_opt
        self.notraisepropertyerror = notraisepropertyerror


class ParamValue(Param):
    __slots__ = ('value',)
    def __init__(self, value):
        self.value = value


class ParamContext(Param):
    __slots__ = tuple()


class ParamIndex(Param):
    __slots__ = tuple()


def tiramisu_copy(val):  # pragma: no cover
    return val
