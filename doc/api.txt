Auto generated library's API
================================

.. autosummary::
    :toctree: api
    :template: module.rst

    tiramisu.option
    tiramisu.setting
    tiramisu.config
    tiramisu.value
    tiramisu.autolib
    tiramisu.error
    tiramisu.storage

