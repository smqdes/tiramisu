Errors that may be encountered
==================================

Three builtins exceptions are used : 
-----------------------------------------

- **ValueError** : Validation error, parameters error, a list instead 
                   of a Multi, or if the value does not make sense

- **TypeError** : type error in a parameter

- **AttributeError** : wrong path or unknownd  option or optiondescription 
    
And five other exceptions :
------------------------------

.. automodule:: tiramisu.error
   :members:
   
