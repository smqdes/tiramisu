#!/usr/bin/env python
# -*- coding: utf-8 -*-
from distutils.core import setup
from os.path import dirname, abspath, join, normpath, isdir
from os import listdir
import os


package_name = os.environ.get('PACKAGE_DST', 'tiramisu')

def fetch_version():
    """Get version from version.in"""
    return open('VERSION', 'r').readline().strip()


def return_files(component):
    here = dirname(abspath(__file__))
    path = normpath(join(here, 'tiramisu', component))
    dir_content = [content for content in listdir(path)
                   if not content == '__pycache__']
    paths = filter(isdir, [join(path, content)
                      for content in dir_content])
    lst = ['.'.join(path.split('/')[-3:]) for path in paths]
    #lst = [package_name + '.' + '.'.join(path.split('/')[-2:]) for path in paths]
    return lst


packages = [package_name, package_name + '.storage', package_name + '.option']
packages.extend(return_files('storage'))
packages.extend(return_files('option'))

if package_name != 'tiramisu':
    package_dir = {package_name: 'tiramisu'}
else:
    package_dir = {}

setup(
    author="Tiramisu's team",
    author_email='contact@cadoles.com',
    name=package_name,
    version=fetch_version(),
    description='an options controller tool',
    url='http://tiramisu.labs.libre-entreprise.org/',
    classifiers=[
        "Programming Language :: Python",
        "Programming Language :: Python :: 2",
        "Programming Language :: Python :: 3",
        "Development Status :: 4 - Beta",
        "Environment :: Other Environment",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: GNU Library or Lesser General Public License (LGPL)",
        "Operating System :: OS Independent",
        "Topic :: Software Development :: Libraries :: Python Modules",
        "Topic :: Text Processing :: Linguistic"
    ],
    long_description="""\
An options controller tool
-------------------------------------

Due to more and more available options required to set up an operating system,
compiler options or whatever, it became quite annoying to hand the necessary
options to where they are actually used and even more annoying to add new
options. To circumvent these problems the configuration control was
introduced...

Tiramisu is an options handler and an options controller, wich aims at
producing flexible and fast options access.


This version requires Python 2.6 or later.
""",
    packages=packages,
    package_dir=package_dir
)
