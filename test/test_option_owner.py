from .autopath import do_autopath
do_autopath()

from py.test import raises

from tiramisu.setting import owners, groups
from tiramisu import ChoiceOption, BoolOption, IntOption, FloatOption, \
    StrOption, OptionDescription, SymLinkOption, Leadership, Config
from tiramisu.error import ConfigError, ConstError, PropertiesOptionError, APIError
from tiramisu.storage import list_sessions


def teardown_function(function):
    assert list_sessions() == [], 'session list is not empty when leaving "{}"'.format(function.__name__)


def make_description():
    gcoption = ChoiceOption('name', 'GC name', ['ref', 'framework'], 'ref')
    gcdummy = BoolOption('dummy', 'dummy', default=False)
    objspaceoption = ChoiceOption('objspace', 'Object space',
                                  ['std', 'thunk'], 'std')
    booloption = BoolOption('bool', 'Test boolean option', default=True)
    intoption = IntOption('int', 'Test int option', default=0)
    floatoption = FloatOption('float', 'Test float option', default=2.3)
    stroption = StrOption('str', 'Test string option', default="abc")
    boolop = BoolOption('boolop', 'Test boolean option op', default=True)
    wantref_option = BoolOption('wantref', 'Test requires', default=False)
    wantframework_option = BoolOption('wantframework', 'Test requires',
                                      default=False)

    gcgroup = OptionDescription('gc', '', [gcoption, gcdummy, floatoption])
    descr = OptionDescription('tiram', '', [gcgroup, booloption, objspaceoption,
                                            wantref_option, stroption,
                                            wantframework_option,
                                            intoption, boolop])
    return descr


def test_default_owner():
    gcdummy = BoolOption('dummy', 'dummy', default=False)
    descr = OptionDescription('tiramisu', '', [gcdummy])
    api = Config(descr)
    assert api.option('dummy').value.get() is False
    assert api.option('dummy').owner.get() == 'default'
    api.option('dummy').value.set(True)
    owner = api.owner.get()
    assert api.option('dummy').owner.get() == owner


def test_hidden_owner():
    gcdummy = BoolOption('dummy', 'dummy', default=False, properties=('hidden',))
    descr = OptionDescription('tiramisu', '', [gcdummy])
    api = Config(descr)
    api.property.read_write()
    #raises(PropertiesOptionError, "api.forcepermissive.option('dummy').owner.get()")
    #raises(PropertiesOptionError, "api.option('dummy').owner.isdefault()")
    #raises(PropertiesOptionError, "api.forcepermissive.option('dummy').owner.isdefault()")
    api.permissive.set(frozenset(['hidden']))
    api.forcepermissive.option('dummy').value.get()
    api.forcepermissive.option('dummy').owner.isdefault()


def test_addowner():
    gcdummy = BoolOption('dummy', 'dummy', default=False)
    descr = OptionDescription('tiramisu', '', [gcdummy])
    api = Config(descr)
    assert api.option('dummy').value.get() is False
    assert api.option('dummy').owner.get() == 'default'
    assert api.option('dummy').owner.isdefault()
    api.owner.set('gen_config')
    api.option('dummy').value.set(True)
    assert api.option('dummy').owner.get() == owners.gen_config
    assert not api.option('dummy').owner.isdefault()


def test_addowner_multiple_time():
    owners.addowner("testowner2")
    raises(ConstError, 'owners.addowner("testowner2")')


def test_delete_owner():
    owners.addowner('deleted2')
    raises(ConstError, 'del(owners.deleted2)')


def test_owner_is_not_a_string():
    gcdummy = BoolOption('dummy', 'dummy', default=False)
    descr = OptionDescription('tiramisu', '', [gcdummy])
    api = Config(descr)
    assert api.option('dummy').value.get() is False
    assert api.option('dummy').owner.get() == owners.default
    assert api.option('dummy').owner.get() == 'default'
    assert isinstance(api.option('dummy').owner.get(), owners.Owner)
    api.option('dummy').value.set(True)
    assert api.option('dummy').owner.get() == 'user'


def test_setowner_without_valid_owner():
    gcdummy = BoolOption('dummy', 'dummy', default=False)
    descr = OptionDescription('tiramisu', '', [gcdummy])
    api = Config(descr)
    assert api.option('dummy').value.get() is False
    assert api.option('dummy').owner.get() == 'default'


def test_setowner_for_value():
    gcdummy = BoolOption('dummy', 'dummy', default=False)
    descr = OptionDescription('tiramisu', '', [gcdummy])
    api = Config(descr)
    assert api.option('dummy').value.get() is False
    assert api.option('dummy').owner.get() == 'default'
    owners.addowner("new2")
    raises(ConfigError, "api.option('dummy').owner.set('new2')")
    api.option('dummy').value.set(False)
    assert api.option('dummy').owner.get() == owners.user
    api.option('dummy').owner.set('new2')
    assert api.option('dummy').owner.get() == owners.new2


def test_setowner_forbidden():
    gcdummy = BoolOption('dummy', 'dummy', default=False)
    descr = OptionDescription('tiramisu', '', [gcdummy])
    api = Config(descr)
    assert api.option('dummy').value.get() is False
    assert api.option('dummy').owner.get() == 'default'
    raises(ValueError, "api.owner.set('default')")
    api.option('dummy').value.set(False)
    raises(ValueError, "api.option('dummy').owner.set('default')")


def test_setowner_read_only():
    gcdummy = BoolOption('dummy', 'dummy', default=False)
    descr = OptionDescription('tiramisu', '', [gcdummy])
    api = Config(descr)
    api.property.read_write()
    assert api.option('dummy').value.get() is False
    assert api.option('dummy').owner.get() == 'default'
    owners.addowner("readonly2")
    api.option('dummy').value.set(False)
    assert api.option('dummy').owner.get() == owners.user
    api.property.read_only()
    raises(PropertiesOptionError,
           "api.option('dummy').owner.set('readonly2')")
    assert api.option('dummy').owner.get() == owners.user


def test_setowner_optiondescription():
    gcdummy = BoolOption('dummy', 'dummy', default=False)
    descr1 = OptionDescription('tiramisu', '', [gcdummy])
    descr = OptionDescription('tiramisu', '', [descr1])
    api = Config(descr)
    raises(APIError, "api.option('tiramisu').owner.get()")
    raises(APIError, "api.option('tiramisu').owner.set('user')")


def test_setowner_symlinkoption():
    gcdummy = BoolOption('dummy', 'dummy', default=False)
    s = SymLinkOption('symdummy', gcdummy)
    descr1 = OptionDescription('tiramisu', '', [gcdummy, s])
    descr = OptionDescription('tiramisu', '', [descr1])
    api = Config(descr)
    assert api.option('tiramisu.symdummy').owner.isdefault()
    api.option('tiramisu.dummy').value.set(True)
    assert not api.option('tiramisu.symdummy').owner.isdefault()
    raises(ConfigError, "api.option('tiramisu.symdummy').owner.set('user')")


def test_owner_leadership():
    b = IntOption('int', 'Test int option', default=[0], multi=True)
    c = StrOption('str', 'Test string option', multi=True)
    descr = Leadership("int", "", [b, c])
    od = OptionDescription('od', '', [descr])
    api = Config(od)
    raises(ConfigError, "api.option('int.str', 0).owner.set('user')")

    api.option('int.int').value.set([0, 1])
    api.option('int.str', 0).value.set('yes')
    assert not api.option('int.str', 0).owner.isdefault()
    assert api.option('int.str', 1).owner.isdefault()
    api.option('int.str', 0).owner.set('user')
    assert api.option('int.str', 0).owner.get() == owners.user
    assert api.option('int.str', 1).owner.isdefault()
    assert api.option('int.str', 0).value.get() == 'yes'
    assert api.option('int.str', 1).value.get() == None
