"""these tests are here to create some :class:`tiramisu.option.Option`'s
and to compare them
"""
from .autopath import do_autopath
do_autopath()

from py.test import raises

from tiramisu.error import APIError, ConfigError
from tiramisu import IntOption, SymLinkOption, OptionDescription, Config
from tiramisu.setting import groups
from tiramisu.storage import list_sessions


def teardown_function(function):
    assert list_sessions() == [], 'session list is not empty when leaving "{}"'.format(function.__name__)


def a_func():
    return None


def test_option_valid_name():
    IntOption('test', '')
    raises(ValueError, 'IntOption(1, "")')
    # raises(ValueError, 'IntOption("1test", "")')
    i = IntOption("test1", "")
    # raises(ValueError, 'IntOption("_test", "")')
    # raises(ValueError, 'IntOption("   ", "")')
    raises(ValueError, 'SymLinkOption(1, i)')
    i = SymLinkOption("test1", i)


def test_option_with_callback():
    # no default value with callback
    raises(ValueError, "IntOption('test', '', default=1, callback=a_func)")


def test_option_get_information():
    description = "it's ok"
    string = 'some informations'
    i = IntOption('test', description)
    raises(ValueError, "i.impl_get_information('noinfo')")
    i.impl_set_information('info', string)
    assert i.impl_get_information('info') == string
    raises(ValueError, "i.impl_get_information('noinfo')")
    assert i.impl_get_information('noinfo', 'default') == 'default'
    assert i.impl_get_information('doc') == description


def test_option_get_information_config():
    description = "it's ok"
    string = 'some informations'
    string
    i = IntOption('test', description)
    od = OptionDescription('od', '', [i])
    Config(od)
    raises(ValueError, "i.impl_get_information('noinfo')")
    raises(AttributeError, "i.impl_set_information('info', string)")
#    assert i.impl_get_information('info') == string
    raises(ValueError, "i.impl_get_information('noinfo')")
    assert i.impl_get_information('noinfo', 'default') == 'default'
    assert i.impl_get_information('doc') == description


def test_option_get_information_config2():
    description = "it's ok"
    string = 'some informations'
    i = IntOption('test', description)
    i.impl_set_information('info', string)
    od = OptionDescription('od', '', [i])
    Config(od)
    raises(ValueError, "i.impl_get_information('noinfo')")
    raises(AttributeError, "i.impl_set_information('info', 'hello')")
    assert i.impl_get_information('info') == string
    raises(ValueError, "i.impl_get_information('noinfo')")
    assert i.impl_get_information('noinfo', 'default') == 'default'
    assert i.impl_get_information('doc') == description


def test_optiondescription_get_information():
    description = "it's ok"
    string = 'some informations'
    o = OptionDescription('test', description, [])
    o.impl_set_information('info', string)
    assert o.impl_get_information('info') == string
    raises(ValueError, "o.impl_get_information('noinfo')")
    assert o.impl_get_information('noinfo', 'default') == 'default'
    assert o.impl_get_information('doc') == description


def test_option_isoptiondescription():
    i = IntOption('test', '')
    od = OptionDescription('od', '', [i])
    od = OptionDescription('od', '', [od])
    cfg = Config(od)
    assert cfg.option('od').option.isoptiondescription()
    assert not cfg.option('od.test').option.isoptiondescription()


def test_option_double():
    i = IntOption('test', '')
    od = OptionDescription('od1', '', [i])
    od = OptionDescription('od2', '', [od])
    od = OptionDescription('od3', '', [od])
    cfg = Config(od)
    assert cfg.option('od2.od1.test').value.get() is None
    assert cfg.option('od2').option('od1').option('test').value.get() is None


def test_option_multi():
    IntOption('test', '', multi=True)
    IntOption('test', '', multi=True, default_multi=1)
    IntOption('test', '', default=[1], multi=True, default_multi=1)
    #add default_multi to not multi's option
    raises(ValueError, "IntOption('test', '', default_multi=1)")
    #unvalid default_multi
    raises(ValueError, "IntOption('test', '', multi=True, default_multi='yes')")
    #not default_multi with callback
    raises(ValueError, "IntOption('test', '', multi=True, default_multi=1, callback=a_func)")


def test_unknown_option():
    i = IntOption('test', '')
    od1 = OptionDescription('od', '', [i])
    od2 = OptionDescription('od', '', [od1])
    api = Config(od2)
    # test is an option, not an optiondescription
    raises(TypeError, "api.option('od.test.unknown').value.get()")
    # unknown is an unknown option
    raises(AttributeError, "api.option('unknown').value.get()")
    # unknown is an unknown option
    raises(AttributeError, "api.option('od.unknown').value.get()")
    # unknown is an unknown optiondescription
    raises(AttributeError, "api.option('od.unknown.suboption').value.get()")


def test_optiondescription_list():
    groups.notfamily1 = groups.GroupType('notfamily1')
    i = IntOption('test', '')
    i2 = IntOption('test', '')
    od1 = OptionDescription('od', '', [i])
    od1.impl_set_group_type(groups.family)
    od3 = OptionDescription('od2', '', [i2])
    od3.impl_set_group_type(groups.notfamily1)
    od2 = OptionDescription('od', '', [od1, od3])
    od4 = OptionDescription('od', '', [od2])
    api = Config(od4)
    assert len(list(api.option('od').list('option'))) == 0
    assert len(list(api.option('od').list('optiondescription'))) == 2
    assert len(list(api.option('od').list('optiondescription', group_type=groups.family))) == 1
    assert len(list(api.option('od').list('optiondescription', group_type=groups.notfamily1))) == 1
    assert len(list(api.option('od.od').list('option'))) == 1
    assert len(list(api.option('od.od2').list('option'))) == 1
    try:
        list(api.option('od').list('unknown'))
    except AssertionError:
        pass
    else:
        raise Exception('must raise')
    try:
        list(api.option('od').list('option', group_type='toto'))
    except AssertionError:
        pass
    else:
        raise Exception('must raise')


def test_optiondescription_group():
    groups.notfamily = groups.GroupType('notfamily')
    i = IntOption('test', '')
    i2 = IntOption('test', '')
    od1 = OptionDescription('od', '', [i])
    od1.impl_set_group_type(groups.family)
    od3 = OptionDescription('od2', '', [i2])
    od3.impl_set_group_type(groups.notfamily)
    od2 = OptionDescription('od', '', [od1, od3])
    api = Config(od2)
    assert len(list(api.option.list('option'))) == 0
    assert len(list(api.option.list('optiondescription'))) == 2
    assert len(list(api.option.list('optiondescription', group_type=groups.family))) == 1
    assert len(list(api.option.list('optiondescription', group_type=groups.notfamily))) == 1
    try:
        list(api.option.list('unknown'))
    except AssertionError:
        pass
    else:
        raise Exception('must raise')
    try:
        list(api.option.list('option', group_type='toto'))
    except AssertionError:
        pass
    else:
        raise Exception('must raise')


def test_optiondescription_group_redefined():
    try:
        groups.notfamily = groups.GroupType('notfamily')
    except:
        pass
    i = IntOption('test', '')
    od1 = OptionDescription('od', '', [i])
    od1.impl_set_group_type(groups.family)
    raises(ValueError, "od1.impl_set_group_type(groups.notfamily)")


def test_optiondescription_group_leadership():
    i = IntOption('test', '')
    od1 = OptionDescription('od', '', [i])
    raises(ConfigError, "od1.impl_set_group_type(groups.leadership)")



def test_asign_optiondescription():
    i = IntOption('test', '')
    od1 = OptionDescription('od', '', [i])
    od2 = OptionDescription('od', '', [od1])
    api = Config(od2)
    raises(APIError, "api.option('od').value.set('test')")
    raises(APIError, "api.option('od').value.reset()")


def test_intoption():
    i1 = IntOption('test1', 'description', min_number=3)
    i2 = IntOption('test2', 'description', max_number=3)
    od = OptionDescription('od', '', [i1, i2])
    cfg = Config(od)
    raises(ValueError, "cfg.option('test1').value.set(2)")
    cfg.option('test1').value.set(3)
    cfg.option('test1').value.set(4)
    cfg.option('test2').value.set(2)
    cfg.option('test2').value.set(3)
    raises(ValueError, "cfg.option('test2').value.set(4)")


def test_get_display_type():
    i1 = IntOption('test1', 'description', min_number=3)
    assert i1.get_display_type() == 'integer'


def test_option_not_in_config():
    i1 = IntOption('test1', 'description', min_number=3)
    raises(AttributeError, "i1.impl_getpath()")
