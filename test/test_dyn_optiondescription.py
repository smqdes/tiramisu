# coding: utf-8
from .autopath import do_autopath
do_autopath()
from py.test import raises

from tiramisu.setting import groups, owners
from tiramisu import BoolOption, StrOption, ChoiceOption, IPOption, \
    NetworkOption, NetmaskOption, IntOption, FloatOption, \
    UnicodeOption, PortOption, BroadcastOption, DomainnameOption, \
    EmailOption, URLOption, UsernameOption, FilenameOption, SymLinkOption, \
    OptionDescription, DynOptionDescription, SynDynOption, submulti, Leadership, \
    Config, Params, ParamOption, ParamValue
from tiramisu.error import PropertiesOptionError, ConfigError, ConflictError
from tiramisu.storage import list_sessions


def teardown_function(function):
    assert list_sessions() == [], 'session list is not empty when leaving "{}"'.format(function.__name__)


def return_true(value, param=None, suffix=None):
    if value == 'val' and param in [None, 'yes']:
        return
    raise ValueError('no value')


def return_dynval(value='val', suffix=None):
    return value


def return_list2(suffix=None):
    return [str(suffix), 'val2']


def return_list(val=None, suffix=None):
    if val:
        return val
    else:
        return ['val1', 'val2']


def return_same_list(*args, **kwargs):
    return ['val1', 'val1']


def return_wrong_list(*args, **kwargs):
    return ['---', ' ']


def return_raise(suffix):
    raise Exception('error')


def return_str(*args, **kwargs):
    return 'str'


def test_build_dyndescription():
    st1 = StrOption('st', '')
    dod = DynOptionDescription('dod', '', [st1], callback=return_list)
    od1 = OptionDescription('od', '', [dod])
    cfg = Config(od1)
    assert cfg.value.dict() == {'dodval1.stval1': None, 'dodval2.stval2': None}


def test_build_dyndescription_raise():
    st1 = StrOption('st', '')
    dod = DynOptionDescription('dod', '', [st1], callback=return_raise)
    od1 = OptionDescription('od', '', [dod])
    cfg = Config(od1)
    raises(ConfigError, "cfg.value.dict()")


def test_build_dyndescription_not_list():
    st1 = StrOption('st', '')
    dod = DynOptionDescription('dod', '', [st1], callback=return_str)
    od1 = OptionDescription('od', '', [dod])
    cfg = Config(od1)
    raises(ValueError, "cfg.value.dict()")


def test_subpath_dyndescription():
    st1 = StrOption('st', '')
    dod = DynOptionDescription('dod', '', [st1], callback=return_list)
    od1 = OptionDescription('od', '', [dod])
    od2 = OptionDescription('od', '', [od1])
    api = Config(od2)
    assert api.value.dict() == {'od.dodval1.stval1': None, 'od.dodval2.stval2': None}


def test_list_dyndescription():
    st = StrOption('st', '')
    dod = DynOptionDescription('dod', '', [st], callback=return_list)
    od = OptionDescription('od', '', [dod])
    od2 = OptionDescription('od', '', [od])
    api = Config(od2)
    assert api.option('od.dodval1.stval1').value.get() is None
    assert api.option('od.dodval2.stval2').value.get() is None


def test_unknown_dyndescription():
    st = StrOption('st', '')
    dod = DynOptionDescription('dod', '', [st], callback=return_list)
    od = OptionDescription('od', '', [dod])
    od2 = OptionDescription('od', '', [od])
    api = Config(od2)
    raises(AttributeError, "api.option('od.dodval3').value.get()")
    raises(AttributeError, "api.option('od.dodval1.novalue').value.get()")
    raises(AttributeError, "api.option('od.dodval1.stnoval1').value.get()")


def test_getdoc_dyndescription():
    st1 = StrOption('st', 'doc1')
    dod = DynOptionDescription('dod', 'doc2', [st1], callback=return_list)
    od1 = OptionDescription('od', '', [dod])
    od2 = OptionDescription('od', '', [od1])
    api = Config(od2)
    assert api.option('od.dodval1.stval1').option.name() == 'stval1'
    assert api.option('od.dodval2.stval2').option.name() == 'stval2'
    assert api.option('od.dodval1').option.name() == 'dodval1'
    assert api.option('od.dodval2').option.name() == 'dodval2'
    assert api.option('od.dodval1.stval1').option.doc() == 'doc1'
    assert api.option('od.dodval2.stval2').option.doc() == 'doc1'
    assert api.option('od.dodval1').option.doc() == 'doc2val1'
    assert api.option('od.dodval2').option.doc() == 'doc2val2'


def test_mod_dyndescription():
    st = StrOption('st', '')
    dod = DynOptionDescription('dod', '', [st], callback=return_list)
    od = OptionDescription('od', '', [dod])
    od2 = OptionDescription('od', '', [od])
    api = Config(od2)
    owner = api.owner.get()
    #
    assert api.option('od.dodval1.stval1').value.get() is None
    assert api.option('od.dodval2.stval2').value.get() is None
    assert api.option('od.dodval1.stval1').owner.isdefault()
    assert api.option('od.dodval2.stval2').owner.isdefault()
    #
    api.option('od.dodval1.stval1').value.set('yes')
    assert api.option('od.dodval1.stval1').value.get() == 'yes'
    assert api.option('od.dodval2.stval2').value.get() is None
    assert api.option('od.dodval1.stval1').owner.get() == owner
    assert api.option('od.dodval2.stval2').owner.isdefault()
    #
    api.option('od.dodval2.stval2').value.set('no')
    assert api.option('od.dodval1.stval1').value.get() == 'yes'
    assert api.option('od.dodval2.stval2').value.get() == 'no'
    assert api.option('od.dodval1.stval1').owner.get() == owner
    assert api.option('od.dodval2.stval2').owner.get() == owner


def test_del_dyndescription():
    st = StrOption('st', '')
    dod = DynOptionDescription('dod', '', [st], callback=return_list)
    od = OptionDescription('od', '', [dod])
    od2 = OptionDescription('od', '', [od])
    api = Config(od2)
    owner = api.owner.get()
    assert api.option('od.dodval1.stval1').value.get() is None
    assert api.option('od.dodval2.stval2').value.get() is None
    api.option('od.dodval1.stval1').value.set('yes')
    assert api.option('od.dodval1.stval1').owner.get() == owner
    api.option('od.dodval1.stval1').value.reset()
    assert api.option('od.dodval1.stval1').owner.isdefault()


def test_multi_dyndescription():
    st = StrOption('st', '', multi=True)
    dod = DynOptionDescription('dod', '', [st], callback=return_list)
    od = OptionDescription('od', '', [dod])
    od2 = OptionDescription('od', '', [od])
    api = Config(od2)
    owner = api.owner.get()
    assert api.option('od.dodval1.stval1').value.get() == []
    assert api.option('od.dodval2.stval2').value.get() == []
    assert api.option('od.dodval1.stval1').owner.isdefault()
    assert api.option('od.dodval2.stval2').owner.isdefault()
    api.option('od.dodval1.stval1').value.set(['yes'])
    assert api.option('od.dodval1.stval1').value.get() == ['yes']
    assert api.option('od.dodval2.stval2').value.get() == []
    assert api.option('od.dodval1.stval1').owner.get() == owner
    assert api.option('od.dodval2.stval2').owner.isdefault()
    api.option('od.dodval2.stval2').value.set(['no'])
    assert api.option('od.dodval1.stval1').value.get() == ['yes']
    assert api.option('od.dodval2.stval2').value.get() == ['no']
    assert api.option('od.dodval1.stval1').owner.get() == owner
    assert api.option('od.dodval2.stval2').owner.get() == owner
    api.option('od.dodval1.stval1').value.set(['yes', 'yes'])
    assert api.option('od.dodval1.stval1').value.get() == ['yes', 'yes']
    api.option('od.dodval1.stval1').value.set(['yes'])
    assert api.option('od.dodval1.stval1').value.get() == ['yes']


def test_prop_dyndescription():
    st = StrOption('st', '', properties=('test',))
    dod = DynOptionDescription('dod', '', [st], callback=return_list)
    od = OptionDescription('od', '', [dod])
    od2 = OptionDescription('od', '', [od])
    api = Config(od2)
    assert set(api.option('od.dodval1.stval1').property.get()) == set(['test'])
    assert set(api.option('od.dodval2.stval2').property.get()) == set(['test'])
    api.option('od.dodval2.stval2').property.add('test2')
    assert set(api.option('od.dodval1.stval1').property.get()) == set(['test'])
    assert set(api.option('od.dodval2.stval2').property.get()) == set(['test', 'test2'])
    api.option('od.dodval1.stval1').property.pop('test')
    assert set(api.option('od.dodval1.stval1').property.get()) == set([])
    #
    assert set(api.option('od.dodval1').property.get()) == set([])
    assert set(api.option('od.dodval2').property.get()) == set([])
    api.option('od.dodval1').property.add('test1')
    assert set(api.option('od.dodval1').property.get()) == set(['test1'])
    assert set(api.option('od.dodval2').property.get()) == set([])
    api.option('od.dodval1').property.pop('test1')
    assert set(api.option('od.dodval1').property.get()) == set([])
    assert set(api.option('od.dodval2').property.get()) == set([])


def test_prop_dyndescription_force_store_value():
    st = StrOption('st', '', properties=('force_store_value',))
    dod = DynOptionDescription('dod', '', [st], callback=return_list)
    od = OptionDescription('od', '', [dod])
    od2 = OptionDescription('od', '', [od])
    raises(ConfigError, "Config(od2)")


def test_callback_dyndescription():
    st = StrOption('st', '', callback=return_dynval)
    dod = DynOptionDescription('dod', '', [st], callback=return_list)
    od = OptionDescription('od', '', [dod])
    od2 = OptionDescription('od', '', [od])
    api = Config(od2)
    owner = api.owner.get()
    assert api.option('od.dodval1.stval1').value.get() == 'val'
    assert api.option('od.dodval2.stval2').value.get() == 'val'
    assert api.option('od.dodval1.stval1').owner.isdefault()
    assert api.option('od.dodval2.stval2').owner.isdefault()
    api.option('od.dodval1.stval1').value.set('val2')
    assert api.option('od.dodval1.stval1').value.get() == 'val2'
    assert api.option('od.dodval2.stval2').value.get() == 'val'
    assert api.option('od.dodval1.stval1').owner.get() == owner
    assert api.option('od.dodval2.stval2').owner.isdefault()
    api.option('od.dodval1.stval1').value.reset()
    assert api.option('od.dodval1.stval1').value.get() == 'val'
    assert api.option('od.dodval2.stval2').value.get() == 'val'
    assert api.option('od.dodval1.stval1').owner.isdefault()
    assert api.option('od.dodval2.stval2').owner.isdefault()


def test_callback_list_dyndescription():
    st = StrOption('st', '', callback=return_list2, multi=True)
    dod = DynOptionDescription('dod', '', [st], callback=return_list)
    od = OptionDescription('od', '', [dod])
    od2 = OptionDescription('od', '', [od])
    api = Config(od2)
    owner = api.owner.get()
    assert api.option('od.dodval1.stval1').value.get() == ['val1', 'val2']
    assert api.option('od.dodval2.stval2').value.get() == ['val2', 'val2']
    assert api.option('od.dodval1.stval1').owner.isdefault()
    assert api.option('od.dodval2.stval2').owner.isdefault()
    api.option('od.dodval1.stval1').value.set(['val3', 'val2'])
    assert api.option('od.dodval1.stval1').value.get() == ['val3', 'val2']
    assert api.option('od.dodval2.stval2').value.get() == ['val2', 'val2']
    assert api.option('od.dodval1.stval1').owner.get() == owner
    assert api.option('od.dodval2.stval2').owner.isdefault()


def test_mandatory_dyndescription():
    st = StrOption('st', '', properties=('mandatory',))
    dod = DynOptionDescription('dod', '', [st], callback=return_list)
    od = OptionDescription('od', '', [dod])
    od2 = OptionDescription('od', '', [od])
    api = Config(od2)
    api.property.read_only()
    raises(PropertiesOptionError, "api.option('od.dodval1.stval1').value.get()")
    raises(PropertiesOptionError, "api.option('od.dodval2.stval2').value.get()")
    api.property.read_write()
    api.option('od.dodval1.stval1').value.set('val')
    api.property.read_only()
    assert api.option('od.dodval1.stval1').value.get() == 'val'
    raises(PropertiesOptionError, "api.option('od.dodval2.stval2').value.get()")
    api.property.read_write()
    api.option('od.dodval1.stval1').value.reset()
    api.property.read_only()
    raises(PropertiesOptionError, "api.option('od.dodval1.stval1').value.get()")
    assert list(api.value.mandatory()) == ['od.dodval1.stval1', 'od.dodval2.stval2']


def test_build_dyndescription_context():
    val1 = StrOption('val1', '', ['val1', 'val2'], multi=True)
    st1 = StrOption('st', '')
    dod = DynOptionDescription('dod', '', [st1], callback=return_list, callback_params=Params(ParamOption(val1)))
    od1 = OptionDescription('od', '', [dod, val1])
    cfg = Config(od1)
    assert cfg.value.dict() == {'dodval1.stval1': None, 'dodval2.stval2': None, 'val1': ['val1', 'val2']}


def test_subpath_dyndescription_context():
    val1 = StrOption('val1', '', ['val1', 'val2'], multi=True)
    st1 = StrOption('st', '')
    dod = DynOptionDescription('dod', '', [st1], callback=return_list, callback_params=Params(ParamOption(val1)))
    od1 = OptionDescription('od', '', [dod, val1])
    od2 = OptionDescription('od', '', [od1])
    api = Config(od2)
    assert api.value.dict() == {'od.dodval1.stval1': None, 'od.dodval2.stval2': None, 'od.val1': ['val1', 'val2']}


def test_list_dyndescription_context():
    val1 = StrOption('val1', '', ['val1', 'val2'], multi=True)
    st = StrOption('st', '')
    dod = DynOptionDescription('dod', '', [st], callback=return_list, callback_params=Params(ParamOption(val1)))
    od = OptionDescription('od', '', [dod, val1])
    od2 = OptionDescription('od', '', [od])
    api = Config(od2)
    assert api.option('od.dodval1.stval1').value.get() is None
    assert api.option('od.dodval2.stval2').value.get() is None
    raises(AttributeError, "api.option('od.dodval3').value.get()")


def test_mod_dyndescription_context():
    val1 = StrOption('val1', '', ['val1', 'val2'], multi=True)
    st = StrOption('st', '')
    dod = DynOptionDescription('dod', '', [st], callback=return_list, callback_params=Params(ParamOption(val1)))
    od = OptionDescription('od', '', [dod, val1])
    od2 = OptionDescription('od', '', [od])
    api = Config(od2)
    owner = api.owner.get()
    assert api.option('od.dodval1.stval1').value.get() is None
    assert api.option('od.dodval2.stval2').value.get() is None
    assert api.option('od.dodval1.stval1').owner.isdefault()
    assert api.option('od.dodval2.stval2').owner.isdefault()
    api.option('od.dodval1.stval1').value.set('yes')
    assert api.option('od.dodval1.stval1').value.get() == 'yes'
    assert api.option('od.dodval2.stval2').value.get() is None
    assert api.option('od.dodval1.stval1').owner.get() == owner
    assert api.option('od.dodval2.stval2').owner.isdefault()
    api.option('od.dodval2.stval2').value.set('no')
    assert api.option('od.dodval1.stval1').value.get() == 'yes'
    assert api.option('od.dodval2.stval2').value.get() == 'no'
    assert api.option('od.dodval1.stval1').owner.get() == owner
    assert api.option('od.dodval2.stval2').owner.get() == owner


def test_del_dyndescription_context():
    val1 = StrOption('val1', '', ['val1', 'val2'], multi=True)
    st = StrOption('st', '')
    dod = DynOptionDescription('dod', '', [st], callback=return_list, callback_params=Params(ParamOption(val1)))
    od = OptionDescription('od', '', [dod, val1])
    od2 = OptionDescription('od', '', [od])
    api = Config(od2)
    owner = api.owner.get()
    assert api.option('od.dodval1.stval1').value.get() is None
    assert api.option('od.dodval2.stval2').value.get() is None
    api.option('od.dodval1.stval1').value.set('yes')
    assert api.option('od.dodval1.stval1').owner.get() == owner
    api.option('od.dodval1.stval1').value.reset()
    assert api.option('od.dodval1.stval1').owner.isdefault()


def test_multi_dyndescription_context():
    val1 = StrOption('val1', '', ['val1', 'val2'], multi=True)
    st = StrOption('st', '', multi=True)
    dod = DynOptionDescription('dod', '', [st], callback=return_list, callback_params=Params(ParamOption(val1)))
    od = OptionDescription('od', '', [dod, val1])
    od2 = OptionDescription('od', '', [od])
    api = Config(od2)
    owner = api.owner.get()
    assert api.option('od.dodval1.stval1').value.get() == []
    assert api.option('od.dodval2.stval2').value.get() == []
    assert api.option('od.dodval1.stval1').owner.isdefault()
    assert api.option('od.dodval2.stval2').owner.isdefault()
    api.option('od.dodval1.stval1').value.set(['yes'])
    assert api.option('od.dodval1.stval1').value.get() == ['yes']
    assert api.option('od.dodval2.stval2').value.get() == []
    assert api.option('od.dodval1.stval1').owner.get() == owner
    assert api.option('od.dodval2.stval2').owner.isdefault()
    api.option('od.dodval2.stval2').value.set(['no'])
    assert api.option('od.dodval1.stval1').value.get() == ['yes']
    assert api.option('od.dodval2.stval2').value.get() == ['no']
    assert api.option('od.dodval1.stval1').owner.get() == owner
    assert api.option('od.dodval2.stval2').owner.get() == owner
    api.option('od.dodval1.stval1').value.set(['yes', 'yes'])
    assert api.option('od.dodval1.stval1').value.get() == ['yes', 'yes']
    api.option('od.dodval1.stval1').value.set(['yes'])
    assert api.option('od.dodval1.stval1').value.get() == ['yes']


def test_prop_dyndescription_context():
    val1 = StrOption('val1', '', ['val1', 'val2'], multi=True)
    st = StrOption('st', '', properties=('test',))
    dod = DynOptionDescription('dod', '', [st], callback=return_list, callback_params=Params(ParamOption(val1)))
    od = OptionDescription('od', '', [dod, val1])
    od2 = OptionDescription('od', '', [od])
    api = Config(od2)
    assert set(api.option('od.dodval1.stval1').property.get()) == set(['test'])
    assert set(api.option('od.dodval2.stval2').property.get()) == set(['test'])
    api.option('od.dodval2.stval2').property.add('test2')
    assert set(api.option('od.dodval1.stval1').property.get()) == set(['test'])
    assert set(api.option('od.dodval2.stval2').property.get()) == set(['test', 'test2'])
    api.option('od.dodval1.stval1').property.pop('test')
    assert set(api.option('od.dodval1.stval1').property.get()) == set([])
    assert set(api.option('od.dodval2.stval2').property.get()) == set(['test', 'test2'])


def test_callback_dyndescription_context():
    val1 = StrOption('val1', '', ['val1', 'val2'], multi=True)
    st = StrOption('st', '', callback=return_dynval)
    dod = DynOptionDescription('dod', '', [st], callback=return_list)
    od = OptionDescription('od', '', [dod, val1])
    od2 = OptionDescription('od', '', [od])
    api = Config(od2)
    owner = api.owner.get()
    assert api.option('od.dodval1.stval1').value.get() == 'val'
    assert api.option('od.dodval2.stval2').value.get() == 'val'
    assert api.option('od.dodval1.stval1').owner.isdefault()
    assert api.option('od.dodval2.stval2').owner.isdefault()
    api.option('od.dodval1.stval1').value.set('val2')
    assert api.option('od.dodval1.stval1').value.get() == 'val2'
    assert api.option('od.dodval2.stval2').value.get() == 'val'
    assert api.option('od.dodval1.stval1').owner.get() == owner
    assert api.option('od.dodval2.stval2').owner.isdefault()
    api.option('od.dodval1.stval1').value.reset()
    assert api.option('od.dodval1.stval1').value.get() == 'val'
    assert api.option('od.dodval2.stval2').value.get() == 'val'
    assert api.option('od.dodval1.stval1').owner.isdefault()
    assert api.option('od.dodval2.stval2').owner.isdefault()


def test_mandatory_dyndescription_context():
    val1 = StrOption('val1', '', ['val1', 'val2'], multi=True)
    st = StrOption('st', '', properties=('mandatory',))
    dod = DynOptionDescription('dod', '', [st], callback=return_list, callback_params=Params(ParamOption(val1)))
    od = OptionDescription('od', '', [dod, val1])
    od2 = OptionDescription('od', '', [od])
    api = Config(od2)
    api.property.read_only()
    raises(PropertiesOptionError, "api.option('od.dodval1.stval1').value.get()")
    raises(PropertiesOptionError, "api.option('od.dodval2.stval2').value.get()")
    api.property.read_write()
    api.option('od.dodval1.stval1').value.set('val')
    api.property.read_only()
    assert api.option('od.dodval1.stval1').value.get() == 'val'
    raises(PropertiesOptionError, "api.option('od.dodval2.stval2').value.get()")
    api.property.read_write()
    api.option('od.dodval1.stval1').value.reset()
    api.property.read_only()
    raises(PropertiesOptionError, "api.option('od.dodval1.stval1').value.get()")
    assert list(api.value.mandatory()) == ['od.dodval1.stval1', 'od.dodval2.stval2']


def test_increase_dyndescription_context():
    val1 = StrOption('val1', '', ['val1', 'val2'], multi=True)
    st = StrOption('st', '', properties=('mandatory',))
    dod = DynOptionDescription('dod', '', [st], callback=return_list, callback_params=Params(ParamOption(val1)))
    od = OptionDescription('od', '', [dod, val1])
    od2 = OptionDescription('od', '', [od])
    api = Config(od2)
    api.property.read_write()
    assert api.option('od.dodval1.stval1').value.get() is None
    assert api.option('od.dodval2.stval2').value.get() is None
    raises(AttributeError, "api.option('od.dodval3.stval3').value.get()")
    api.option('od.val1').value.set(['val1', 'val2', 'val3'])
    assert api.option('od.dodval1.stval1').value.get() is None
    assert api.option('od.dodval2.stval2').value.get() is None
    assert api.option('od.dodval3.stval3').value.get() is None


def test_decrease_dyndescription_context():
    val1 = StrOption('val1', '', ['val1', 'val2'], multi=True)
    st = StrOption('st', '', properties=('mandatory',))
    dod = DynOptionDescription('dod', '', [st], callback=return_list, callback_params=Params(ParamOption(val1)))
    od = OptionDescription('od', '', [dod, val1])
    od2 = OptionDescription('od', '', [od])
    api = Config(od2)
    owner = api.owner.get()
    api.property.read_write()
    assert api.option('od.dodval1.stval1').value.get() is None
    assert api.option('od.dodval2.stval2').value.get() is None
    api.option('od.dodval2.stval2').value.set('yes')
    assert api.option('od.dodval1.stval1').value.get() is None
    assert api.option('od.dodval2.stval2').value.get() == 'yes'
    assert api.option('od.dodval1.stval1').owner.isdefault()
    assert api.option('od.dodval2.stval2').owner.get() == owner
    raises(AttributeError, "api.option('od.dodval3').value.get()")
    api.option('od.val1').value.set(['val1'])
    assert api.option('od.dodval1.stval1').value.get() is None
    raises(AttributeError, "api.option('od.dodval2').value.get()")
    raises(AttributeError, "api.option('od.dodval3').value.get()")
    assert api.option('od.dodval1.stval1').owner.isdefault()
    raises(AttributeError, "api.option('od.dodval2.stval2').owner.get()")
    raises(AttributeError, "api.option('od.dodval2.stval2').value.get()")


def test_dyndescription_root():
    boolean = BoolOption('boolean', '', True)
    st1 = StrOption('st', '', requires=[{'option': boolean, 'expected': False,
                                        'action': 'disabled'}])
    dod = DynOptionDescription('dod', '', [boolean, st1], callback=return_list)
    raises(ConfigError, "Config(dod)")


def test_requires_dyndescription():
    boolean = BoolOption('boolean', '', True)
    st1 = StrOption('st', '', requires=[{'option': boolean, 'expected': False,
                                        'action': 'disabled'}])
    dod = DynOptionDescription('dod', '', [st1], callback=return_list)
    od1 = OptionDescription('od', '', [dod])
    od2 = OptionDescription('od', '', [od1, boolean])
    api = Config(od2)
    api.property.read_write()
    assert api.option('od.dodval1.stval1').value.get() is None
    assert api.option('od.dodval2.stval2').value.get() is None
    #
    api.option('boolean').value.set(False)
    props = []
    try:
        api.option('od.dodval1.stval1').value.get()
    except PropertiesOptionError as err:
        props = err.proptype
    assert frozenset(props) == frozenset(['disabled'])
    props = []
    try:
        api.option('od.dodval2.stval2').value.get()
    except PropertiesOptionError as err:
        props = err.proptype
    assert frozenset(props) == frozenset(['disabled'])
    #
    api.option('boolean').value.set(True)
    assert api.option('od.dodval1.stval1').value.get() is None
    assert api.option('od.dodval2.stval2').value.get() is None
    #transitive
    api.option('boolean').property.add('disabled')
    props = []
    try:
        api.option('od.dodval1.stval1').value.get()
    except PropertiesOptionError as err:
        props = err.proptype
    assert frozenset(props) == frozenset(['disabled'])
    props = []
    try:
        api.option('od.dodval2.stval2').value.get()
    except PropertiesOptionError as err:
        props = err.proptype
    assert frozenset(props) == frozenset(['disabled'])


def test_requires_dyndescription_boolean():
    boolean1 = BoolOption('boolean1', '', True)
    boolean = BoolOption('boolean', '', True, requires=[{'option': boolean1,
                                                         'expected': False,
                                                         'action': 'disabled'}])
    st = StrOption('st', '', requires=[{'option': boolean, 'expected': False,
                                        'action': 'disabled'}])
    dod = DynOptionDescription('dod', '', [st], callback=return_list)
    od = OptionDescription('od', '', [dod])
    od2 = OptionDescription('od', '', [od, boolean1, boolean])
    cfg = Config(od2)
    cfg.property.read_write()
    assert cfg.value.dict() == {'boolean1': True,
                                'boolean': True,
                                'od.dodval1.stval1': None,
                                'od.dodval2.stval2': None}
    #
    cfg.option('boolean').value.set(False)
    assert cfg.value.dict() == {'boolean1': True,
                                'boolean': False}
    #
    cfg.option('boolean').value.set(True)
    assert cfg.value.dict() == {'boolean1': True,
                                'boolean': True,
                                'od.dodval1.stval1': None,
                                'od.dodval2.stval2': None}
    #
    cfg.option('boolean1').value.set(False)
    assert cfg.value.dict() == {'boolean1': False}


def test_requires_dyndescription_in_dyn():
    boolean = BoolOption('boolean', '', True)
    st = StrOption('st', '', requires=[{'option': boolean, 'expected': False,
                                        'action': 'disabled'}])
    dod = DynOptionDescription('dod', '', [boolean, st], callback=return_list)
    od = OptionDescription('od', '', [dod])
    od2 = OptionDescription('od', '', [od])
    cfg = Config(od2)
    cfg.property.read_write()

    assert cfg.option('od.dodval1.stval1').value.get() is None
    assert cfg.option('od.dodval2.stval2').value.get() is None
    #
    cfg.option('od.dodval1.booleanval1').value.set(False)

    props = []
    try:
        cfg.option('od.dodval1.stval1').value.get()
    except PropertiesOptionError as err:
        props = err.proptype
    assert props == frozenset(['disabled'])
    props = []
    cfg.option('od.dodval2.stval2').value.get()
    #
    cfg.option('od.dodval1.booleanval1').value.set(True)
    assert cfg.option('od.dodval1.stval1').value.get() is None
    assert cfg.option('od.dodval2.stval2').value.get() is None


def test_requires_dyndescription2():
    boolean = BoolOption('boolean', '', True)
    st1 = StrOption('st', '')
    dod = DynOptionDescription('dod', '', [st1], callback=return_list,
                               requires=[{'option': boolean, 'expected': False,
                                          'action': 'disabled'}])
    od1 = OptionDescription('od', '', [dod])
    od2 = OptionDescription('od', '', [od1, boolean])
    api = Config(od2)
    api.property.read_write()
    assert api.option('od.dodval1.stval1').value.get() is None
    assert api.option('od.dodval2.stval2').value.get() is None
    #
    api.option('boolean').value.set(False)
    props = []
    try:
        api.option('od.dodval1.stval1').value.get()
    except PropertiesOptionError as err:
        props = err.proptype
    assert frozenset(props) == frozenset(['disabled'])
    props = []
    try:
        api.option('od.dodval2.stval2').value.get()
    except PropertiesOptionError as err:
        props = err.proptype
    assert frozenset(props) == frozenset(['disabled'])
    #
    api.option('boolean').value.set(True)
    assert api.option('od.dodval1.stval1').value.get() is None
    assert api.option('od.dodval2.stval2').value.get() is None
    #transitive
    api.option('boolean').property.add('disabled')
    props = []
    try:
        api.option('od.dodval1.stval1').value.get()
    except PropertiesOptionError as err:
        props = err.proptype
    assert frozenset(props) == frozenset(['disabled'])
    props = []
    try:
        api.option('od.dodval2.stval2').value.get()
    except PropertiesOptionError as err:
        props = err.proptype
    assert frozenset(props) == frozenset(['disabled'])


def test_validator_dyndescription():
    val1 = StrOption('val1', '', ['val1', 'val2'], multi=True)
    st = StrOption('st', '', validator=return_true, validator_params=Params((ParamValue('yes'),)), default='val')
    dod = DynOptionDescription('dod', '', [st], callback=return_list)
    od = OptionDescription('od', '', [dod, val1])
    od2 = OptionDescription('od', '', [od])
    api = Config(od2)
    assert api.option('od.dodval1.stval1').value.get() == 'val'
    raises(ValueError, "api.option('od.dodval1.stval1').value.set('no')")
    api.option('od.dodval1.stval1').value.set('val')


def test_makedict_dyndescription_context():
    val1 = StrOption('val1', '', ['val1', 'val2'], multi=True)
    st = StrOption('st', '')
    dod = DynOptionDescription('dod', '', [st], callback=return_list)
    od = OptionDescription('od', '', [dod, val1])
    od2 = OptionDescription('od', '', [od])
    api = Config(od2)
    api.option('od.dodval1.stval1').value.set('yes')
    assert api.value.dict() == {'od.val1': ['val1', 'val2'], 'od.dodval1.stval1': 'yes', 'od.dodval2.stval2': None}
    assert api.value.dict(flatten=True) == {'val1': ['val1', 'val2'], 'stval1': 'yes', 'stval2': None}
    assert api.value.dict(withoption='stval1') == {'od.dodval1.stval1': 'yes'}
    assert api.option('od').value.dict(withoption='stval1') == {'dodval1.stval1': 'yes'}
    assert api.option('od.dodval1').value.dict(withoption='stval1') == {'stval1': 'yes'}


def test_find_dyndescription_context():
    val1 = StrOption('val1', '', ['val1', 'val2'], multi=True)
    st = StrOption('st', '')
    dod = DynOptionDescription('dod', '', [st], callback=return_list)
    od = OptionDescription('od', '', [dod, val1])
    od2 = OptionDescription('od', '', [od])
    api = Config(od2)
    api.option('od.dodval1.stval1').value.set('yes')
    assert api.option.find('stval1', first=True).value.get() == "yes"
    assert isinstance(api.option.find('stval1', first=True).option.get(), SynDynOption)
    #assert api.option.find(bytype=StrOption, type='path') == ['od.dodval1.stval1', 'od.dodval2.stval2', 'od.val1']
    #opts = api.option.find(byvalue='yes')
    #assert len(opts) == 1
    #assert isinstance(opts[0], SynDynOption)
    #assert opts[0].impl_getname() == 'stval1'
    raises(AttributeError, "list(api.option.find('strnotexists'))")


def test_information_dyndescription_context():
    val1 = StrOption('val1', '', ['val1', 'val2'], multi=True)
    st = StrOption('st', '')
    dod = DynOptionDescription('dod', '', [st], callback=return_list)
    od = OptionDescription('od', '', [dod, val1])
    od2 = OptionDescription('od', '', [od])
    dod.impl_set_information('testod', 'val1')
    st.impl_set_information('testst', 'val2')
    api = Config(od2)
    api.information.set('testcfgod', 'val3')
    assert api.option('od.dodval1').information.get('testod') == 'val1'
    assert api.option('od.dodval2').information.get('testod') == 'val1'
    assert api.option('od.dodval1.stval1').information.get('testst') == 'val2'
    assert api.option('od.dodval2.stval2').information.get('testst') == 'val2'
    assert api.information.get('testcfgod') == 'val3'


def test_consistency_dyndescription():
    st1 = StrOption('st', '')
    st2 = StrOption('st2', '')
    dod = DynOptionDescription('dod', '', [st1, st2], callback=return_list)
    od1 = OptionDescription('od', '', [dod])
    st1.impl_add_consistency('not_equal', st2)
    od2 = OptionDescription('od', '', [od1])
    api = Config(od2)
    api.option('od.dodval1.stval1').value.set('yes')
    raises(ValueError, "api.option('od.dodval1.st2val1').value.set('yes')")
    api.option('od.dodval2.stval2').value.set('yes')
    raises(ValueError, "api.option('od.dodval2.st2val2').value.set('yes')")
    raises(ValueError, "api.option('od.dodval1.st2val1').value.set('yes')")
    api.option('od.dodval2.stval2').value.reset()
    raises(ValueError, "api.option('od.dodval1.st2val1').value.set('yes')")
    api.option('od.dodval2.st2val2').value.set('yes')
    raises(ValueError, "api.option('od.dodval2.stval2').value.set('yes')")
    #
    api.option('od.dodval1.stval1').value.reset()
    api.option('od.dodval2.st2val2').value.reset()
    api.option('od.dodval1.st2val1').value.set('yes')
    raises(ValueError, "api.option('od.dodval1.stval1').value.set('yes')")


def test_consistency_dyndescription_default():
    st = StrOption('st', '', 'yes')
    st2 = StrOption('st2', '')
    dod = DynOptionDescription('dod', '', [st, st2], callback=return_list)
    od = OptionDescription('od', '', [dod])
    st.impl_add_consistency('not_equal', st2)
    od2 = OptionDescription('od', '', [od])
    api = Config(od2)
    raises(ValueError, "api.option('od.dodval1.st2val1').value.set('yes')")
    raises(ValueError, "api.option('od.dodval2.st2val2').value.set('yes')")


def test_consistency_dyndescription_default_multi2():
    st = StrOption('st', '', ['yes'], multi=True)
    st2 = StrOption('st2', '', ['yes'], multi=True)
    dod = DynOptionDescription('dod', '', [st, st2], callback=return_list)
    dod
    raises(ValueError, "st.impl_add_consistency('not_equal', st2)")


def test_consistency_only_one_dyndescription():
    st = StrOption('st', '')
    st
    st2 = StrOption('st2', '')
    dod = DynOptionDescription('dod', '', [st2], callback=return_list)
    raises(ConfigError, "st.impl_add_consistency('not_equal', st2)")
    raises(ConfigError, "st2.impl_add_consistency('not_equal', st)")


def test_consistency_became_dyndescription():
    st = StrOption('st', '')
    st2 = StrOption('st2', '')
    st2.impl_add_consistency('not_equal', st)
    od = DynOptionDescription('dod', '', [st2], callback=return_list)
    od2 = OptionDescription('od', '', [od, st])
    od2
    raises(ConfigError, "c = Config(od2)")


def test_consistency_became_dyndescription2():
    st = StrOption('st', '')
    st2 = StrOption('st2', '')
    st.impl_add_consistency('not_equal', st2)
    od = DynOptionDescription('dod', '', [st2], callback=return_list)
    od2 = OptionDescription('od', '', [od, st])
    od2
    raises(ConfigError, "c = Config(od2)")


def test_consistency_external_dyndescription():
    st = StrOption('st', '')
    st1 = StrOption('st1', '')
    st2 = StrOption('st2', '')
    dod = DynOptionDescription('dod', '', [st1, st2], callback=return_list)
    od = OptionDescription('od', '', [dod, st])
    od
    raises(ConfigError, "st.impl_add_consistency('not_equal', st2)")


def test_consistency_notsame_dyndescription():
    st1 = StrOption('st1', '')
    st2 = StrOption('st2', '')
    dod = DynOptionDescription('dod', '', [st1, st2], callback=return_list)
    tst1 = StrOption('tst1', '')
    tst2 = StrOption('tst2', '')
    tdod = DynOptionDescription('tdod', '', [tst1, tst2], callback=return_list)
    od = OptionDescription('od', '', [dod, tdod])
    od
    raises(ConfigError, "st1.impl_add_consistency('not_equal', tst1)")


def test_all_dyndescription():
    st = StrOption('st', '')
    ip = IPOption('ip', '')
    network = NetworkOption('network', '')
    netmask = NetmaskOption('netmask', '')
    ch = ChoiceOption('ch', '', ('val1', 'val2', 'val3'))
    ch1 = ChoiceOption('ch1', '', return_list)
    boo = BoolOption('boo', '')
    intr = IntOption('intr', '')
    floa = FloatOption('floa', '')
    uni = UnicodeOption('uni', '')
    port = PortOption('port', '')
    broad = BroadcastOption('broad', '')
    domain = DomainnameOption('domain', '')
    email = EmailOption('email', '')
    url = URLOption('url', '')
    username = UsernameOption('username', '')
    filename = FilenameOption('filename', '')
    dod = DynOptionDescription('dod', '', [st, ip, network, netmask, ch, ch1,
                                           boo, intr, floa, uni, port, broad,
                                           domain, email, url, username,
                                           filename], callback=return_list)
    od = OptionDescription('od', '', [dod])
    api = Config(od)
    assert api.option('dodval1.stval1').value.get() is None
    assert api.option('dodval1.ipval1').value.get() is None
    assert api.option('dodval1.networkval1').value.get() is None
    assert api.option('dodval1.netmaskval1').value.get() is None
    assert api.option('dodval1.chval1').value.get() is None
    assert api.option('dodval1.ch1val1').value.get() is None
    assert api.option('dodval1.booval1').value.get() is None
    assert api.option('dodval1.intrval1').value.get() is None
    assert api.option('dodval1.floaval1').value.get() is None
    assert api.option('dodval1.unival1').value.get() is None
    assert api.option('dodval1.portval1').value.get() is None
    assert api.option('dodval1.broadval1').value.get() is None
    assert api.option('dodval1.domainval1').value.get() is None
    assert api.option('dodval1.emailval1').value.get() is None
    assert api.option('dodval1.urlval1').value.get() is None
    assert api.option('dodval1.usernameval1').value.get() is None
    assert api.option('dodval1.filenameval1').value.get() is None
    #
    api.option('dodval1.stval1').value.set("no")
    api.option('dodval1.ipval1').value.set("1.1.1.1")
    api.option('dodval1.networkval1').value.set("1.1.1.0")
    api.option('dodval1.netmaskval1').value.set("255.255.255.0")
    api.option('dodval1.chval1').value.set("val1")
    api.option('dodval1.ch1val1').value.set("val2")
    api.option('dodval1.booval1').value.set(True)
    api.option('dodval1.intrval1').value.set(1)
    api.option('dodval1.floaval1').value.set(0.1)
    api.option('dodval1.unival1').value.set(u"no")
    api.option('dodval1.portval1').value.set('80')
    api.option('dodval1.broadval1').value.set("1.1.1.255")
    api.option('dodval1.domainval1').value.set("test.com")
    api.option('dodval1.emailval1').value.set("test@test.com")
    api.option('dodval1.urlval1').value.set("http://test.com")
    api.option('dodval1.usernameval1').value.set("user1")
    api.option('dodval1.filenameval1').value.set("/tmp")
    assert api.option('dodval1.stval1').value.get() == "no"
    assert api.option('dodval1.ipval1').value.get() == "1.1.1.1"
    assert api.option('dodval1.networkval1').value.get() == "1.1.1.0"
    assert api.option('dodval1.netmaskval1').value.get() == "255.255.255.0"
    assert api.option('dodval1.chval1').value.get() == "val1"
    assert api.option('dodval1.ch1val1').value.get() == "val2"
    assert api.option('dodval1.booval1').value.get() is True
    assert api.option('dodval1.intrval1').value.get() == 1
    assert api.option('dodval1.floaval1').value.get() == 0.1
    assert api.option('dodval1.unival1').value.get() == u"no"
    assert api.option('dodval1.portval1').value.get() == '80'
    assert api.option('dodval1.broadval1').value.get() == "1.1.1.255"
    assert api.option('dodval1.domainval1').value.get() == "test.com"
    assert api.option('dodval1.emailval1').value.get() == "test@test.com"
    assert api.option('dodval1.urlval1').value.get() == "http://test.com"
    assert api.option('dodval1.usernameval1').value.get() == "user1"
    assert api.option('dodval1.filenameval1').value.get() == "/tmp"
    assert api.option('dodval2.stval2').value.get() is None
    assert api.option('dodval2.ipval2').value.get() is None
    assert api.option('dodval2.networkval2').value.get() is None
    assert api.option('dodval2.netmaskval2').value.get() is None
    assert api.option('dodval2.chval2').value.get() is None
    assert api.option('dodval2.ch1val2').value.get() is None
    assert api.option('dodval2.booval2').value.get() is None
    assert api.option('dodval2.intrval2').value.get() is None
    assert api.option('dodval2.floaval2').value.get() is None
    assert api.option('dodval2.unival2').value.get() is None
    assert api.option('dodval2.portval2').value.get() is None
    assert api.option('dodval2.broadval2').value.get() is None
    assert api.option('dodval2.domainval2').value.get() is None
    assert api.option('dodval2.emailval2').value.get() is None
    assert api.option('dodval2.urlval2').value.get() is None
    assert api.option('dodval2.usernameval2').value.get() is None
    assert api.option('dodval2.filenameval2').value.get() is None


def test_consistency_ip_netmask_dyndescription():
    ipa = IPOption('a', '')
    netb = NetmaskOption('b', '')
    dod = DynOptionDescription('dod', '', [ipa, netb], callback=return_list)
    netb.impl_add_consistency('ip_netmask', ipa)
    od1 = OptionDescription('od', '', [dod])
    cfg = Config(od1)
    cfg.option('dodval1.aval1').value.set('192.168.1.1')
    cfg.option('dodval1.bval1').value.set('255.255.255.0')
    cfg.option('dodval2.aval2').value.set('192.168.1.2')
    cfg.option('dodval2.bval2').value.set('255.255.255.128')
    cfg.option('dodval2.bval2').value.set('255.255.255.0')
    raises(ValueError, "cfg.option('dodval2.bval2').value.set('255.255.255.255')")


def test_consistency_ip_in_network_dyndescription():
    neta = NetworkOption('a', '')
    netb = NetmaskOption('b', '')
    ipc = IPOption('c', '')
    dod = DynOptionDescription('dod', '', [neta, netb, ipc], callback=return_list)
    ipc.impl_add_consistency('in_network', neta, netb)
    od1 = OptionDescription('od', '', [dod])
    cfg = Config(od1)
    cfg.option('dodval1.aval1').value.set('192.168.1.0')
    cfg.option('dodval1.bval1').value.set('255.255.255.0')
    cfg.option('dodval1.cval1').value.set('192.168.1.1')


def test_leadership_dyndescription():
    st1 = StrOption('st1', "", multi=True)
    st2 = StrOption('st2', "", multi=True)
    stm = Leadership('st1', '', [st1, st2])
    st = DynOptionDescription('st', '', [stm], callback=return_list)
    od = OptionDescription('od', '', [st])
    od2 = OptionDescription('od', '', [od])
    api = Config(od2)
    owner = api.owner.get()
    #
    assert api.value.dict() == {'od.stval1.st1val1.st2val1': [], 'od.stval2.st1val2.st2val2': [], 'od.stval2.st1val2.st1val2': [], 'od.stval1.st1val1.st1val1': []}
    assert api.option('od.stval1.st1val1.st1val1').value.get() == []
    assert api.option('od.stval2.st1val2.st1val2').value.get() == []
    assert api.option('od.stval1.st1val1.st1val1').owner.isdefault()
    assert api.option('od.stval2.st1val2.st1val2').owner.isdefault()
    #
    api.option('od.stval1.st1val1.st1val1').value.set(['yes'])
    assert api.value.dict() == {'od.stval1.st1val1.st2val1': [None], 'od.stval2.st1val2.st2val2': [], 'od.stval2.st1val2.st1val2': [], 'od.stval1.st1val1.st1val1': ['yes']}
    assert api.option('od.stval1.st1val1.st1val1').value.get() == ['yes']
    assert api.option('od.stval1.st1val1.st2val1', 0).value.get() == None
    assert api.option('od.stval2.st1val2.st1val2').value.get() == []
    assert api.option('od.stval1.st1val1.st1val1').owner.get() == owner
    assert api.option('od.stval1.st1val1.st2val1', 0).owner.isdefault()
    assert api.option('od.stval2.st1val2.st1val2').owner.isdefault()
    #
    api.option('od.stval1.st1val1.st2val1', 0).value.set('no')
    assert api.option('od.stval1.st1val1.st1val1').value.get() == ['yes']
    assert api.option('od.stval1.st1val1.st2val1', 0).value.get() == 'no'
    assert api.option('od.stval2.st1val2.st1val2').value.get() == []
    assert api.option('od.stval1.st1val1.st1val1').owner.get() == owner
    assert api.option('od.stval1.st1val1.st2val1', 0).owner.get() == owner
    assert api.option('od.stval2.st1val2.st1val2').owner.isdefault()
    #
    api.option('od.stval1.st1val1.st1val1').value.pop(0)
    assert api.option('od.stval1.st1val1.st1val1').value.get() == []
    assert api.option('od.stval2.st1val2.st1val2').value.get() == []
    assert api.option('od.stval1.st1val1.st1val1').owner.get() == owner
    assert api.option('od.stval2.st1val2.st1val2').owner.isdefault()
    #
    api.option('od.stval1.st1val1.st1val1').value.set(['yes'])
    api.option('od.stval1.st1val1.st2val1', 0).value.set('yes')
    assert api.option('od.stval1.st1val1.st1val1').owner.get() == owner
    assert api.option('od.stval1.st1val1.st2val1', 0).owner.get() == owner
    assert api.option('od.stval2.st1val2.st1val2').owner.isdefault()
    assert api.option('od.stval1.st1val1.st1val1').owner.get() == owner
    api.option('od.stval1.st1val1.st2val1', 0).value.reset()
    assert api.option('od.stval1.st1val1.st1val1').owner.get() == owner
    assert api.option('od.stval1.st1val1.st2val1', 0).owner.isdefault()
    #
    api.option('od.stval1.st1val1.st1val1').value.set(['yes'])
    api.option('od.stval1.st1val1.st2val1', 0).value.set('yes')
    api.option('od.stval1.st1val1.st1val1').value.reset()
    assert api.option('od.stval1.st1val1.st1val1').value.get() == []
    assert api.option('od.stval2.st1val2.st1val2').value.get() == []
    assert api.option('od.stval1.st1val1.st1val1').owner.isdefault()
    assert api.option('od.stval2.st1val2.st1val2').owner.isdefault()


def test_leadership_default_multi_dyndescription():
    st1 = StrOption('st1', "", multi=True)
    st2 = StrOption('st2', "", multi=True, default_multi='no')
    stm = Leadership('st1', '', [st1, st2])
    st = DynOptionDescription('st', '', [stm], callback=return_list)
    od = OptionDescription('od', '', [st])
    od2 = OptionDescription('od', '', [od])
    api = Config(od2)
    owner = api.owner.get()
    #
    assert api.option('od.stval1.st1val1.st1val1').value.get() == []
    assert api.option('od.stval2.st1val2.st1val2').value.get() == []
    assert api.option('od.stval1.st1val1.st1val1').owner.isdefault()
    assert api.option('od.stval2.st1val2.st1val2').owner.isdefault()
    #
    api.option('od.stval1.st1val1.st1val1').value.set(['yes'])
    assert api.option('od.stval1.st1val1.st1val1').value.get() == ['yes']
    assert api.option('od.stval1.st1val1.st2val1', 0).value.get() == 'no'
    assert api.option('od.stval2.st1val2.st1val2').value.get() == []
    assert api.option('od.stval1.st1val1.st1val1').owner.get() == owner
    assert api.option('od.stval1.st1val1.st2val1', 0).owner.isdefault()
    assert api.option('od.stval2.st1val2.st1val2').owner.isdefault()

def test_leadership_dyndescription_param():
    val1 = StrOption('val1', '', ['val1', 'val2'], multi=True)
    odval = OptionDescription('odval1', '', [val1])
    st1 = StrOption('st1', "", multi=True)
    st2 = StrOption('st2', "", multi=True)
    stm = Leadership('st1', '', [st1, st2])
    st = DynOptionDescription('st', '', [stm], callback=return_list, callback_params=Params(ParamOption(val1)))
    od = OptionDescription('od', '', [st, odval])
    od2 = OptionDescription('od', '', [od])
    cfg = Config(od2)
    owner = cfg.owner.get()
    assert cfg.value.dict() == {'od.stval1.st1val1.st2val1': [], 'od.stval2.st1val2.st2val2': [], 'od.stval2.st1val2.st1val2': [], 'od.stval1.st1val1.st1val1': [], 'od.odval1.val1': ['val1', 'val2']}
    assert cfg.option('od.stval1.st1val1.st1val1').value.get() == []
    assert cfg.option('od.stval2.st1val2.st1val2').value.get() == []
    assert cfg.option('od.stval1.st1val1.st1val1').owner.get() == owners.default
    assert cfg.option('od.stval2.st1val2.st1val2').owner.get() == owners.default
    #
    cfg.option('od.stval1.st1val1.st1val1').value.set(['yes'])
    assert cfg.value.dict() == {'od.stval1.st1val1.st2val1': [None], 'od.stval2.st1val2.st2val2': [], 'od.stval2.st1val2.st1val2': [], 'od.stval1.st1val1.st1val1': ['yes'], 'od.odval1.val1': ['val1', 'val2']}
    assert cfg.option('od.stval1.st1val1.st1val1').value.get() == ['yes']
    assert cfg.option('od.stval1.st1val1.st2val1', 0).value.get() == None
    assert cfg.option('od.stval2.st1val2.st1val2').value.get() == []
    assert cfg.option('od.stval1.st1val1.st1val1').owner.get() == owner
    assert cfg.option('od.stval1.st1val1.st2val1', 0).owner.get() == owners.default
    assert cfg.option('od.stval2.st1val2.st1val2').owner.get() == owners.default
    #
    cfg.option('od.stval1.st1val1.st2val1', 0).value.set('no')
    assert cfg.option('od.stval1.st1val1.st1val1').value.get() == ['yes']
    assert cfg.option('od.stval1.st1val1.st2val1', 0).value.get() == 'no'
    assert cfg.option('od.stval2.st1val2.st1val2').value.get() == []
    assert cfg.option('od.stval1.st1val1.st1val1').owner.get() == owner
    assert cfg.option('od.stval1.st1val1.st2val1', 0).owner.get() == owner
    assert cfg.option('od.stval2.st1val2.st1val2').owner.get() == owners.default
    #
    cfg.option('od.stval1.st1val1.st1val1').value.pop(0)
    assert cfg.option('od.stval1.st1val1.st1val1').value.get() == []
    assert cfg.option('od.stval2.st1val2.st1val2').value.get() == []
    assert cfg.option('od.stval1.st1val1.st1val1').owner.get() == owner
    assert cfg.option('od.stval2.st1val2.st1val2').owner.get() == owners.default
    #
    cfg.option('od.stval1.st1val1.st1val1').value.set(['yes'])
    cfg.option('od.stval1.st1val1.st2val1', 0).value.set('yes')
    assert cfg.option('od.stval1.st1val1.st1val1').owner.get() == owner
    assert cfg.option('od.stval1.st1val1.st2val1', 0).owner.get() == owner
    assert cfg.option('od.stval2.st1val2.st1val2').owner.get() == owners.default
    #
    cfg.option('od.stval1.st1val1.st2val1', 0).value.reset()
    assert cfg.option('od.stval1.st1val1.st1val1').owner.get() == owner
    assert cfg.option('od.stval1.st1val1.st2val1', 0).owner.get() == owners.default
    assert cfg.option('od.stval2.st1val2.st1val2').owner.get() == owners.default
    #
    cfg.option('od.stval1.st1val1.st1val1').value.set(['yes'])
    cfg.option('od.stval1.st1val1.st2val1', 0).value.set('yes')
    cfg.option('od.stval1.st1val1.st1val1').value.reset()
    assert cfg.option('od.stval1.st1val1.st1val1').value.get() == []
    assert cfg.option('od.stval2.st1val2.st1val2').value.get() == []
    assert cfg.option('od.stval1.st1val1.st1val1').owner.get() == owners.default
    assert cfg.option('od.stval2.st1val2.st1val2').owner.get() == owners.default


def test_leadership_default_multi_dyndescription():
    st1 = StrOption('st1', "", multi=True)
    st2 = StrOption('st2', "", multi=True, default_multi='no')
    stm = Leadership('st1', '', [st1, st2])
    st = DynOptionDescription('st', '', [stm], callback=return_list)
    od = OptionDescription('od', '', [st])
    od2 = OptionDescription('od', '', [od])
    api = Config(od2)
    owner = api.owner.get()
    #
    assert api.option('od.stval1.st1val1.st1val1').value.get() == []
    assert api.option('od.stval2.st1val2.st1val2').value.get() == []
    assert api.option('od.stval1.st1val1.st1val1').owner.isdefault()
    assert api.option('od.stval2.st1val2.st1val2').owner.isdefault()
    #
    api.option('od.stval1.st1val1.st1val1').value.set(['yes'])
    assert api.option('od.stval1.st1val1.st1val1').value.get() == ['yes']
    assert api.option('od.stval1.st1val1.st2val1', 0).value.get() == 'no'
    assert api.option('od.stval2.st1val2.st1val2').value.get() == []
    assert api.option('od.stval1.st1val1.st1val1').owner.get() == owner
    assert api.option('od.stval1.st1val1.st2val1', 0).owner.isdefault()
    assert api.option('od.stval2.st1val2.st1val2').owner.isdefault()


def _test_leadership(cfg):
    owner = cfg.owner.get()
    cfg.option('od.val1.val1').value.set(['val1', 'val2'])
    cfg.option('od.val1.val2', 0).value.set('val1')
    cfg.option('od.val1.val2', 1).value.set('val2')
    assert cfg.value.dict() == {'od.stval1.st1val1.st2val1': [], 'od.stval2.st1val2.st2val2': [], 'od.stval2.st1val2.st1val2': [], 'od.stval1.st1val1.st1val1': [], 'od.val1.val1': ['val1', 'val2'], 'od.val1.val2': ['val1', 'val2']}
    assert cfg.option('od.stval1.st1val1.st1val1').value.get() == []
    assert cfg.option('od.stval2.st1val2.st1val2').value.get() == []
    assert cfg.option('od.stval1.st1val1.st1val1').owner.get() == owners.default
    assert cfg.option('od.stval2.st1val2.st1val2').owner.get() == owners.default
    #
    cfg.option('od.stval1.st1val1.st1val1').value.set(['yes'])
    assert cfg.value.dict() == {'od.stval1.st1val1.st2val1': [None], 'od.stval2.st1val2.st2val2': [], 'od.stval2.st1val2.st1val2': [], 'od.stval1.st1val1.st1val1': ['yes'], 'od.val1.val1': ['val1', 'val2'], 'od.val1.val2': ['val1', 'val2']}
    assert cfg.option('od.stval1.st1val1.st1val1').value.get() == ['yes']
    assert cfg.option('od.stval1.st1val1.st2val1', 0).value.get() == None
    assert cfg.option('od.stval2.st1val2.st1val2').value.get() == []
    assert cfg.option('od.stval1.st1val1.st1val1').owner.get() == owner
    assert cfg.option('od.stval1.st1val1.st2val1', 0).owner.get() == owners.default
    assert cfg.option('od.stval2.st1val2.st1val2').owner.get() == owners.default
    #
    cfg.option('od.stval1.st1val1.st2val1', 0).value.set('no')
    assert cfg.option('od.stval1.st1val1.st1val1').value.get() == ['yes']
    assert cfg.option('od.stval1.st1val1.st2val1', 0).value.get() == 'no'
    assert cfg.option('od.stval2.st1val2.st1val2').value.get() == []
    assert cfg.option('od.stval1.st1val1.st1val1').owner.get() == owner
    assert cfg.option('od.stval1.st1val1.st2val1', 0).owner.get() == owner
    assert cfg.option('od.stval2.st1val2.st1val2').owner.get() == owners.default
    #
    cfg.option('od.stval1.st1val1.st1val1').value.pop(0)
    assert cfg.option('od.stval1.st1val1.st1val1').value.get() == []
    assert cfg.option('od.stval2.st1val2.st1val2').value.get() == []
    assert cfg.option('od.stval1.st1val1.st1val1').owner.get() == owner
    assert cfg.option('od.stval2.st1val2.st1val2').owner.get() == owners.default
    #
    cfg.option('od.stval1.st1val1.st1val1').value.set(['yes'])
    cfg.option('od.stval1.st1val1.st2val1', 0).value.set('yes')
    assert cfg.option('od.stval1.st1val1.st1val1').owner.get() == owner
    assert cfg.option('od.stval1.st1val1.st2val1', 0).owner.get() == owner
    assert cfg.option('od.stval2.st1val2.st1val2').owner.get() == owners.default
    #
    cfg.option('od.stval1.st1val1.st2val1', 0).value.reset()
    assert cfg.option('od.stval1.st1val1.st1val1').owner.get() == owner
    assert cfg.option('od.stval1.st1val1.st2val1', 0).owner.get() == owners.default
    assert cfg.option('od.stval2.st1val2.st1val2').owner.get() == owners.default
    #
    cfg.option('od.stval1.st1val1.st1val1').value.set(['yes'])
    cfg.option('od.stval1.st1val1.st2val1', 0).value.set('yes')
    cfg.option('od.stval1.st1val1.st1val1').value.reset()
    assert cfg.option('od.stval1.st1val1.st1val1').value.get() == []
    assert cfg.option('od.stval2.st1val2.st1val2').value.get() == []
    assert cfg.option('od.stval1.st1val1.st1val1').owner.get() == owners.default
    assert cfg.option('od.stval2.st1val2.st1val2').owner.get() == owners.default


def test_leadership_dyndescription_param_leader():
    val1 = StrOption('val1', "", multi=True)
    val2 = StrOption('val2', "", multi=True)
    odval = Leadership('val1', '', [val1, val2])
    st1 = StrOption('st1', "", multi=True)
    st2 = StrOption('st2', "", multi=True)
    stm = Leadership('st1', '', [st1, st2])
    st = DynOptionDescription('st', '', [stm], callback=return_list, callback_params=Params(ParamOption(val1)))
    od = OptionDescription('od', '', [st, odval])
    od2 = OptionDescription('od', '', [od])
    cfg = Config(od2)
    _test_leadership(cfg)


def test_leadership_default_multi_dyndescription():
    st1 = StrOption('st1', "", multi=True)
    st2 = StrOption('st2', "", multi=True, default_multi='no')
    stm = Leadership('st1', '', [st1, st2])
    st = DynOptionDescription('st', '', [stm], callback=return_list)
    od = OptionDescription('od', '', [st])
    od2 = OptionDescription('od', '', [od])
    api = Config(od2)
    owner = api.owner.get()
    #
    assert api.option('od.stval1.st1val1.st1val1').value.get() == []
    assert api.option('od.stval2.st1val2.st1val2').value.get() == []
    assert api.option('od.stval1.st1val1.st1val1').owner.isdefault()
    assert api.option('od.stval2.st1val2.st1val2').owner.isdefault()
    #
    api.option('od.stval1.st1val1.st1val1').value.set(['yes'])
    assert api.option('od.stval1.st1val1.st1val1').value.get() == ['yes']
    assert api.option('od.stval1.st1val1.st2val1', 0).value.get() == 'no'
    assert api.option('od.stval2.st1val2.st1val2').value.get() == []
    assert api.option('od.stval1.st1val1.st1val1').owner.get() == owner
    assert api.option('od.stval1.st1val1.st2val1', 0).owner.isdefault()
    assert api.option('od.stval2.st1val2.st1val2').owner.isdefault()


def test_leadership_dyndescription_param_follower():
    val1 = StrOption('val1', "", multi=True)
    val2 = StrOption('val2', "", multi=True)
    odval = Leadership('val1', '', [val1, val2])
    st1 = StrOption('st1', "", multi=True)
    st2 = StrOption('st2', "", multi=True)
    stm = Leadership('st1', '', [st1, st2])
    st = DynOptionDescription('st', '', [stm], callback=return_list, callback_params=Params(ParamOption(val2)))
    od = OptionDescription('od', '', [st, odval])
    od2 = OptionDescription('od', '', [od])
    cfg = Config(od2)
    _test_leadership(cfg)


def test_leadership_default_multi_dyndescription():
    st1 = StrOption('st1', "", multi=True)
    st2 = StrOption('st2', "", multi=True, default_multi='no')
    stm = Leadership('st1', '', [st1, st2])
    st = DynOptionDescription('st', '', [stm], callback=return_list)
    od = OptionDescription('od', '', [st])
    od2 = OptionDescription('od', '', [od])
    api = Config(od2)
    owner = api.owner.get()
    #
    assert api.option('od.stval1.st1val1.st1val1').value.get() == []
    assert api.option('od.stval2.st1val2.st1val2').value.get() == []
    assert api.option('od.stval1.st1val1.st1val1').owner.isdefault()
    assert api.option('od.stval2.st1val2.st1val2').owner.isdefault()
    #
    api.option('od.stval1.st1val1.st1val1').value.set(['yes'])
    assert api.option('od.stval1.st1val1.st1val1').value.get() == ['yes']
    assert api.option('od.stval1.st1val1.st2val1', 0).value.get() == 'no'
    assert api.option('od.stval2.st1val2.st1val2').value.get() == []
    assert api.option('od.stval1.st1val1.st1val1').owner.get() == owner
    assert api.option('od.stval1.st1val1.st2val1', 0).owner.isdefault()
    assert api.option('od.stval2.st1val2.st1val2').owner.isdefault()


def test_leadership_submulti_dyndescription():
    st1 = StrOption('st1', "", multi=True)
    st2 = StrOption('st2', "", multi=submulti)
    stm = Leadership('st1', '', [st1, st2])
    std = DynOptionDescription('st', '', [stm], callback=return_list)
    od1 = OptionDescription('od', '', [std])
    od2 = OptionDescription('od', '', [od1])
    api = Config(od2)
    owner = api.owner.get()
    #
    assert api.option('od.stval1.st1val1.st1val1').value.get() == []
    assert api.option('od.stval2.st1val2.st1val2').value.get() == []
    assert api.option('od.stval1.st1val1.st1val1').owner.isdefault()
    assert api.option('od.stval2.st1val2.st1val2').owner.isdefault()
    #
    api.option('od.stval1.st1val1.st1val1').value.set(['yes'])
    assert api.option('od.stval1.st1val1.st1val1').value.get() == ['yes']
    assert api.option('od.stval1.st1val1.st2val1', 0).value.get() == []
    assert api.option('od.stval2.st1val2.st1val2').value.get() == []
    assert api.option('od.stval1.st1val1.st1val1').owner.get() == owner
    assert api.option('od.stval1.st1val1.st2val1', 0).owner.isdefault()
    assert api.option('od.stval2.st1val2.st1val2').owner.isdefault()
    #
    api.option('od.stval1.st1val1.st2val1', 0).value.set(['no'])
    assert api.option('od.stval1.st1val1.st1val1').value.get() == ['yes']
    assert api.option('od.stval1.st1val1.st2val1', 0).value.get() == ['no']
    assert api.option('od.stval2.st1val2.st1val2').value.get() == []
    assert api.option('od.stval1.st1val1.st1val1').owner.get() == owner
    assert api.option('od.stval1.st1val1.st2val1', 0).owner.get() == owner
    assert api.option('od.stval2.st1val2.st1val2').owner.isdefault()


def test_leadership_callback_dyndescription():
    st1 = StrOption('st1', "", multi=True)
    st2 = StrOption('st2', "", multi=True, callback=return_dynval, callback_params=Params(kwargs={'value': ParamOption(st1)}))
    stm = Leadership('st1', '', [st1, st2])
    st1 = DynOptionDescription('st', '', [stm], callback=return_list)
    od1 = OptionDescription('od', '', [st1])
    od2 = OptionDescription('od', '', [od1])
    api = Config(od2)
    owner = api.owner.get()
    assert api.value.dict() == {'od.stval1.st1val1.st2val1': [], 'od.stval2.st1val2.st2val2': [], 'od.stval2.st1val2.st1val2': [], 'od.stval1.st1val1.st1val1': []}
    assert api.option('od.stval1.st1val1.st1val1').value.get() ==[]
    assert api.option('od.stval2.st1val2.st1val2').value.get() == []
    assert api.option('od.stval1.st1val1.st1val1').owner.isdefault()
    assert api.option('od.stval2.st1val2.st1val2').owner.isdefault()
    #
    api.option('od.stval1.st1val1.st1val1').value.set(['yes'])
    assert api.value.dict() == {'od.stval1.st1val1.st2val1': ['yes'], 'od.stval2.st1val2.st2val2': [], 'od.stval2.st1val2.st1val2': [], 'od.stval1.st1val1.st1val1': ['yes']}
    assert api.option('od.stval1.st1val1.st1val1').value.get() == ['yes']
    assert api.option('od.stval1.st1val1.st2val1', 0).value.get() == 'yes'
    assert api.option('od.stval2.st1val2.st1val2').value.get() == []
    assert api.option('od.stval1.st1val1.st1val1').owner.get() == owner
    assert api.option('od.stval1.st1val1.st2val1', 0).owner.isdefault()
    assert api.option('od.stval2.st1val2.st1val2').owner.isdefault()
    #
    api.option('od.stval1.st1val1.st2val1', 0).value.set('no')
    assert api.option('od.stval1.st1val1.st1val1').value.get() == ['yes']
    assert api.option('od.stval1.st1val1.st2val1', 0).value.get() == 'no'
    assert api.option('od.stval2.st1val2.st1val2').value.get() == []
    assert api.option('od.stval1.st1val1.st1val1').owner.get() == owner
    assert api.option('od.stval1.st1val1.st2val1', 0).owner.get() == owner
    assert api.option('od.stval2.st1val2.st1val2').owner.isdefault()
    #
    api.option('od.stval1.st1val1.st1val1').value.pop(0)
    assert api.option('od.stval1.st1val1.st1val1').value.get() == []
    assert api.option('od.stval2.st1val2.st1val2').value.get() == []
    assert api.option('od.stval1.st1val1.st1val1').owner.get() == owner
    assert api.option('od.stval2.st1val2.st1val2').owner.isdefault()
    #
    api.option('od.stval1.st1val1.st1val1').value.set(['yes'])
    api.option('od.stval1.st1val1.st2val1', 0).value.set('yes')
    assert api.option('od.stval1.st1val1.st1val1').owner.get() == owner
    assert api.option('od.stval1.st1val1.st1val1').owner.get() == owner
    assert api.option('od.stval1.st1val1.st2val1', 0).owner.get() == owner
    assert api.option('od.stval2.st1val2.st1val2').owner.isdefault()
    api.option('od.stval1.st1val1.st2val1', 0).value.reset()
    assert api.option('od.stval1.st1val1.st1val1').owner.get() == owner
    assert api.option('od.stval1.st1val1.st2val1', 0).owner.isdefault()
    assert api.option('od.stval2.st1val2.st1val2').owner.isdefault()
    #
    api.option('od.stval1.st1val1.st1val1').value.set(['yes'])
    api.option('od.stval1.st1val1.st2val1', 0).value.set('yes')
    api.option('od.stval1.st1val1.st1val1').value.reset()
    assert api.option('od.stval1.st1val1.st1val1').value.get() == []
    assert api.option('od.stval2.st1val2.st1val2').value.get() == []
    assert api.option('od.stval1.st1val1.st1val1').owner.isdefault()
    assert api.option('od.stval2.st1val2.st1val2').owner.isdefault()
    #
    api.option('od.stval1.st1val1.st1val1').value.set(['yes'])
    assert api.option('od.stval1.st1val1.st2val1', 0).value.get() == 'yes'


def test_leadership_callback_value_dyndescription():
    st1 = StrOption('st1', "", multi=True)
    st2 = StrOption('st2', "", multi=True, callback=return_dynval, callback_params=Params(kwargs={'value': ParamValue('val')}))
    stm = Leadership('st1', '', [st1, st2])
    st = DynOptionDescription('st', '', [stm], callback=return_list)
    od = OptionDescription('od', '', [st])
    od2 = OptionDescription('od', '', [od])
    api = Config(od2)
    assert api.option('od.stval1.st1val1.st1val1').value.get() == []
    api.option('od.stval1.st1val1.st1val1').value.set(['yes'])
    assert api.option('od.stval1.st1val1.st1val1').value.get() == ['yes']
    api.option('od.stval1.st1val1.st2val1', 0).value.set('val')
    assert api.option('od.stval1.st1val1.st2val1', 0).value.get() == 'val'


def test_leadership_callback_nomulti_dyndescription():
    v11 = StrOption('v1', '', "val")
    st1 = StrOption('st1', "", multi=True)
    st2 = StrOption('st2', "", multi=True, callback=return_dynval, callback_params=Params(ParamOption(v11)))
    stm = Leadership('st1', '', [st1, st2])
    stt = DynOptionDescription('st', '', [stm], callback=return_list)
    od1 = OptionDescription('od', '', [stt])
    od2 = OptionDescription('od', '', [od1, v11])
    api = Config(od2)
    assert api.option('od.stval1.st1val1.st1val1').value.get() == []
    api.option('od.stval1.st1val1.st1val1').value.set(['yes'])
    assert api.option('od.stval1.st1val1.st1val1').value.get() == ['yes']
    assert api.option('od.stval1.st1val1.st2val1', 0).value.get() == 'val'


def test_leadership_callback_samegroup_dyndescription():
    st1 = StrOption('st1', "", multi=True)
    st2 = StrOption('st2', "", multi=True)
    st3 = StrOption('st3', "", multi=True, callback=return_dynval, callback_params=Params(ParamOption(st2)))
    stm = Leadership('st1', '', [st1, st2, st3])
    stt = DynOptionDescription('st', '', [stm], callback=return_list)
    od1 = OptionDescription('od', '', [stt])
    od2 = OptionDescription('od', '', [od1])
    api = Config(od2)
    owner = api.owner.get()
    assert api.value.dict() == {'od.stval1.st1val1.st1val1': [],
                                      'od.stval1.st1val1.st2val1': [],
                                      'od.stval1.st1val1.st3val1': [],
                                      'od.stval2.st1val2.st1val2': [],
                                      'od.stval2.st1val2.st2val2': [],
                                      'od.stval2.st1val2.st3val2': []}
    assert api.option('od.stval1.st1val1.st1val1').value.get() == []
    assert api.option('od.stval2.st1val2.st1val2').value.get() == []
    assert api.option('od.stval1.st1val1.st1val1').owner.isdefault()
    assert api.option('od.stval2.st1val2.st1val2').owner.isdefault()
    #
    api.option('od.stval1.st1val1.st1val1').value.set(['yes'])
    assert api.value.dict() == {'od.stval1.st1val1.st1val1': ['yes'],
                                      'od.stval1.st1val1.st2val1': [None],
                                      'od.stval1.st1val1.st3val1': [None],
                                      'od.stval2.st1val2.st1val2': [],
                                      'od.stval2.st1val2.st2val2': [],
                                      'od.stval2.st1val2.st3val2': []}
    assert api.option('od.stval1.st1val1.st1val1').owner.get() == owner
    assert api.option('od.stval1.st1val1.st2val1', 0).owner.isdefault()
    assert api.option('od.stval1.st1val1.st3val1', 0).owner.isdefault()
    assert api.option('od.stval2.st1val2.st1val2').owner.isdefault()
    #
    api.option('od.stval1.st1val1.st2val1', 0).value.set('yes')
    assert api.value.dict() == {'od.stval1.st1val1.st1val1': ['yes'],
                                      'od.stval1.st1val1.st2val1': ['yes'],
                                      'od.stval1.st1val1.st3val1': ['yes'],
                                      'od.stval2.st1val2.st1val2': [],
                                      'od.stval2.st1val2.st2val2': [],
                                      'od.stval2.st1val2.st3val2': []}
    assert api.option('od.stval1.st1val1.st1val1').owner.get() == owner
    assert api.option('od.stval1.st1val1.st2val1', 0).owner.get() == owner
    assert api.option('od.stval1.st1val1.st3val1', 0).owner.isdefault()
    assert api.option('od.stval2.st1val2.st1val2').owner.isdefault()


def test_invalid_conflict_dyndescription():
    st = StrOption('st', '')
    dod = DynOptionDescription('dod', '', [st], callback=return_list)
    dodinvalid = StrOption('dodinvalid', '')
    dod, dodinvalid
    raises(ConflictError, "OptionDescription('od', '', [dod, dodinvalid])")


def test_invalid_subod_dyndescription():
    st2 = StrOption('st2', '')
    od1 = OptionDescription('od1', '', [st2])
    od1
    raises(ConfigError, "DynOptionDescription('dod', '', [od1], callback=return_list)")


def test_invalid_subdynod_dyndescription():
    st2 = StrOption('st2', '')
    od1 = DynOptionDescription('od1', '', [st2], callback=return_list)
    od1
    raises(ConfigError, "DynOptionDescription('dod', '', [od1], callback=return_list)")


def test_invalid_symlink_dyndescription():
    st = StrOption('st', '')
    st2 = SymLinkOption('st2', st)
    st2
    raises(ConfigError, "DynOptionDescription('dod', '', [st, st2], callback=return_list)")


def test_nocallback_dyndescription():
    st = StrOption('st', '')
    st2 = StrOption('st2', '')
    st, st2
    raises(ConfigError, "DynOptionDescription('dod', '', [st, st2])")


def test_invalid_samevalue_dyndescription():
    st1 = StrOption('st', '')
    dod = DynOptionDescription('dod', '', [st1], callback=return_same_list)
    od1 = OptionDescription('od', '', [dod])
    cfg = Config(od1)
    raises(ValueError, "cfg.value.dict()")


def test_invalid_name_dyndescription():
    st1 = StrOption('st', '')
    dod = DynOptionDescription('dod', '', [st1], callback=return_wrong_list)
    od1 = OptionDescription('od', '', [dod])
    cfg = Config(od1)
    raises(ValueError, "cfg.value.dict()")
