# coding: utf-8
from .autopath import do_autopath
do_autopath()

from py.test import raises

from tiramisu import IntOption, UnicodeOption, OptionDescription, Config
from tiramisu.error import PropertiesOptionError, ConfigError
from tiramisu.api import TIRAMISU_VERSION
from tiramisu.storage import list_sessions, delete_session


def teardown_function(function):
    assert list_sessions() == [], 'session list is not empty when leaving "{}"'.format(function.__name__)


def make_description():
    u1 = IntOption('u1', '', properties=('frozen', 'mandatory', 'disabled', ))
    u2 = IntOption('u2', '', properties=('frozen', 'mandatory', 'disabled', ))
    return OptionDescription('od1', '', [u1, u2])


def test_permissive():
    descr = make_description()
    api = Config(descr)
    api.property.read_write()
    api.property.read_write()
    props = frozenset()
    try:
        api.option('u1').value.get()
    except PropertiesOptionError as err:
        props = err.proptype
    assert set(props) == {'disabled'}
    api.unrestraint.permissive.set(frozenset(['disabled']))
    assert api.unrestraint.permissive.get() == frozenset(['disabled'])
    props = frozenset()
    try:
        api.option('u1').value.get()
    except PropertiesOptionError as err:
        props = err.proptype
    assert set(props) == {'disabled'}
    api.property.add('permissive')
    api.option('u1').value.get()
    api.property.pop('permissive')
    props = frozenset()
    try:
        api.option('u1').value.get()
    except PropertiesOptionError as err:
        props = err.proptype
    assert set(props) == {'disabled'}


def test_permissive_add():
    descr = make_description()
    api = Config(descr)
    api.property.read_write()
    api.property.read_write()
    props = frozenset()
    try:
        api.option('u1').value.get()
    except PropertiesOptionError as err:
        props = err.proptype
    assert set(props) == {'disabled'}
    api.unrestraint.permissive.add('disabled')
    assert api.unrestraint.permissive.get() == frozenset(['hidden', 'disabled'])
    props = frozenset()
    try:
        api.option('u1').value.get()
    except PropertiesOptionError as err:
        props = err.proptype
    assert set(props) == {'disabled'}
    api.property.add('permissive')
    api.option('u1').value.get()
    api.property.pop('permissive')
    props = frozenset()
    try:
        api.option('u1').value.get()
    except PropertiesOptionError as err:
        props = err.proptype
    assert set(props) == {'disabled'}


def test_permissive_pop():
    descr = make_description()
    api = Config(descr)
    api.property.read_write()
    api.property.read_write()
    props = frozenset()
    try:
        api.forcepermissive.option('u1').value.get()
    except PropertiesOptionError as err:
        props = err.proptype
    assert set(props) == {'disabled'}
    api.unrestraint.permissive.add('disabled')
    assert api.unrestraint.permissive.get() == frozenset(['hidden', 'disabled'])
    api.forcepermissive.option('u1').value.get()
    api.unrestraint.permissive.pop('disabled')
    props = frozenset()
    try:
        api.forcepermissive.option('u1').value.get()
    except PropertiesOptionError as err:
        props = err.proptype
    assert set(props) == {'disabled'}


def test_permissive_reset():
    descr = make_description()
    api = Config(descr)
    api.property.read_write()
    assert api.unrestraint.permissive.get() == frozenset(['hidden'])
    #
    api.unrestraint.permissive.set(frozenset(['disabled']))
    assert api.unrestraint.permissive.get() == frozenset(['disabled'])
    #
    api.unrestraint.permissive.reset()
    assert api.unrestraint.permissive.get() == frozenset()


def test_permissive_mandatory():
    descr = make_description()
    api = Config(descr)
    api.property.read_only()
    props = frozenset()
    try:
        api.option('u1').value.get()
    except PropertiesOptionError as err:
        props = err.proptype
    if TIRAMISU_VERSION == 2:
        assert frozenset(props) == frozenset(['disabled', 'mandatory'])
    else:
        assert frozenset(props) == frozenset(['disabled'])
    api.unrestraint.permissive.set(frozenset(['mandatory', 'disabled']))
    assert api.unrestraint.permissive.get() == frozenset(['mandatory', 'disabled'])
    api.property.add('permissive')
    api.option('u1').value.get()
    api.property.pop('permissive')
    try:
        api.option('u1').value.get()
    except PropertiesOptionError as err:
        props = err.proptype
    if TIRAMISU_VERSION == 2:
        assert frozenset(props) == frozenset(['disabled', 'mandatory'])
    else:
        assert frozenset(props) == frozenset(['disabled'])


def test_permissive_frozen():
    descr = make_description()
    api = Config(descr)
    api.property.read_write()
    api.unrestraint.permissive.set(frozenset(['frozen', 'disabled']))
    assert api.unrestraint.permissive.get() == frozenset(['frozen', 'disabled'])
    assert api.permissive.get() == frozenset(['frozen', 'disabled'])
    try:
        api.option('u1').value.set(1)
    except PropertiesOptionError as err:
        props = err.proptype
    if TIRAMISU_VERSION == 2:
        assert frozenset(props) == frozenset(['disabled', 'frozen'])
    else:
        assert frozenset(props) == frozenset(['disabled'])
    api.property.add('permissive')
    api.option('u1').value.set(1)
    assert api.option('u1').value.get() == 1
    api.property.pop('permissive')
    try:
        api.option('u1').value.set(1)
    except PropertiesOptionError as err:
        props = err.proptype
    if TIRAMISU_VERSION == 2:
        assert frozenset(props) == frozenset(['disabled', 'frozen'])
    else:
        assert frozenset(props) == frozenset(['disabled'])


def test_invalid_permissive():
    descr = make_description()
    api = Config(descr)
    api.property.read_write()
    raises(TypeError, "api.unrestraint.permissive.set(['frozen', 'disabled'])")


def test_forbidden_permissive():
    descr = make_description()
    api = Config(descr)
    api.property.read_write()
    raises(ConfigError, "api.permissive.set(frozenset(['force_default_on_freeze']))")
    raises(ConfigError, "api.permissive.set(frozenset(['force_metaconfig_on_freeze']))")


def test_permissive_option():
    descr = make_description()
    api = Config(descr)
    api.property.read_write()

    props = frozenset()
    try:
        api.option('u1').value.get()
    except PropertiesOptionError as err:
        props = err.proptype
    assert set(props) == {'disabled'}
    props = frozenset()
    try:
        api.option('u2').value.get()
    except PropertiesOptionError as err:
        props = err.proptype
    assert set(props) == {'disabled'}

    api.unrestraint.option('u1').permissive.set(frozenset(['disabled']))
    props = frozenset()
    try:
        api.option('u1').value.get()
    except PropertiesOptionError as err:
        props = err.proptype
    assert frozenset(props) == frozenset()
    props = frozenset()
    try:
        api.option('u2').value.get()
    except PropertiesOptionError as err:
        props = err.proptype
    assert set(props) == {'disabled'}

    api.property.add('permissive')
    api.option('u1').value.get()
    props = frozenset()
    try:
        api.option('u2').value.get()
    except PropertiesOptionError as err:
        props = err.proptype
    assert set(props) == {'disabled'}

    api.property.pop('permissive')
    props = frozenset()
    try:
        api.option('u1').value.get()
    except PropertiesOptionError as err:
        props = err.proptype
    assert frozenset(props) == frozenset()
    props = frozenset()
    try:
        api.option('u2').value.get()
    except PropertiesOptionError as err:
        props = err.proptype
    assert set(props) == {'disabled'}


def test_permissive_option_cache():
    descr = make_description()
    api = Config(descr)
    api.property.read_write()

    props = frozenset()
    try:
        api.option('u1').value.get()
    except PropertiesOptionError as err:
        props = err.proptype
    assert set(props) == {'disabled'}
    props = frozenset()
    try:
        api.option('u2').value.get()
    except PropertiesOptionError as err:
        props = err.proptype
    assert set(props) == {'disabled'}

    api.unrestraint.option('u1').permissive.set(frozenset(['disabled']))
    props = frozenset()
    try:
        api.option('u1').value.get()
    except PropertiesOptionError as err:
        props = err.proptype
    assert frozenset(props) == frozenset()
    props = frozenset()
    try:
        api.option('u2').value.get()
    except PropertiesOptionError as err:
        props = err.proptype
    assert set(props) == {'disabled'}

    api.property.add('permissive')
    api.option('u1').value.get()
    props = frozenset()
    try:
        api.option('u2').value.get()
    except PropertiesOptionError as err:
        props = err.proptype
    assert set(props) == {'disabled'}

    api.property.pop('permissive')
    props = frozenset()
    try:
        api.option('u1').value.get()
    except PropertiesOptionError as err:
        props = err.proptype
    assert frozenset(props) == frozenset()
    props = frozenset()
    try:
        api.option('u2').value.get()
    except PropertiesOptionError as err:
        props = err.proptype
    assert set(props) == {'disabled'}


def test_permissive_option_mandatory():
    descr = make_description()
    api = Config(descr)
    api.property.read_only()
    props = frozenset()
    try:
        api.option('u1').value.get()
    except PropertiesOptionError as err:
        props = err.proptype
    if TIRAMISU_VERSION == 2:
        assert frozenset(props) == frozenset(['disabled', 'mandatory'])
    else:
        assert frozenset(props) == frozenset(['disabled'])
    api.unrestraint.option('u1').permissive.set(frozenset(['mandatory', 'disabled']))
    assert api.unrestraint.option('u1').permissive.get() == frozenset(['mandatory', 'disabled'])
    api.property.add('permissive')
    api.option('u1').value.get()
    api.property.pop('permissive')
    try:
        api.option('u1').value.get()
    except PropertiesOptionError as err:
        props = err.proptype
    if TIRAMISU_VERSION == 2:
        assert frozenset(props) == frozenset(['disabled', 'mandatory'])
    else:
        assert frozenset(props) == frozenset(['disabled'])


def test_permissive_option_frozen():
    descr = make_description()
    api = Config(descr)
    api.property.read_write()
    api.unrestraint.option('u1').permissive.set(frozenset(['frozen', 'disabled']))
    api.option('u1').value.set(1)
    assert api.option('u1').value.get() == 1
    api.property.add('permissive')
    assert api.option('u1').value.get() == 1
    api.property.pop('permissive')
    assert api.option('u1').value.get() == 1


if TIRAMISU_VERSION == 3:
    def test_invalid_option_permissive():
        descr = make_description()
        api = Config(descr)
        api.property.read_write()
        raises(TypeError, "api.unrestraint.option('u1').permissive.set(['frozen', 'disabled'])")


def test_remove_option_permissive():
    var1 = UnicodeOption('var1', '', u'value', properties=('hidden',))
    od1 = OptionDescription('od1', '', [var1])
    rootod = OptionDescription('rootod', '', [od1])
    api = Config(rootod)
    api.property.read_write()
    raises(PropertiesOptionError, "api.option('od1.var1').value.get()")
    api.forcepermissive.option('od1.var1').permissive.set(frozenset(['hidden']))
    assert api.forcepermissive.option('od1.var1').permissive.get() == frozenset(['hidden'])
    assert api.option('od1.var1').value.get() == 'value'
    api.forcepermissive.option('od1.var1').permissive.set(frozenset())
    assert api.forcepermissive.option('od1.var1').permissive.get() == frozenset()
    raises(PropertiesOptionError, "api.option('od1.var1').value.get()")


def test_reset_option_permissive():
    var1 = UnicodeOption('var1', '', u'value', properties=('hidden',))
    od1 = OptionDescription('od1', '', [var1])
    rootod = OptionDescription('rootod', '', [od1])
    api = Config(rootod)
    api.property.read_write()
    raises(PropertiesOptionError, "api.option('od1.var1').value.get()")
    api.forcepermissive.option('od1.var1').permissive.set(frozenset(['hidden']))
    assert api.forcepermissive.option('od1.var1').permissive.get() == frozenset(['hidden'])
    assert api.option('od1.var1').value.get() == 'value'
    api.forcepermissive.option('od1.var1').permissive.reset()
    assert api.forcepermissive.option('od1.var1').permissive.get() == frozenset()
    raises(PropertiesOptionError, "api.option('od1.var1').value.get()")
