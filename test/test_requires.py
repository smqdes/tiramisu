# coding: utf-8
from .autopath import do_autopath
do_autopath()

from copy import copy
from tiramisu.i18n import _
from tiramisu.setting import groups
from tiramisu import setting
setting.expires_time = 1
from tiramisu import IPOption, OptionDescription, BoolOption, IntOption, StrOption, \
                     Leadership, Config
from tiramisu.error import PropertiesOptionError, RequirementError
from py.test import raises
from tiramisu.storage import list_sessions, delete_session


def teardown_function(function):
    assert list_sessions() == [], 'session list is not empty when leaving "{}"'.format(function.__name__)


def test_properties():
    a = BoolOption('activate_service', '', True)
    b = IPOption('ip_address_service', '', properties=('disabled',))
    od = OptionDescription('service', '', [a, b])
    api = Config(od)
    api.property.read_write()
    props = []
    try:
        api.option('ip_address_service').value.get()
    except PropertiesOptionError as err:
        props = err.proptype
    assert frozenset(props) == frozenset(['disabled'])
    api.unrestraint.option('ip_address_service').property.pop('disabled')
    api.option('ip_address_service').value.get()
    api.unrestraint.option('ip_address_service').property.add('disabled')
    props = []
    try:
        api.option('ip_address_service').value.get()
    except PropertiesOptionError as err:
        props = err.proptype
    assert frozenset(props) == frozenset(['disabled'])
    # pop twice
    api.unrestraint.option('ip_address_service').property.pop('disabled')
    api.unrestraint.option('ip_address_service').property.pop('disabled')


def test_requires():
    a = BoolOption('activate_service', '', True)
    b = IPOption('ip_address_service', '',
                 requires=[{'option': a, 'expected': False, 'action': 'disabled'}])
    od = OptionDescription('service', '', [a, b])
    api = Config(od)
    api.property.read_write()
    assert not api.option('activate_service').option.requires()
    assert api.option('ip_address_service').option.requires()
    api.option('ip_address_service').value.get()
    api.option('activate_service').value.set(False)
    props = []
    try:
        api.option('ip_address_service').value.get()
    except PropertiesOptionError as err:
        props = err.proptype
    assert frozenset(props) == frozenset(['disabled'])
    api.option('activate_service').value.set(True)
    api.option('ip_address_service').value.get()


def test_requires_inverse():
    a = BoolOption('activate_service', '', True)
    b = IPOption('ip_address_service', '',
                 requires=[{'option': a, 'expected': False, 'action': 'disabled', 'inverse': True}])
    od = OptionDescription('service', '', [a, b])
    api = Config(od)
    api.property.read_write()
    props = []
    try:
        api.option('ip_address_service').value.get()
    except PropertiesOptionError as err:
        props = err.proptype
    assert frozenset(props) == frozenset(['disabled'])
    api.option('activate_service').value.set(False)
    api.option('ip_address_service').value.get()
    api.option('activate_service').value.set(True)
    try:
        api.option('ip_address_service').value.get()
    except PropertiesOptionError as err:
        props = err.proptype
    assert frozenset(props) == frozenset(['disabled'])


def test_requires_self():
    a = StrOption('ip_address_service', '',
                  requires=[{'option': 'self', 'expected': 'b', 'action': 'disabled'}])
    od = OptionDescription('service', '', [a])
    api = Config(od)
    api.property.read_write()
    assert api.option('ip_address_service').value.get() == None
    api.option('ip_address_service').value.set('a')
    assert api.option('ip_address_service').value.get() == 'a'
    api.option('ip_address_service').value.set('b')
    props = []
    try:
        api.option('ip_address_service').value.get()
    except PropertiesOptionError as err:
        props = err.proptype
    assert frozenset(props) == frozenset(['disabled'])


def test_requires_with_requires():
    a = BoolOption('activate_service', '', True)
    b = IPOption('ip_address_service', '',
                 requires=[{'option': a, 'expected': False, 'action': 'disabled'}])
    od = OptionDescription('service', '', [a, b])
    api = Config(od)
    api.property.read_write()
    api.option('ip_address_service').property.add('test')
    api.option('ip_address_service').value.get()
    api.option('activate_service').value.set(False)
    props = []
    try:
        api.option('ip_address_service').value.get()
    except PropertiesOptionError as err:
        props = err.proptype
    assert frozenset(props) == frozenset(['disabled'])
    api.option('activate_service').value.set(True)
    api.option('ip_address_service').value.get()


def test_requires_invalid():
    a = BoolOption('activate_service', '', True)
    a
    raises(ValueError, "IPOption('ip_address_service', '', requires='string')")
    raises(ValueError, "IPOption('ip_address_service', '', requires=[{'option': a, 'expected': False, 'action': 'disabled', 'unknown': True}])")
    raises(ValueError, "IPOption('ip_address_service', '', requires=[{'option': a, 'expected': False}])")
    raises(ValueError, "IPOption('ip_address_service', '', requires=[{'option': a, 'action': 'disabled'}])")
    raises(ValueError, "IPOption('ip_address_service', '', requires=[{'expected': False, 'action': 'disabled'}])")
    raises(ValueError, "IPOption('ip_address_service', '', requires=[{'option': a, 'expected': False, 'action': 'disabled', 'inverse': 'string'}])")
    raises(ValueError, "IPOption('ip_address_service', '', requires=[{'option': a, 'expected': False, 'action': 'disabled', 'transitive': 'string'}])")
    raises(ValueError, "IPOption('ip_address_service', '', requires=[{'option': a, 'expected': False, 'action': 'disabled', 'same_action': 'string'}])")
    raises(ValueError, "IPOption('ip_address_service', '', requires=[{'option': 'string', 'expected': False, 'action': 'disabled'}])")
    raises(ValueError, "IPOption('ip_address_service', '', requires=[{'option': a, 'expected': 'string', 'action': 'disabled'}])")


def test_requires_same_action():
    activate_service = BoolOption('activate_service', '', True)
    activate_service_web = BoolOption('activate_service_web', '', True,
                                      requires=[{'option': activate_service, 'expected': False,
                                                 'action': 'new'}])

    ip_address_service_web = IPOption('ip_address_service_web', '',
                                      requires=[{'option': activate_service_web, 'expected': False,
                                                 'action': 'disabled', 'inverse': False,
                                                 'transitive': True, 'same_action': False}])
    od1 = OptionDescription('service', '', [activate_service, activate_service_web, ip_address_service_web])
    api = Config(od1)
    api.property.read_write()
    api.property.add('new')
    api.option('activate_service').value.get()
    api.option('activate_service_web').value.get()
    api.option('ip_address_service_web').value.get()
    api.option('activate_service').value.set(False)
    #
    props = []
    try:
        api.option('activate_service_web').value.get()
    except PropertiesOptionError as err:
        props = err.proptype
    assert frozenset(props) == frozenset(['new'])
    #
    props = []
    try:
        api.option('ip_address_service_web').value.get()
    except PropertiesOptionError as err:
        props = err.proptype
        submsg = '"disabled" (' + _('the value of "{0}" is {1}').format('activate_service', '"False"') + ')'
        assert str(err) == str(_('cannot access to {0} "{1}" because has {2} {3}').format('option', 'ip_address_service_web', 'property', submsg))
        #access to cache
        assert str(err) == str(_('cannot access to {0} "{1}" because has {2} {3}').format('option', 'ip_address_service_web', 'property', submsg))
    assert frozenset(props) == frozenset(['disabled'])


def test_multiple_requires():
    a = StrOption('activate_service', '')
    b = IPOption('ip_address_service', '',
                 requires=[{'option': a, 'expected': 'yes', 'action': 'disabled'},
                           {'option': a, 'expected': 'ok', 'action': 'disabled'}])
    od = OptionDescription('service', '', [a, b])
    api = Config(od)
    api.property.read_write()
    api.option('ip_address_service').value.get()
    api.option('activate_service').value.set('yes')
    props = []
    try:
        api.option('ip_address_service').value.get()
    except PropertiesOptionError as err:
        props = err.proptype
    assert frozenset(props) == frozenset(['disabled'])

    api.option('activate_service').value.set('ok')
    props = []
    try:
        api.option('ip_address_service').value.get()
    except PropertiesOptionError as err:
        props = err.proptype
    assert frozenset(props) == frozenset(['disabled'])

    api.option('activate_service').value.set('no')
    api.option('ip_address_service').value.get()


def test_multiple_requires_cumulative():
    a = StrOption('activate_service', '')
    b = IPOption('ip_address_service', '',
                 requires=[{'option': a, 'expected': 'yes', 'action': 'disabled'},
                           {'option': a, 'expected': 'yes', 'action': 'hidden'}])
    od = OptionDescription('service', '', [a, b])
    api = Config(od)
    api.property.read_write()
    api.option('ip_address_service').value.get()
    api.option('activate_service').value.set('yes')
    props = []
    try:
        api.option('ip_address_service').value.get()
    except PropertiesOptionError as err:
        props = err.proptype
    assert set(props) == set(['hidden', 'disabled'])

    api.option('activate_service').value.set('ok')
    api.option('ip_address_service').value.get()

    api.option('activate_service').value.set('no')
    api.option('ip_address_service').value.get()


def test_multiple_requires_cumulative_inverse():
    a = StrOption('activate_service', '')
    b = IPOption('ip_address_service', '',
                 requires=[{'option': a, 'expected': 'yes', 'action': 'disabled', 'inverse': True},
                           {'option': a, 'expected': 'yes', 'action': 'hidden', 'inverse': True}])
    od = OptionDescription('service', '', [a, b])
    api = Config(od)
    api.property.read_write()
    props = []
    try:
        api.option('ip_address_service').value.get()
    except PropertiesOptionError as err:
        props = err.proptype
    assert set(props) == set(['hidden', 'disabled'])
    api.option('activate_service').value.set('yes')
    api.option('ip_address_service').value.get()

    api.option('activate_service').value.set('ok')
    props = []
    try:
        api.option('ip_address_service').value.get()
    except PropertiesOptionError as err:
        props = err.proptype
    assert set(props) == set(['hidden', 'disabled'])

    api.option('activate_service').value.set('no')
    props = []
    try:
        api.option('ip_address_service').value.get()
    except PropertiesOptionError as err:
        props = err.proptype
    assert set(props) == set(['hidden', 'disabled'])


def test_multiple_requires_inverse():
    a = StrOption('activate_service', '')
    b = IPOption('ip_address_service', '',
                 requires=[{'option': a, 'expected': 'yes', 'action': 'disabled', 'inverse': True},
                           {'option': a, 'expected': 'ok', 'action': 'disabled', 'inverse': True}])
    od = OptionDescription('service', '', [a, b])
    api = Config(od)
    api.property.read_write()
    props = []
    try:
        api.option('ip_address_service').value.get()
    except PropertiesOptionError as err:
        props = err.proptype
    assert frozenset(props) == frozenset(['disabled'])

    api.option('activate_service').value.set('yes')
    api.option('ip_address_service').value.get()

    api.option('activate_service').value.set('ok')
    api.option('ip_address_service').value.get()

    api.option('activate_service').value.set('no')
    props = []
    try:
        api.option('ip_address_service').value.get()
    except PropertiesOptionError as err:
        props = err.proptype
    assert frozenset(props) == frozenset(['disabled'])


def test_requires_transitive():
    a = BoolOption('activate_service', '', True)
    b = BoolOption('activate_service_web', '', True,
                   requires=[{'option': a, 'expected': False, 'action': 'disabled'}])

    d = IPOption('ip_address_service_web', '',
                 requires=[{'option': b, 'expected': False, 'action': 'disabled'}])
    od = OptionDescription('service', '', [a, b, d])
    api = Config(od)
    api.property.read_write()
    api.option('activate_service').value.get()
    api.option('activate_service_web').value.get()
    api.option('ip_address_service_web').value.get()
    api.option('activate_service').value.set(False)
    #
    props = []
    try:
        api.option('activate_service_web').value.get()
    except PropertiesOptionError as err:
        props = err.proptype
    assert frozenset(props) == frozenset(['disabled'])
    #
    props = []
    try:
        api.option('ip_address_service_web').value.get()
    except PropertiesOptionError as err:
        props = err.proptype
    assert frozenset(props) == frozenset(['disabled'])


def test_requires_transitive_unrestraint():
    a = BoolOption('activate_service', '', True)
    b = BoolOption('activate_service_web', '', True,
                   requires=[{'option': a, 'expected': False, 'action': 'disabled'}])

    d = IPOption('ip_address_service_web', '',
                 requires=[{'option': b, 'expected': False, 'action': 'disabled'}])
    od = OptionDescription('service', '', [a, b, d])
    api = Config(od)
    api.property.read_write()
    api.option('activate_service').value.get()
    api.option('activate_service_web').value.get()
    api.option('ip_address_service_web').value.get()
    api.option('activate_service').value.set(False)
    #
    assert api.unrestraint.option('activate_service_web').property.get() == {'disabled'}
    assert api.unrestraint.option('ip_address_service_web').property.get() == {'disabled'}


def test_requires_transitive_owner():
    a = BoolOption('activate_service', '', True)
    b = BoolOption('activate_service_web', '', True,
                   requires=[{'option': a, 'expected': False, 'action': 'disabled'}])

    d = IPOption('ip_address_service_web', '',
                 requires=[{'option': b, 'expected': False, 'action': 'disabled'}])
    od = OptionDescription('service', '', [a, b, d])
    api = Config(od)
    api.property.read_write()
    api.option('activate_service').value.get()
    api.option('activate_service_web').value.get()
    api.option('ip_address_service_web').value.get()
    #no more default value
    api.option('ip_address_service_web').value.set('1.1.1.1')
    api.option('activate_service').value.set(False)
    props = []
    try:
        api.option('ip_address_service_web').value.get()
    except PropertiesOptionError as err:
        props = err.proptype
    assert frozenset(props) == frozenset(['disabled'])


def test_requires_transitive_bis():
    a = BoolOption('activate_service', '', True)
    abis = BoolOption('activate_service_bis', '', True)
    b = BoolOption('activate_service_web', '', True,
                   requires=[{'option': a, 'expected': True, 'action': 'disabled', 'inverse': True}])

    d = IPOption('ip_address_service_web', '',
                 requires=[{'option': b, 'expected': True, 'action': 'disabled', 'inverse': True}])
    od = OptionDescription('service', '', [a, abis, b, d])
    api = Config(od)
    api.property.read_write()
    #
    api.option('activate_service_web').value.get()
    api.option('ip_address_service_web').value.get()
    api.option('activate_service').value.set(False)
    #
    props = []
    try:
        api.option('activate_service_web').value.get()
    except PropertiesOptionError as err:
        props = err.proptype
    assert frozenset(props) == frozenset(['disabled'])
    #
    props = []
    try:
        api.option('ip_address_service_web').value.get()
    except PropertiesOptionError as err:
        props = err.proptype
    assert frozenset(props) == frozenset(['disabled'])


def test_requires_transitive_hidden_permissive():
    a = BoolOption('activate_service', '', True)
    b = BoolOption('activate_service_web', '', True,
                   requires=[{'option': a, 'expected': False, 'action': 'hidden'}])
    d = IPOption('ip_address_service_web', '',
                 requires=[{'option': b, 'expected': False, 'action': 'disabled'}])
    od = OptionDescription('service', '', [a, b, d])
    api = Config(od)
    api.property.read_write()
    api.option('activate_service').value.get()
    api.option('ip_address_service_web').value.get()
    api.option('ip_address_service_web').value.get()
    api.option('activate_service').value.set(False)
    #
    api.option('ip_address_service_web').value.get()


def test_requires_transitive_hidden_disabled():
    a = BoolOption('activate_service', '', True)
    b = BoolOption('activate_service_web', '', True,
                   requires=[{'option': a, 'expected': False, 'action': 'hidden'}])
    d = IPOption('ip_address_service_web', '',
                 requires=[{'option': b, 'expected': False, 'action': 'disabled'}])
    od = OptionDescription('service', '', [a, b, d])
    api = Config(od)
    api.property.read_write()
    api.option('activate_service').value.get()
    api.option('activate_service_web').value.get()
    api.option('ip_address_service_web').value.get()
    api.option('activate_service').value.set(False)
    #
    props = []
    try:
        api.option('activate_service_web').value.get()
    except PropertiesOptionError as err:
        props = err.proptype
    assert frozenset(props) == frozenset(['hidden'])
    api.option('ip_address_service_web').value.get()


def test_requires_transitive_hidden_disabled_multiple():
    a = BoolOption('activate_service', '', True)
    b = BoolOption('activate_service_web', '', True,
                   requires=[{'option': a, 'expected': False, 'action': 'hidden'},
                             {'option': a, 'expected': False, 'action': 'disabled'}])
    d = IPOption('ip_address_service_web', '',
                 requires=[{'option': b, 'expected': False, 'action': 'mandatory'}])
    od = OptionDescription('service', '', [a, b, d])
    api = Config(od)
    api.property.read_write()
    api.option('activate_service').value.get()
    api.option('activate_service_web').value.get()
    api.option('ip_address_service_web').value.get()
    api.option('activate_service').value.set(False)
    #
    props = []
    try:
        api.option('activate_service_web').value.get()
    except PropertiesOptionError as err:
        props = err.proptype
    assert set(props) == {'disabled', 'hidden'}
    del props
    #
    req = None
    try:
        api.option('ip_address_service_web').value.get()
    except RequirementError as err:
        req = err
    assert req, "ip_address_service_web should raise RequirementError"
    assert str(req) == str(_('cannot access to option "{}" because required option "{}" has {} {}').format('ip_address_service_web', 'activate_service_web', 'property', '"disabled"'))
    del req
    #
    api.permissive.set(frozenset())
    try:
        api.option('ip_address_service_web').value.get()
    except RequirementError as err:
        req = err
    assert req, "ip_address_service_web should raise RequirementError"
    assert str(req) == str(_('cannot access to option "{}" because required option "{}" has {} {}').format('ip_address_service_web', 'activate_service_web', 'properties', '"disabled" and "hidden"'))
    del req


def test_requires_not_transitive():
    a = BoolOption('activate_service', '', True)
    b = BoolOption('activate_service_web', '', True,
                   requires=[{'option': a, 'expected': False, 'action': 'disabled'}])
    d = IPOption('ip_address_service_web', '',
                 requires=[{'option': b, 'expected': False,
                            'action': 'disabled', 'transitive': False}])
    od = OptionDescription('service', '', [a, b, d])
    api = Config(od)
    api.property.read_write()
    api.option('activate_service').value.get()
    api.option('activate_service_web').value.get()
    api.option('ip_address_service_web').value.get()
    api.option('activate_service').value.set(False)
    #
    props = []
    try:
        api.option('activate_service_web').value.get()
    except PropertiesOptionError as err:
        props = err.proptype
    assert frozenset(props) == frozenset(['disabled'])
    #
    api.option('ip_address_service_web').value.get()


def test_requires_not_transitive_not_same_action():
    a = BoolOption('activate_service', '', True)
    b = BoolOption('activate_service_web', '', True,
                   requires=[{'option': a, 'expected': False, 'action': 'disabled'}])
    d = IPOption('ip_address_service_web', '',
                 requires=[{'option': b, 'expected': False,
                            'action': 'hidden', 'transitive': False}])
    od = OptionDescription('service', '', [a, b, d])
    api = Config(od)
    api.property.read_write()
    api.option('activate_service').value.get()
    api.option('activate_service_web').value.get()
    api.option('ip_address_service_web').value.get()
    api.option('activate_service').value.set(False)
    #
    props = []
    try:
        api.option('activate_service_web').value.get()
    except PropertiesOptionError as err:
        props = err.proptype
    assert frozenset(props) == frozenset(['disabled'])
    #
    raises(RequirementError, "api.option('ip_address_service_web').value.get()")


def test_requires_None():
    a = BoolOption('activate_service', '')
    b = IPOption('ip_address_service', '',
                 requires=[{'option': a, 'expected': None, 'action': 'disabled'}])
    od = OptionDescription('service', '', [a, b])
    api = Config(od)
    api.property.read_write()
    props = []
    try:
        api.option('ip_address_service').value.get()
    except PropertiesOptionError as err:
        props = err.proptype
    assert frozenset(props) == frozenset(['disabled'])
    api.option('activate_service').value.set(False)
    api.option('ip_address_service').value.get()


def test_requires_multi_disabled():
    a = BoolOption('activate_service', '')
    b = IntOption('num_service', '')
    c = IPOption('ip_address_service', '',
                 requires=[{'option': a, 'expected': True, 'action': 'disabled'},
                           {'option': b, 'expected': 1, 'action': 'disabled'}])
    od = OptionDescription('service', '', [a, b, c])
    api = Config(od)
    api.property.read_write()

    api.option('ip_address_service').value.get()

    api.option('activate_service').value.set(True)
    props = []
    try:
        api.option('ip_address_service').value.get()
    except PropertiesOptionError as err:
        props = err.proptype
    assert frozenset(props) == frozenset(['disabled'])

    api.option('activate_service').value.set(False)
    api.option('ip_address_service').value.get()

    api.option('num_service').value.set(1)
    props = []
    try:
        api.option('ip_address_service').value.get()
    except PropertiesOptionError as err:
        props = err.proptype
    assert frozenset(props) == frozenset(['disabled'])

    api.option('activate_service').value.set(True)
    props = []
    try:
        api.option('ip_address_service').value.get()
    except PropertiesOptionError as err:
        props = err.proptype
    assert frozenset(props) == frozenset(['disabled'])


def test_requires_multi_disabled_new_format():
    a = BoolOption('activate_service', '')
    b = IntOption('num_service', '')
    c = IPOption('ip_address_service', '',
            requires=[{'expected': [{'option': a, 'value': True}, {'option': b, 'value': 1}], 'action': 'disabled'}])
    od = OptionDescription('service', '', [a, b, c])
    api = Config(od)
    api.property.read_write()

    api.option('ip_address_service').value.get()

    api.option('activate_service').value.set(True)
    props = []
    try:
        api.option('ip_address_service').value.get()
    except PropertiesOptionError as err:
        props = err.proptype
    assert frozenset(props) == frozenset(['disabled'])

    api.option('activate_service').value.set(False)
    api.option('ip_address_service').value.get()

    api.option('num_service').value.set(1)
    props = []
    try:
        api.option('ip_address_service').value.get()
    except PropertiesOptionError as err:
        props = err.proptype
    assert frozenset(props) == frozenset(['disabled'])

    api.option('activate_service').value.set(True)
    props = []
    try:
        api.option('ip_address_service').value.get()
    except PropertiesOptionError as err:
        props = err.proptype
    assert frozenset(props) == frozenset(['disabled'])


def test_requires_unknown_operator():
    a = BoolOption('activate_service', '')
    b = IntOption('num_service', '')
    raises(ValueError, """IPOption('ip_address_service', '',
            requires=[{'expected': [{'option': a, 'value': True}, {'option': b, 'value': 1}],
            'action': 'disabled', 'operator': 'unknown'}])""")


def test_requires_keys():
    a = BoolOption('activate_service', '')
    b = IntOption('num_service', '')
    raises(ValueError, """IPOption('ip_address_service', '',
            requires=[{'expected': [{'option': a, 'value2': True}, {'option': b, 'value': 1}],
            'action': 'disabled', 'operator': 'and'}])""")


def test_requires_unvalid():
    a = BoolOption('activate_service', '')
    b = IntOption('num_service', '')
    raises(ValueError, """IPOption('ip_address_service', '',
            requires=[{'expected': [{'option': a, 'value': 'unvalid'}, {'option': b, 'value': 1}],
            'action': 'disabled', 'operator': 'and'}])""")


def test_requires_multi_disabled_new_format_and():
    a = BoolOption('activate_service', '')
    b = IntOption('num_service', '')
    c = IPOption('ip_address_service', '',
            requires=[{'expected': [{'option': a, 'value': True}, {'option': b, 'value': 1}], 'action': 'disabled', 'operator': 'and'}])
    od = OptionDescription('service', '', [a, b, c])
    api = Config(od)
    api.property.read_write()

    api.option('ip_address_service').value.get()

    api.option('activate_service').value.set(True)
    props = []
    try:
        api.option('ip_address_service').value.get()
    except PropertiesOptionError as err:
        props = err.proptype
    assert props == []

    api.option('activate_service').value.set(False)
    api.option('ip_address_service').value.get()

    api.option('num_service').value.set(1)
    props = []
    try:
        api.option('ip_address_service').value.get()
    except PropertiesOptionError as err:
        props = err.proptype
    assert props == []

    api.option('activate_service').value.set(True)
    props = []
    try:
        api.option('ip_address_service').value.get()
    except PropertiesOptionError as err:
        props = err.proptype
    assert frozenset(props) == frozenset(['disabled'])


def test_requires_multi_disabled_new_format_and_2():
    a = BoolOption('activate_service', '')
    b = IntOption('num_service', '')
    c = IPOption('ip_address_service', '',
            requires=[{'expected': [{'option': a, 'value': True}, {'option': b, 'value': 1}], 'action': 'disabled', 'operator': 'and'},
                      {'expected': [{'option': a, 'value': False}, {'option': b, 'value': 1}], 'action': 'expert'}])
    od = OptionDescription('service', '', [a, b, c])
    api = Config(od)
    api.property.add('expert')
    api.property.read_write()
    api.option('ip_address_service').value.get()

    api.option('activate_service').value.set(True)
    props = []
    try:
        api.option('ip_address_service').value.get()
    except PropertiesOptionError as err:
        props = err.proptype
    assert props == []

    api.option('activate_service').value.set(False)
    api.option('num_service').value.set(1)
    props = []
    try:
        api.option('ip_address_service').value.get()
    except PropertiesOptionError as err:
        props = err.proptype
    assert frozenset(props) == frozenset(['expert'])

    api.option('activate_service').value.set(True)
    props = []
    try:
        api.option('ip_address_service').value.get()
    except PropertiesOptionError as err:
        props = err.proptype
    assert frozenset(props) == frozenset(['disabled', 'expert'])


def test_requires_multi_disabled_inverse():
    a = BoolOption('activate_service', '')
    b = IntOption('num_service', '')
    c = IPOption('ip_address_service', '',
                 requires=[{'option': a, 'expected': True,
                            'action': 'disabled', 'inverse': True},
                           {'option': b, 'expected': 1,
                            'action': 'disabled', 'inverse': True}])
    od = OptionDescription('service', '', [a, b, c])
    api = Config(od)
    api.property.read_write()

    props = []
    try:
        api.option('ip_address_service').value.get()
    except PropertiesOptionError as err:
        props = err.proptype
    assert frozenset(props) == frozenset(['disabled'])

    api.option('activate_service').value.set(True)
    props = []
    try:
        api.option('ip_address_service').value.get()
    except PropertiesOptionError as err:
        props = err.proptype
    assert frozenset(props) == frozenset(['disabled'])

    api.option('activate_service').value.set(False)
    props = []
    try:
        api.option('ip_address_service').value.get()
    except PropertiesOptionError as err:
        props = err.proptype
    assert frozenset(props) == frozenset(['disabled'])

    api.option('num_service').value.set(1)
    props = []
    try:
        api.option('ip_address_service').value.get()
    except PropertiesOptionError as err:
        props = err.proptype
    assert frozenset(props) == frozenset(['disabled'])

    api.option('activate_service').value.set(True)
    api.option('ip_address_service').value.get()


def test_requires_multi_disabled_2():
    a = BoolOption('a', '')
    b = BoolOption('b', '')
    c = BoolOption('c', '')
    d = BoolOption('d', '')
    e = BoolOption('e', '')
    f = BoolOption('f', '')
    g = BoolOption('g', '')
    h = BoolOption('h', '')
    i = BoolOption('i', '')
    j = BoolOption('j', '')
    k = BoolOption('k', '')
    l = BoolOption('l', '')
    m = BoolOption('m', '')
    list_bools = [a, b, c, d, e, f, g, h, i, j, k, l, m]
    requires = []
    for boo in list_bools:
        requires.append({'option': boo, 'expected': True, 'action': 'disabled'})
    z = IPOption('z', '', requires=requires)
    y = copy(list_bools)
    y.append(z)
    od = OptionDescription('service', '', y)
    api = Config(od)
    api.property.read_write()

    api.option('z').value.get()
    for boo in list_bools:
        api.option(boo.impl_getname()).value.set(True)
        props = []
        try:
            api.option('z').value.get()
        except PropertiesOptionError as err:
            props = err.proptype
        assert frozenset(props) == frozenset(['disabled'])
    for boo in list_bools:
        api.option(boo.impl_getname()).value.set(False)
        if boo == m:
            api.option('z').value.get()
        else:
            props = []
            try:
                api.option('z').value.get()
            except PropertiesOptionError as err:
                props = err.proptype
            assert frozenset(props) == frozenset(['disabled'])


def test_requires_multi_disabled_inverse_2():
    a = BoolOption('a', '')
    b = BoolOption('b', '')
    c = BoolOption('c', '')
    d = BoolOption('d', '')
    e = BoolOption('e', '')
    f = BoolOption('f', '')
    g = BoolOption('g', '')
    h = BoolOption('h', '')
    i = BoolOption('i', '')
    j = BoolOption('j', '')
    k = BoolOption('k', '')
    l = BoolOption('l', '')
    m = BoolOption('m', '')
    list_bools = [a, b, c, d, e, f, g, h, i, j, k, l, m]
    requires = []
    for boo in list_bools:
        requires.append({'option': boo, 'expected': True, 'action': 'disabled',
                         'inverse': True})
    z = IPOption('z', '', requires=requires)
    y = copy(list_bools)
    y.append(z)
    od = OptionDescription('service', '', y)
    api = Config(od)
    api.property.read_write()

    props = []
    try:
        api.option('z').value.get()
    except PropertiesOptionError as err:
        props = err.proptype
    assert frozenset(props) == frozenset(['disabled'])
    for boo in list_bools:
        api.option(boo.impl_getname()).value.set(True)
        if boo == m:
            api.option('z').value.get()
        else:
            props = []
            try:
                api.option('z').value.get()
            except PropertiesOptionError as err:
                props = err.proptype
            assert frozenset(props) == frozenset(['disabled'])
    for boo in list_bools:
        api.option(boo.impl_getname()).value.set(False)
        props = []
        try:
            api.option('z').value.get()
        except PropertiesOptionError as err:
            props = err.proptype
        assert frozenset(props) == frozenset(['disabled'])


def test_requires_requirement_append():
    a = BoolOption('activate_service', '', True)
    b = IPOption('ip_address_service', '',
                 requires=[{'option': a, 'expected': False, 'action': 'disabled'}])
    od = OptionDescription('service', '', [a, b])
    api = Config(od)
    api.property.read_write()
    api.property.get()
    api.option('ip_address_service').property.get()
    raises(ValueError, "api.option('ip_address_service').property.add('disabled')")
    api.option('activate_service').value.set(False)
    # disabled is now set, test to remove disabled before store in storage
    api.unrestraint.option('ip_address_service').property.add("test")


def test_requires_different_inverse():
    a = BoolOption('activate_service', '', True)
    b = IPOption('ip_address_service', '', requires=[
        {'option': a, 'expected': True, 'action': 'disabled', 'inverse': True},
        {'option': a, 'expected': True, 'action': 'disabled', 'inverse': False}])
    od = OptionDescription('service', '', [a, b])
    api = Config(od)
    api.property.read_write()
    raises(PropertiesOptionError, "api.option('ip_address_service').value.get()")
    api.option('activate_service').value.set(False)
    raises(PropertiesOptionError, "api.option('ip_address_service').value.get()")


def test_requires_different_inverse_unicode():
    a = BoolOption('activate_service', '', True)
    d = StrOption('activate_other_service', '', 'val2')
    b = IPOption('ip_address_service', '', requires=[
        {'option': a, 'expected': True, 'action': 'disabled', 'inverse': True},
        {'option': d, 'expected': 'val1', 'action': 'disabled', 'inverse': False}])
    od = OptionDescription('service', '', [a, d, b])
    api = Config(od)
    api.property.read_write()
    assert api.option('ip_address_service').value.get() == None
    api.option('activate_service').value.set(False)
    raises(PropertiesOptionError, "api.option('ip_address_service').value.get()")
    api.option('activate_service').value.set(True)
    assert api.option('ip_address_service').value.get() == None
    api.option('activate_other_service').value.set('val1')
    raises(PropertiesOptionError, "api.option('ip_address_service').value.get()")
    api.option('activate_service').value.set(False)
    raises(PropertiesOptionError, "api.option('ip_address_service').value.get()")


def test_requires_recursive_path():
    a = BoolOption('activate_service', '', True)
    b = IPOption('ip_address_service', '',
                 requires=[{'option': a, 'expected': False, 'action': 'disabled'}])
    od1 = OptionDescription('service', '', [a, b], requires=[{'option': a, 'expected': False, 'action': 'disabled'}])
    od = OptionDescription('base', '', [od1])
    api = Config(od)
    api.property.read_write()
    raises(RequirementError, "api.option('service.a').value.get()")


def test_optiondescription_requires():
    a = BoolOption('activate_service', '', True)
    b = BoolOption('ip_address_service', '', multi=True)
    a, b
    OptionDescription('service', '', [b], requires=[{'option': a, 'expected': False, 'action': 'disabled'}])


def test_optiondescription_requires_multi():
    a = BoolOption('activate_service', '', True)
    b = IPOption('ip_address_service', '', multi=True)
    a, b
    raises(ValueError, "OptionDescription('service', '', [a], requires=[{'option': b, 'expected': False, 'action': 'disabled'}])")


def test_properties_conflict():
    a = BoolOption('activate_service', '', True)
    a
    raises(ValueError, "IPOption('ip_address_service', '', properties=('disabled',), requires=[{'option': a, 'expected': False, 'action': 'disabled'}])")
    raises(ValueError, "od1 = OptionDescription('service', '', [a], properties=('disabled',), requires=[{'option': a, 'expected': False, 'action': 'disabled'}])")


def test_leadership_requires():
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé", multi=True)
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "masque du sous-réseau", multi=True,
                                   requires=[{'option': ip_admin_eth0, 'expected': '192.168.1.1', 'action': 'disabled'}])
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    maconfig = OptionDescription('toto', '', [interface1])
    api = Config(maconfig)
    api.property.read_write()
    assert api.option('ip_admin_eth0.ip_admin_eth0').value.get() == []
    api.option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.1.2'])
    assert api.option('ip_admin_eth0.netmask_admin_eth0', 0).value.get() is None
    assert api.option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.2']
    #
    api.option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.1.2', '192.168.1.1'])
    assert api.option('ip_admin_eth0.netmask_admin_eth0', 0).value.get() is None
    raises(PropertiesOptionError, "api.option('ip_admin_eth0.netmask_admin_eth0', 1).value.get()")
    #
    api.option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.1.2', '192.168.1.2'])
    assert api.option('ip_admin_eth0.netmask_admin_eth0', 0).value.get() is None
    assert api.option('ip_admin_eth0.netmask_admin_eth0', 1).value.get() is None
    api.option('ip_admin_eth0.netmask_admin_eth0', 1).value.set('255.255.255.255')
    assert api.option('ip_admin_eth0.netmask_admin_eth0', 1).value.get() == '255.255.255.255'
    assert api.value.dict() == {'ip_admin_eth0.ip_admin_eth0': ['192.168.1.2', '192.168.1.2'],
                                'ip_admin_eth0.netmask_admin_eth0': [None, '255.255.255.255']}
    #
    api.option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.1.2', '192.168.1.1'])
    assert api.option('ip_admin_eth0.netmask_admin_eth0', 0).value.get() is None
    raises(PropertiesOptionError, "api.option('ip_admin_eth0.netmask_admin_eth0', 1).value.get()")
    ret = api.value.dict()
    assert set(ret.keys()) == set(['ip_admin_eth0.ip_admin_eth0', 'ip_admin_eth0.netmask_admin_eth0'])
    assert ret['ip_admin_eth0.ip_admin_eth0'] == ['192.168.1.2', '192.168.1.1']
    assert len(ret['ip_admin_eth0.netmask_admin_eth0']) == 2
    assert ret['ip_admin_eth0.netmask_admin_eth0'][0] is None
    assert isinstance(ret['ip_admin_eth0.netmask_admin_eth0'][1], PropertiesOptionError)
    del ret['ip_admin_eth0.netmask_admin_eth0'][1]
    del ret['ip_admin_eth0.netmask_admin_eth0'][0]
    del ret['ip_admin_eth0.netmask_admin_eth0']
    #
    api.option('ip_admin_eth0.netmask_admin_eth0', 0).value.set('255.255.255.255')
    ret = api.value.dict()
    assert set(ret.keys()) == set(['ip_admin_eth0.ip_admin_eth0', 'ip_admin_eth0.netmask_admin_eth0'])
    assert ret['ip_admin_eth0.ip_admin_eth0'] == ['192.168.1.2', '192.168.1.1']
    assert len(ret['ip_admin_eth0.netmask_admin_eth0']) == 2
    assert ret['ip_admin_eth0.netmask_admin_eth0'][0] == '255.255.255.255'
    assert isinstance(ret['ip_admin_eth0.netmask_admin_eth0'][1], PropertiesOptionError)
    del ret['ip_admin_eth0.netmask_admin_eth0'][1]
    del ret['ip_admin_eth0.netmask_admin_eth0'][0]
    del ret['ip_admin_eth0.netmask_admin_eth0']


def test_leadership_requires_both():
    ip_admin = StrOption('ip_admin_eth0', "ip réseau autorisé")
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé", multi=True,
                              requires=[{'option': ip_admin, 'expected': '192.168.1.1', 'action': 'disabled'}])
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "masque du sous-réseau", multi=True)
    raises(RequirementError, "Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0], requires=[{'option': ip_admin, 'expected': '192.168.1.1', 'action': 'disabled'}])")


def test_leadership_requires_properties_invalid():
    ip_admin = StrOption('ip_admin', "ip réseau autorisé")
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé", multi=True)
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "masque du sous-réseau", multi=True,
                                   requires=[{'option': ip_admin_eth0, 'expected': '192.168.1.1', 'action': 'disabled'}])
    raises(ValueError, "Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0], properties=('disabled',), requires=[{'option': ip_admin, 'expected': '192.168.1.1', 'action': 'disabled'}])")


def test_leadership_requires_properties_invalid_2():
    ip_admin = StrOption('ip_admin', "ip réseau autorisé")
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé", multi=True,
                              requires=[{'option': ip_admin, 'expected': '192.168.1.1', 'action': 'disabled'}])
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "masque du sous-réseau", multi=True,
                                   requires=[{'option': ip_admin_eth0, 'expected': '192.168.1.1', 'action': 'disabled'}])
    raises(ValueError, "Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0], properties=('disabled',))")


def test_leadership_requires_properties():
    ip_admin = StrOption('ip_admin', "ip réseau autorisé")
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé", multi=True)
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "masque du sous-réseau", multi=True,
                                   requires=[{'option': ip_admin_eth0, 'expected': '192.168.1.1', 'action': 'disabled'}])
    Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0], properties=('hidden',),
                 requires=[{'option': ip_admin, 'expected': '192.168.1.1', 'action': 'disabled'}])


def test_leadership_requires_leader():
    activate = BoolOption('activate', "Activer l'accès au réseau", True)
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé", multi=True,
                              requires=[{'option': activate, 'expected': False, 'action': 'disabled'}])
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "masque du sous-réseau", multi=True)
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    maconfig = OptionDescription('toto', '', [activate, interface1])
    api = Config(maconfig)
    api.property.read_write()
    #
    assert api.option('ip_admin_eth0.ip_admin_eth0').value.get() == []
    api.option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.1.2'])
    assert api.option('ip_admin_eth0.netmask_admin_eth0', 0).value.get() is None
    assert api.option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.2']
    #
    api.option('activate').value.set(False)
    raises(PropertiesOptionError, "api.option('ip_admin_eth0.ip_admin_eth0').value.get()")
    raises(PropertiesOptionError, "api.option('ip_admin_eth0.netmask_admin_eth0', 0).value.get()")
    #
    api.option('activate').value.set(True)
    assert api.option('ip_admin_eth0.netmask_admin_eth0', 0).value.get() is None
    #
    api.option('activate').value.set(False)
    raises(PropertiesOptionError, "api.option('ip_admin_eth0.ip_admin_eth0').value.get()")
    raises(PropertiesOptionError, "api.option('ip_admin_eth0.netmask_admin_eth0', 0).value.get()")
    assert api.value.dict() == {'activate': False}


def test_leadership_requires_leadership():
    activate = BoolOption('activate', "Activer l'accès au réseau", True)
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé", multi=True)
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "masque du sous-réseau", multi=True)
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0],
                              requires=[{'option': activate, 'expected': False, 'action': 'disabled'}])
    maconfig = OptionDescription('toto', '', [activate, interface1])
    api = Config(maconfig)
    api.property.read_write()
    #
    assert api.option('ip_admin_eth0.ip_admin_eth0').value.get() == []
    api.option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.1.2'])
    assert api.option('ip_admin_eth0.netmask_admin_eth0', 0).value.get() is None
    assert api.option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.2']
    #
    api.option('activate').value.set(False)
    raises(PropertiesOptionError, "api.option('ip_admin_eth0.ip_admin_eth0').value.get()")
    raises(PropertiesOptionError, "api.option('ip_admin_eth0.netmask_admin_eth0', 0).value.get()")
    #
    api.option('activate').value.set(True)
    assert api.option('ip_admin_eth0.netmask_admin_eth0', 0).value.get() is None
    #
    api.option('activate').value.set(False)
    raises(PropertiesOptionError, "api.option('ip_admin_eth0.ip_admin_eth0').value.get()")
    raises(PropertiesOptionError, "api.option('ip_admin_eth0.netmask_admin_eth0', 0).value.get()")
    assert api.value.dict() == {'activate': False}


def test_leadership_requires_no_leader():
    activate = BoolOption('activate', "Activer l'accès au réseau", True)
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé", multi=True)
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "masque du sous-réseau", multi=True,
                                   requires=[{'option': activate, 'expected': False, 'action': 'disabled'}])
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    maconfig = OptionDescription('toto', '', [activate, interface1])
    api = Config(maconfig)
    api.property.read_write()
    assert api.option('ip_admin_eth0.ip_admin_eth0').value.get() == []
    api.option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.1.2'])
    assert api.option('ip_admin_eth0.netmask_admin_eth0', 0).value.get() is None
    assert api.option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.2']
    api.option('activate').value.set(False)
    api.option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.1.2', '192.168.1.1'])
    assert api.option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.2', '192.168.1.1']
    raises(PropertiesOptionError, "api.option('ip_admin_eth0.netmask_admin_eth0', 0).value.get()")
    raises(PropertiesOptionError, "api.option('ip_admin_eth0.netmask_admin_eth0', 1).value.get()")
    api.option('activate').value.set(True)
    assert api.option('ip_admin_eth0.netmask_admin_eth0', 0).value.get() is None
    assert api.option('ip_admin_eth0.netmask_admin_eth0', 1).value.get() is None
    api.option('ip_admin_eth0.netmask_admin_eth0', 1).value.set('255.255.255.255')
    assert api.option('ip_admin_eth0.netmask_admin_eth0', 1).value.get() == '255.255.255.255'
    api.option('activate').value.set(False)
    raises(PropertiesOptionError, "api.option('ip_admin_eth0.netmask_admin_eth0', 0).value.get()")
    raises(PropertiesOptionError, "api.option('ip_admin_eth0.netmask_admin_eth0', 1).value.get()")
    assert api.value.dict() == {'ip_admin_eth0.ip_admin_eth0': ['192.168.1.2', '192.168.1.1'], 'activate': False}


def test_leadership_requires_complet():
    optiontoto = StrOption('unicodetoto', "Unicode leader")
    option = StrOption('unicode', "Unicode leader", multi=True)
    option1 = StrOption('unicode1', "Unicode follower 1", multi=True)
    option2 = StrOption('unicode2', "Values 'test' must show 'Unicode follower 3'", multi=True)
    option3 = StrOption('unicode3', "Unicode follower 3", requires=[{'option': option,
                                                                     'expected': u'test',
                                                                     'action': 'hidden',
                                                                     'inverse': True}],
                        multi=True)
    option4 = StrOption('unicode4', "Unicode follower 4", requires=[{'option': option2,
                                                                     'expected': u'test',
                                                                     'action': 'hidden',
                                                                     'inverse': True}],
                        multi=True)
    option5 = StrOption('unicode5', "Unicode follower 5", requires=[{'option': optiontoto,
                                                                     'expected': u'test',
                                                                     'action': 'hidden',
                                                                     'inverse': True}],
                        multi=True)
    option6 = StrOption('unicode6', "Unicode follower 6", requires=[{'option': optiontoto,
                                                                     'expected': u'test',
                                                                     'action': 'hidden',
                                                                     'inverse': True},
                                                                    {'option': option2,
                                                                     'expected': u'test',
                                                                     'action': 'hidden',
                                                                     'inverse': True}],
                        multi=True)
    option7 = StrOption('unicode7', "Unicode follower 7", requires=[{'option': option2,
                                                                     'expected': u'test',
                                                                     'action': 'hidden',
                                                                     'inverse': True},
                                                                    {'option': optiontoto,
                                                                     'expected': u'test',
                                                                     'action': 'hidden',
                                                                     'inverse': True}],
                        multi=True)
    descr1 = Leadership("unicode", "Common configuration 1",
                        [option, option1, option2, option3, option4, option5, option6, option7])
    descr = OptionDescription("options", "Common configuration 2", [descr1, optiontoto])
    descr = OptionDescription("unicode1_leadership_requires", "Leader followers with Unicode follower 3 hidden when Unicode follower 2 is test", [descr])
    config = Config(descr)
    config.property.read_write()
    config.option('options.unicode.unicode').value.set(['test', 'trah'])
    config.option('options.unicode.unicode2', 0).value.set('test')
    dico = config.value.dict()
    assert dico.keys() == set(['options.unicode.unicode', 'options.unicode.unicode1', 'options.unicode.unicode2', 'options.unicode.unicode3', 'options.unicode.unicode4', 'options.unicodetoto'])
    assert dico['options.unicode.unicode'] == ['test', 'trah']
    assert dico['options.unicode.unicode1'] == [None, None]
    assert dico['options.unicode.unicode2'] == ['test', None]
    assert dico['options.unicode.unicode3'][0] is None
    assert isinstance(dico['options.unicode.unicode3'][1], PropertiesOptionError)
    assert dico['options.unicode.unicode4'][0] is None
    assert isinstance(dico['options.unicode.unicode4'][1], PropertiesOptionError)
    assert dico['options.unicodetoto'] is None
    del dico['options.unicode.unicode3'][1]
    del dico['options.unicode.unicode3']
    del dico['options.unicode.unicode4'][1]
    del dico['options.unicode.unicode4']
    #
    config.option('options.unicodetoto').value.set('test')
    dico = config.value.dict()
    assert dico.keys() == set(['options.unicode.unicode', 'options.unicode.unicode1', 'options.unicode.unicode2', 'options.unicode.unicode3', 'options.unicode.unicode4', 'options.unicode.unicode5', 'options.unicode.unicode6', 'options.unicode.unicode7', 'options.unicodetoto'])
    assert dico['options.unicode.unicode'] == ['test', 'trah']
    assert dico['options.unicode.unicode1'] == [None, None]
    assert dico['options.unicode.unicode2'] == ['test', None]
    assert dico['options.unicode.unicode3'][0] is None
    assert isinstance(dico['options.unicode.unicode3'][1], PropertiesOptionError)
    assert dico['options.unicode.unicode4'][0] is None
    assert isinstance(dico['options.unicode.unicode4'][1], PropertiesOptionError)
    assert dico['options.unicode.unicode5'] == [None, None]
    assert dico['options.unicode.unicode6'][0] is None
    assert isinstance(dico['options.unicode.unicode6'][1], PropertiesOptionError)
    assert dico['options.unicode.unicode7'][0] is None
    assert isinstance(dico['options.unicode.unicode7'][1], PropertiesOptionError)
    assert dico['options.unicodetoto'] == 'test'
    del dico['options.unicode.unicode3'][1]
    del dico['options.unicode.unicode3']
    del dico['options.unicode.unicode4'][1]
    del dico['options.unicode.unicode4']
    del dico['options.unicode.unicode6'][1]
    del dico['options.unicode.unicode6']
    del dico['options.unicode.unicode7'][1]
    del dico['options.unicode.unicode7']


def test_leadership_requires_transitive():
    optiontoto = StrOption('unicodetoto', "Unicode leader")
    option = StrOption('unicode', "Unicode leader", multi=True)
    option1 = StrOption('unicode1', "Unicode follower 1", multi=True)
    option2 = StrOption('unicode2', "Unicode follower 2", requires=[{'option': optiontoto,
                                                                     'expected': u'test',
                                                                     'action': 'disabled',
                                                                     'transitive': True,
                                                                     'inverse': True}],
                        multi=True)
    option3 = StrOption('unicode3', "Unicode follower 3", requires=[{'option': option2,
                                                                     'expected': u'test',
                                                                     'action': 'disabled',
                                                                     'transitive': True,
                                                                     'inverse': True}],
                        multi=True)
    option4 = StrOption('unicode4', "Unicode follower 4", requires=[{'option': option3,
                                                                     'expected': u'test',
                                                                     'action': 'disabled',
                                                                     'transitive': True,
                                                                     'inverse': True}],
                        multi=True)
    descr1 = Leadership("unicode", "Common configuration 1",
                        [option, option1, option2, option3, option4])
    descr = OptionDescription("options", "Common configuration 2", [descr1, optiontoto])
    descr = OptionDescription("unicode1", "", [descr])
    config = Config(descr)
    config.property.read_write()
    assert config.value.dict() == {'options.unicode.unicode': [], 'options.unicode.unicode1': [], 'options.unicode.unicode3': [], 'options.unicode.unicode4': [], 'options.unicodetoto': None}
    #
    config.option('options.unicodetoto').value.set('test')
    assert config.value.dict() == {'options.unicode.unicode': [], 'options.unicode.unicode1': [], 'options.unicode.unicode2': [], 'options.unicode.unicode3': [], 'options.unicode.unicode4': [], 'options.unicodetoto': 'test'}
    #
    config.option('options.unicode.unicode').value.set(['a', 'b', 'c'])
    dico = config.value.dict()
    assert list(dico.keys()) == ['options.unicode.unicode', 'options.unicode.unicode1', 'options.unicode.unicode2', 'options.unicode.unicode3', 'options.unicode.unicode4', 'options.unicodetoto']
    assert dico['options.unicodetoto'] == 'test'
    assert dico['options.unicode.unicode'] == ['a', 'b', 'c']
    assert dico['options.unicode.unicode1'] == [None, None, None]
    assert dico['options.unicode.unicode2'] == [None, None, None]
    assert isinstance(dico['options.unicode.unicode3'][0], PropertiesOptionError)
    assert isinstance(dico['options.unicode.unicode3'][1], PropertiesOptionError)
    assert isinstance(dico['options.unicode.unicode3'][2], PropertiesOptionError)
    assert isinstance(dico['options.unicode.unicode4'][0], PropertiesOptionError)
    assert isinstance(dico['options.unicode.unicode4'][1], PropertiesOptionError)
    assert isinstance(dico['options.unicode.unicode4'][2], PropertiesOptionError)
    del (dico['options.unicode.unicode3'][2])
    del (dico['options.unicode.unicode3'][1])
    del (dico['options.unicode.unicode3'][0])
    del (dico['options.unicode.unicode4'][2])
    del (dico['options.unicode.unicode4'][1])
    del (dico['options.unicode.unicode4'][0])
    #
    config.option('options.unicode.unicode2', 1).value.set('test')
    config.option('options.unicode.unicode3', 1).value.set('test')
    dico = config.value.dict()
    assert list(dico.keys()) == ['options.unicode.unicode', 'options.unicode.unicode1', 'options.unicode.unicode2', 'options.unicode.unicode3', 'options.unicode.unicode4', 'options.unicodetoto']
    assert dico['options.unicodetoto'] == 'test'
    assert dico['options.unicode.unicode'] == ['a', 'b', 'c']
    assert dico['options.unicode.unicode1'] == [None, None, None]
    assert dico['options.unicode.unicode2'] == [None, 'test', None]
    assert isinstance(dico['options.unicode.unicode3'][0], PropertiesOptionError)
    assert dico['options.unicode.unicode3'][1] == 'test'
    assert isinstance(dico['options.unicode.unicode3'][2], PropertiesOptionError)
    assert isinstance(dico['options.unicode.unicode4'][0], PropertiesOptionError)
    assert dico['options.unicode.unicode4'][1] == None
    assert isinstance(dico['options.unicode.unicode4'][2], PropertiesOptionError)
    del (dico['options.unicode.unicode3'][2])
    del (dico['options.unicode.unicode3'][1])
    del (dico['options.unicode.unicode3'][0])
    del (dico['options.unicode.unicode4'][2])
    del (dico['options.unicode.unicode4'][1])
    del (dico['options.unicode.unicode4'][0])
    #
    config.option('options.unicode.unicode2', 1).value.set('rah')
    dico = config.value.dict()
    assert list(dico.keys()) == ['options.unicode.unicode', 'options.unicode.unicode1', 'options.unicode.unicode2', 'options.unicode.unicode3', 'options.unicode.unicode4', 'options.unicodetoto']
    assert dico['options.unicodetoto'] == 'test'
    assert dico['options.unicode.unicode'] == ['a', 'b', 'c']
    assert dico['options.unicode.unicode1'] == [None, None, None]
    assert dico['options.unicode.unicode2'] == [None, 'rah', None]
    assert isinstance(dico['options.unicode.unicode3'][0], PropertiesOptionError)
    assert isinstance(dico['options.unicode.unicode3'][1], PropertiesOptionError)
    assert isinstance(dico['options.unicode.unicode3'][2], PropertiesOptionError)
    assert isinstance(dico['options.unicode.unicode4'][0], PropertiesOptionError)
    assert isinstance(dico['options.unicode.unicode4'][1], PropertiesOptionError)
    assert isinstance(dico['options.unicode.unicode4'][2], PropertiesOptionError)
    del (dico['options.unicode.unicode3'][2])
    del (dico['options.unicode.unicode3'][1])
    del (dico['options.unicode.unicode3'][0])
    del (dico['options.unicode.unicode4'][2])
    del (dico['options.unicode.unicode4'][1])
    del (dico['options.unicode.unicode4'][0])
    #
    config.option('options.unicode.unicode2', 1).value.set('test')
    config.option('options.unicodetoto').value.set('rah')
    dico = config.value.dict()
    assert list(dico.keys()) == ['options.unicode.unicode', 'options.unicode.unicode1', 'options.unicode.unicode3', 'options.unicode.unicode4', 'options.unicodetoto']
    assert dico['options.unicodetoto'] == 'rah'
    assert dico['options.unicode.unicode'] == ['a', 'b', 'c']
    assert dico['options.unicode.unicode1'] == [None, None, None]
    assert isinstance(dico['options.unicode.unicode3'][0], PropertiesOptionError)
    assert isinstance(dico['options.unicode.unicode3'][1], PropertiesOptionError)
    assert isinstance(dico['options.unicode.unicode3'][2], PropertiesOptionError)
    assert isinstance(dico['options.unicode.unicode4'][0], PropertiesOptionError)
    assert isinstance(dico['options.unicode.unicode4'][1], PropertiesOptionError)
    assert isinstance(dico['options.unicode.unicode4'][2], PropertiesOptionError)
    del (dico['options.unicode.unicode3'][2])
    del (dico['options.unicode.unicode3'][1])
    del (dico['options.unicode.unicode3'][0])
    del (dico['options.unicode.unicode4'][2])
    del (dico['options.unicode.unicode4'][1])
    del (dico['options.unicode.unicode4'][0])
