#this test is much more to test that **it's there** and answers attribute access
from .autopath import do_autopath
do_autopath()

from py.test import raises

from tiramisu import BoolOption, OptionDescription, ChoiceOption,\
    IntOption, FloatOption, StrOption, Config
from tiramisu.storage import list_sessions


def teardown_function(function):
    assert list_sessions() == [], 'session list is not empty when leaving "{}"'.format(function.__name__)


def make_description():
    gcoption = ChoiceOption('name', 'GC name', ['ref', 'framework'], 'ref')
    gcdummy = BoolOption('dummy', 'dummy', default=False)
    gcdummy2 = BoolOption('hide', 'dummy', default=True)
    objspaceoption = ChoiceOption('objspace', 'Object space',
                                  ['std', 'thunk'], 'std')
    booloption = BoolOption('bool', 'Test boolean option', default=True)
    intoption = IntOption('int', 'Test int option', default=0)
    floatoption = FloatOption('float', 'Test float option', default=2.3)
    stroption = StrOption('str', 'Test string option', default="abc")
    boolop = BoolOption('boolop', 'Test boolean option op', default=True)
    wantref_option = BoolOption('wantref', 'Test requires', default=False)
    wantframework_option = BoolOption('wantframework', 'Test requires',
                                      default=False)

    gcgroup = OptionDescription('gc', '', [gcoption, gcdummy, floatoption, gcdummy2])
    descr = OptionDescription('tiram', '', [gcgroup, booloption, objspaceoption,
                                            wantref_option, stroption,
                                            wantframework_option,
                                            intoption, boolop])
    return descr


def test_root_config_answers_ok():
    "if you hide the root config, the options in this namespace behave normally"
    gcdummy = BoolOption('dummy', 'dummy', default=False)
    boolop = BoolOption('boolop', 'Test boolean option op', default=True)
    descr = OptionDescription('tiramisu', '', [gcdummy, boolop])
    api = Config(descr)
    #settings = cfg.cfgimpl_get_settings()
    #settings.append('hidden')

    assert api.option('dummy').value.get() is False
    assert api.option('boolop').value.get() is True


#def test_optname_shall_not_start_with_numbers():
#    raises(ValueError, "gcdummy = BoolOption('123dummy', 'dummy', default=False)")
#    raises(ValueError, "descr = OptionDescription('123tiramisu', '', [])")
#
#
def test_option_has_an_api_name():
    b = BoolOption('impl_has_dependency', 'dummy', default=True)
    descr = OptionDescription('tiramisu', '', [b])
    api = Config(descr)
    assert api.option('impl_has_dependency').value.get() is True
    assert b.impl_has_dependency() is False
