"configuration objects global API"
from .autopath import do_autopath
do_autopath()

from py.test import raises

from tiramisu.option import UsernameOption
from tiramisu.storage import list_sessions


def teardown_function(function):
    assert list_sessions() == [], 'session list is not empty when leaving "{}"'.format(function.__name__)


def test_username():
    UsernameOption('a', '', 'string')
    UsernameOption('a', '', '_string')
    UsernameOption('a', '', 's_tring')
    UsernameOption('a', '', 'string_')
    UsernameOption('a', '', 'string$')
    UsernameOption('a', '', '_string$')
    raises(ValueError, "UsernameOption('a', '', 'strin$g')")
    UsernameOption('a', '', 's-tring')
    raises(ValueError, "UsernameOption('a', '', '-string')")
    UsernameOption('a', '', 's9tring')
    raises(ValueError, "UsernameOption('a', '', '9string')")
    raises(ValueError, "UsernameOption('a', '', '')")
    UsernameOption('a', '', 's')
    UsernameOption('a', '', 's2345678901234567890123456789012')
    raises(ValueError, "UsernameOption('a', '', 's23456789012345678901234567890123')")
    UsernameOption('a', '', 's234567890123456789012345678901$')
    raises(ValueError, "UsernameOption('a', '', 's2345678901234567890123456789012$')")
