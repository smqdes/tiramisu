# coding: utf-8
from .autopath import do_autopath
do_autopath()
from py.test import raises

from tiramisu.setting import groups
from tiramisu import Config, MetaConfig
from tiramisu import ChoiceOption, BoolOption, IntOption, \
    StrOption, OptionDescription
from .test_state import _diff_opts, _diff_conf
from tiramisu.storage import list_sessions


def teardown_function(function):
    assert list_sessions() == [], 'session list is not empty when leaving "{}"'.format(function.__name__)


def make_description():
    numero_etab = StrOption('numero_etab', "identifiant de l'établissement")
    nom_machine = StrOption('nom_machine', "nom de la machine", default="eoleng")
    nombre_interfaces = IntOption('nombre_interfaces', "nombre d'interfaces à activer",
                                  default=1)
    activer_proxy_client = BoolOption('activer_proxy_client', "utiliser un proxy",
                                      default=False)
    mode_conteneur_actif = BoolOption('mode_conteneur_actif', "le serveur est en mode conteneur",
                                      default=False)
    mode_conteneur_actif2 = BoolOption('mode_conteneur_actif2', "le serveur est en mode conteneur2",
                                       default=False, properties=('hidden',))

    adresse_serveur_ntp = StrOption('serveur_ntp', "adresse serveur ntp", multi=True)
    time_zone = ChoiceOption('time_zone', 'fuseau horaire du serveur',
                             ('Paris', 'Londres'), 'Paris')
    wantref_option = BoolOption('wantref', 'Test requires', default=False, properties=('force_store_value',))

    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé")
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "masque du sous-réseau")

    leader = OptionDescription('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    interface1 = OptionDescription('interface1', '', [leader])
    interface1.impl_set_group_type(groups.family)

    general = OptionDescription('general', '', [numero_etab, nom_machine,
                                nombre_interfaces, activer_proxy_client,
                                mode_conteneur_actif, mode_conteneur_actif2,
                                adresse_serveur_ntp, time_zone, wantref_option])
    general.impl_set_group_type(groups.family)
    new = OptionDescription('new', '', [], properties=('hidden',))
    new.impl_set_group_type(groups.family)
    creole = OptionDescription('creole', 'first tiramisu configuration', [general, interface1, new])
    descr = OptionDescription('baseconfig', 'baseconifgdescr', [creole])
    return descr


def test_copy():
    cfg = Config(make_description())
    ncfg = cfg.config.copy()
    assert cfg.option('creole.general.numero_etab').value.get() == None
    cfg.option('creole.general.numero_etab').value.set('oui')
    assert cfg.option('creole.general.numero_etab').value.get() == 'oui'
    assert ncfg.option('creole.general.numero_etab').value.get() == None
#    _diff_opts(cfg.cfgimpl_get_description(), ncfg.cfgimpl_get_description())
#    _diff_conf(cfg, ncfg)
#    cfg.creole.general.numero_etab = 'oui'
#    raises(AssertionError, "_diff_conf(cfg, ncfg)")
#    ncfg.creole.general.numero_etab = 'oui'
#    _diff_conf(cfg, ncfg)
def to_tuple(val):
    return tuple([tuple(v) for v in val])


def test_copy_force_store_value():
    descr = make_description()
    conf = Config(descr)
    conf2 = Config(descr)
    assert to_tuple(conf.value.exportation()) == ((), (), (), ())
    assert to_tuple(conf2.value.exportation()) == ((), (), (), ())
    #
    conf.property.read_write()
    assert to_tuple(conf.value.exportation()) == (('creole.general.wantref',), (None,), (False,), ('forced',))
    assert to_tuple(conf2.value.exportation()) == ((), (), (), ())
    #
    conf2.property.read_only()
    assert to_tuple(conf.value.exportation()) == (('creole.general.wantref',), (None,), (False,), ('forced',))
    assert to_tuple(conf2.value.exportation()) == (('creole.general.wantref',), (None,), (False,), ('forced',))
    #
    conf.option('creole.general.wantref').value.set(True)
    assert to_tuple(conf.value.exportation()) == (('creole.general.wantref',), (None,), (True,), ('user',))
    assert to_tuple(conf2.value.exportation()) == (('creole.general.wantref',), (None,), (False,), ('forced',))


def test_copy_force_store_value_metaconfig():
    descr = make_description()
    meta = MetaConfig([], optiondescription=descr)
    conf = meta.config.new(session_id='conf')
    assert meta.property.get() == conf.property.get()
    assert meta.permissive.get() == conf.permissive.get()
    conf.property.read_write()
    assert to_tuple(conf.value.exportation()) == (('creole.general.wantref',), (None,), (False,), ('forced',))
    assert to_tuple(meta.value.exportation()) == ((), (), (), ())
