from .autopath import do_autopath
do_autopath()

from py.test import raises

from tiramisu import Config
from tiramisu.config import KernelConfig
from tiramisu.setting import groups, owners
from tiramisu import ChoiceOption, BoolOption, IntOption, FloatOption, \
    StrOption, OptionDescription, SymLinkOption, IPOption, NetmaskOption, Leadership, \
    undefined, Params, ParamOption, ParamValue, ParamContext
from tiramisu.api import TIRAMISU_VERSION
from tiramisu.error import PropertiesOptionError, ConflictError, LeadershipError, ConfigError
from tiramisu.i18n import _
from tiramisu.storage import list_sessions


def teardown_function(function):
    assert list_sessions() == [], 'session list is not empty when leaving "{}"'.format(function.__name__)


def return_val():
    return 'val'


def return_concat(*args):
    return '.'.join(list(args))


def return_list(value=None):
    return ['val', 'val']


def return_list2(*args):
    l = []
    for arg in args:
        if isinstance(arg, list):
            l.extend(arg)
        else:
            l.append(arg)
    return l


def return_value(value=None):
    return value


def return_value2(*args, **kwargs):
    value = list(args)
    value.extend(kwargs.values())
    return value


def return_value3(value=None, index=None):
    if index is not None and isinstance(value, list):
        if len(value) > index:
            return value[index]
        return None
    return value


def return_index(val1, val2=None, index=None, self=None):
    if index is None:
        return [val1, val2]
    if index == 0:
        return val1
    if index == 1:
        return val2

def return_calc(i, j, k):
    return i + j + k


def is_config(config, **kwargs):
    if isinstance(config, KernelConfig):
        return 'yes'
    else:
        return 'no'


def ret_from_config(config):
    api = Config(config)
    return api.option('val1').value.get()


def return_raise(*arg):
    raise Exception('test')


def return_valueerror(*arg):
    raise ValueError('test')


def make_description_duplicates():
    gcoption = ChoiceOption('name', 'GC name', ('ref', 'framework'), 'ref')
    ## dummy 1
    gcdummy = BoolOption('dummy', 'dummy', default=False)
    objspaceoption = ChoiceOption('objspace', 'Object space',
                                  ('std', 'thunk'), 'std')
    booloption = BoolOption('bool', 'Test boolean option', default=True)
    intoption = IntOption('int', 'Test int option', default=0)
    floatoption = FloatOption('float', 'Test float option', default=2.3)
    stroption = StrOption('str', 'Test string option', default="abc")
    boolop = BoolOption('boolop', 'Test boolean option op', default=True)
    wantref_option = BoolOption('wantref', 'Test requires', default=False,
                                requires=({'option': boolop, 'expected': True, 'action': 'hidden'},))
    wantframework_option = BoolOption('wantframework', 'Test requires',
                                      default=False,
                                      requires=({'option': boolop, 'expected': True, 'action': 'hidden'},))
    # dummy2 (same path)
    gcdummy2 = BoolOption('dummy', 'dummy2', default=True)
    # dummy3 (same name)
    gcdummy3 = BoolOption('dummy', 'dummy2', default=True)
    gcgroup = OptionDescription('gc', '', [gcoption, gcdummy, gcdummy2, floatoption])
    descr = OptionDescription('constraints', '', [gcgroup, booloption, objspaceoption,
                              wantref_option, stroption,
                              wantframework_option,
                              intoption, boolop, gcdummy3])
    return descr


def test_identical_paths():
    """If in the schema (the option description) there is something that
    have the same name, an exection is raised
    """
    raises(ConflictError, "make_description_duplicates()")


def test_hidden_if_in():
    intoption = IntOption('int', 'Test int option', default=0)
    stroption = StrOption('str', 'Test string option', default="abc",
                          requires=({'option': intoption, 'expected': 1, 'action': 'hidden'},))
    descr = OptionDescription('constraints', '', [stroption, intoption])
    api = Config(descr)
    api.property.read_write()
    assert not 'hidden' in api.option('str').property.get()
    api.option('int').value.set(1)
    raises(PropertiesOptionError, "api.option('str').value.get()")
    raises(PropertiesOptionError, "api.option('str').value.set('uvw')")
    assert 'hidden' in api.unrestraint.option('str').property.get()


def test_hidden_if_in_with_group():
    gcoption = ChoiceOption('name', 'GC name', ('ref', 'framework'), 'ref')
    gcdummy = BoolOption('dummy', 'dummy', default=False)

    floatoption = FloatOption('float', 'Test float option', default=2.3)

    objspaceoption = ChoiceOption('objspace', 'Object space',
                                  ('std', 'thunk'), 'std')
    booloption = BoolOption('bool', 'Test boolean option', default=True)
    intoption = IntOption('int', 'Test int option', default=0)
    stroption = StrOption('str', 'Test string option', default="abc")
    gcgroup = OptionDescription('gc', '', [gcoption, gcdummy, floatoption],
                                requires=({'option': intoption, 'expected': 1, 'action': 'hidden'},))
    descr = OptionDescription('constraints', '', [gcgroup, booloption,
                              objspaceoption, stroption, intoption])
    api = Config(descr)
    api.property.read_write()
    assert not 'hidden' in api.option('str').property.get()
    api.option('int').value.set(1)
    raises(PropertiesOptionError, "api.option('gc.name').value.get()")


def test_disabled_with_group():
    gcoption = ChoiceOption('name', 'GC name', ('ref', 'framework'), 'ref')
    gcdummy = BoolOption('dummy', 'dummy', default=False)

    floatoption = FloatOption('float', 'Test float option', default=2.3)

    objspaceoption = ChoiceOption('objspace', 'Object space',
                                  ('std', 'thunk'), 'std')
    booloption = BoolOption('bool', 'Test boolean option', default=True)
    intoption = IntOption('int', 'Test int option', default=0)
    stroption = StrOption('str', 'Test string option', default="abc")
    gcgroup = OptionDescription('gc', '', [gcoption, gcdummy, floatoption],
                                requires=({'option': intoption, 'expected': 1, 'action': 'disabled'},))
    descr = OptionDescription('constraints', '', [gcgroup, booloption,
                                                  objspaceoption, stroption, intoption])
    api = Config(descr)
    api.property.read_write()
    assert api.option('gc.name').value.get()
    api.option('int').value.set(1)
    raises(PropertiesOptionError, "api.option('gc.name').value.get()")
#____________________________________________________________


def make_description_callback():
    gcoption = ChoiceOption('name', 'GC name', ('ref', 'framework'), 'ref')
    gcdummy = BoolOption('dummy', 'dummy')
    objspaceoption = ChoiceOption('objspace', 'Object space',
                                  ('std', 'thunk'), 'std')
    booloption = BoolOption('bool', 'Test boolean option', default=True)
    intoption = IntOption('int', 'Test int option', default=0)
    floatoption = FloatOption('float', 'Test float option', default=2.3)
    stroption = StrOption('str', 'Test string option', default="abc")
    boolop = BoolOption('boolop', 'Test boolean option op', default=True)
    wantref_option = BoolOption('wantref', 'Test requires', default=False,
                                requires=({'option': boolop, 'expected': True, 'action': 'hidden'},))
    wantframework_option = BoolOption('wantframework', 'Test requires',
                                      default=False,
                                      requires=({'option': boolop, 'expected': True, 'action': 'hidden'},))
    gcgroup = OptionDescription('gc', '', [gcoption, gcdummy, floatoption])
    descr = OptionDescription('constraints', '', [gcgroup, booloption, objspaceoption,
                              wantref_option, stroption,
                              wantframework_option,
                              intoption, boolop])
    return descr


def test_has_callback():
    descr = make_description_callback()
    # here the owner is 'default'
    api = Config(descr)
    api.property.read_write()
    api.option('bool').value.set(False)
    # because dummy has a callback
    api.property.add('freeze')
    api.option('gc.dummy').property.add('frozen')
    raises(PropertiesOptionError, "api.option('gc.dummy').value.set(True)")


def test_freeze_and_has_callback():
    descr = make_description_callback()
    api = Config(descr)
    api.property.read_write()
    api.option('bool').value.set(False)
    api.property.add('freeze')
    api.option('gc.dummy').property.add('frozen')
    raises(PropertiesOptionError, "api.option('gc.dummy').value.set(True)")


def test_callback():
    val1 = StrOption('val1', "", callback=return_val)
    val2 = StrOption('val2', "")
    maconfig = OptionDescription('rootconfig', '', [val1, val2])
    api = Config(maconfig)
    api.property.read_write()
    assert api.option('val1').option.callbacks() != (None, None)
    assert api.option('val2').option.callbacks() == (None, None)
    assert api.option('val1').value.get() == 'val'
    api.option('val1').value.set('new-val')
    assert api.option('val1').value.get() == 'new-val'
    api.option('val1').value.reset()
    assert api.option('val1').value.get() == 'val'


def test_callback_params_without_callback():
    raises(ValueError, "StrOption('val2', '', callback_params=Params(ParamValue('yes')))")


def test_params():
    raises(ValueError, "Params([ParamContext()])")
    raises(ValueError, "Params('str')")
    raises(ValueError, "Params(('str',))")
    raises(ValueError, "Params(kwargs=[ParamContext()])")
    raises(ValueError, "Params(kwargs={'a': 'str'})")


def test_param_option():
    val1 = StrOption('val1', "")
    raises(ValueError, "ParamOption('str')")
    raises(ValueError, "ParamOption(val1, 'str')")


def test_callback_invalid():
    raises(AssertionError, 'val1 = StrOption("val1", "", callback="string")')
    raises(AssertionError, 'val1 = StrOption("val1", "", callback=return_val, callback_params="string")')
    val1 = StrOption('val1', "", 'val')
    val1
    raises(AssertionError, "StrOption('val2', '', callback=return_value, callback_params={'': 'string'})")
    raises(AssertionError, "StrOption('val4', '', callback=return_value, callback_params={'value': (('string', False),)})")
    raises(AssertionError, "StrOption('val4', '', callback=return_value, callback_params={'value': ((val1, 'string'),)})")
    raises(AssertionError, "StrOption('val4', '', callback=return_value, callback_params={'value': ((val1, False, 'unknown'),)})")
    raises(AssertionError, "StrOption('val4', '', callback=return_value, callback_params={'value': ((val1,),)})")


def test_callback_with_context():
    context = ParamContext()
    value = ParamValue('string')
    params = Params((context,), {'value': value})
    val1 = StrOption("val1", "", callback=is_config, callback_params=params)
    maconfig = OptionDescription('rootconfig', '', [val1])
    api = Config(maconfig)
    assert api.option('val1').value.get() == 'yes'


def test_callback_with_context_named():
    context = ParamContext()
    params = Params(kwargs={'config': context})
    val1 = StrOption("val1", "", callback=is_config, callback_params=params)
    maconfig = OptionDescription('rootconfig', '', [val1])
    api = Config(maconfig)
    assert api.option('val1').value.get() == 'yes'


def test_callback_with_error():
    val1 = StrOption("val1", "", callback=is_config, callback_params=Params(ParamValue('string'), kwargs={'value': ParamValue('string')}))
    maconfig = OptionDescription('rootconfig', '', [val1])
    api = Config(maconfig)
    assert api.option('val1').value.get() == 'no'


def test_callback_with_context_value():
    context = ParamContext()
    params = Params((context,))
    val1 = StrOption("val1", "")
    val2 = StrOption("val2", "", callback=ret_from_config, callback_params=params)
    maconfig = OptionDescription('rootconfig', '', [val1, val2])
    api = Config(maconfig)
    api.option('val1').value.set('yes')
    assert api.option('val1').value.get() == 'yes'
    assert api.option('val2').value.get() == 'yes'
    api.option('val1').value.set('no')
    assert api.option('val1').value.get() == 'no'
    assert api.option('val2').value.get() == 'no'


def test_callback_value():
    val1 = StrOption('val1', "", 'val')
    val2 = StrOption('val2', "", callback=return_value, callback_params=Params(ParamOption(val1)))
    val3 = StrOption('val3', "", callback=return_value, callback_params=Params(ParamValue('yes')))
    val4 = StrOption('val4', "", callback=return_value, callback_params=Params(kwargs={'value': ParamOption(val1)}))
    val5 = StrOption('val5', "", callback=return_value, callback_params=Params(ParamValue('yes')))
    maconfig = OptionDescription('rootconfig', '', [val1, val2, val3, val4, val5])
    api = Config(maconfig)
    api.property.read_write()
    assert api.option('val1').value.get() == 'val'
    assert api.option('val2').value.get() == 'val'
    assert api.option('val4').value.get() == 'val'
    api.option('val1').value.set('new-val')
    assert api.option('val1').value.get() == 'new-val'
    assert api.option('val2').value.get() == 'new-val'
    assert api.option('val4').value.get() == 'new-val'
    api.option('val1').value.reset()
    assert api.option('val1').value.get() == 'val'
    assert api.option('val2').value.get() == 'val'
    assert api.option('val3').value.get() == 'yes'
    assert api.option('val4').value.get() == 'val'
    assert api.option('val5').value.get() == 'yes'


def test_callback_value_tuple():
    val1 = StrOption('val1', "", 'val1')
    val2 = StrOption('val2', "", 'val2')
    val3 = StrOption('val3', "", callback=return_concat, callback_params=Params((ParamOption(val1), ParamOption(val2))))
    val4 = StrOption('val4', "", callback=return_concat, callback_params=Params((ParamValue('yes'), ParamValue('no'))))
    maconfig = OptionDescription('rootconfig', '', [val1, val2, val3, val4])
    api = Config(maconfig)
    api.property.read_write()
    assert api.option('val1').value.get() == 'val1'
    assert api.option('val2').value.get() == 'val2'
    assert api.option('val3').value.get() == 'val1.val2'
    assert api.option('val4').value.get() == 'yes.no'
    api.option('val1').value.set('new-val')
    assert api.option('val3').value.get() == 'new-val.val2'
    api.option('val1').value.reset()
    assert api.option('val3').value.get() == 'val1.val2'


def test_callback_value_force_permissive():
    val1 = StrOption('val1', "", 'val', properties=('disabled',))
    val2 = StrOption('val2', "", callback=return_value, callback_params=Params(ParamOption(val1)))
    val3 = StrOption('val3', "", callback=return_value, callback_params=Params(ParamOption(val1, True)))
    maconfig = OptionDescription('rootconfig', '', [val1, val2, val3])
    api = Config(maconfig)
    api.property.read_only()
    raises(ConfigError, "api.option('val2').value.get()")
    api.option('val3').value.get() is None


def test_callback_value_force_permissive_kwargs():
    val1 = StrOption('val1', "", 'val', properties=('disabled',))
    val2 = StrOption('val2', "", callback=return_value, callback_params=Params(value=ParamOption(val1)))
    val3 = StrOption('val3', "", callback=return_value, callback_params=Params(value=ParamOption(val1, True)))
    maconfig = OptionDescription('rootconfig', '', [val1, val2, val3])
    api = Config(maconfig)
    api.property.read_only()
    raises(ConfigError, "api.option('val2').value.get()")
    api.option('val3').value.get() is None


def test_callback_symlink():
    val1 = StrOption('val1', "", 'val')
    val2 = SymLinkOption('val2', val1)
    val3 = StrOption('val3', "", callback=return_value, callback_params=Params(ParamOption(val2)))
    maconfig = OptionDescription('rootconfig', '', [val1, val2, val3])
    api = Config(maconfig)
    api.property.read_write()
    assert api.option('val1').value.get() == 'val'
    assert api.option('val2').value.get() == 'val'
    assert api.option('val3').value.get() == 'val'
    api.option('val1').value.set('new-val')
    assert api.option('val1').value.get() == 'new-val'
    assert api.option('val3').value.get() == 'new-val'
    api.option('val1').value.reset()
    assert api.option('val1').value.get() == 'val'
    assert api.option('val3').value.get() == 'val'


def test_callback_list():
    val1 = StrOption('val1', "", callback=return_list)
    maconfig = OptionDescription('rootconfig', '', [val1])
    api = Config(maconfig)
    api.property.read_write()
    raises(ValueError, "api.option('val1').value.get()")


def test_callback_list2():
    val1 = StrOption('val1', "", callback=return_list)
    #val2 = StrOption('val2', "", callback=return_value, callback_params=Params(ParamOption(val1)))
    val2 = StrOption('val2', "", callback=return_value, callback_params=Params(ParamOption(val1)))  # , 'forcepermissive': False}]})
    maconfig = OptionDescription('rootconfig', '', [val1, val2])
    api = Config(maconfig)
    api.property.read_write()
    raises(ValueError, "api.option('val1').value.get()")
    #cfg.val2
    raises(ValueError, "api.option('val2').value.get()")


def test_callback_multi():
    val1 = StrOption('val1', "", callback=return_val, multi=True)
    maconfig = OptionDescription('rootconfig', '', [val1])
    api = Config(maconfig)
    api.property.read_write()
    assert api.option('val1').value.get() == ['val']
    api.option('val1').value.set(['new-val'])
    assert api.option('val1').value.get() == ['new-val']
    api.option('val1').value.set(['new-val', 'new-val2'])
    assert api.option('val1').value.get() == ['new-val', 'new-val2']
    api.option('val1').value.reset()
    assert api.option('val1').value.get() == ['val']


def test_callback_multi_value():
    val1 = StrOption('val1', "", ['val'], multi=True)
    #val2 = StrOption('val2', "", multi=True, callback=return_value, callback_params=Params(ParamOption(val1)))
    #val3 = StrOption('val3', "", multi=True, callback=return_value, callback_params=Params(ParamValue('yes')))
    #val4 = StrOption('val4', "", multi=True, callback=return_list2, callback_params={'': ((val1, False), 'yes')})
    option = ParamOption(val1)
    params1 = Params((option,))
    value = ParamValue('yes')
    params2 = Params((value,))
    params3 = Params((option, value))
    val2 = StrOption('val2', "", multi=True, callback=return_value, callback_params=params1)
    val3 = StrOption('val3', "", multi=True, callback=return_value, callback_params=params2)
    val4 = StrOption('val4', "", multi=True, callback=return_list2, callback_params=params3)
    maconfig = OptionDescription('rootconfig', '', [val1, val2, val3, val4])
    api = Config(maconfig)
    api.property.read_write()
    assert api.option('val1').value.get() == ['val']
    assert api.option('val2').value.get() == ['val']
    assert api.option('val4').value.get() == ['val', 'yes']
    api.option('val1').value.set(['new-val'])
    assert api.option('val1').value.get() == ['new-val']
    assert api.option('val2').value.get() == ['new-val']
    assert api.option('val4').value.get() == ['new-val', 'yes']
    api.option('val1').value.set(['new-val', 'new-val2'])
    assert api.option('val1').value.get() == ['new-val', 'new-val2']
    assert api.option('val2').value.get() == ['new-val', 'new-val2']
    assert api.option('val4').value.get() == ['new-val', 'new-val2', 'yes']
    api.option('val1').value.reset()
    assert api.option('val1').value.get() == ['val']
    assert api.option('val2').value.get() == ['val']
    assert api.option('val3').value.get() == ['yes']
    assert api.option('val4').value.get() == ['val', 'yes']
    api.option('val2').value.set(['val', 'new'])
    assert api.option('val1').value.get() == ['val']
    assert api.option('val2').value.get() == ['val', 'new']


def test_callback_multi_list():
    val1 = StrOption('val1', "", callback=return_list, multi=True)
    maconfig = OptionDescription('rootconfig', '', [val1])
    api = Config(maconfig)
    api.property.read_write()
    assert api.option('val1').value.get() == ['val', 'val']
    api.option('val1').value.set(['new-val'])
    assert api.option('val1').value.get() == ['new-val']
    api.option('val1').value.set(['new-val', 'new-val2'])
    assert api.option('val1').value.get() == ['new-val', 'new-val2']
    api.option('val1').value.reset()
    assert api.option('val1').value.get() == ['val', 'val']


def test_callback_multi_list_extend():
    val1 = StrOption('val1', "", callback=return_list2, callback_params=Params((ParamValue(['1', '2', '3']), ParamValue(['4', '5']))), multi=True)
    maconfig = OptionDescription('rootconfig', '', [val1])
    api = Config(maconfig)
    api.property.read_write()
    assert api.option('val1').value.get() == ['1', '2', '3', '4', '5']


def test_callback_multi_callback():
    val1 = StrOption('val1', "", multi=True, callback=return_val)
    interface1 = OptionDescription('val1', '', [val1])
    maconfig = OptionDescription('rootconfig', '', [interface1])
    api = Config(maconfig)
    api.property.read_write()
    assert api.option('val1.val1').value.get() == ['val']
    api.option('val1.val1').value.set(['val1', undefined])
    assert api.option('val1.val1').value.get() == ['val1', 'val']


def test_callback_leader_and_followers_leader():
    val1 = StrOption('val1', "", multi=True, callback=return_val)
    val2 = StrOption('val2', "", multi=True)
    interface1 = Leadership('val1', '', [val1, val2])
    maconfig = OptionDescription('rootconfig', '', [interface1])
    api = Config(maconfig)
    api.property.read_write()
    assert api.option('val1.val1').value.get() == ['val']
    api.option('val1.val1').value.set([undefined, undefined])
    assert api.option('val1.val1').value.get() == ['val', 'val']
    assert api.option('val1.val2', 0).value.get() == None
    assert api.option('val1.val2', 1).value.get() == None


def test_callback_follower():
    val1 = StrOption('val1', "", multi=True)
    val2 = StrOption('val2', "", multi=True, callback=return_value3, callback_params=Params(ParamValue(['string', 'new'])))
    interface1 = Leadership('val1', '', [val1, val2])
    maconfig = OptionDescription('rootconfig', '', [interface1])
    api = Config(maconfig)
    api.property.read_write()
    api.option('val1.val1').value.set(['val'])
    assert api.option('val1.val2', 0).value.get() == 'string'
    api.option('val1.val1').value.set(['val', 'val1'])
    assert api.option('val1.val2', 0).value.get() == 'string'
    assert api.option('val1.val2', 1).value.get() == 'new'
    api.option('val1.val1').value.set(['val', 'val1', 'val2'])
    assert api.option('val1.val2', 0).value.get() == 'string'
    assert api.option('val1.val2', 1).value.get() == 'new'
    assert api.option('val1.val2', 2).value.get() == None
    api.option('val1.val1').value.set(['val', 'val1', 'val2', 'val3'])
    assert api.option('val1.val2', 0).value.get() == 'string'
    assert api.option('val1.val2', 1).value.get() == 'new'
    assert api.option('val1.val2', 2).value.get() == None
    assert api.option('val1.val2', 3).value.get() == None


def test_callback_leader_and_followers_leader2():
    val1 = StrOption('val1', "", multi=True)
    val2 = StrOption('val2', "", multi=True, default_multi='val2')
    val3 = StrOption('val3', "", multi=True, callback=return_value, callback_params=Params(ParamOption(val2)))
    val4 = StrOption('val4', "", multi=True, callback=return_value, callback_params=Params(ParamOption(val3)))
    interface1 = Leadership('val1', '', [val1, val2, val3, val4])
    maconfig = OptionDescription('rootconfig', '', [interface1])
    api = Config(maconfig)
    api.property.read_write()
    api.option('val1.val1').value.set(['val'])
    assert api.option('val1.val4', 0).value.get() == 'val2'
    assert api.option('val1.val3', 0).value.get() == 'val2'
    assert api.option('val1.val2', 0).value.get() == 'val2'


def test_callback_leader_and_followers_leader_mandatory():
    val = StrOption('val', "", default='val')
    val1 = StrOption('val1', "", multi=True, callback=return_value2, callback_params=Params(ParamOption(val)), properties=('mandatory',))
    val3 = StrOption('val3', "", multi=True, callback=return_index, callback_params=Params(ParamOption(val1)), properties=('mandatory',))
    val4 = StrOption('val4', "", multi=True, callback=return_index, callback_params=Params(ParamOption(val1)), properties=('mandatory',))
    interface1 = Leadership('val1', '', [val1, val3, val4])
    maconfig = OptionDescription('rootconfig', '', [val, interface1])
    api = Config(maconfig)
    api.property.read_only()
    assert api.option('val1.val3', 0).value.get() == 'val'
    assert api.option('val1.val4', 0).value.get() == 'val'
    assert api.option('val1.val1').value.get() == ['val']
    api.property.read_write()
    api.option('val1.val1').value.set([undefined, 'val3'])
    api.property.read_only()
    assert api.option('val1.val1').value.get() == ['val', 'val3']
    assert api.option('val1.val3', 0).value.get() == 'val'
    raises(PropertiesOptionError, "api.option('val1.val3', 1).value.get()")
    raises(PropertiesOptionError, "api.option('val1.val4', 1).value.get()")


def test_callback_leader_and_followers_leader_mandatory2():
    val = StrOption('val', "", default='val')
    val_ = StrOption('val_', "", default='val_')
    val1 = StrOption('val1', "", multi=True, callback=return_index, callback_params=Params(ParamOption(val), {'val2': ParamOption(val_)}), properties=('mandatory',))
    val3 = StrOption('val3', "", multi=True, callback=return_index, callback_params=Params(ParamOption(val1), {'val2': ParamOption(val_)}), properties=('mandatory',))
    val4 = StrOption('val4', "", multi=True, callback=return_index, callback_params=Params(ParamOption(val1), {'val2': ParamOption(val_)}), properties=('mandatory',))
    interface1 = Leadership('val1', '', [val1, val3, val4])
    maconfig = OptionDescription('rootconfig', '', [val, val_, interface1])
    api = Config(maconfig)
    api.property.read_only()
    assert api.option('val1.val3', 0).value.get() == 'val'
    assert api.option('val1.val3', 1).value.get() == 'val_'
    assert api.option('val1.val4', 0).value.get() == 'val'
    assert api.option('val1.val4', 1).value.get() == 'val_'
    assert api.option('val1.val1').value.get() == ['val', 'val_']
    api.property.read_write()
    api.option('val1.val1').value.set(['val', 'val_', 'val3'])
    assert api.option('val1.val1').value.get() == ['val', 'val_', 'val3']
    api.property.read_only()
    assert api.option('val1.val3', 0).value.get() == 'val'
    assert api.option('val1.val3', 1).value.get() == 'val_'
    assert api.option('val1.val4', 0).value.get() == 'val'
    assert api.option('val1.val4', 1).value.get() == 'val_'
    raises(PropertiesOptionError, "api.option('val1.val3', 2).value.get()")
    raises(PropertiesOptionError, "api.option('val1.val4', 2).value.get()")
    assert api.option('val1.val1').value.get() == ['val', 'val_', 'val3']


def test_callback_leader_and_followers_leader_mandatory3():
    val = StrOption('val', "", default='val')
    val_ = StrOption('val_', "", default='val_')
    val1 = StrOption('val1', "", multi=True, callback=return_value2, callback_params=Params(ParamOption(val), {'val': ParamOption(val_)}), properties=('mandatory',))
    val3 = StrOption('val3', "", multi=True, callback=return_value, callback_params=Params(ParamOption(val1)), properties=('mandatory',))
    val4 = StrOption('val4', "", multi=True, callback=return_value, callback_params=Params(ParamOption(val1)), properties=('mandatory',))
    interface1 = Leadership('val1', '', [val1, val3, val4])
    maconfig = OptionDescription('rootconfig', '', [val, val_, interface1])
    api = Config(maconfig)
    api.property.read_only()
    assert api.option('val1.val3', 0).value.get() == 'val'
    assert api.option('val1.val3', 1).value.get() == 'val_'
    assert api.option('val1.val4', 0).value.get() == 'val'
    assert api.option('val1.val4', 1).value.get() == 'val_'
    assert api.option('val1.val1').value.get() == ['val', 'val_']
    api.property.read_write()
    api.option('val1.val1').value.set(['val', 'val_', 'val3'])
    api.property.read_only()
    assert api.option('val1.val3', 0).value.get() == 'val'
    assert api.option('val1.val3', 1).value.get() == 'val_'
    assert api.option('val1.val3', 2).value.get() == 'val3'
    assert api.option('val1.val4', 0).value.get() == 'val'
    assert api.option('val1.val4', 1).value.get() == 'val_'
    assert api.option('val1.val4', 2).value.get() == 'val3'
    assert api.option('val1.val1').value.get() == ['val', 'val_', 'val3']


def test_callback_leader_and_followers_leader_mandatory4():
    val = StrOption('val', "", default='val')
    val1 = StrOption('val1', "", multi=True, callback=return_value2, callback_params=Params(ParamOption(val)), properties=('mandatory',))
    val3 = StrOption('val3', "", multi=True, callback=return_value, callback_params=Params(ParamOption(val1)), properties=('mandatory',))
    val4 = StrOption('val4', "", multi=True, callback=return_value, callback_params=Params(ParamOption(val1)), properties=('mandatory',))
    interface1 = Leadership('val1', '', [val1, val3, val4])
    maconfig = OptionDescription('rootconfig', '', [val, interface1])
    api = Config(maconfig)
    api.property.read_only()
    #raises(IndexError, "api.option('val1.val3').value.get()")
    assert api.option('val1.val3', 0).value.get() == 'val'
    assert api.option('val1.val4', 0).value.get() == 'val'
    assert api.option('val1.val1').value.get() == ['val']
    api.property.read_write()
    api.option('val1.val1').value.set(['val', 'val3'])
    api.property.read_only()
    assert api.option('val1.val1').value.get() == ['val', 'val3']
    assert api.option('val1.val3', 0).value.get() == 'val'
    assert api.option('val1.val3', 1).value.get() == 'val3'
    assert api.option('val1.val4', 0).value.get() == 'val'
    assert api.option('val1.val4', 1).value.get() == 'val3'


def test_callback_leader_and_followers_leader3():
    val1 = StrOption('val1', "", multi=True, properties=('mandatory', 'empty'))
    val2 = StrOption('val2', "", multi=True, default_multi='val2', properties=('expert',))
    val3 = StrOption('val3', "", multi=True, callback=return_value, callback_params=Params(ParamOption(val2)))
    val4 = StrOption('val4', "", multi=True, callback=return_value, callback_params=Params(ParamOption(val3)))
    interface1 = Leadership('val1', '', [val1, val2, val3, val4])
    maconfig = OptionDescription('rootconfig', '', [interface1])
    api = Config(maconfig)
    api.property.read_write()
    assert list(api.value.mandatory()) == ['val1.val1']


def test_callback_leader_and_followers_leader4():
    val1 = StrOption('val1', "", ['val1'], multi=True, properties=('mandatory',))
    val2 = StrOption('val2', "", multi=True, default_multi='val2', properties=('expert', 'mandatory'))
    val3 = StrOption('val3', "", multi=True, callback=return_value, callback_params=Params(ParamOption(val2)))
    val4 = StrOption('val4', "", multi=True, callback=return_value, callback_params=Params(ParamOption(val3)))
    interface1 = Leadership('val1', '', [val1, val2, val3, val4])
    maconfig = OptionDescription('rootconfig', '', [interface1])
    api = Config(maconfig)
    api.property.read_write()
    api.property.add('expert')
    api.permissive.set(frozenset(['expert']))
    assert list(api.value.mandatory()) == []


def test_consistency_leader_and_followers_leader_mandatory_transitive():
    #default value
    val1 = IPOption('val1', "", ['192.168.0.1'], multi=True, properties=('mandatory',))
    val2 = NetmaskOption('val2', "", multi=True, default_multi='255.255.255.0', properties=('disabled', 'mandatory'))
    val2.impl_add_consistency('ip_netmask', val1)
    #no value
    val3 = IPOption('val3', "", multi=True, properties=('mandatory',))
    val4 = NetmaskOption('val4', "", multi=True, default_multi='255.255.255.0', properties=('disabled', 'mandatory'))
    val4.impl_add_consistency('ip_netmask', val3)
    interface1 = Leadership('val1', '', [val1, val2])
    interface2 = Leadership('val3', '', [val3, val4])
    maconfig = OptionDescription('rootconfig', '', [interface1, interface2])
    api = Config(maconfig)
    api.property.read_write()
    try:
        api.option('val1.val1').value.get()
    except PropertiesOptionError as error:
        assert str(error) == str(_('cannot access to {0} "{1}" because "{2}" has {3} {4}').format('option', 'val1', 'val2', 'property', '"disabled"'))
    else:
        raise Exception('must raises')
    raises(PropertiesOptionError, "api.option('val3.val3').value.get()")
    assert list(api.value.mandatory()) == []


def test_consistency_leader_and_followers_leader_mandatory_non_transitive():
    #no value
    val1 = IPOption('val1', "", multi=True, properties=('mandatory',))
    val2 = NetmaskOption('val2', "", multi=True, default_multi='255.255.255.0', properties=('disabled', 'mandatory'))
    val2.impl_add_consistency('ip_netmask', val1, transitive=False)
    #default value
    val3 = IPOption('val3', "", ['192.168.0.1'], multi=True, properties=('mandatory',))
    val4 = NetmaskOption('val4', "", multi=True, default_multi='255.255.255.0', properties=('disabled', 'mandatory'))
    val4.impl_add_consistency('ip_netmask', val3, transitive=False)
    interface1 = Leadership('val1', '', [val1, val2])
    interface2 = Leadership('val3', '', [val3, val4])
    maconfig = OptionDescription('rootconfig', '', [interface1, interface2])
    api = Config(maconfig)
    api.property.read_write()
    if TIRAMISU_VERSION == 2:
        assert list(api.value.mandatory()) == ["val1.val1", "val1.val2"]
    else:
        assert list(api.value.mandatory()) == ["val1.val1"]


def test_callback_leader_and_followers_leader_list():
    val1 = StrOption('val1', "", multi=True, callback=return_list)
    val2 = StrOption('val2', "", multi=True)
    interface1 = Leadership('val1', '', [val1, val2])
    maconfig = OptionDescription('rootconfig', '', [interface1])
    api = Config(maconfig)
    api.property.read_write()
    assert api.option('val1.val1').value.get() == ['val', 'val']
    assert api.option('val1.val2', 0).value.get() == None
    assert api.option('val1.val2', 1).value.get() == None
    api.option('val1.val1').value.set(['val', 'val', undefined])
    assert api.option('val1.val1').value.get() == ['val', 'val', None]
    assert api.option('val1.val2', 0).value.get() == None
    assert api.option('val1.val2', 1).value.get() == None
    assert api.option('val1.val2', 1).value.get() == None
    api.option('val1.val1').value.reset()
    assert api.option('val1.val1').value.get() == ['val', 'val']
    assert api.option('val1.val2', 0).value.get() == None
    assert api.option('val1.val2', 1).value.get() == None
    api.option('val1.val1').value.pop(1)
    assert api.option('val1.val1').value.get() == ['val']
    assert api.option('val1.val2', 0).value.get() == None


def test_callback_leader_and_followers_leader_follower_list():
    val1 = StrOption('val1', "", multi=True)
    val2 = StrOption('val2', "", multi=True, callback=return_list)
    interface1 = Leadership('val1', '', [val1, val2])
    maconfig = OptionDescription('rootconfig', '', [interface1])
    api = Config(maconfig)
    api.property.read_write()
    assert api.option('val1.val1').value.get() == []
    api.option('val1.val1').value.set(['val1'])
    raises(LeadershipError, "api.option('val1.val2', 0).value.get()")


def test_callback_leader_and_followers_follower():
    val1 = StrOption('val1', "", multi=True)
    val2 = StrOption('val2', "", multi=True, callback=return_val)
    interface1 = Leadership('val1', '', [val1, val2])
    maconfig = OptionDescription('rootconfig', '', [interface1])
    api = Config(maconfig)
    api.property.read_write()
    assert api.option('val1.val1').value.get() == []
    #
    api.option('val1.val1').value.set(['val1'])
    assert api.option('val1.val1').value.get() == ['val1']
    assert api.option('val1.val2', 0).value.get() == 'val'
    #
    api.option('val1.val1').value.set(['val1', 'val2'])
    assert api.option('val1.val1').value.get() == ['val1', 'val2']
    assert api.option('val1.val2', 0).value.get() == 'val'
    assert api.option('val1.val2', 1).value.get() == 'val'
    #
    api.option('val1.val1').value.set(['val1', 'val2', 'val3'])
    assert api.option('val1.val1').value.get() == ['val1', 'val2', 'val3']
    assert api.option('val1.val2', 0).value.get() == 'val'
    assert api.option('val1.val2', 1).value.get() == 'val'
    assert api.option('val1.val2', 2).value.get() == 'val'
    #
    api.option('val1.val1').value.pop(2)
    assert api.option('val1.val1').value.get() == ['val1', 'val2']
    assert api.option('val1.val2', 0).value.get() == 'val'
    assert api.option('val1.val2', 1).value.get() == 'val'
    #
    api.option('val1.val2', 0).value.set('val2')
    api.option('val1.val2', 1).value.set('val2')
    assert api.option('val1.val2', 0).value.get() == 'val2'
    assert api.option('val1.val2', 1).value.get() == 'val2'
    #
    api.option('val1.val1').value.set(['val1', 'val2', 'val3'])
    assert api.option('val1.val2', 0).value.get() == 'val2'
    assert api.option('val1.val2', 1).value.get() == 'val2'
    assert api.option('val1.val2', 2).value.get() == 'val'


def test_callback_leader_and_followers():
    val1 = StrOption('val1', "", multi=True)
    val2 = StrOption('val2', "", multi=True, callback=return_val)
    interface1 = Leadership('val1', '', [val1, val2])
    maconfig = OptionDescription('rootconfig', '', [interface1])
    api = Config(maconfig)
    api.property.read_write()


def test_callback_leader_and_followers_follower_cal():
    val3 = StrOption('val3', "", multi=True)
    val1 = StrOption('val1', "", multi=True, callback=return_value, callback_params=Params(ParamOption(val3)))
    val2 = StrOption('val2', "", multi=True, callback=return_val)
    interface1 = Leadership('val1', '', [val1, val2])
    maconfig = OptionDescription('rootconfig', '', [interface1, val3])
    api = Config(maconfig)
    api.property.read_write()
    #
    assert api.option('val3').value.get() == []
    assert api.option('val1.val1').value.get() == []
    #
    api.option('val1.val1').value.set(['val1'])
    api.option('val3').value.set(['val1'])
    assert api.option('val1.val1').value.get() == ['val1']
    assert api.option('val1.val2', 0).value.get() == 'val'
    #
    api.option('val1.val1').value.reset()
    api.option('val1.val2', 0).value.set('val')
    #
    api.option('val3').value.set(['val1', 'val2'])
    assert api.option('val1.val2', 0).value.get() == 'val'
    assert api.option('val1.val2', 1).value.get() == 'val'
    assert api.option('val1.val1').value.get() == ['val1', 'val2']
    # len of follower is higher than leader's one
    api.option('val1.val2', 0).value.set('val1')
    api.option('val1.val2', 1).value.set('val2')
    api.option('val3').value.set(['val1'])
    assert api.option('val1.val1').value.get() == ['val1']
    raises(LeadershipError, "api.option('val1.val2', 0).value.get()")
    #
    api.option('val3').value.set(['val1', 'val2', 'val3'])
    assert api.option('val1.val2', 0).value.get() == 'val1'
    assert api.option('val1.val2', 1).value.get() == 'val2'
    assert api.option('val1.val2', 2).value.get() == 'val'


def test_callback_leader_and_followers_leader_disabled():
    #properties must be transitive
    val1 = StrOption('val1', "", ['val1'], multi=True, properties=('disabled',))
    val2 = StrOption('val2', "", multi=True)
    interface1 = Leadership('val1', '', [val1, val2])
    maconfig = OptionDescription('rootconfig', '', [interface1])
    api = Config(maconfig)
    api.property.read_write()
    raises(PropertiesOptionError, "api.option('val1.val1').value.get()")
    raises(PropertiesOptionError, "api.option('val1.val1').value.set(['yes'])")
    raises(PropertiesOptionError, "api.option('val1.val2', 0).value.get()")


def test_callback_leader_and_followers_leader_callback_disabled():
    val0 = StrOption('val0', "", multi=True, properties=('disabled',))
    val1 = StrOption('val1', "", multi=True, callback=return_value, callback_params=Params(ParamOption(val0)))
    val2 = StrOption('val2', "", multi=True)
    interface1 = Leadership('val1', '', [val1, val2])
    maconfig = OptionDescription('rootconfig', '', [interface1, val0])
    api = Config(maconfig)
    api.property.read_write()
    raises(ConfigError, "api.option('val1.val1').value.get()")
    raises(ConfigError, "api.option('val1.val2').value.get()")
    api.property.pop('disabled')
    api.option('val1.val1').value.set([])
    api.property.add('disabled')
    assert api.option('val1.val1').value.get() == []


def test_callback_leader_and_followers_follower_disabled():
    val1 = StrOption('val1', "", multi=True)
    val2 = StrOption('val2', "", multi=True, properties=('disabled',))
    interface1 = Leadership('val1', '', [val1, val2])
    maconfig = OptionDescription('rootconfig', '', [interface1])
    api = Config(maconfig)
    api.property.read_write()
    assert api.option('val1.val1').value.get() == []
    #raises(PropertiesOptionError, "api.option('val1.val2').value.get()")
    api.option('val1.val1').value.set(['yes'])
    assert api.option('val1.val1').value.get() == ['yes']
    api.property.pop('disabled')
    assert api.option('val1.val2', 0).value.get() == None
    api.option('val1.val2', 0).value.set('no')
    api.option('val1.val1').value.set(['yes', 'yes2', 'yes3'])
    api.option('val1.val2', 2).value.set('no1')
    assert api.option('val1.val2', 0).value.get() == 'no'
    assert api.option('val1.val2', 1).value.get() == None
    assert api.option('val1.val2', 2).value.get() == 'no1'
    api.property.add('disabled')
    api.option('val1.val1').value.pop(0)
    assert api.option('val1.val1').value.get() == ['yes2', 'yes3']
    api.property.pop('disabled')
    assert api.option('val1.val2', 0).value.get() == None
    assert api.option('val1.val2', 1).value.get() == 'no1'


def test_callback_leader_and_followers_follower_callback_disabled():
    val0 = StrOption('val0', "", multi=True, properties=('disabled',))
    val1 = StrOption('val1', "", multi=True)
    val2 = StrOption('val2', "", multi=True, callback=return_value, callback_params=Params(ParamOption(val0)))
    interface1 = Leadership('val1', '', [val1, val2])
    maconfig = OptionDescription('rootconfig', '', [interface1, val0])
    api = Config(maconfig)
    api.property.read_write()
    assert api.option('val1.val1').value.get() == []
    api.option('val1.val1').value.set(['yes'])
    assert api.option('val1.val1').value.get() == ['yes']
    api.property.pop('disabled')
    api.option('val1.val2', 0).value.set('no')
    api.option('val1.val1').value.set(['yes', 'yes1'])
    assert api.option('val1.val2', 0).value.get() == 'no'
    api.property.add('disabled')
    api.option('val1.val1').value.pop(1)


def test_callback_leader_and_followers_value():
    val4 = StrOption('val4', '', multi=True, default=['val10', 'val11'])
    val1 = StrOption('val1', "", multi=True)
    val2 = StrOption('val2', "", multi=True, callback=return_value, callback_params=Params(ParamOption(val1)))
    val3 = StrOption('val3', "", multi=True, callback=return_value, callback_params=Params(ParamValue('yes')))
    val5 = StrOption('val5', "", multi=True, callback=return_value, callback_params=Params(ParamOption(val4)))
    val6 = StrOption('val6', "", multi=True, callback=return_value, callback_params=Params(ParamOption(val5)))
    interface1 = Leadership('val1', '', [val1, val2, val3, val5, val6])
    maconfig = OptionDescription('rootconfig', '', [interface1, val4])
    api = Config(maconfig)
    api.property.read_write()
    api.option('val4').value.get() == ['val10', 'val11']
    assert api.option('val1.val1').value.get() == []
    #raises(LeadershipError, "cfg.val1.val1")
    #raises(LeadershipError, "cfg.val1.val2")
    #raises(LeadershipError, "cfg.val1.val3")
    #raises(LeadershipError, "cfg.val1.val5")
    #raises(LeadershipError, "cfg.val1.val6")
    #
    #default calculation has greater length
    #raises(LeadershipError, "api.option('val1.val1').value.set(['val1']")
    #
    api.option('val1.val1').value.set(['val1', 'val2'])
    assert api.option('val1.val1').value.get() == ['val1', 'val2']
    assert api.option('val1.val2', 0).value.get() == 'val1'
    assert api.option('val1.val2', 1).value.get() == 'val2'
    assert api.option('val1.val3', 0).value.get() == 'yes'
    assert api.option('val1.val3', 1).value.get() == 'yes'
    raises(LeadershipError, "api.option('val1.val5', 0).value.get()")
    raises(LeadershipError, "api.option('val1.val5', 1).value.get()")
    raises(LeadershipError, "api.option('val1.val6', 0).value.get()")
    raises(LeadershipError, "api.option('val1.val6', 1).value.get()")
    #
    api.option('val1.val1').value.set(['val1', 'val2', 'val3'])
    assert api.option('val1.val1').value.get() == ['val1', 'val2', 'val3']
    assert api.option('val1.val2', 0).value.get() == 'val1'
    assert api.option('val1.val2', 1).value.get() == 'val2'
    assert api.option('val1.val2', 2).value.get() == 'val3'
    assert api.option('val1.val3', 0).value.get() == 'yes'
    assert api.option('val1.val3', 1).value.get() == 'yes'
    assert api.option('val1.val3', 2).value.get() == 'yes'
    raises(LeadershipError, "api.option('val1.val5', 2).value.get()")
    raises(LeadershipError, "api.option('val1.val6', 2).value.get()")
    #
    api.option('val1.val1').value.pop(2)
    assert api.option('val1.val1').value.get() == ['val1', 'val2']
    assert api.option('val1.val2', 0).value.get() == 'val1'
    assert api.option('val1.val2', 1).value.get() == 'val2'
    assert api.option('val1.val3', 0).value.get() == 'yes'
    assert api.option('val1.val3', 1).value.get() == 'yes'
    #
    api.option('val1.val2', 0).value.set('val2')
    api.option('val1.val2', 1).value.set('val2')
    api.option('val1.val3', 0).value.set('val2')
    api.option('val1.val3', 1).value.set('val2')
    api.option('val1.val5', 0).value.set('val2')
    api.option('val1.val5', 1).value.set('val2')
    assert api.option('val1.val2', 0).value.get() == 'val2'
    assert api.option('val1.val2', 1).value.get() == 'val2'
    assert api.option('val1.val3', 0).value.get() == 'val2'
    assert api.option('val1.val3', 1).value.get() == 'val2'
    assert api.option('val1.val5', 0).value.get() == 'val2'
    assert api.option('val1.val5', 1).value.get() == 'val2'
    assert api.option('val1.val6', 0).value.get() == 'val2'
    assert api.option('val1.val6', 1).value.get() == 'val2'
    #
    api.option('val1.val1').value.set(['val1', 'val2', 'val3'])
    assert api.option('val1.val2', 2).value.get() == 'val3'
    assert api.option('val1.val3', 2).value.get() == 'yes'


def test_callback_leader():
    val2 = StrOption('val2', "", multi=True, callback=return_value)
    val1 = StrOption('val1', "", multi=True, callback=return_value, callback_params=Params(ParamOption(val2)))
    raises(ValueError, "Leadership('val1', '', [val1, val2])")


def test_callback_different_type():
    val = IntOption('val', "", default=2)
    val_ = IntOption('val_', "", default=3)
    val1 = IntOption('val1', "", multi=True)
    val2 = IntOption('val2', "", multi=True, callback=return_calc, callback_params=Params((ParamOption(val), ParamOption(val1)), {'k': ParamOption(val_)}))
    interface1 = Leadership('val1', '', [val1, val2])
    maconfig = OptionDescription('rootconfig', '', [interface1, val, val_])
    api = Config(maconfig)
    api.property.read_write()
    assert api.option('val1.val1').value.get() == []
    api.option('val1.val1').value.set([1])
    assert api.option('val1.val1').value.get() == [1]
    assert api.option('val1.val2', 0).value.get() == 6
    api.option('val1.val1').value.set([1, 3])
    assert api.option('val1.val1').value.get() == [1, 3]
    assert api.option('val1.val2', 0).value.get() == 6
    assert api.option('val1.val2', 1).value.get() == 8
    api.option('val1.val1').value.set([1, 3, 5])
    assert api.option('val1.val1').value.get() == [1, 3, 5]
    assert api.option('val1.val2', 0).value.get() == 6
    assert api.option('val1.val2', 1).value.get() == 8
    assert api.option('val1.val2', 2).value.get() == 10


def test_callback_hidden():
    opt1 = BoolOption('opt1', '')
    opt2 = BoolOption('opt2', '', callback=return_value, callback_params=Params(ParamOption(opt1)))
    od1 = OptionDescription('od1', '', [opt1], properties=('hidden',))
    od2 = OptionDescription('od2', '', [opt2])
    maconfig = OptionDescription('rootconfig', '', [od1, od2])
    api = Config(maconfig)
    api.property.read_write()
    raises(PropertiesOptionError, "api.option('od1.opt1').value.get()")
    # do not raise, forcepermissive
    api.option('od2.opt2').value.get()


def test_callback_hidden_permissive():
    opt1 = BoolOption('opt1', '')
    opt2 = BoolOption('opt2', '', callback=return_value, callback_params=Params(ParamOption(opt1)))
    od1 = OptionDescription('od1', '', [opt1], properties=('hidden',))
    od2 = OptionDescription('od2', '', [opt2])
    maconfig = OptionDescription('rootconfig', '', [od1, od2])
    api = Config(maconfig)
    api.permissive.set(frozenset(['hidden']))
    api.property.read_write()
    raises(PropertiesOptionError, "api.option('od1.opt1').value.get()")
    api.option('od2.opt2').value.get()


def test_callback_hidden_permissive_callback():
    opt1 = BoolOption('opt1', '')
    opt2 = BoolOption('opt2', '', callback=return_value, callback_params=Params(ParamOption(opt1, True)))
    od1 = OptionDescription('od1', '', [opt1], properties=('hidden',))
    od2 = OptionDescription('od2', '', [opt2])
    maconfig = OptionDescription('rootconfig', '', [od1, od2])
    api = Config(maconfig)
    api.property.read_write()
    raises(PropertiesOptionError, "api.option('od1.opt1').value.get()")
    api.option('od2.opt2').value.get()


def test_callback_two_disabled():
    opt1 = BoolOption('opt1', '', properties=('disabled',))
    opt2 = BoolOption('opt2', '', callback=return_value, callback_params=Params(ParamOption(opt1)), properties=('disabled',))
    od1 = OptionDescription('od1', '', [opt1])
    od2 = OptionDescription('od2', '', [opt2])
    maconfig = OptionDescription('rootconfig', '', [od1, od2])
    api = Config(maconfig)
    api.property.read_write()
    raises(PropertiesOptionError, "api.option('od2.opt2').value.get()")


def test_callback_two_disabled2():
    opt1 = BoolOption('opt1', '', properties=('hidden',))
    opt2 = BoolOption('opt2', '', callback=return_value, callback_params=Params(ParamOption(opt1)), properties=('hidden',))
    od1 = OptionDescription('od1', '', [opt1])
    od2 = OptionDescription('od2', '', [opt2])
    maconfig = OptionDescription('rootconfig', '', [od1, od2])
    api = Config(maconfig)
    api.property.read_write()
    api.permissive.set(frozenset(['hidden']))
    raises(PropertiesOptionError, "api.option('od2.opt2').value.get()")
    assert api.forcepermissive.option('od2.opt2').owner.isdefault()


def test_callback_calculating_invalid():
    opt1 = IntOption('opt1', '', 1)
    opt2 = BoolOption('opt2', '', callback=return_value, callback_params=Params(ParamOption(opt1)))
    od1 = OptionDescription('od1', '', [opt1])
    od2 = OptionDescription('od2', '', [opt2])
    maconfig = OptionDescription('rootconfig', '', [od1, od2])
    api = Config(maconfig)
    api.property.read_write()
    raises(ValueError, "api.option('od2.opt2').value.get()")
    api.unrestraint.option('od2.opt2').property.add('disabled')
    raises(PropertiesOptionError, "api.option('od2.opt2').value.get()")


def test_callback_calculating_disabled():
    opt1 = BoolOption('opt1', '', properties=('disabled',))
    opt2 = BoolOption('opt2', '', callback=return_value, callback_params=Params(ParamOption(opt1)))
    od1 = OptionDescription('od1', '', [opt1])
    od2 = OptionDescription('od2', '', [opt2])
    maconfig = OptionDescription('rootconfig', '', [od1, od2])
    api = Config(maconfig)
    api.property.read_write()
    raises(ConfigError, "api.option('od2.opt2').value.get()")


def test_callback_calculating_mandatory():
    opt1 = BoolOption('opt1', '', properties=('disabled',))
    opt2 = BoolOption('opt2', '', callback=return_value, callback_params=Params(ParamOption(opt1)), properties=('mandatory',))
    od1 = OptionDescription('od1', '', [opt1])
    od2 = OptionDescription('od2', '', [opt2])
    maconfig = OptionDescription('rootconfig', '', [od1, od2])
    api = Config(maconfig)
    api.property.read_only()
    raises(ConfigError, "api.option('od2.opt2').value.get()")


def test_callback_calculating_mandatory_multi():
    opt1 = BoolOption('opt1', '', multi=True, properties=('disabled',))
    opt2 = BoolOption('opt2', '', multi=True, callback=return_value, callback_params=Params(ParamOption(opt1)), properties=('mandatory',))
    od1 = OptionDescription('od1', '', [opt1])
    od2 = OptionDescription('od2', '', [opt2])
    maconfig = OptionDescription('rootconfig', '', [od1, od2])
    api = Config(maconfig)
    api.property.read_only()
    raises(ConfigError, "api.option('od2.opt2').value.get()")


def test_callback_two_disabled_multi():
    opt1 = BoolOption('opt1', '', properties=('disabled',))
    opt2 = BoolOption('opt2', '', callback=return_value, callback_params=Params(ParamOption(opt1)), properties=('disabled',), multi=True)
    od1 = OptionDescription('od1', '', [opt1])
    od2 = OptionDescription('od2', '', [opt2])
    maconfig = OptionDescription('rootconfig', '', [od1, od2])
    api = Config(maconfig)
    api.property.read_write()
    raises(PropertiesOptionError, "api.option('od2.opt2').value.get()")


def test_callback_multi_list_params():
    val1 = StrOption('val1', "", multi=True, default=['val1', 'val2'])
    val2 = StrOption('val2', "", multi=True, callback=return_list, callback_params=Params(ParamOption(val1)))
    oval2 = OptionDescription('val2', '', [val2])
    maconfig = OptionDescription('rootconfig', '', [val1, oval2])
    api = Config(maconfig)
    api.property.read_write()
    assert api.option('val2.val2').value.get() == ['val', 'val']


def test_callback_multi_list_params_key():
    val1 = StrOption('val1', "", multi=True, default=['val1', 'val2'])
    val2 = StrOption('val2', "", multi=True, callback=return_list, callback_params=Params(kwargs={'value': ParamOption(val1)}))
    oval2 = OptionDescription('val2', '', [val2])
    maconfig = OptionDescription('rootconfig', '', [val1, oval2])
    api = Config(maconfig)
    api.property.read_write()
    assert api.option('val2.val2').value.get() == ['val', 'val']


def test_leadership_callback_description():
    st1 = StrOption('st1', "", multi=True)
    st2 = StrOption('st2', "", multi=True, callback=return_value, callback_params=Params(ParamOption(st1)))
    stm = Leadership('st1', '', [st1, st2])
    st = OptionDescription('st', '', [stm])
    od = OptionDescription('od', '', [st])
    od2 = OptionDescription('od', '', [od])
    api = Config(od2)
    owner = api.owner.get()
    assert api.option('od.st.st1.st1').value.get() == []
    assert api.option('od.st.st1.st1').owner.isdefault()
    ##
    api.option('od.st.st1.st1').value.set(['yes'])
    api.option('od.st.st1.st2', 0).value.set('yes')
    assert api.option('od.st.st1.st1').owner.get() == owner
    assert api.option('od.st.st1.st2', 0).owner.get() == owner


def test_callback_raise():
    opt1 = BoolOption('opt1', 'Option 1', callback=return_raise)
    opt2 = BoolOption('opt2', 'Option 2', callback=return_valueerror)
    od1 = OptionDescription('od1', '', [opt1])
    od2 = OptionDescription('od2', '', [opt2])
    maconfig = OptionDescription('rootconfig', '', [od1, od2])
    api = Config(maconfig)
    api.property.read_write()
    try:
        api.option('od1.opt1').value.get()
    except ConfigError as err:
        assert '"Option 1"' in str(err)
    try:
        api.option('od2.opt2').value.get()
    except ConfigError as err:
        assert '"Option 2"' in str(err)
