# -*- coding: utf-8 -*-

"""theses tests are much more to test that config, option description, vs...
**it's there** and answers via attribute access"""
from py.test import raises
import weakref

from .autopath import do_autopath
do_autopath()

from tiramisu import Config
from tiramisu.config import SubConfig
from tiramisu.i18n import _
from tiramisu import Config, IntOption, FloatOption, StrOption, ChoiceOption, \
    BoolOption, UnicodeOption, SymLinkOption, OptionDescription, undefined
from tiramisu.error import ConflictError, ConfigError, PropertiesOptionError, APIError
from tiramisu.storage import list_sessions


def teardown_function(function):
    assert list_sessions() == [], 'session list is not empty when leaving "{}"'.format(function.__name__)


def make_description():
    gcoption = ChoiceOption('name', 'GC name', ('ref', 'framework'), 'ref')
    gcdummy = BoolOption('dummy', 'dummy', default=False)
    objspaceoption = ChoiceOption('objspace', 'Object space',
                                  ('std', 'thunk'), 'std')
    booloption = BoolOption('bool', 'Test boolean option', default=True)
    intoption = IntOption('int', 'Test int option', default=0)
    floatoption = FloatOption('float', 'Test float option', default=2.3)
    stroption = StrOption('str', 'Test string option', default="abc", properties=('mandatory', ))
    boolop = BoolOption('boolop', 'Test boolean option op', default=True, properties=('hidden',))
    wantref_option = BoolOption('wantref', 'Test requires', default=False)
    wantref_option.impl_set_information('info', 'default value')
    wantframework_option = BoolOption('wantframework', 'Test requires',
                                      default=False)

    gcgroup = OptionDescription('gc', '', [gcoption, gcdummy, floatoption])
    descr = OptionDescription('tiram', '', [gcgroup, booloption, objspaceoption,
                                            wantref_option, stroption,
                                            wantframework_option,
                                            intoption, boolop])
    return descr


def test_base_config():
    """making a :class:`tiramisu.config.Config()` object
    and a :class:`tiramisu.option.OptionDescription()` object
    """
    gcdummy = BoolOption('dummy', 'dummy', default=False)
    descr = OptionDescription('tiramisu', '', [gcdummy])
    cfg = Config(descr)
    assert cfg.option('dummy').value.get() is False
    #dmo = cfg.unwrap_from_path('dummy')
    #assert dmo.impl_getname() == 'dummy'


def test_base_config_name():
    gcdummy = BoolOption('dummy', 'dummy', default=False)
    descr = OptionDescription('tiramisu', '', [gcdummy])
    cfg = Config(descr, session_id='cfg')
    cfg.config.name() == 'cfg'
    #raises(ValueError, "Config(descr, session_id='unvalid name')")
#
#
#def test_not_config():
#    assert raises(TypeError, "Config('str')")


def test_base_path():
    gcdummy = BoolOption('dummy', 'dummy', default=False)
    descr = OptionDescription('tiramisu', '', [gcdummy])
    Config(descr)
    base = OptionDescription('config', '', [descr])
    base
    raises(ConfigError, "Config(base)")


def test_base_config_force_permissive():
    descr = make_description()
    config = Config(descr)
    config.property.read_write()
    config.permissive.set(frozenset(['hidden']))
    raises(PropertiesOptionError, "config.option('boolop').value.get()")
    assert config.forcepermissive.option('boolop').value.get() is True


def test_base_config_in_a_tree():
    "how options are organized into a tree, see :ref:`tree`"
    descr = make_description()
    config = Config(descr)
    #
    config.option('bool').value.set(False)
    #
    assert config.option('gc.name').value.get() == 'ref'
    config.option('gc.name').value.set('framework')
    assert config.option('gc.name').value.get() == 'framework'
    #
    assert config.option('objspace').value.get() == 'std'
    config.option('objspace').value.set('thunk')
    assert config.option('objspace').value.get() == 'thunk'
    #
    assert config.option('gc.float').value.get() == 2.3
    config.option('gc.float').value.set(3.4)
    assert config.option('gc.float').value.get() == 3.4
    #
    assert config.option('int').value.get() == 0
    config.option('int').value.set(123)
    assert config.option('int').value.get() == 123
    #
    assert config.option('wantref').value.get() is False
    config.option('wantref').value.set(True)
    assert config.option('wantref').value.get() is True
    #
    assert config.option('str').value.get() == 'abc'
    config.option('str').value.set('def')
    assert config.option('str').value.get() == 'def'
    #
    raises(AttributeError, "config.option('gc.foo').value.get()")
    ##
    config = Config(descr)
    assert config.option('bool').value.get() is True
    assert config.option('gc.name').value.get() == 'ref'
    assert config.option('wantframework').value.get() is False


def test_not_valid_properties():
    raises(TypeError, "stroption = StrOption('str', 'Test string option', default='abc', properties=['mandatory',])")


def test_information_config():
    descr = make_description()
    config = Config(descr)
    string = 'some informations'
    #
    assert list(config.information.list()) == []
    config.information.set('info', string)
    assert config.information.get('info') == string
    assert list(config.information.list()) == ['info']
    #
    raises(ValueError, "config.information.get('noinfo')")
    assert config.information.get('noinfo', 'default') == 'default'
    config.information.reset('info')
    raises(ValueError, "config.information.get('info')")
    raises(ValueError, "config.information.reset('noinfo')")
    assert list(config.information.list()) == []


def test_information_option():
    descr = make_description()
    config = Config(descr)
    string = 'some informations'
    #
    list(config.option('gc.name').information.list()) == []
    config.option('gc.name').information.set('info', string)
    assert config.option('gc.name').information.get('info') == string
    list(config.option('gc.name').information.list()) == ['info']
    #
    raises(ValueError, "config.option('gc.name').information.get('noinfo')")
    assert config.option('gc.name').information.get('noinfo', 'default') == 'default'
    config.option('gc.name').information.reset('info')
    raises(ValueError, "config.option('gc.name').information.get('info')")
    raises(ValueError, "config.option('gc.name').information.reset('noinfo')")
    list(config.option('gc.name').information.list()) == []
    #
    assert config.option('wantref').information.get('info') == 'default value'
    config.option('wantref').information.set('info', 'default value')
    assert config.option('wantref').information.get('info') == 'default value'
    config.option('wantref').information.reset('info')
    assert config.option('wantref').information.get('info') == 'default value'


def to_tuple(val):
    ret = []
    for v in val:
        t = []
        for w in v:
            if isinstance(w, list):
                t.append(tuple(w))
            else:
                t.append(w)
        ret.append(tuple(t))
    return tuple(ret)


def test_get_modified_values():
    g1 = IntOption('g1', '', 1)
    g2 = StrOption('g2', '', 'héhé')
    g3 = UnicodeOption('g3', '', u'héhé')
    g4 = BoolOption('g4', '', True)
    g5 = StrOption('g5', '')
    g6 = StrOption('g6', '', multi=True)
    d1 = OptionDescription('od', '', [g1, g2, g3, g4, g5, g6])
    root = OptionDescription('root', '', [d1])
    config = Config(root)
    assert to_tuple(config.value.exportation()) == ((), (), (), ())
    assert not config.option('od.g5').option.ismulti()
    assert not config.option('od.g5').option.issubmulti()
    config.option('od.g5').value.set('yes')
    assert to_tuple(config.value.exportation()) == (('od.g5',), (None,), ('yes',), ('user',))
    config.option('od.g4').value.set(False)
    assert to_tuple(config.value.exportation()) == (('od.g5', 'od.g4'), (None, None), ('yes', False), ('user', 'user'))
    config.option('od.g4').value.set(undefined)
    assert to_tuple(config.value.exportation()) == (('od.g5', 'od.g4'), (None, None), ('yes', True), ('user', 'user'))
    config.option('od.g4').value.reset()
    assert to_tuple(config.value.exportation()) == (('od.g5',), (None,), ('yes',), ('user',))
    assert config.option('od.g6').option.ismulti()
    config.option('od.g6').value.set([undefined])
    assert to_tuple(config.value.exportation()) == (('od.g5', 'od.g6'), (None, None), ('yes', (None,)), ('user', 'user'))
    config.option('od.g6').value.set([])
    assert to_tuple(config.value.exportation()) == (('od.g5', 'od.g6'), (None, None), ('yes', tuple()), ('user', 'user'))
    config.option('od.g6').value.set(['3'])
    assert to_tuple(config.value.exportation()) == (('od.g5', 'od.g6'), (None, None), ('yes', ('3',)), ('user', 'user'))
    config.option('od.g6').value.set([])
    assert to_tuple(config.value.exportation()) == (('od.g5', 'od.g6'), (None, None), ('yes', tuple()), ('user', 'user'))


def test_get_modified_values_not_modif():
    g1 = StrOption('g1', '', multi=True)
    d1 = OptionDescription('od', '', [g1])
    root = OptionDescription('root', '', [d1])
    config = Config(root)
    assert config.option('od.g1').value.get() == []
    value = config.option('od.g1').value.get()
    value.append('val')
    assert config.option('od.g1').value.get() == []


def test_duplicated_option():
    g1 = IntOption('g1', '', 1)
    g1
    #in same OptionDescription
    raises(ConflictError, "d1 = OptionDescription('od', '', [g1, g1])")


def test_duplicated_option_diff_od():
    g1 = IntOption('g1', '', 1)
    d1 = OptionDescription('od1', '', [g1])
    #in different OptionDescription
    d2 = OptionDescription('od2', '', [g1, d1])
    d2
    raises(ConflictError, 'Config(d2)')


def test_cannot_assign_value_to_option_description():
    descr = make_description()
    cfg = Config(descr)
    raises(APIError, "cfg.option('gc').value.set(3)")


def test_config_multi():
    i1 = IntOption('test1', '', multi=True)
    i2 = IntOption('test2', '', multi=True, default_multi=1)
    i3 = IntOption('test3', '', default=[2], multi=True, default_multi=1)
    od = OptionDescription('test', '', [i1, i2, i3])
    config = Config(od)
    assert config.option('test1').value.get() == []
    assert config.option('test2').value.get() == []
    config.option('test2').value.set([undefined])
    assert config.option('test2').value.get() == [1]
    assert config.option('test3').value.get() == [2]
    config.option('test3').value.set([undefined, undefined])
    assert config.option('test3').value.get() == [2, 1]


def test_prefix_error():
    i1 = IntOption('test1', '')
    od = OptionDescription('test', '', [i1])
    config = Config(od)
    config.property.read_write()
    config.option('test1').value.set(1)
    try:
        config.option('test1').value.set('yes')
    except Exception as err:
        assert str(err) == '"yes" is an invalid integer for "test1"'
    try:
        config.option('test1').value.set('yes')
    except Exception as err:
        err.prefix = ''
        assert str(err) == 'invalid value'


def test_no_validation():
    i1 = IntOption('test1', '')
    od = OptionDescription('test', '', [i1])
    config = Config(od)
    config.property.read_write()
    config.option('test1').value.set(1)
    raises(ValueError, "config.option('test1').value.set('yes')")
    assert config.option('test1').value.get() == 1
    config.property.pop('validator')
    config.option('test1').value.set('yes')
    assert config.option('test1').value.get() == 'yes'
    config.property.add('validator')
    raises(ValueError, "config.option('test1').value.get()")
    config.option('test1').value.reset()
    assert config.option('test1').value.get() is None


def test_subconfig():
    i = IntOption('i', '')
    o = OptionDescription('val', '', [i])
    o2 = OptionDescription('val', '', [o])
    c = Config(o2)
    c
    raises(TypeError, "SubConfig(i, weakref.ref(c))")


def test_config_subconfig():
    i1 = IntOption('i1', '')
    i2 = IntOption('i2', '', default=1)
    i3 = IntOption('i3', '')
    i4 = IntOption('i4', '', default=2)
    od1 = OptionDescription('od1', '', [i1, i2, i3, i4])
    od2 = OptionDescription('od2', '', [od1])
    conf1 = Config(od2, session_id='conf1')
    raises(ConfigError, "conf2 = Config(od1, session_id='conf2')")


def test_config_invalidsession():
    i = IntOption('i', '')
    o = OptionDescription('val', '', [i])
    o2 = OptionDescription('val', '', [o])
    raises(ValueError, 'Config(o2, session_id=2)')


def test_config_od_name():
    i = IntOption('i', '')
    s = SymLinkOption('s', i)
    o = OptionDescription('val', '', [i, s])
    o2 = OptionDescription('val', '', [o])
    c = Config(o2)
    assert c.option('val.i').option.name() == 'i'
    assert c.option('val.s').option.name() == 's'
    assert c.option('val.s').option.name(follow_symlink=True) == 'i'


def test_config_od_type():
    i = IntOption('i', '')
    o = OptionDescription('val', '', [i])
    o2 = OptionDescription('val', '', [o])
    c = Config(o2)
    assert c.option('val.i').option.type() == 'integer'


def test_config_default():
    i = IntOption('i', '', 8)
    o = OptionDescription('val', '', [i])
    o2 = OptionDescription('val', '', [o])
    c = Config(o2)
    assert c.option('val.i').value.default() == 8
    c.option('val.i').value.set(9)
    assert c.option('val.i').value.get() == 9
    assert c.option('val.i').value.default() == 8
