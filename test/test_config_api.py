"configuration objects global API"
from py.test import raises

from .autopath import do_autopath
do_autopath()

from tiramisu import Config, IntOption, FloatOption, StrOption, ChoiceOption, \
    BoolOption, FilenameOption, UnicodeOption, SymLinkOption, IPOption, \
    PortOption, NetworkOption, NetmaskOption, BroadcastOption, \
    DomainnameOption, OptionDescription
from tiramisu.error import PropertiesOptionError
from tiramisu.storage import list_sessions


def teardown_function(function):
    assert list_sessions() == [], 'session list is not empty when leaving "{}"'.format(function.__name__)


def make_description():
    gcoption = ChoiceOption('name', 'GC name', ('ref', 'framework'), 'ref')
    gcdummy = BoolOption('dummy', 'dummy', default=False)
    prop = BoolOption('prop', 'prop 1', properties=('disabled',))
    prop2 = StrOption('prop', 'prop 2', properties=('hidden',))
    objspaceoption = ChoiceOption('objspace', 'Object space',
                                  ('std', 'thunk'), 'std')
    booloption = BoolOption('bool', 'Test boolean option', default=True)
    booloption2 = BoolOption('bool', 'Test boolean option', default=False)
    intoption = IntOption('int', 'Test int option', default=0)
    floatoption2 = FloatOption('float', 'Test float option', default=2.3)
    floatoption = FloatOption('float', 'Test float option', default=2.3)
    stroption = StrOption('str', 'Test string option', default="abc")
    boolop = BoolOption('boolop', 'Test boolean option op', default=True)
    wantref_option = BoolOption('wantref', 'Tests', default=False)
    wantframework_option = BoolOption('wantframework', 'Test', default=False)
    gcgroup2 = OptionDescription('gc2', '', [booloption2, prop])
    gcgroup = OptionDescription('gc', '', [gcgroup2, gcoption, gcdummy, floatoption, prop2])
    descr = OptionDescription('tiramisu', '', [gcgroup, booloption, objspaceoption,
                                               wantref_option, stroption,
                                               wantframework_option,
                                               intoption, boolop, floatoption2])
    return descr


def _is_same_opt(opt1, opt2):
    if "id" in dir(opt1):
        assert opt1.id == opt2.id
    else:
        assert opt1 == opt2


def test_od_not_list():
    b = BoolOption('bool', '', multi=True)
    raises(AssertionError, "OptionDescription('od', '', b)")


def test_str():
    descr = make_description()
    c = Config(descr)
    c  # does not crash


def test_make_dict():
    "serialization of the whole config to a dict"
    descr = OptionDescription("opt", "", [
        OptionDescription("s1", "", [
            BoolOption("a", "", default=False),
            BoolOption("b", "", default=False, properties=('hidden',))]),
        IntOption("int", "", default=42)])
    config = Config(descr)
    config.property.read_write()
    config.permissive.set(frozenset(['hidden']))
    d = config.value.dict()
    assert d == {"s1.a": False, "int": 42}
    config.option('int').value.set(43)
    config.option('s1.a').value.set(True)
    d = config.value.dict()
    assert d == {"s1.a": True, "int": 43}
    d2 = config.value.dict(flatten=True)
    assert d2 == {'a': True, 'int': 43}
    raises(ValueError, 'd2 = config.value.dict(withvalue="3")')
    d = config.forcepermissive.value.dict()
    assert d == {"s1.a": True, "s1.b": False, "int": 43}


def test_make_dict_with_disabled():
    descr = OptionDescription("opt", "", [
        OptionDescription("s1", "", [
            BoolOption("a", "", default=False),
            BoolOption("b", "", default=False, properties=('disabled',))]),
        OptionDescription("s2", "", [
            BoolOption("a", "", default=False),
            BoolOption("b", "", default=False)], properties=('disabled',)),
        IntOption("int", "", default=42)])
    config = Config(descr)
    config.property.read_only()
    assert config.value.dict() == {"s1.a": False, "int": 42}
    assert config.forcepermissive.value.dict() == {"s1.a": False, "int": 42}
    assert config.unrestraint.value.dict() == {"int": 42, "s1.a": False, "s1.b": False, "s2.a": False, "s2.b": False}


def test_make_dict_with_disabled_withoption():
    descr = OptionDescription("opt", "", [
        OptionDescription("s1", "", [
            BoolOption("a", "", default=False),
            BoolOption("b", "", default=False, properties=('disabled',))]),
        OptionDescription("s2", "", [
            BoolOption("a", "", default=False),
            BoolOption("b", "", default=False)], properties=('disabled',)),
        IntOption("int", "", default=42)])
    config = Config(descr)
    config.property.read_only()
    assert config.value.dict(withoption="a") == {"s1.a": False}
    assert config.forcepermissive.value.dict(withoption="a") == {"s1.a": False}
    assert config.unrestraint.value.dict(withoption="a") == {"s1.a": False, "s1.b": False, "s2.a": False, "s2.b": False}


def test_make_dict_with_disabled_in_callback():
    descr = OptionDescription("opt", "", [
        OptionDescription("s1", "", [
            BoolOption("a", "", default=False),
            BoolOption("b", "", default=False, properties=('disabled',))]),
        OptionDescription("s2", "", [
            BoolOption("a", "", default=False),
            BoolOption("b", "", default=False)], properties=('disabled',)),
        IntOption("int", "", default=42)])
    config = Config(descr)
    config.property.read_only()
    d = config.value.dict()
    assert d == {"s1.a": False, "int": 42}


def test_make_dict_fullpath():
    descr = OptionDescription("root", "", [
        OptionDescription("opt", "", [
            OptionDescription("s1", "", [
                BoolOption("a", "", default=False),
                BoolOption("b", "", default=False, properties=('disabled',))]),
            OptionDescription("s2", "", [
                BoolOption("a", "", default=False),
                BoolOption("b", "", default=False)], properties=('disabled',)),
            IntOption("int", "", default=42)]),
        IntOption("introot", "", default=42)])
    config = Config(descr)
    config.property.read_only()
    assert config.value.dict() == {"opt.s1.a": False, "opt.int": 42, "introot": 42}
    assert config.option('opt').value.dict() == {"s1.a": False, "int": 42}
    assert config.value.dict(fullpath=True) == {"opt.s1.a": False, "opt.int": 42, "introot": 42}
    assert config.option('opt').value.dict(fullpath=True) == {"opt.s1.a": False, "opt.int": 42}


def test_find_in_config():
    "finds option in config"
    descr = make_description()
    conf = Config(descr)
    conf.property.read_only()
    conf.permissive.set(frozenset(['hidden']))
    ret = list(conf.option.find('dummy'))
    assert len(ret) == 1
    _is_same_opt(ret[0].option.get(), conf.option('gc.dummy').option.get())
    #
    ret = conf.option.find('dummy', first=True).option.get()
    _is_same_opt(ret, conf.option('gc.dummy').option.get())
    #
    ret = list(conf.option.find('float'))
    assert len(ret) == 2
    _is_same_opt(ret[0].option.get(), conf.option('gc.float').option.get())
    _is_same_opt(ret[1].option.get(), conf.option('float').option.get())
    #
    _is_same_opt(conf.option.find('bool', first=True).option.get(), conf.option('gc.gc2.bool').option.get())
    _is_same_opt(conf.option.find('bool', value=True, first=True).option.get(), conf.option('bool').option.get())
    _is_same_opt(conf.option.find('dummy', first=True).option.get(), conf.option('gc.dummy').option.get())
    _is_same_opt(conf.option.find('float', first=True).option.get(), conf.option('gc.float').option.get())
    #FIXME cannot find an option without name
    #ret = conf.find(bytype=ChoiceOption)
    #assert len(ret) == 2
    #_is_same_opt(ret[0], conf.unwrap_from_path('gc.name'))
    #_is_same_opt(ret[1], conf.unwrap_from_path('objspace'))
    #
    #_is_same_opt(conf.find_first(bytype=ChoiceOption), conf.unwrap_from_path('gc.name'))
    #ret = conf.find(byvalue='ref')
    #assert len(ret) == 1
    #_is_same_opt(ret[0], conf.unwrap_from_path('gc.name'))
    #_is_same_opt(conf.find_first(byvalue='ref'), conf.unwrap_from_path('gc.name'))
    #
    ret = list(conf.option.find('prop'))
    assert len(ret) == 1
    _is_same_opt(ret[0].option.get(), conf.option('gc.prop').option.get())
    #
    ret = list(conf.option.find('prop', value=None))
    assert len(ret) == 1
    ret = list(conf.option.find('prop'))
    assert len(ret) == 1
    _is_same_opt(ret[0].option.get(), conf.option('gc.prop').option.get())
    #
    conf.property.read_write()
    raises(AttributeError, "assert conf.option.find('prop').option.get()")
    ret = list(conf.unrestraint.option.find(name='prop'))
    assert len(ret) == 2
    _is_same_opt(ret[0].option.get(), conf.unrestraint.option('gc.gc2.prop').option.get())
    _is_same_opt(ret[1].option.get(), conf.forcepermissive.option('gc.prop').option.get())
    #
    ret = list(conf.forcepermissive.option.find('prop'))
    assert len(ret) == 1
    _is_same_opt(ret[0].option.get(), conf.forcepermissive.option('gc.prop').option.get())
    #
    _is_same_opt(conf.forcepermissive.option.find('prop', first=True).option.get(), conf.forcepermissive.option('gc.prop').option.get())
    # combinaison of filters
    ret = list(conf.unrestraint.option.find('prop', type=BoolOption))
    assert len(ret) == 1
    _is_same_opt(ret[0].option.get(), conf.unrestraint.option('gc.gc2.prop').option.get())
    _is_same_opt(conf.unrestraint.option.find('prop', type=BoolOption, first=True).option.get(), conf.unrestraint.option('gc.gc2.prop').option.get())
    #
    ret = list(conf.option.find('dummy', value=False))
    assert len(ret) == 1
    _is_same_opt(ret[0].option.get(), conf.option('gc.dummy').option.get())
    #
    _is_same_opt(conf.option.find('dummy', value=False, first=True).option.get(), conf.option('gc.dummy').option.get())
    #subconfig
    ret = list(conf.option('gc').find('dummy'))
    assert len(ret) == 1
    _is_same_opt(ret[0].option.get(), conf.option('gc.dummy').option.get())
    #
    ret = list(conf.option('gc').find('float'))
    assert len(ret) == 1
    _is_same_opt(ret[0].option.get(), conf.option('gc.float').option.get())
    #
    ret = list(conf.option('gc').find('bool'))
    assert len(ret) == 1
    _is_same_opt(ret[0].option.get(), conf.option('gc.gc2.bool').option.get())
    _is_same_opt(conf.option('gc').find('bool', value=False, first=True).option.get(), conf.option('gc.gc2.bool').option.get())
    #
    raises(AttributeError, "assert conf.option('gc').find('bool', value=True, first=True).option.get()")
    #
    raises(AttributeError, "conf.option('gc').find('wantref').option.get()")
    #
    ret = list(conf.unrestraint.option('gc').find('prop'))
    assert len(ret) == 2
    _is_same_opt(ret[0].option.get(), conf.unrestraint.option('gc.gc2.prop').option.get())
    _is_same_opt(ret[1].option.get(), conf.forcepermissive.option('gc.prop').option.get())
    #
    conf.property.read_only()
    ret = list(conf.option('gc').find('prop'))
    assert len(ret) == 1
    _is_same_opt(ret[0].option.get(), conf.option('gc.prop').option.get())
    # not OptionDescription
    raises(AttributeError, "conf.option.find('gc', first=True)")
    raises(AttributeError, "conf.option.find('gc2', first=True)")


def test_find_multi():
    b = BoolOption('bool', '', multi=True)
    o = OptionDescription('od', '', [b])
    conf = Config(o)
    #
    raises(AttributeError, "list(conf.option.find('bool', value=True))")
    raises(AttributeError, "list(conf.option.find('bool', value=True, first=True))")
    conf.option('bool').value.set([False])
    raises(AttributeError, "list(conf.option.find('bool', value=True))")
    raises(AttributeError, "list(conf.option.find('bool', value=True, first=True))")
    conf.option('bool').value.set([False, False])
    raises(AttributeError, "list(conf.option.find('bool', value=True))")
    raises(AttributeError, "list(conf.option.find('bool', value=True, first=True))")
    conf.option('bool').value.set([False, False, True])
    ret = list(conf.option.find('bool', value=True))
    assert len(ret) == 1
    _is_same_opt(ret[0].option.get(), b)
    _is_same_opt(conf.option.find('bool', value=True, first=True).option.get(), b)


def test_does_not_find_in_config():
    descr = make_description()
    conf = Config(descr)
    raises(AttributeError, "list(conf.option.find('IDontExist'))")


def test_filename():
    a = FilenameOption('a', '')
    o = OptionDescription('o', '', [a])
    cfg = Config(o)
    cfg.option('a').value.set('/')
    cfg.option('a').value.set('/tmp')
    cfg.option('a').value.set('/tmp/')
    cfg.option('a').value.set('/tmp/text.txt')
    cfg.option('a').value.set('tmp')
    cfg.option('a').value.set('tmp/')
    cfg.option('a').value.set('tmp/text.txt')
    raises(ValueError, "cfg.option('a').value.set('/tmp/with space.txt')")
    raises(ValueError, "cfg.option('a').value.set('/tmp/with$.txt')")


def test_invalid_option():
    ChoiceOption('a', '', ('1', '2'))
    raises(TypeError, "ChoiceOption('a', '', [1, 2])")
    raises(TypeError, "ChoiceOption('a', '', 1)")
    raises(ValueError, "ChoiceOption('a', '', (1,), 3)")
    FloatOption('a', '')
    raises(ValueError, "FloatOption('a', '', 'string')")
    UnicodeOption('a', '')
    raises(ValueError, "UnicodeOption('a', '', 1)")
    u = UnicodeOption('a', '')
    SymLinkOption('a', u)
    raises(ValueError, "SymLinkOption('a', 'string')")
    IPOption('a', '')
    raises(ValueError, "IPOption('a', '', 1)")
    raises(ValueError, "IPOption('a', '', 'string')")
    PortOption('a', '')
    raises(ValueError, "PortOption('a', '', 'string')")
    raises(ValueError, "PortOption('a', '', '11:12:13', allow_range=True)")
    raises(ValueError, "PortOption('a', '', 11111111111111111111)")
    raises(ValueError, "PortOption('a', '', allow_zero=True, allow_wellknown=False, allow_registred=True, allow_private=False)")
    raises(ValueError, "PortOption('a', '', allow_zero=True, allow_wellknown=True, allow_registred=False, allow_private=True)")
    raises(ValueError, "PortOption('a', '', allow_zero=True, allow_wellknown=False, allow_registred=False, allow_private=True)")
    raises(ValueError, "PortOption('a', '', allow_zero=True, allow_wellknown=False, allow_registred=True, allow_private=True)")
    raises(ValueError, "PortOption('a', '', allow_zero=False, allow_wellknown=False, allow_registred=False, allow_private=False)")
    NetworkOption('a', '')
    raises(ValueError, "NetworkOption('a', '', 'string')")
    NetmaskOption('a', '')
    raises(ValueError, "NetmaskOption('a', '', 'string')")
    BroadcastOption('a', '')
    raises(ValueError, "BroadcastOption('a', '', 'string')")
    DomainnameOption('a', '')
    raises(ValueError, "DomainnameOption('a', '', 'string')")
    raises(ValueError, "DomainnameOption('a', '', type_='string')")
    raises(ValueError, "DomainnameOption('a', '', allow_ip='string')")
    raises(ValueError, "DomainnameOption('a', '', allow_without_dot='string')")
    raises(ValueError, "DomainnameOption('a', '', 1)")
    #
    ChoiceOption('a', '', (1,), multi=True, default_multi=1)
    raises(ValueError, "ChoiceOption('a', '', (1,), default_multi=1)")
    raises(ValueError, "ChoiceOption('a', '', (1,), multi=True, default=[1,], default_multi=2)")
    raises(ValueError, "FloatOption('a', '', multi=True, default_multi='string')")
    raises(ValueError, "UnicodeOption('a', '', multi=True, default_multi=1)")
    raises(ValueError, "IPOption('a', '', multi=True, default_multi=1)")
    raises(ValueError, "IPOption('a', '', multi=True, default_multi='string')")
    raises(ValueError, "PortOption('a', '', multi=True, default_multi='string')")
    raises(ValueError, "PortOption('a', '', multi=True, default_multi='11:12:13', allow_range=True)")
    raises(ValueError, "PortOption('a', '', multi=True, default_multi=11111111111111111111)")
    raises(ValueError, "NetworkOption('a', '', multi=True, default_multi='string')")
    raises(ValueError, "NetmaskOption('a', '', multi=True, default_multi='string')")
    raises(ValueError, "BroadcastOption('a', '', multi=True, default_multi='string')")
    raises(ValueError, "DomainnameOption('a', '', multi=True, default_multi='string')")
    raises(ValueError, "DomainnameOption('a', '', multi=True, default_multi=1)")


def test_help():
    stro = StrOption('s', '', multi=True)
    od1 = OptionDescription('o', '', [stro])
    od2 = OptionDescription('o', '', [od1])
    cfg = Config(od2)
    cfg.help(_display=False)
    cfg.config.help(_display=False)
    cfg.option.help(_display=False)
    #FIXMEcfg.option('o').help(_display=False)
    cfg.option('o.s').help(_display=False)


def test_config_reset():
    descr = make_description()
    c = Config(descr)
    c.owner.set('test')
    assert c.owner.get() == 'test'
    assert not c.option('gc.gc2.bool').value.get()
    assert not c.option('boolop').property.get()
    assert not c.option('boolop').permissive.get()
    assert not c.option('wantref').information.get('info', None)
    #
    c.option('gc.gc2.bool').value.set(True)
    c.option('boolop').property.add('test')
    c.option('float').permissive.set(frozenset(['test']))
    c.option('wantref').information.set('info', 'info')
    assert c.option('gc.gc2.bool').value.get()
    assert c.option('boolop').property.get()
    assert c.option('float').permissive.get()
    assert c.option('wantref').information.get('info', None)
    #
    assert c.owner.get() == 'test'
    c.config.reset()
    assert c.owner.get() == 'test'
    assert not c.option('gc.gc2.bool').value.get()
    assert not c.option('boolop').property.get()
    assert not c.option('float').permissive.get()
    assert not c.option('wantref').information.get('info', None)
