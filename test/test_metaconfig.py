from py.test import raises
from .autopath import do_autopath
do_autopath()

from tiramisu.setting import groups, owners
from tiramisu import IntOption, StrOption, NetworkOption, NetmaskOption, BoolOption, ChoiceOption, \
                     IPOption, OptionDescription, Leadership, Config, GroupConfig, MetaConfig, \
                     Params, ParamOption, ParamValue
from tiramisu.error import ConfigError, ConflictError, PropertiesOptionError, LeadershipError, APIError
from tiramisu.storage import list_sessions


def teardown_function(function):
    assert list_sessions() == [], 'session list is not empty when leaving "{}"'.format(function.__name__)


owners.addowner('config')
owners.addowner('meta1')
owners.addowner('meta2')


def return_value(value=None):
    return value


def return_condition(val, condition, expected):
    if condition == expected:
        return val
    return None


def raise_exception():
    raise Exception('test')


def make_description():
    i1 = IntOption('i1', '')
    i2 = IntOption('i2', '', default=1)
    i3 = IntOption('i3', '')
    i4 = IntOption('i4', '', default=2)
    i5 = IntOption('i5', '', default=[2], multi=True)
    i6 = IntOption('i6', '', properties=('disabled',))
    od1 = OptionDescription('od1', '', [i1, i2, i3, i4, i5, i6])
    od2 = OptionDescription('od2', '', [od1])
    return od2


def make_metaconfig(double=False):
    od2 = make_description()
    conf1 = Config(od2, session_id='conf1')
    conf2 = Config(od2, session_id='conf2')
    meta = MetaConfig([conf1, conf2], session_id='meta')
    if double:
        meta.owner.set(owners.meta2)
        meta = MetaConfig([meta], session_id='doublemeta')
    meta.property.read_write()
    meta.owner.set(owners.meta1)
    return meta


def test_unknown_config():
    meta = make_metaconfig()
    raises(ConfigError, "meta.config('unknown')")


def test_error_metaconfig():
    od2 = make_description()
    conf1 = Config(od2, session_id='conf1')
    raises(TypeError, "MetaConfig([GroupConfig([conf1])], session_id='meta')")


def test_path():
    meta = make_metaconfig()
    assert meta.config.path() == 'meta'
    assert meta.config('conf1').config.path() == 'meta.conf1'
    assert meta.config('conf2').config.path() == 'meta.conf2'


#FIXME ne pas mettre 2 meta dans une config
#FIXME ne pas mettre 2 OD differents dans un meta
def test_none():
    meta = make_metaconfig()
    assert meta.option('od1.i3').value.get() is meta.config('conf1').option('od1.i3').value.get() is meta.config('conf2').option('od1.i3').value.get() is None
    assert meta.option('od1.i3').owner.get() is meta.config('conf1').option('od1.i3').owner.get() is meta.config('conf2').option('od1.i3').owner.get() is owners.default
    #
    meta.option('od1.i3').value.set(3)
    assert meta.option('od1.i3').value.get() == meta.config('conf1').option('od1.i3').value.get() == meta.config('conf2').option('od1.i3').value.get() == 3
    assert meta.option('od1.i3').owner.get() is meta.config('conf1').option('od1.i3').owner.get() is meta.config('conf2').option('od1.i3').owner.get() is owners.meta1
    #
    meta.config('conf1').option('od1.i3').value.set(2)
    assert meta.option('od1.i3').value.get() == meta.config('conf2').option('od1.i3').value.get() == 3
    assert meta.config('conf1').option('od1.i3').value.get() == 2
    assert meta.option('od1.i3').owner.get() is meta.config('conf2').option('od1.i3').owner.get() is owners.meta1
    assert meta.config('conf1').option('od1.i3').owner.get() is owners.user
    #
    meta.option('od1.i3').value.set(4)
    assert meta.option('od1.i3').value.get() == meta.config('conf2').option('od1.i3').value.get() == 4
    assert meta.config('conf1').option('od1.i3').value.get() == 2
    assert meta.option('od1.i3').owner.get() is meta.config('conf2').option('od1.i3').owner.get() is owners.meta1
    assert meta.config('conf1').option('od1.i3').owner.get() is owners.user
    #
    meta.option('od1.i3').value.reset()
    assert meta.option('od1.i3').value.get() is meta.config('conf2').option('od1.i3').value.get() is None
    assert meta.config('conf1').option('od1.i3').value.get() == 2
    assert meta.option('od1.i3').owner.get() is meta.config('conf2').option('od1.i3').owner.get() is owners.default
    assert meta.config('conf1').option('od1.i3').owner.get() is owners.user
    #
    meta.config('conf1').option('od1.i3').value.reset()
    assert meta.option('od1.i3').value.get() is meta.config('conf1').option('od1.i3').value.get() is meta.config('conf2').option('od1.i3').value.get() is None
    assert meta.option('od1.i3').owner.get() is meta.config('conf1').option('od1.i3').owner.get() is meta.config('conf2').option('od1.i3').owner.get() is owners.default
    #
    assert meta.config(None).config.name() == meta.config.name()


def test_reset():
    meta = make_metaconfig()
    assert meta.option('od1.i2').value.get() == 1
    meta.option('od1.i2').value.set(2)
    meta.config('conf1').option('od1.i2').value.set(3)
    assert meta.option('od1.i2').value.get() == 2
    assert meta.config('conf1').option('od1.i2').value.get() == 3
    assert meta.config('conf2').option('od1.i2').value.get() == 2
    meta.config.reset()
    assert meta.option('od1.i2').value.get() == 1
    assert meta.config('conf1').option('od1.i2').value.get() == 3
    assert meta.config('conf2').option('od1.i2').value.get() == 1


def test_default():
    meta = make_metaconfig()
    assert meta.option('od1.i2').value.get() == meta.config('conf1').option('od1.i2').value.get() == meta.config('conf2').option('od1.i2').value.get() == 1
    assert meta.option('od1.i2').owner.get() is meta.config('conf1').option('od1.i2').owner.get() is meta.config('conf2').option('od1.i2').owner.get() is owners.default
    #
    meta.option('od1.i2').value.set(3)
    assert meta.option('od1.i2').value.get() == meta.config('conf1').option('od1.i2').value.get() == meta.config('conf2').option('od1.i2').value.get() == 3
    assert meta.option('od1.i2').owner.get() is meta.config('conf1').option('od1.i2').owner.get() is meta.config('conf2').option('od1.i2').owner.get() is owners.meta1
    #
    meta.config('conf1').option('od1.i2').value.set(2)
    assert meta.option('od1.i2').value.get() == meta.config('conf2').option('od1.i2').value.get() == 3
    assert meta.config('conf1').option('od1.i2').value.get() == 2
    assert meta.option('od1.i2').owner.get() is meta.config('conf2').option('od1.i2').owner.get() is owners.meta1
    assert meta.config('conf1').option('od1.i2').owner.get() is owners.user
    #
    meta.option('od1.i2').value.set(4)
    assert meta.option('od1.i2').value.get() == meta.config('conf2').option('od1.i2').value.get() == 4
    assert meta.config('conf1').option('od1.i2').value.get() == 2
    assert meta.option('od1.i2').owner.get() is meta.config('conf2').option('od1.i2').owner.get() is owners.meta1
    assert meta.config('conf1').option('od1.i2').owner.get() is owners.user
    #
    meta.option('od1.i2').value.reset()
    assert meta.option('od1.i2').value.get() == meta.config('conf2').option('od1.i2').value.get() == 1
    assert meta.config('conf1').option('od1.i2').value.get() == 2
    assert meta.option('od1.i2').owner.get() is meta.config('conf2').option('od1.i2').owner.get() is owners.default
    assert meta.config('conf1').option('od1.i2').owner.get() is owners.user
    #
    meta.config('conf1').option('od1.i2').value.reset()
    assert meta.option('od1.i2').value.get() == meta.config('conf1').option('od1.i2').value.get() == meta.config('conf2').option('od1.i2').value.get() == 1
    assert meta.option('od1.i2').owner.get() is meta.config('conf1').option('od1.i2').owner.get() is meta.config('conf2').option('od1.i2').owner.get() is owners.default


def test_contexts():
    meta = make_metaconfig()
    errors = meta.value.set('od1.i2', 6, only_config=True)
    assert meta.option('od1.i2').value.get() == 1
    assert meta.option('od1.i2').owner.get() == owners.default
    assert meta.config('conf1').option('od1.i2').value.get() == meta.config('conf1').option('od1.i2').value.get() == 6
    assert meta.config('conf1').option('od1.i2').owner.get() == meta.config('conf1').option('od1.i2').owner.get() is owners.user
    assert len(errors) == 0


def test_find():
    meta = make_metaconfig()
    ret = list(meta.option.find('i2'))
    assert len(ret) == 1
    assert 1 == ret[0].value.get()
    assert 1 == meta.option.find('i2', first=True).value.get()
    assert meta.value.dict() == {'od1.i4': 2, 'od1.i1': None, 'od1.i3': None,
                                      'od1.i2': 1, 'od1.i5': [2]}


def test_meta_meta():
    meta = make_metaconfig(double=True)
    assert meta.option('od1.i2').value.get() == meta.config('meta').option('od1.i2').value.get() == meta.config('meta.conf1').option('od1.i2').value.get() == meta.config('meta.conf2').option('od1.i2').value.get() == 1
    assert meta.option('od1.i2').owner.get() is meta.config('meta').option('od1.i2').owner.get() is meta.config('meta.conf1').option('od1.i2').owner.get() is meta.config('meta.conf2').option('od1.i2').owner.get() is owners.default
    #
    meta.option('od1.i2').value.set(3)
    assert meta.option('od1.i2').value.get() == meta.config('meta').option('od1.i2').value.get() == meta.config('meta.conf1').option('od1.i2').value.get() == meta.config('meta.conf2').option('od1.i2').value.get() == 3
    assert meta.option('od1.i2').owner.get() is meta.config('meta').option('od1.i2').owner.get() is meta.config('meta.conf1').option('od1.i2').owner.get() is meta.config('meta.conf2').option('od1.i2').owner.get() is owners.meta1
    #
    meta.config('meta.conf1').option('od1.i2').value.set(2)
    assert meta.option('od1.i2').value.get() == meta.config('meta').option('od1.i2').value.get() == meta.config('meta.conf2').option('od1.i2').value.get() == 3
    assert meta.config('meta.conf1').option('od1.i2').value.get() == 2
    assert meta.option('od1.i2').owner.get() is meta.config('meta').option('od1.i2').owner.get() is meta.config('meta.conf2').option('od1.i2').owner.get() is owners.meta1
    assert meta.config('meta.conf1').option('od1.i2').owner.get() is owners.user
    #
    meta.config('meta').option('od1.i2').value.set(4)
    assert meta.option('od1.i2').value.get() == 3
    assert meta.config('meta').option('od1.i2').value.get() == meta.config('meta.conf2').option('od1.i2').value.get() == 4
    assert meta.config('meta.conf1').option('od1.i2').value.get() == 2
    assert meta.option('od1.i2').owner.get() is owners.meta1
    assert meta.config('meta').option('od1.i2').owner.get() is meta.config('meta.conf2').option('od1.i2').owner.get() is owners.meta2
    assert meta.config('meta.conf1').option('od1.i2').owner.get() is owners.user
    #
    meta.config('meta').option('od1.i2').value.reset()
    assert meta.option('od1.i2').value.get() == meta.config('meta').option('od1.i2').value.get() == meta.config('meta.conf2').option('od1.i2').value.get() == 3
    assert meta.config('meta.conf1').option('od1.i2').value.get() == 2
    assert meta.option('od1.i2').owner.get() is meta.config('meta').option('od1.i2').owner.get() is meta.config('meta.conf2').option('od1.i2').owner.get() is owners.meta1
    assert meta.config('meta.conf1').option('od1.i2').owner.get() is owners.user
    #
    meta.option('od1.i2').value.reset()
    assert meta.option('od1.i2').value.get() == meta.config('meta').option('od1.i2').value.get() == meta.config('meta.conf2').option('od1.i2').value.get() == 1
    assert meta.config('meta.conf1').option('od1.i2').value.get() == 2
    assert meta.option('od1.i2').owner.get() is meta.config('meta').option('od1.i2').owner.get() is meta.config('meta.conf2').option('od1.i2').owner.get() is owners.default
    assert meta.config('meta.conf1').option('od1.i2').owner.get() is owners.user
    #
    meta.config('meta.conf1').option('od1.i2').value.reset()
    assert meta.option('od1.i2').value.get() == meta.config('meta').option('od1.i2').value.get() == meta.config('meta.conf1').option('od1.i2').value.get() == meta.config('meta.conf2').option('od1.i2').value.get() == 1
    assert meta.option('od1.i2').owner.get() is meta.config('meta').option('od1.i2').owner.get() is meta.config('meta.conf1').option('od1.i2').owner.get() is meta.config('meta.conf2').option('od1.i2').owner.get() is owners.default


def test_meta_new_config():
    od = make_description()
    meta = MetaConfig(['name1', 'name2'], optiondescription=od)
    assert len(list(meta.config.list())) == 2
    meta.config.new('newconf1')
    assert len(list(meta.config.list())) == 3


def test_meta_new_config_owner():
    od = make_description()
    meta = MetaConfig(['name1', 'name2'], optiondescription=od)
    meta.owner.set('meta')
    meta.config.new('newconf1')
    assert meta.owner.get() == 'meta'


def test_meta_new_metaconfig():
    od = make_description()
    meta = MetaConfig(['name1', 'name2'], optiondescription=od)
    meta.config.new('newconf1', type='metaconfig')
    meta.config('newconf1').config.new('newconf2', type='metaconfig')
    meta.config('newconf1').config('newconf2').config.new('newconf3')
    assert meta.config('newconf1').config('newconf2').config('newconf3').config.name() == 'newconf3'


def test_meta_pop_config():
    od = make_description()
    meta = MetaConfig(['name1', 'name2'], optiondescription=od)
    assert len(list(meta.config.list())) == 2
    meta.config.new('newconf1')
    assert len(list(meta.config.list())) == 3
    meta.config.pop('newconf1')
    assert len(list(meta.config.list())) == 2
    raises(ConfigError, "meta.config.pop('newconf1')")


def test_meta_new_config_wrong_name():
    od = make_description()
    meta = MetaConfig(['name1', 'name2'], optiondescription=od)
    assert len(list(meta.config.list())) == 2
    raises(ConflictError, "meta.config.new('name1')")
    assert len(list(meta.config.list())) == 2


def test_meta_meta_set():
    meta = make_metaconfig(double=True)
    errors1 = meta.value.set('od1.i1', 7, only_config=True)
    errors2 = meta.value.set('od1.i6', 7, only_config=True)
    assert len(errors1) == 0
    assert len(errors2) == 2
    conf1 = meta.config('meta.conf1')._config_bag.context
    conf2 = meta.config('meta.conf2')._config_bag.context
    assert meta.config('meta.conf1').option('od1.i1').value.get() == meta.config('meta.conf2').option('od1.i1').value.get() == 7
    #
    dconfigs = []
    for conf in meta.config.find('i1', value=7).config.list():
        dconfigs.append(conf._config_bag.context)
    assert [conf1, conf2] == dconfigs
    meta.config('meta.conf1').option('od1.i1').value.set(8)
    #
    dconfigs = []
    for conf in meta.config.find('i1').config.list():
        dconfigs.append(conf._config_bag.context)
    assert [conf1, conf2] == dconfigs
    assert conf2 == list(meta.config.find('i1', value=7).config.list())[0]._config_bag.context
    assert conf1 == list(meta.config.find('i1', value=8).config.list())[0]._config_bag.context
    #
    dconfigs = []
    for conf in meta.config.find('i5', value=2).config.list():
        dconfigs.append(conf._config_bag.context)
    assert [conf1, conf2] == dconfigs
    #
    raises(AttributeError, "meta.config.find('i1', value=10)")
    raises(AttributeError, "meta.config.find('not', value=10)")
    raises(AttributeError, "meta.config.find('i6')")
    raises(ValueError, "meta.value.set('od1.i6', 7, only_config=True, force_default=True)")
    raises(ValueError, "meta.value.set('od1.i6', 7, only_config=True, force_default_if_same=True)")
    raises(ValueError, "meta.value.set('od1.i6', 7, only_config=True, force_dont_change_value=True)")


def test_not_meta():
    i1 = IntOption('i1', '')
    od1 = OptionDescription('od1', '', [i1])
    od2 = OptionDescription('od2', '', [od1])
    conf1 = Config(od2, session_id='conf1')
    conf2 = Config(od2, session_id='conf2')
    conf3 = Config(od2)
    conf4 = Config(od2, session_id='conf4')
    raises(TypeError, "GroupConfig(conf1)")
    #same name
    #raises(ConflictError, "GroupConfig([conf2, conf4], session_id='conf2')")
    raises(ConflictError, "GroupConfig([conf2, conf2], session_id='conf8')")
    grp = GroupConfig([conf1, conf2])
    raises(APIError, "grp.option('od1.i1').value.get()")
    conf1, conf2 = grp.config.list()
    errors = grp.value.set('od1.i1', 7)
    assert len(errors) == 0
    assert grp.config('conf1').option('od1.i1').value.get() == grp.config('conf2').option('od1.i1').value.get() == 7
    assert grp.config('conf1').option('od1.i1').owner.get() is grp.config('conf2').option('od1.i1').owner.get() is owners.user
    grp.option('od1.i1').value.reset()
    assert grp.config('conf1').option('od1.i1').owner.get() is grp.config('conf2').option('od1.i1').owner.get() is owners.default


def test_group_find_firsts():
    i1 = IntOption('i1', '')
    od1 = OptionDescription('od1', '', [i1])
    od2 = OptionDescription('od2', '', [od1])
    conf1 = Config(od2, session_id='conf1')
    conf2 = Config(od2, session_id='conf2')
    grp = GroupConfig([conf1, conf2])
    itr = grp.config.find('i1').config.list()
    conf1._config_bag.context == next(itr)._config_bag.context
    conf2._config_bag.context == next(itr)._config_bag.context
    try:
        next(itr)
    except StopIteration:
        pass
    except:
        raise Exception('no')
    else:
        raise Exception('no')


def test_group_group():
    i1 = IntOption('i1', '')
    od1 = OptionDescription('od1', '', [i1])
    od2 = OptionDescription('od2', '', [od1])
    conf1 = Config(od2, session_id='conf9')
    conf2 = Config(od2, session_id='conf10')
    grp = GroupConfig([conf1, conf2], 'grp')
    grp2 = GroupConfig([grp])
    errors = grp2.value.set('od1.i1', 2)
    assert len(errors) == 0
    assert grp2.config('grp.conf9').option('od1.i1').value.get() == 2
    assert grp2.config('grp.conf9').option('od1.i1').owner.get() is owners.user


def test_group_group_path():
    i1 = IntOption('i1', '')
    od1 = OptionDescription('od1', '', [i1])
    od2 = OptionDescription('od2', '', [od1])
    conf1 = Config(od2, session_id='conf9')
    conf2 = Config(od2, session_id='conf10')
    grp = GroupConfig([conf1, conf2], 'grp')
    grp2 = GroupConfig([grp], 'grp2')
    assert grp2.config.path() == 'grp2'
    assert grp2.config('grp').config.path() == 'grp'
    assert grp2.config('grp.conf9').config.path() == 'conf9'
    assert grp2.config('grp.conf10').config.path() == 'conf10'


def test_meta_unconsistent():
    i1 = IntOption('i1', '')
    i2 = IntOption('i2', '', default=1)
    i3 = IntOption('i3', '')
    i4 = IntOption('i4', '', default=2)
    od1 = OptionDescription('od1', '', [i1, i2, i3, i4])
    od2 = OptionDescription('od2', '', [od1])
    i5 = IntOption('i5', '')
    od3 = OptionDescription('od3', '', [i5])
    conf1 = Config(od2, session_id='conf1')
    conf2 = Config(od2, session_id='conf2')
    conf3 = Config(od2, session_id='conf3')
    conf4 = Config(od3, session_id='conf4')
    meta = MetaConfig([conf1, conf2])
    meta.owner.set(owners.meta1)
    raises(TypeError, 'MetaConfig("string")')
    #same descr but conf1 already in meta
    raises(ValueError, "MetaConfig([conf1, conf3])")
    #not same descr
    raises(ValueError, "MetaConfig([conf3, conf4])")


def test_meta_leadership():
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip", multi=True)
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "mask", multi=True, properties=('hidden',))
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    od = OptionDescription('root', '', [interface1])
    conf1 = Config(od, session_id='conf1')
    conf2 = Config(od, session_id='conf2')
    meta = MetaConfig([conf1, conf2])
    meta.property.read_only()
    itr = meta.config.find('ip_admin_eth0').config.list()
    assert conf1._config_bag.context == next(itr)._config_bag.context
    assert conf2._config_bag.context == next(itr)._config_bag.context
    itr = meta.config.find('netmask_admin_eth0').config.list()
    assert conf1._config_bag.context == next(itr)._config_bag.context
    assert conf2._config_bag.context == next(itr)._config_bag.context
    meta.property.read_write()
    raises(AttributeError, "meta.config.find('netmask_admin_eth0')")
    itr = meta.unrestraint.config.find('netmask_admin_eth0').config.list()
    assert conf1._config_bag.context == next(itr)._config_bag.context
    assert conf2._config_bag.context == next(itr)._config_bag.context
    meta.property.read_only()
    itr = meta.config.find('netmask_admin_eth0').config.list()
    assert conf1._config_bag.context == next(itr)._config_bag.context
    assert conf2._config_bag.context == next(itr)._config_bag.context


def test_meta_leadership_value2():
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip", multi=True)
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "mask", multi=True, properties=('hidden',))
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    od = OptionDescription('root', '', [interface1])
    conf1 = Config(od, session_id='conf1')
    conf2 = Config(od, session_id='conf2')
    meta = MetaConfig([conf1, conf2], session_id="meta")
    meta.config('conf1').option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.1.8'])
    assert meta.config('conf1').option('ip_admin_eth0.netmask_admin_eth0', 0).value.get() == None
    #FIXME devrait raise ! assert meta.config('conf1').option('ip_admin_eth0.ip_admin_eth0', 0).value.get() == None
    #
    meta.config('conf1').option('ip_admin_eth0.ip_admin_eth0').value.reset()
    #
    meta.option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.1.1'])
    assert meta.config('conf1').option('ip_admin_eth0.netmask_admin_eth0', 0).value.get() == None
    meta.option('ip_admin_eth0.netmask_admin_eth0', 0).value.set('255.255.255.0')
    assert meta.config('conf1').option('ip_admin_eth0.netmask_admin_eth0', 0).value.get() == '255.255.255.0'
    meta.option('ip_admin_eth0.netmask_admin_eth0', 0).value.set('255.255.0.0')
    assert meta.config('conf1').option('ip_admin_eth0.netmask_admin_eth0', 0).value.get() == '255.255.0.0'
    #
    meta.config('conf1').option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.1.1'])
    assert meta.config('conf1').option('ip_admin_eth0.netmask_admin_eth0', 0).value.get() == None


def test_meta_leadership_value_default():
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip", multi=True, default=['192.168.1.1'])
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "mask", multi=True)
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    od = OptionDescription('root', '', [interface1])
    conf1 = Config(od, session_id='conf1')
    conf2 = Config(od, session_id='conf2')
    meta = MetaConfig([conf1, conf2])
    assert meta.config('conf1').option('ip_admin_eth0.netmask_admin_eth0', 0).value.get() == None
    #
    meta.option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.1.1'])
    assert meta.config('conf1').option('ip_admin_eth0.netmask_admin_eth0', 0).value.get() == None
    #
    meta.option('ip_admin_eth0.netmask_admin_eth0', 0).value.set('255.255.255.0')
    assert meta.config('conf1').option('ip_admin_eth0.netmask_admin_eth0', 0).value.get() == '255.255.255.0'
    #
    meta.option('ip_admin_eth0.netmask_admin_eth0', 0).value.set('255.255.0.0')
    assert meta.config('conf1').option('ip_admin_eth0.netmask_admin_eth0', 0).value.get() == '255.255.0.0'
    #
    meta.config('conf1').option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.1.1'])
    assert meta.config('conf1').option('ip_admin_eth0.netmask_admin_eth0', 0).value.get() == None


def test_meta_leadership_owners():
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip", multi=True)
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "mask", multi=True, properties=('hidden',))
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    od = OptionDescription('root', '', [interface1])
    conf1 = Config(od, session_id='conf1')
    conf2 = Config(od, session_id='conf2')
    meta = MetaConfig([conf1, conf2])
    meta.owner.set(owners.meta1)
    assert meta.config('conf1').option('ip_admin_eth0.ip_admin_eth0').owner.isdefault()
    raises(LeadershipError, "meta.config('conf1').option('ip_admin_eth0.netmask_admin_eth0', 0).owner.isdefault()")
    #
    meta.config('conf1').option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.1.1'])
    assert meta.config('conf1').option('ip_admin_eth0.ip_admin_eth0').owner.get() == owners.user
    assert meta.config('conf1').option('ip_admin_eth0.netmask_admin_eth0', 0).owner.isdefault()
    #
    meta.config('conf1').option('ip_admin_eth0.ip_admin_eth0').value.reset()
    assert meta.config('conf1').option('ip_admin_eth0.ip_admin_eth0').owner.isdefault()
    #
    meta.option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.1.1'])
    assert meta.config('conf1').option('ip_admin_eth0.ip_admin_eth0').owner.get() == owners.meta1
    assert meta.config('conf1').option('ip_admin_eth0.netmask_admin_eth0', 0).owner.isdefault()
    #
    meta.option('ip_admin_eth0.netmask_admin_eth0', 0).value.set('255.255.255.0')
    assert meta.config('conf1').option('ip_admin_eth0.ip_admin_eth0').owner.get() == owners.meta1
    assert meta.config('conf1').option('ip_admin_eth0.netmask_admin_eth0', 0).owner.get() == owners.meta1
    #
    meta.option('ip_admin_eth0.netmask_admin_eth0', 0).value.set('255.255.0.0')
    assert meta.config('conf1').option('ip_admin_eth0.ip_admin_eth0').owner.get() == owners.meta1
    assert meta.config('conf1').option('ip_admin_eth0.netmask_admin_eth0', 0).owner.get() == owners.meta1
    #
    meta.config('conf1').option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.1.1'])
    assert meta.config('conf1').option('ip_admin_eth0.ip_admin_eth0').owner.get() == owners.user
    assert meta.config('conf1').option('ip_admin_eth0.netmask_admin_eth0', 0).owner.get() == owners.default


def test_meta_force_default():
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip", multi=True)
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "mask", multi=True, properties=('hidden',))
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    od = OptionDescription('root', '', [interface1])
    conf1 = Config(od, session_id='conf1')
    conf2 = Config(od, session_id='conf2')
    meta = MetaConfig([conf1, conf2])
    meta.property.read_write()
    meta.owner.set('meta1')
    assert meta.option('ip_admin_eth0.ip_admin_eth0').value.get() == []
    assert meta.config('conf1').option('ip_admin_eth0.ip_admin_eth0').value.get() == []
    assert meta.config('conf2').option('ip_admin_eth0.ip_admin_eth0').value.get() == []
    #
    errors = meta.value.set('ip_admin_eth0.ip_admin_eth0', ['192.168.1.1'])
    assert len(errors) == 0
    assert meta.option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.1']
    assert meta.config('conf1').option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.1']
    assert meta.config('conf2').option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.1']
    #
    meta.config('conf1').option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.1.2'])
    assert meta.option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.1']
    assert meta.config('conf1').option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.2']
    assert meta.config('conf2').option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.1']
    #
    errors = meta.value.set('ip_admin_eth0.ip_admin_eth0', ['192.168.1.3'])
    assert len(errors) == 0
    assert meta.option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.3']
    assert meta.config('conf1').option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.2']
    assert meta.config('conf2').option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.3']
    #
    errors = meta.value.set('ip_admin_eth0.ip_admin_eth0', ['192.168.1.4'], force_default=True)
    assert len(errors) == 0
    assert meta.option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.4']
    assert meta.config('conf1').option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.4']
    assert meta.config('conf2').option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.4']


def test_meta_force_dont_change_value():
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip", multi=True)
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "mask", multi=True, properties=('hidden',))
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    od = OptionDescription('root', '', [interface1])
    conf1 = Config(od, session_id='conf1')
    conf2 = Config(od, session_id='conf2')
    meta = MetaConfig([conf1, conf2])
    meta.property.read_write()
    meta.owner.set('meta1')
    assert meta.option('ip_admin_eth0.ip_admin_eth0').value.get() == []
    assert meta.config('conf1').option('ip_admin_eth0.ip_admin_eth0').value.get() == []
    assert meta.config('conf2').option('ip_admin_eth0.ip_admin_eth0').value.get() == []
    meta.config('conf1').option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.1.4'])
    assert meta.config('conf1').option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.4']
    assert meta.config('conf2').option('ip_admin_eth0.ip_admin_eth0').value.get() == []
    assert meta.config('conf1').option('ip_admin_eth0.ip_admin_eth0').owner.get() is owners.user
    assert meta.config('conf2').option('ip_admin_eth0.ip_admin_eth0').owner.isdefault()
    errors = meta.value.set('ip_admin_eth0.ip_admin_eth0', ['192.168.1.4'], force_dont_change_value=True)
    assert len(errors) == 0
    assert meta.option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.4']
    assert meta.config('conf1').option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.4']
    assert meta.config('conf2').option('ip_admin_eth0.ip_admin_eth0').value.get() == []
    assert meta.config('conf1').option('ip_admin_eth0.ip_admin_eth0').owner.get() is owners.user
    assert meta.config('conf2').option('ip_admin_eth0.ip_admin_eth0').owner.get() is owners.user


def test_meta_force_default_if_same():
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip", multi=True)
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "mask", multi=True, properties=('hidden',))
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    od = OptionDescription('root', '', [interface1])
    conf1 = Config(od, session_id='conf1')
    conf2 = Config(od, session_id='conf2')
    meta = MetaConfig([conf1, conf2])
    meta.property.read_write()
    meta.owner.set('meta1')
    #
    assert meta.option('ip_admin_eth0.ip_admin_eth0').value.get() == []
    assert meta.config('conf1').option('ip_admin_eth0.ip_admin_eth0').value.get() == []
    assert meta.config('conf2').option('ip_admin_eth0.ip_admin_eth0').value.get() == []
    #
    meta.config('conf1').option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.1.4'])
    assert meta.config('conf1').option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.4']
    assert meta.config('conf2').option('ip_admin_eth0.ip_admin_eth0').value.get() == []
    assert meta.config('conf1').option('ip_admin_eth0.ip_admin_eth0').owner.get() is owners.user
    assert meta.config('conf2').option('ip_admin_eth0.ip_admin_eth0').owner.isdefault()
    errors = meta.value.set('ip_admin_eth0.ip_admin_eth0', ['192.168.1.4'], force_default_if_same=True)
    assert len(errors) == 0
    assert meta.option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.4']
    assert meta.config('conf1').option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.4']
    assert meta.config('conf2').option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.4']
    assert meta.config('conf1').option('ip_admin_eth0.ip_admin_eth0').owner.get() is owners.meta1
    assert meta.config('conf2').option('ip_admin_eth0.ip_admin_eth0').owner.get() is owners.meta1
    #
    meta.config('conf1').option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.1.3'])
    assert meta.config('conf1').option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.3']
    assert meta.config('conf2').option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.4']
    assert meta.config('conf1').option('ip_admin_eth0.ip_admin_eth0').owner.get() is owners.user
    assert meta.config('conf2').option('ip_admin_eth0.ip_admin_eth0').owner.get() is owners.meta1
    errors = meta.value.set('ip_admin_eth0.ip_admin_eth0', ['192.168.1.5'], force_default_if_same=True)
    assert len(errors) == 0
    assert meta.option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.5']
    assert meta.config('conf1').option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.3']
    assert meta.config('conf2').option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.5']
    assert meta.config('conf1').option('ip_admin_eth0.ip_admin_eth0').owner.get() is owners.user
    assert meta.config('conf2').option('ip_admin_eth0.ip_admin_eth0').owner.get() is owners.meta1


def test_meta_force_default_if_same_and_dont_change():
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip", multi=True)
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "mask", multi=True, properties=('hidden',))
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    od = OptionDescription('root', '', [interface1])
    conf1 = Config(od, session_id='conf1')
    conf2 = Config(od, session_id='conf2')
    meta = MetaConfig([conf1, conf2])
    meta.property.read_write()
    meta.owner.set('meta1')
    #
    assert meta.option('ip_admin_eth0.ip_admin_eth0').value.get() == []
    assert meta.config('conf1').option('ip_admin_eth0.ip_admin_eth0').value.get() == []
    assert meta.config('conf2').option('ip_admin_eth0.ip_admin_eth0').value.get() == []
    #
    meta.config('conf1').option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.1.4'])
    assert meta.config('conf1').option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.4']
    assert meta.config('conf2').option('ip_admin_eth0.ip_admin_eth0').value.get() == []
    assert meta.config('conf1').option('ip_admin_eth0.ip_admin_eth0').owner.get() is owners.user
    assert meta.config('conf2').option('ip_admin_eth0.ip_admin_eth0').owner.isdefault()
    errors = meta.value.set('ip_admin_eth0.ip_admin_eth0', ['192.168.1.4'], force_default_if_same=True, force_dont_change_value=True)
    assert len(errors) == 0
    assert meta.option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.4']
    assert meta.config('conf1').option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.4']
    assert meta.config('conf2').option('ip_admin_eth0.ip_admin_eth0').value.get() == []
    assert meta.config('conf1').option('ip_admin_eth0.ip_admin_eth0').owner.get() is owners.meta1
    assert meta.config('conf2').option('ip_admin_eth0.ip_admin_eth0').owner.get() is owners.user
    #
    meta.config('conf1').option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.1.3'])
    assert meta.config('conf1').option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.3']
    assert meta.config('conf2').option('ip_admin_eth0.ip_admin_eth0').value.get() == []
    assert meta.config('conf1').option('ip_admin_eth0.ip_admin_eth0').owner.get() is owners.user
    assert meta.config('conf2').option('ip_admin_eth0.ip_admin_eth0').owner.get() is owners.user
    errors = meta.value.set('ip_admin_eth0.ip_admin_eth0', ['192.168.1.5'], force_default_if_same=True, force_dont_change_value=True)
    assert len(errors) == 0
    assert meta.option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.5']
    assert meta.config('conf1').option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.3']
    assert meta.config('conf2').option('ip_admin_eth0.ip_admin_eth0').value.get() == []
    assert meta.config('conf1').option('ip_admin_eth0.ip_admin_eth0').owner.get() is owners.user
    assert meta.config('conf2').option('ip_admin_eth0.ip_admin_eth0').owner.get() is owners.user


def test_meta_force_default_and_dont_change():
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip", multi=True)
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "mask", multi=True, properties=('hidden',))
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    od = OptionDescription('root', '', [interface1])
    conf1 = Config(od, session_id='rconf1')
    conf2 = Config(od, session_id='rconf2')
    meta = MetaConfig([conf1, conf2])
    meta.property.read_write()
    meta.owner.set('meta1')
    raises(ValueError, "meta.value.set('ip_admin_eth0.ip_admin_eth0', ['192.168.1.4'], force_default=True, force_dont_change_value=True)")


def test_meta_properties_meta():
    ip_admin_eth0 = NetworkOption('ip_admin_eth0', "ip", multi=True, default=['192.168.1.1'])
    netmask_admin_eth0 = NetmaskOption('netmask_admin_eth0', "mask", multi=True, properties=('disabled',))
    netmask_admin_eth0.impl_add_consistency('network_netmask', ip_admin_eth0)
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    od = OptionDescription('root', '', [interface1])
    conf1 = Config(od, session_id='conf1')
    conf2 = Config(od, session_id='conf2')
    conf1.property.read_write()
    conf2.property.read_write()
    meta = MetaConfig([conf1, conf2])
    meta.property.read_write()
    assert meta.config('conf1').value.dict() == {}


def test_meta_exception_meta():
    ip_admin_eth0 = NetworkOption('ip_admin_eth0', "ip", multi=True, default=['192.168.1.1'])
    netmask_admin_eth0 = NetmaskOption('netmask_admin_eth0', "mask", multi=True, callback=raise_exception)
    netmask_admin_eth0.impl_add_consistency('network_netmask', ip_admin_eth0)
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    od = OptionDescription('root', '', [interface1])
    conf1 = Config(od, session_id='conf1')
    conf2 = Config(od, session_id='conf2')
    meta = MetaConfig([conf1, conf2])
    meta.property.read_write()
    raises(Exception, "conf1.make_dict()")


def test_meta_properties_requires_mandatory():
    probes = BoolOption('probes', 'probes available', False)
    eth0_method = ChoiceOption('eth0_method', '', ('static', 'dhcp'), 'static')
    ip_address = IPOption('ip_address', '')
    ip_eth0 = IPOption('ip_eth0', "ip", requires=({'option': probes, 'expected': True, 'action': 'mandatory'},), callback=return_condition, callback_params=Params(kwargs={'val': ParamOption(ip_address), 'condition': ParamOption(eth0_method), 'expected': ParamValue('dhcp')}))
    ip_gw = IPOption('ip_gw', 'gw')
    ip_gw.impl_add_consistency('not_equal', ip_eth0)
    od = OptionDescription('root', '', [ip_gw, probes, eth0_method, ip_address, ip_eth0])
    conf1 = Config(od, session_id='conf1')
    conf1.property.read_write()
    meta = MetaConfig([conf1], 'meta')
    #
    meta.option('probes').value.set(True)
    meta.option('ip_address').value.set('1.1.1.1')
    meta.option('ip_gw').value.set('1.1.1.2')
    conf1.option('eth0_method').value.set('dhcp')
    conf1.property.read_only()
    assert conf1.value.dict(fullpath=True) == {'probes': True, 'eth0_method': 'dhcp', 'ip_address': '1.1.1.1', 'ip_eth0': '1.1.1.1', 'ip_gw': '1.1.1.2'}


def test_meta_callback():
    val1 = StrOption('val1', "", 'val')
    val2 = StrOption('val2', "", callback=return_value, callback_params=Params(ParamOption(val1)))
    val3 = StrOption('val3', "", callback=return_value, callback_params=Params(ParamValue('yes')))
    val4 = StrOption('val4', "", callback=return_value, callback_params=Params(kwargs={'value': ParamOption(val1)}))
    val5 = StrOption('val5', "", callback=return_value, callback_params=Params(kwargs={'value': ParamValue('yes')}))
    maconfig = OptionDescription('rootconfig', '', [val1, val2, val3, val4, val5])
    cfg = Config(maconfig, session_id='cfg')
    meta = MetaConfig([cfg])
    meta.property.read_write()
    assert meta.config('cfg').value.dict() == {'val3': 'yes', 'val2': 'val', 'val1': 'val', 'val5': 'yes', 'val4': 'val'}
    meta.config('cfg').option('val1').value.set('new')
    assert meta.config('cfg').value.dict() == {'val3': 'yes', 'val2': 'new', 'val1': 'new', 'val5': 'yes', 'val4': 'new'}
    meta.config('cfg').option('val1').value.reset()
    meta.option('val1').value.set('new')
    assert meta.config('cfg').value.dict() == {'val3': 'yes', 'val2': 'new', 'val1': 'new', 'val5': 'yes', 'val4': 'new'}
    meta.config('cfg').option('val4').value.set('new1')
    assert meta.config('cfg').value.dict() == {'val3': 'yes', 'val2': 'new', 'val1': 'new', 'val5': 'yes', 'val4': 'new1'}
    meta.config('cfg').option('val4').value.reset()
    meta.option('val4').value.set('new1')
    assert meta.config('cfg').value.dict() == {'val3': 'yes', 'val2': 'new', 'val1': 'new', 'val5': 'yes', 'val4': 'new1'}
    meta.option('val4').value.reset()


def test_meta_callback_follower():
    val = StrOption('val', "", default='val')
    val1 = StrOption('val1', "", multi=True, callback=return_value, callback_params=Params(ParamOption(val)))
    val3 = StrOption('val2', "", multi=True, callback=return_value, callback_params=Params(ParamOption(val1)))
    val4 = StrOption('val3', "", multi=True, callback=return_value, callback_params=Params(ParamOption(val1)))
    interface1 = Leadership('val1', '', [val1, val3, val4])
    od = OptionDescription('root', '', [interface1])
    maconfig = OptionDescription('rootconfig', '', [val, interface1])
    cfg = Config(maconfig, session_id='cfg1')
    meta = MetaConfig([cfg])
    meta.property.read_write()
    assert meta.config('cfg1').value.dict() == {'val1.val2': ['val'], 'val1.val1': ['val'], 'val1.val3': ['val'], 'val': 'val'}
    meta.config('cfg1').option('val').value.set('val1')
    assert meta.config('cfg1').value.dict() == {'val1.val2': ['val1'], 'val1.val1': ['val1'], 'val1.val3': ['val1'], 'val': 'val1'}
    meta.config('cfg1').option('val').value.reset()
    meta.option('val').value.set('val1')
    assert meta.config('cfg1').value.dict() == {'val1.val2': ['val1'], 'val1.val1': ['val1'], 'val1.val3': ['val1'], 'val': 'val1'}
    meta.option('val').value.reset()
    meta.config('cfg1').option('val1.val2', 0).value.set('val2')
    assert meta.config('cfg1').value.dict() == {'val1.val2': ['val2'], 'val1.val1': ['val'], 'val1.val3': ['val'], 'val': 'val'}
    meta.config('cfg1').option('val1.val2', 0).value.reset()
    assert meta.config('cfg1').value.dict() == {'val1.val2': ['val'], 'val1.val1': ['val'], 'val1.val3': ['val'], 'val': 'val'}
    meta.option('val1.val2', 0).value.set('val2')
    assert meta.config('cfg1').value.dict() == {'val1.val2': ['val2'], 'val1.val1': ['val'], 'val1.val3': ['val'], 'val': 'val'}
    meta.config('cfg1').option('val1.val3', 0).value.set('val6')
    assert meta.config('cfg1').value.dict() == {'val1.val2': ['val2'], 'val1.val1': ['val'], 'val1.val3': ['val6'], 'val': 'val'}
    meta.option('val1.val2', 0).value.reset()
    meta.config('cfg1').option('val1.val3', 0).value.reset()
    meta.config('cfg1').option('val1.val1').value.set(['val3'])
    assert meta.config('cfg1').value.dict() == {'val1.val2': ['val3'], 'val1.val1': ['val3'], 'val1.val3': ['val3'], 'val': 'val'}
    meta.config('cfg1').option('val1.val1').value.reset()
    assert meta.config('cfg1').value.dict() == {'val1.val2': ['val'], 'val1.val1': ['val'], 'val1.val3': ['val'], 'val': 'val'}
    meta.option('val1.val1').value.set(['val3'])
    assert meta.config('cfg1').value.dict() == {'val1.val2': ['val3'], 'val1.val1': ['val3'], 'val1.val3': ['val3'], 'val': 'val'}
    meta.config('cfg1').option('val1.val2', 0).value.set('val2')
    assert meta.config('cfg1').value.dict() == {'val1.val2': ['val2'], 'val1.val1': ['val3'], 'val1.val3': ['val3'], 'val': 'val'}
    meta.option('val1.val1').value.set(['val3', 'rah'])
    assert meta.config('cfg1').value.dict() == {'val1.val2': ['val2', 'rah'], 'val1.val1': ['val3', 'rah'], 'val1.val3': ['val3', 'rah'], 'val': 'val'}
    meta.option('val1.val1').value.pop(1)
    meta.option('val1.val1').value.set(['val4'])
    assert meta.config('cfg1').value.dict() == {'val1.val2': ['val2'], 'val1.val1': ['val4'], 'val1.val3': ['val4'], 'val': 'val'}


def test_meta_reset():
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip", multi=True)
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "mask", multi=True, properties=('hidden',))
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    od = OptionDescription('root', '', [interface1])
    conf1 = Config(od, session_id='conf1')
    conf2 = Config(od, session_id='conf2')
    meta = MetaConfig([conf1, conf2])
    meta.property.read_write()
    meta.owner.set('meta1')
    assert meta.option('ip_admin_eth0.ip_admin_eth0').value.get() == []
    assert meta.config('conf1').option('ip_admin_eth0.ip_admin_eth0').value.get() == []
    assert meta.config('conf2').option('ip_admin_eth0.ip_admin_eth0').value.get() == []
    errors = meta.value.set('ip_admin_eth0.ip_admin_eth0', ['192.168.1.1'])
    assert len(errors) == 0
    assert meta.option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.1']
    assert meta.config('conf1').option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.1']
    assert meta.config('conf2').option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.1']
    meta.config('conf1').option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.1.2'])
    assert meta.option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.1']
    assert meta.config('conf1').option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.2']
    assert meta.config('conf2').option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.1']
    meta.value.reset('ip_admin_eth0.ip_admin_eth0')
    assert meta.option('ip_admin_eth0.ip_admin_eth0').value.get() == []
    assert meta.config('conf1').option('ip_admin_eth0.ip_admin_eth0').value.get() == []
    assert meta.config('conf2').option('ip_admin_eth0.ip_admin_eth0').value.get() == []


def test_meta_properties_meta_copy():
    ip_admin_eth0 = NetworkOption('ip_admin_eth0', "ip", multi=True, default=['192.168.1.1'])
    netmask_admin_eth0 = NetmaskOption('netmask_admin_eth0', "mask", multi=True, properties=('disabled',))
    interface1 = OptionDescription('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    conf1 = Config(interface1, session_id='conf1')
    conf2 = Config(interface1, session_id='conf2')
    conf1.property.read_write()
    conf2.property.read_write()
    meta = MetaConfig([conf1, conf2], session_id='meta1')
    meta.property.read_write()

    conf3 = meta.config('conf1').config.copy(session_id='conf3')
    meta2 = conf3.config.metaconfig()
    assert meta.config.name() == meta2.config.name()

    assert meta.config('conf1').value.dict() == {'ip_admin_eth0': ['192.168.1.1']}
    assert meta.config('conf2').value.dict() == {'ip_admin_eth0': ['192.168.1.1']}
    assert meta.config('conf3').value.dict() == {'ip_admin_eth0': ['192.168.1.1']}
    meta.option('ip_admin_eth0').value.set(['192.168.1.2'])
    assert meta.config('conf1').value.dict() == {'ip_admin_eth0': ['192.168.1.2']}
    assert meta.config('conf2').value.dict() == {'ip_admin_eth0': ['192.168.1.2']}
    assert meta.config('conf3').value.dict() == {'ip_admin_eth0': ['192.168.1.2']}
    ret = meta.value.set('ip_admin_eth0', ['192.168.1.3'], force_default_if_same=True)
    assert meta.config('conf1').value.dict() == {'ip_admin_eth0': ['192.168.1.3']}
    assert meta.config('conf2').value.dict() == {'ip_admin_eth0': ['192.168.1.3']}
    assert meta.config('conf3').value.dict() == {'ip_admin_eth0': ['192.168.1.3']}


def test_meta_properties_meta_deepcopy():
    ip_admin_eth0 = NetworkOption('ip_admin_eth0', "ip", multi=True, default=['192.168.1.1'])
    netmask_admin_eth0 = NetmaskOption('netmask_admin_eth0', "mask", multi=True,
                                       properties=('disabled',))
    interface1 = OptionDescription('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    conf1 = Config(interface1, session_id='conf1')
    conf2 = Config(interface1, session_id='conf2')
    conf1.property.read_write()
    conf2.property.read_write()
    meta = MetaConfig([conf1, conf2])
    meta.permissive.set(frozenset({'hidden'}))
    meta.property.read_write()

    meta2 = meta.config('conf1').config.deepcopy(session_id='conf3')
    assert meta != meta2
    assert meta.permissive.get() == meta2.permissive.get()

    assert meta.config('conf1').value.dict() == {'ip_admin_eth0': ['192.168.1.1']}
    assert meta.config('conf2').value.dict() == {'ip_admin_eth0': ['192.168.1.1']}
    assert meta2.config('conf3').value.dict() == {'ip_admin_eth0': ['192.168.1.1']}
    meta.option('ip_admin_eth0').value.set(['192.168.1.2'])
    assert meta.config('conf1').value.dict() == {'ip_admin_eth0': ['192.168.1.2']}
    assert meta.config('conf2').value.dict() == {'ip_admin_eth0': ['192.168.1.2']}
    assert meta2.config('conf3').value.dict() == {'ip_admin_eth0': ['192.168.1.1']}
    meta.value.set('ip_admin_eth0', ['192.168.1.3'], force_default_if_same=True)
    assert meta.config('conf1').value.dict() == {'ip_admin_eth0': ['192.168.1.3']}
    assert meta.config('conf2').value.dict() == {'ip_admin_eth0': ['192.168.1.3']}
    assert meta2.config('conf3').value.dict() == {'ip_admin_eth0': ['192.168.1.1']}


def test_meta_properties_submeta_deepcopy():
    ip_admin_eth0 = NetworkOption('ip_admin_eth0', "ip", multi=True, default=['192.168.1.1'])
    netmask_admin_eth0 = NetmaskOption('netmask_admin_eth0', "mask", multi=True,
                                       properties=('disabled',))
    interface1 = OptionDescription('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    conf1 = Config(interface1, session_id='conf1')
    conf1.property.read_write()
    meta1 = MetaConfig([conf1], session_id='meta1')
    meta2 = MetaConfig([meta1], session_id='meta2')
    meta_copy = conf1.config.deepcopy(session_id='conf2',
                                      metaconfig_prefix='copy_')
    assert meta_copy.config.name() == 'copy_meta2'
    assert meta_copy.config('copy_meta1').config.name() == 'copy_meta1'
    assert meta_copy.config('copy_meta1').config('conf2').config.name() == 'conf2'


def test_meta_properties_submeta_deepcopy_owner():
    ip_admin_eth0 = NetworkOption('ip_admin_eth0', "ip")
    netmask_admin_eth0 = NetmaskOption('netmask_admin_eth0', "mask")
    interface1 = OptionDescription('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    conf1 = Config(interface1, session_id='conf1')
    conf1.owner.set('conf1_user')
    conf1.property.read_write()
    meta1 = MetaConfig([conf1], session_id='meta1')
    meta1.owner.set('meta1_user')
    meta2 = MetaConfig([meta1], session_id='meta2')
    meta2.owner.set('meta2_user')
    #
    conf1.option('ip_admin_eth0').value.set('192.168.0.1')
    assert conf1.option('ip_admin_eth0').owner.get() == 'conf1_user'
    meta1.option('ip_admin_eth0').value.set('192.168.0.2')
    assert meta1.option('ip_admin_eth0').owner.get() == 'meta1_user'
    meta2.option('ip_admin_eth0').value.set('192.168.0.3')
    assert meta2.option('ip_admin_eth0').owner.get() == 'meta2_user'
    #
    meta2_copy = conf1.config.deepcopy(session_id='conf2',
                                       metaconfig_prefix='copy_')
    meta2_copy.option('netmask_admin_eth0').value.set('255.255.255.255')
    assert meta2_copy.option('ip_admin_eth0').value.get() == '192.168.0.3'
    assert meta2_copy.option('ip_admin_eth0').owner.get() == 'meta2_user'
    assert meta2_copy.option('netmask_admin_eth0').owner.get() == 'meta2_user'
    #
    meta1_copy = meta2_copy.config('copy_meta1')
    meta1_copy.option('netmask_admin_eth0').value.set('255.255.255.255')
    assert meta1_copy.option('ip_admin_eth0').value.get() == '192.168.0.2'
    assert meta1_copy.option('ip_admin_eth0').owner.get() == 'meta1_user'
    assert meta1_copy.option('netmask_admin_eth0').owner.get() == 'meta1_user'
    #
    conf2 = meta1_copy.config('conf2')
    conf2.owner.set('conf2_user')
    conf2.option('netmask_admin_eth0').value.set('255.255.255.255')
    assert conf2.option('netmask_admin_eth0').owner.get() == 'conf2_user'
    assert conf2.option('ip_admin_eth0').value.get() == '192.168.0.1'
    assert conf2.option('ip_admin_eth0').owner.get() == 'conf1_user'


def test_meta_properties_meta_set_value():
    ip_admin_eth0 = NetworkOption('ip_admin_eth0', "ip", multi=True, default=['192.168.1.1'])
    netmask_admin_eth0 = NetmaskOption('netmask_admin_eth0', "mask", multi=True, properties=('disabled',))
    interface1 = OptionDescription('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    conf1 = Config(interface1, session_id='conf1')
    conf2 = Config(interface1, session_id='conf2')
    conf1.property.read_write()
    conf2.property.read_write()
    meta = MetaConfig([conf1, conf2])
    meta.property.read_write()
    assert meta.config('conf1').value.dict() == {'ip_admin_eth0': ['192.168.1.1']}
    ret = meta.value.set('netmask_admin_eth0', ['255.255.255.255'], only_config=True)
    assert len(ret) == 2
    assert isinstance(ret[0], PropertiesOptionError)
    assert isinstance(ret[1], PropertiesOptionError)
    del ret[1]
    del ret[0]
    del ret
    ret = meta.value.set('netmask_admin_eth0', ['255.255.255.255'], force_default=True)
    assert len(ret) == 1
    assert isinstance(ret[0], PropertiesOptionError)
    del ret[0]
    del ret
    ret = meta.value.set('netmask_admin_eth0', ['255.255.255.255'], force_dont_change_value=True)
    assert len(ret) == 3
    assert isinstance(ret[0], PropertiesOptionError)
    assert isinstance(ret[1], PropertiesOptionError)
    assert isinstance(ret[2], PropertiesOptionError)
    del ret[2]
    del ret[1]
    del ret[0]
    del ret
    ret = meta.value.set('netmask_admin_eth0', ['255.255.255.255'], force_default_if_same=True)
    assert len(ret) == 1
    assert isinstance(ret[0], PropertiesOptionError)
    del ret[0]
    del ret
    ret = meta.value.set('ip_admin_eth0', '255.255.255.255', only_config=True)
    assert len(ret) == 2
    assert isinstance(ret[0], ValueError)
    assert isinstance(ret[1], ValueError)
    del ret[1]
    del ret[0]
    del ret
    ret = meta.value.set('ip_admin_eth0', '255.255.255.255', force_default=True)
    assert len(ret) == 1
    assert isinstance(ret[0], ValueError)
    del ret[0]
    del ret
    ret = meta.value.set('ip_admin_eth0', '255.255.255.255', force_dont_change_value=True)
    assert len(ret) == 1
    assert isinstance(ret[0], ValueError)
    del ret[0]
    del ret
    ret = meta.value.set('ip_admin_eth0', '255.255.255.255', force_default_if_same=True)
    assert len(ret) == 1
    assert isinstance(ret[0], ValueError)
    del ret[0]
    del ret


def test_metaconfig_force_metaconfig_on_freeze():
    dummy1 = StrOption('dummy1', 'doc dummy', default='default', properties=('force_metaconfig_on_freeze',))
    group = OptionDescription('group', '', [dummy1])
    config = Config(group, session_id='config')
    config.owner.set(owners.config)
    meta1 = MetaConfig([config], session_id='meta1')
    meta1.owner.set(owners.meta1)
    meta2 = MetaConfig([meta1], session_id='meta2')
    meta2.owner.set(owners.meta2)
    config.property.read_write()

    config.option('dummy1').property.add('frozen')
    #
    assert config.option('dummy1').value.get() == 'default'
    assert config.option('dummy1').owner.get() == 'default'
    #
    meta2.option('dummy1').value.set('meta2')
    #
    assert config.option('dummy1').value.get() == 'meta2'
    assert config.option('dummy1').owner.get() == 'meta2'
    #
    config.option('dummy1').property.pop('frozen')
    config.option('dummy1').value.set('config')
    config.option('dummy1').property.add('frozen')
    #
    assert config.option('dummy1').value.get() == 'meta2'
    assert config.option('dummy1').owner.get() == 'meta2'
    #
    meta1.option('dummy1').value.set('meta1')
    #
    assert config.option('dummy1').value.get() == 'meta1'
    assert config.option('dummy1').owner.get() == 'meta1'
    #
    config.option('dummy1').property.pop('frozen')
    assert config.option('dummy1').value.get() == 'config'
    assert config.option('dummy1').owner.get() == 'config'


def test_metaconfig_force_metaconfig_on_freeze_option():
    dummy1 = StrOption('dummy1', 'doc dummy', default='default')
    dummy2 = StrOption('dummy2', 'doc dummy', default='default', properties=('force_default_on_freeze',))
    group = OptionDescription('group', '', [dummy1, dummy2])
    config = Config(group, session_id='config')
    config.owner.set(owners.config)
    meta1 = MetaConfig([config], session_id='meta1')
    meta1.owner.set(owners.meta1)
    meta2 = MetaConfig([meta1], session_id='meta2')
    meta2.owner.set(owners.meta2)
    config.property.read_write()

    config.option('dummy1').property.add('frozen')
    config.option('dummy1').property.add('force_metaconfig_on_freeze')
    config.option('dummy2').property.add('frozen')
    #
    assert config.option('dummy1').value.get() == 'default'
    assert config.option('dummy1').owner.get() == 'default'
    assert config.option('dummy2').value.get() == 'default'
    assert config.option('dummy2').owner.get() == 'default'
    #
    meta2.option('dummy1').value.set('meta2')
    meta2.option('dummy2').value.set('meta2')
    #
    assert config.option('dummy1').value.get() == 'meta2'
    assert config.option('dummy1').owner.get() == 'meta2'
    assert config.option('dummy2').value.get() == 'default'
    assert config.option('dummy2').owner.get() == 'default'
    #
    config.option('dummy1').property.pop('frozen')
    config.option('dummy2').property.pop('frozen')
    config.option('dummy1').value.set('config')
    config.option('dummy2').value.set('config')
    config.option('dummy1').property.add('frozen')
    config.option('dummy2').property.add('frozen')
    #
    assert config.option('dummy1').value.get() == 'meta2'
    assert config.option('dummy1').owner.get() == 'meta2'
    assert config.option('dummy2').value.get() == 'default'
    assert config.option('dummy2').owner.get() == 'default'
    #
    meta1.option('dummy1').value.set('meta1')
    meta1.option('dummy2').value.set('meta1')
    #
    assert config.option('dummy1').value.get() == 'meta1'
    assert config.option('dummy1').owner.get() == 'meta1'
    assert config.option('dummy2').value.get() == 'default'
    assert config.option('dummy2').owner.get() == 'default'
    #
    meta1.option('dummy1').property.add('force_metaconfig_on_freeze')
    assert config.option('dummy1').value.get() == 'meta2'
    assert config.option('dummy1').owner.get() == 'meta2'
    #
    meta2.option('dummy1').property.add('force_metaconfig_on_freeze')
    assert config.option('dummy1').value.get() == 'default'
    assert config.option('dummy1').owner.get() == 'default'
    #
    meta1.option('dummy1').property.pop('force_metaconfig_on_freeze')
    assert config.option('dummy1').value.get() == 'meta1'
    assert config.option('dummy1').owner.get() == 'meta1'
    #
    config.option('dummy1').property.pop('frozen')
    assert config.option('dummy1').value.get() == 'config'
    assert config.option('dummy1').owner.get() == 'config'
