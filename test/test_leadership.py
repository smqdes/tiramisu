# coding: utf-8
from .autopath import do_autopath
do_autopath()
from py.test import raises

from tiramisu.setting import groups, owners
from tiramisu import ChoiceOption, BoolOption, IntOption, IPOption, NetmaskOption, \
    StrOption, OptionDescription, Leadership, Config
from tiramisu.error import LeadershipError, PropertiesOptionError, APIError, ConfigError
from tiramisu.storage import list_sessions


def teardown_function(function):
    assert list_sessions() == [], 'session list is not empty when leaving "{}"'.format(function.__name__)


def compare(calculated, expected):
    def convert_list(val):
        if isinstance(val, list):
            val = tuple(val)
        return val
    # convert to tuple
    for idx in range(len(calculated[0])):
        right_idx = expected[0].index(calculated[0][idx])
        for typ in range(4):
            assert convert_list(calculated[typ][idx]) == expected[typ][right_idx]


def make_description():
    numero_etab = StrOption('numero_etab', "identifiant de l'établissement")
    nom_machine = StrOption('nom_machine', "nom de la machine", default="eoleng")
    nombre_interfaces = IntOption('nombre_interfaces', "nombre d'interfaces à activer",
                                  default=1)
    activer_proxy_client = BoolOption('activer_proxy_client', "utiliser un proxy",
                                      default=False)
    mode_conteneur_actif = BoolOption('mode_conteneur_actif', "le serveur est en mode conteneur",
                                      default=False)
    mode_conteneur_actif2 = BoolOption('mode_conteneur_actif2', "le serveur est en mode conteneur2",
                                       default=False, properties=('hidden',))

    adresse_serveur_ntp = StrOption('serveur_ntp', "adresse serveur ntp", multi=True)
    time_zone = ChoiceOption('time_zone', 'fuseau horaire du serveur',
                             ('Paris', 'Londres'), 'Paris')

    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé")
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "masque du sous-réseau")

    leader = OptionDescription('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    interface1 = OptionDescription('interface1', '', [leader])
    interface1.impl_set_group_type(groups.family)

    general = OptionDescription('general', '', [numero_etab, nom_machine,
                                nombre_interfaces, activer_proxy_client,
                                mode_conteneur_actif, mode_conteneur_actif2,
                                adresse_serveur_ntp, time_zone])
    general.impl_set_group_type(groups.family)
    new = OptionDescription('new', '', [], properties=('hidden',))
    new.impl_set_group_type(groups.family)
    creole = OptionDescription('creole', 'first tiramisu configuration', [general, interface1, new])
    descr = OptionDescription('baseconfig', 'baseconifgdescr', [creole])
    return descr


def test_base_config():
    descr = make_description()
    api = Config(descr)
    api.property.read_write()
    assert api.option('creole.general.activer_proxy_client').value.get() is False
    assert api.option('creole.general.nom_machine').value.get() == "eoleng"
    assert api.option.find('nom_machine', first=True).value.get() == "eoleng"
    result = {'general.numero_etab': None, 'general.nombre_interfaces': 1,
              'general.serveur_ntp': [], 'interface1.ip_admin_eth0.ip_admin_eth0': None,
              'general.mode_conteneur_actif': False, 'general.time_zone': 'Paris',
              'interface1.ip_admin_eth0.netmask_admin_eth0': None, 'general.nom_machine':
              'eoleng', 'general.activer_proxy_client': False}
    assert api.option('creole').value.dict() == result
    result = {'serveur_ntp': [], 'mode_conteneur_actif': False,
              'ip_admin_eth0': None, 'time_zone': 'Paris', 'numero_etab': None,
              'netmask_admin_eth0': None, 'nom_machine': 'eoleng', 'activer_proxy_client':
              False, 'nombre_interfaces': 1}
    assert api.option('creole').value.dict(flatten=True) == result


def test_make_dict_filter():
    descr = make_description()
    api = Config(descr)
    api.property.read_write()
    subresult = {'numero_etab': None, 'nombre_interfaces': 1,
                 'serveur_ntp': [], 'mode_conteneur_actif': False,
                 'time_zone': 'Paris', 'nom_machine': 'eoleng',
                 'activer_proxy_client': False}
    result = {}
    for key, value in subresult.items():
        result['general.' + key] = value
    assert api.option('creole').value.dict(withoption='numero_etab') == result
    raises(AttributeError, "api.option('creole').value.dict(withoption='numero_etab', withvalue='toto')")
    assert api.option('creole').value.dict(withoption='numero_etab', withvalue=None) == result
    assert api.option('creole.general').value.dict(withoption='numero_etab') == subresult


def test_get_group_type():
    descr = make_description()
    api = Config(descr)
    api.property.read_write()
    grp = api.option('creole.general')
    assert grp.group_type() == groups.family
    assert grp.group_type() == 'family'
    assert isinstance(grp.group_type(), groups.GroupType)


def test_iter_on_groups():
    descr = make_description()
    api = Config(descr)
    api.property.read_write()
    result = api.option('creole').list('optiondescription',
                                       group_type=groups.family)
    group_names = [res.option.name() for res in result]
    assert group_names == ['general', 'interface1']
    for i in api.option('creole').list('optiondescription',
                                       group_type=groups.family):
        #test StopIteration
        break
    result = api.option('creole').list('option',
                                       group_type=groups.family)
    assert list(result) == []
    result = api.option('creole.general').list('optiondescription',
                                                group_type=groups.family)
    assert list(result) == []

def test_list_recursive():
    descr = make_description()
    api = Config(descr)
    api.property.read_write()
    result = api.option('creole').list('all')
    group_names = [res.option.name() for res in result]
    assert group_names == ['general', 'interface1']
    #
    result = api.option.list(recursive=True)
    group_names = [res.option.name() for res in result]
    assert group_names == ['numero_etab', 'nom_machine', 'nombre_interfaces',
                           'activer_proxy_client', 'mode_conteneur_actif',
                           'serveur_ntp', 'time_zone', 'ip_admin_eth0',
                           'netmask_admin_eth0']
    result = list(api.option.list(recursive=True, type='optiondescription'))
    group_names = [res.option.name() for res in result]
    assert group_names == ['general', 'ip_admin_eth0', 'interface1', 'creole']


def test_iter_on_groups_force_permissive():
    descr = make_description()
    api = Config(descr)
    api.property.read_write()
    api.permissive.set(frozenset(['hidden']))
    result = api.forcepermissive.option('creole.general').list()
    group_names = [res.option.name() for res in result]
    ass = ['numero_etab', 'nom_machine', 'nombre_interfaces',
           'activer_proxy_client', 'mode_conteneur_actif',
           'mode_conteneur_actif2', 'serveur_ntp', 'time_zone']
    assert group_names == ass
    # mode_conteneur_actif2 is not visible is not forcepermissive
    result = api.option('creole.general').list()
    group_names = [res.option.name() for res in result]
    ass.remove('mode_conteneur_actif2')
    assert group_names == ass


def test_iter_group_on_groups_force_permissive():
    descr = make_description()
    api = Config(descr)
    api.property.read_write()
    api.permissive.set(frozenset(['hidden']))
    result = api.forcepermissive.option('creole').list(type='optiondescription',
                                                       group_type=groups.family)
    group_names = [res.option.name() for res in result]
    assert group_names == ['general', 'interface1', 'new']


def test_iter_on_groups_props():
    descr = make_description()
    api = Config(descr)
    api.property.read_write()
    api.option('creole.interface1').property.add('disabled')
    result = api.option('creole').list(type='optiondescription',
                                       group_type=groups.family)
    group_names = [res.option.name() for res in result]
    assert group_names == ['general']


def test_iter_on_empty_group():
    api = Config(OptionDescription("name", "descr", []))
    api.property.read_write()
    result = list(api.option.list(type='optiondescription'))
    assert result == []


def test_iter_not_group():
    api = Config(OptionDescription("name", "descr", []))
    api.property.read_write()
    try:
        list(api.option.list(type='optiondescription', group_type='family'))
    except AssertionError:
        pass
    else:
        raise Exception('must raise')


def test_groups_with_leader():
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé", multi=True)
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "masque du sous-réseau", multi=True)
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    assert interface1.impl_get_group_type() == groups.leadership


def test_groups_is_leader():
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé", multi=True)
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "masque du sous-réseau", multi=True, default_multi='value')
    interface1 = Leadership('leadership', '', [ip_admin_eth0, netmask_admin_eth0])
    var = StrOption('var', "ip réseau autorisé", multi=True)
    od2 = OptionDescription('od2', '', [var])
    od1 = OptionDescription('od', '', [interface1, od2])
    api = Config(od1)
    assert not api.option('od2').option.isleadership()
    assert api.option('leadership').option.isleadership()
    assert not api.option('od2.var').option.isleader()
    assert not api.option('od2.var').option.isfollower()
    assert api.option('leadership.ip_admin_eth0').option.ismulti()
    assert api.option('leadership.netmask_admin_eth0').option.ismulti()
    assert not api.option('leadership.ip_admin_eth0').option.issubmulti()
    assert not api.option('leadership.netmask_admin_eth0').option.issubmulti()
    assert api.option('leadership.ip_admin_eth0').option.isleader()
    assert not api.option('leadership.ip_admin_eth0').option.isfollower()
    assert not api.option('leadership.netmask_admin_eth0').option.isleader()
    assert api.option('leadership.netmask_admin_eth0').option.isfollower()
    assert api.option('leadership.netmask_admin_eth0').option.path() == 'leadership.netmask_admin_eth0'
    assert api.option('leadership.netmask_admin_eth0').option.defaultmulti() == 'value'


def test_groups_with_leader_in_root():
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé", multi=True)
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "masque du sous-réseau", multi=True)
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    interface1
    raises(ConfigError, "Config(interface1)")


def test_groups_with_leader_in_config():
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé", multi=True)
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "masque du sous-réseau", multi=True)
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    od = OptionDescription('root', '', [interface1])
    Config(od)
    assert interface1.impl_get_group_type() == groups.leadership


def test_groups_with_leader_make_dict():
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé", multi=True)
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "masque du sous-réseau", multi=True)
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    od = OptionDescription('root', '', [interface1])
    api = Config(od)
    assert api.value.dict() == {'ip_admin_eth0.ip_admin_eth0': [], 'ip_admin_eth0.netmask_admin_eth0': []}
    assert api.option('ip_admin_eth0.ip_admin_eth0').value.len() == 0
    assert api.option('ip_admin_eth0.netmask_admin_eth0').value.len() == 0
    api.option('ip_admin_eth0.ip_admin_eth0').value.set(['ip1', 'ip2'])
    assert api.option('ip_admin_eth0.ip_admin_eth0').value.len() == 2
    assert api.option('ip_admin_eth0.netmask_admin_eth0').value.len() == 2
    assert api.value.dict() == {'ip_admin_eth0.ip_admin_eth0': ['ip1', 'ip2'], 'ip_admin_eth0.netmask_admin_eth0': [None, None]}


def test_groups_with_leader_default_value():
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé", multi=True)
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "masque du sous-réseau", multi=True)
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    od = OptionDescription('root', '', [interface1])
    api = Config(od)
    assert api.option('ip_admin_eth0.ip_admin_eth0').value.get() == []
    assert api.option('ip_admin_eth0.ip_admin_eth0').value.default() == []
    api.option('ip_admin_eth0.ip_admin_eth0').value.set(['ip1', 'ip2'])
    assert api.option('ip_admin_eth0.ip_admin_eth0').value.get() == ['ip1', 'ip2']
    assert api.option('ip_admin_eth0.ip_admin_eth0').value.default() == []


def test_groups_with_leader_default_value_2():
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé", ['ip1', 'ip2'], multi=True)
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "masque du sous-réseau", default_multi='netmask1', multi=True)
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    od = OptionDescription('root', '', [interface1])
    api = Config(od)
    assert api.option('ip_admin_eth0.ip_admin_eth0').value.get() == ['ip1', 'ip2']
    assert api.option('ip_admin_eth0.ip_admin_eth0').value.default() == ['ip1', 'ip2']
    api.option('ip_admin_eth0.ip_admin_eth0').value.set(['ip3', 'ip4'])
    assert api.option('ip_admin_eth0.ip_admin_eth0').value.get() == ['ip3', 'ip4']
    assert api.option('ip_admin_eth0.ip_admin_eth0').value.default() == ['ip1', 'ip2']
    #
    assert api.option('ip_admin_eth0.netmask_admin_eth0', 0).value.get() == 'netmask1'
    assert api.option('ip_admin_eth0.netmask_admin_eth0', 1).value.get() == 'netmask1'
    assert api.option('ip_admin_eth0.netmask_admin_eth0', 0).value.default() == 'netmask1'
    assert api.option('ip_admin_eth0.netmask_admin_eth0', 1).value.default() == 'netmask1'
    assert api.option('ip_admin_eth0.netmask_admin_eth0').value.default() == ['netmask1', 'netmask1']
    api.option('ip_admin_eth0.netmask_admin_eth0', 1).value.set('netmask2')
    assert api.option('ip_admin_eth0.netmask_admin_eth0', 0).value.get() == 'netmask1'
    assert api.option('ip_admin_eth0.netmask_admin_eth0', 1).value.get() == 'netmask2'
    assert api.option('ip_admin_eth0.netmask_admin_eth0', 0).value.default() == 'netmask1'
    assert api.option('ip_admin_eth0.netmask_admin_eth0', 1).value.default() == 'netmask1'
    assert api.option('ip_admin_eth0.netmask_admin_eth0').value.default() == ['netmask1', 'netmask1']


def test_groups_with_leader_hidden_in_config():
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé", multi=True, properties=('hidden',))
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "masque du sous-réseau", multi=True, properties=('hidden',))
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    od = OptionDescription('root', '', [interface1])
    api = Config(od)
    api.property.read_write()
    api.permissive.set(frozenset(['hidden']))
    assert api.forcepermissive.option('ip_admin_eth0.ip_admin_eth0').value.get() == []
    api.forcepermissive.option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.1.1'])
    assert api.forcepermissive.option('ip_admin_eth0.netmask_admin_eth0', 0).value.get() is None
    raises(PropertiesOptionError, "api.option('ip_admin_eth0.ip_admin_eth0').value.get()")
    raises(PropertiesOptionError, "api.option('ip_admin_eth0.netmask_admin_eth0', 0).value.get()")


def test_groups_with_leader_hidden_in_config2():
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé", multi=True)
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "masque du sous-réseau", multi=True, properties=('hidden',))
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    od = OptionDescription('root', '', [interface1])
    api = Config(od)
    api.property.read_write()
    api.permissive.set(frozenset(['hidden']))
    assert api.forcepermissive.option('ip_admin_eth0.ip_admin_eth0').value.get() == []
    assert api.option('ip_admin_eth0.ip_admin_eth0').value.get() == []
    api.forcepermissive.option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.1.1'])
    raises(PropertiesOptionError, "api.option('ip_admin_eth0.netmask_admin_eth0', 0).value.get()")
    api.option('ip_admin_eth0.ip_admin_eth0').value.reset()
    assert api.forcepermissive.option('ip_admin_eth0.ip_admin_eth0').value.get() == []
    #del
    api.option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.1.1'])
    api.property.pop('hidden')
    assert api.forcepermissive.option('ip_admin_eth0.netmask_admin_eth0', 0).value.get() is None
    api.forcepermissive.option('ip_admin_eth0.netmask_admin_eth0', 0).value.set('255.255.255.0')
    assert api.forcepermissive.option('ip_admin_eth0.netmask_admin_eth0', 0).value.get() == '255.255.255.0'
    api.property.add('hidden')
    api.option('ip_admin_eth0.ip_admin_eth0').value.reset()
    api.option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.1.1'])
    api.property.pop('hidden')
    assert api.forcepermissive.option('ip_admin_eth0.netmask_admin_eth0', 0).value.get() is None


def test_groups_with_leader_reset_empty():
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé", multi=True)
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "masque du sous-réseau", multi=True)
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    od_ = OptionDescription('root', '', [interface1])
    api = Config(od_)
    api.property.read_write()
    api.option('ip_admin_eth0.ip_admin_eth0').value.reset()
    raises(LeadershipError, "api.option('ip_admin_eth0.netmask_admin_eth0', 0).value.reset()")
    api.option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.1.1'])
    api.option('ip_admin_eth0.netmask_admin_eth0', 0).value.reset()


def test_groups_with_leader_reset_out_of_range():
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé", multi=True)
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "masque du sous-réseau", multi=True)
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    od_ = OptionDescription('root', '', [interface1])
    api = Config(od_)
    api.property.read_write()
    api.option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.1.1'])
    api.forcepermissive.option('ip_admin_eth0.netmask_admin_eth0', 0).value.set('255.255.255.0')
    api.option('ip_admin_eth0.netmask_admin_eth0', 0).value.reset()
    raises(LeadershipError, "api.option('ip_admin_eth0.netmask_admin_eth0', 1).value.reset()")
    raises(IndexError, "api.option('ip_admin_eth0.ip_admin_eth0').value.pop(1)")


def test_groups_with_leader_hidden_in_config3():
    #if leader is hidden, follower are hidden too
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé", multi=True, properties=('hidden',))
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "masque du sous-réseau", multi=True)
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    od = OptionDescription('root', '', [interface1])
    api = Config(od)
    api.property.read_write()
    api.permissive.set(frozenset(['hidden']))
    assert api.forcepermissive.option('ip_admin_eth0.ip_admin_eth0').value.get() == []
    api.forcepermissive.option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.1.1'])
    assert api.forcepermissive.option('ip_admin_eth0.netmask_admin_eth0', 0).value.get() is None
    raises(PropertiesOptionError, "api.option('ip_admin_eth0.ip_admin_eth0').value.get()")
    raises(PropertiesOptionError, "api.option('ip_admin_eth0.netmask_admin_eth0', 0).value.get()")


def test_allowed_groups():
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé", multi=True)
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "masque du sous-réseau", multi=True)
    interface1 = OptionDescription('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    interface1
    raises(ValueError, "interface1.impl_set_group_type('toto')")


def test_values_with_leader_disabled_leader():
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé", multi=True)
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "masque du sous-réseau", multi=True)
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    maconfig = OptionDescription('toto', '', [interface1])
    api = Config(maconfig)
    api.property.read_write()
    api.option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.230.145'])
    api.option('ip_admin_eth0.ip_admin_eth0').value.pop(0)
    api.option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.230.145'])
    api.option('ip_admin_eth0.netmask_admin_eth0', 0).value.set("192.168.230.145")
    api.option('ip_admin_eth0.netmask_admin_eth0', 0).value.reset()
    api.option('ip_admin_eth0.ip_admin_eth0').property.add('disabled')
    raises(PropertiesOptionError, "api.option('ip_admin_eth0.netmask_admin_eth0', 0).value.set('192.168.230.145')")


def test_sub_group_in_leader_group():
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé", multi=True)
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "masque du sous-réseau", multi=True)
    subgroup = OptionDescription("subgroup", '', [])
    raises(ValueError, "Leadership('ip_admin_eth0', '', [subgroup, ip_admin_eth0, netmask_admin_eth0])")


def test_group_always_has_multis():
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé", multi=True)
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "masque du sous-réseau")
    raises(ValueError, "Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])")


#____________________________________________________________
def test_values_with_leader_and_followers():
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé", multi=True)
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "masque du sous-réseau", multi=True)
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    maconfig = OptionDescription('toto', '', [interface1])
    api = Config(maconfig)
    api.property.read_write()
    owner = api.owner.get()
    assert interface1.impl_get_group_type() == groups.leadership
    assert api.option('ip_admin_eth0.ip_admin_eth0').owner.isdefault()
    #
    api.option('ip_admin_eth0.ip_admin_eth0').value.set(["192.168.230.145"])
    assert api.option('ip_admin_eth0.ip_admin_eth0').value.get() == ["192.168.230.145"]
    assert api.option('ip_admin_eth0.netmask_admin_eth0', 0).value.get() is None
    assert api.option('ip_admin_eth0.ip_admin_eth0').owner.get() == owner
    assert api.option('ip_admin_eth0.netmask_admin_eth0', 0).owner.isdefault()
    api.option('ip_admin_eth0.ip_admin_eth0').value.set(["192.168.230.145", "192.168.230.147"])
    raises(APIError, "api.option('ip_admin_eth0.netmask_admin_eth0').value.set([None])")
    raises(APIError, "api.option('ip_admin_eth0.netmask_admin_eth0').value.pop(0)")


def test_reset_values_with_leader_and_followers():
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé", multi=True)
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "masque du sous-réseau", multi=True)
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    maconfig = OptionDescription('toto', '', [interface1])
    api = Config(maconfig)
    api.property.read_write()
    owner = api.owner.get()
    assert interface1.impl_get_group_type() == groups.leadership
    assert api.option('ip_admin_eth0.ip_admin_eth0').owner.isdefault()
    api.option('ip_admin_eth0.ip_admin_eth0').value.set(["192.168.230.145"])
    assert api.option('ip_admin_eth0.ip_admin_eth0').owner.get() == owner
    assert api.option('ip_admin_eth0.netmask_admin_eth0', 0).owner.isdefault()
    api.option('ip_admin_eth0.ip_admin_eth0').value.reset()
    assert api.option('ip_admin_eth0.ip_admin_eth0').owner.isdefault()
    assert api.option('ip_admin_eth0.ip_admin_eth0').value.get() == []
    #reset
    api.option('ip_admin_eth0.ip_admin_eth0').value.set(["192.168.230.145"])
    api.option('ip_admin_eth0.ip_admin_eth0').value.reset()
    assert api.option('ip_admin_eth0.ip_admin_eth0').owner.isdefault()
    assert api.option('ip_admin_eth0.ip_admin_eth0').value.get() == []


def test_reset_values_with_leader_and_followers_default_value():
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé", multi=True, default=['192.168.230.145'])
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "masque du sous-réseau", multi=True, default=['255.255.255.0'])
    raises(ValueError, "Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])")


def test_reset_values_with_leader_and_followers_default():
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé", multi=True, default=['192.168.230.145'])
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "masque du sous-réseau", multi=True)
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    maconfig = OptionDescription('toto', '', [interface1])
    api = Config(maconfig)
    api.property.read_write()
    owner = api.owner.get()
    assert api.option('ip_admin_eth0.ip_admin_eth0').owner.isdefault()
    assert api.option('ip_admin_eth0.netmask_admin_eth0', 0).owner.isdefault()

    api.option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.230.146'])
    assert api.option('ip_admin_eth0.ip_admin_eth0').owner.get() == owner
    assert api.option('ip_admin_eth0.netmask_admin_eth0', 0).owner.isdefault()
    api.option('ip_admin_eth0.ip_admin_eth0').value.reset()
    assert api.option('ip_admin_eth0.ip_admin_eth0').owner.isdefault()
    assert api.option('ip_admin_eth0.netmask_admin_eth0', 0).owner.isdefault()
    assert api.option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.230.145']
    api.option('ip_admin_eth0.netmask_admin_eth0', 0).value.set(None)

    api.option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.230.146'])
    api.option('ip_admin_eth0.netmask_admin_eth0', 0).value.set('255.255.255.0')
    assert api.option('ip_admin_eth0.ip_admin_eth0').owner.get() == owner
    assert api.option('ip_admin_eth0.netmask_admin_eth0', 0).owner.get() == owner
    api.option('ip_admin_eth0.ip_admin_eth0').value.reset()
    assert api.option('ip_admin_eth0.ip_admin_eth0').owner.isdefault()
    assert api.option('ip_admin_eth0.netmask_admin_eth0', 0).owner.get() == owners.default
    assert api.option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.230.145']
    api.option('ip_admin_eth0.netmask_admin_eth0', 0).value.set(None)

    api.option('ip_admin_eth0.netmask_admin_eth0', 0).value.set('255.255.255.0')
    assert api.option('ip_admin_eth0.ip_admin_eth0').owner.isdefault()
    assert api.option('ip_admin_eth0.netmask_admin_eth0', 0).owner.get() == owner
    api.option('ip_admin_eth0.ip_admin_eth0').value.reset()
    assert api.option('ip_admin_eth0.ip_admin_eth0').owner.isdefault()
    assert api.option('ip_admin_eth0.netmask_admin_eth0', 0).owner.isdefault()
    assert api.option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.230.145']
    api.option('ip_admin_eth0.netmask_admin_eth0', 0).value.set(None)


def test_values_with_leader_and_followers_follower():
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé", multi=True)
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "masque du sous-réseau", multi=True)
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    maconfig = OptionDescription('toto', '', [interface1])
    api = Config(maconfig)
    api.property.read_write()
    raises(LeadershipError,
           "api.option('ip_admin_eth0.netmask_admin_eth0', 0).value.set('255.255.255.0')")
    api.option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.230.145'])
    api.option('ip_admin_eth0.netmask_admin_eth0', 0).value.set('255.255.255.0')
    api.option('ip_admin_eth0.netmask_admin_eth0', 0).value.reset()
    api.option('ip_admin_eth0.netmask_admin_eth0', 0).value.set('255.255.255.0')
    #
    api.option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.230.145', '192.168.230.145'])
    assert api.option('ip_admin_eth0.netmask_admin_eth0', 0).value.get() == '255.255.255.0'
    assert api.option('ip_admin_eth0.netmask_admin_eth0', 1).value.get() is None
    api.option('ip_admin_eth0.netmask_admin_eth0', 0).value.set('255.255.255.0')
    api.option('ip_admin_eth0.netmask_admin_eth0', 1).value.set('255.255.255.0')
    raises(APIError, "api.option('ip_admin_eth0.netmask_admin_eth0', 1).value.pop(1)")
    #reset
    api.option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.230.145',
                                                         '192.168.230.145',
                                                         '192.168.230.145'])
    api.option('ip_admin_eth0.ip_admin_eth0').value.reset()
    assert api.option('ip_admin_eth0.ip_admin_eth0').value.get() == []


def test_values_with_leader_and_followers_pop():
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé", multi=True)
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "masque du sous-réseau", multi=True)
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    maconfig = OptionDescription('toto', '', [interface1])
    api = Config(maconfig)
    api.property.read_write()
    api.option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.230.145'])
    api.option('ip_admin_eth0.netmask_admin_eth0', 0).value.set('255.255.255.0')
    api.option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.230.145', '192.168.230.146'])
    api.option('ip_admin_eth0.netmask_admin_eth0', 1).value.set('255.255.0.0')
    assert api.option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.230.145', '192.168.230.146']
    assert api.option('ip_admin_eth0.netmask_admin_eth0', 0).value.get() == '255.255.255.0'
    assert api.option('ip_admin_eth0.netmask_admin_eth0', 1).value.get() == '255.255.0.0'
    api.option('ip_admin_eth0.ip_admin_eth0').value.pop(0)
    assert api.option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.230.146']
    assert api.option('ip_admin_eth0.netmask_admin_eth0', 0).value.get() == '255.255.0.0'


def test_values_with_leader_and_followers_leader():
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé", multi=True)
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "masque du sous-réseau", multi=True)
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    maconfig = OptionDescription('toto', '', [interface1])
    api = Config(maconfig)
    api.property.read_write()
    api.option('ip_admin_eth0.ip_admin_eth0').value.set(["192.168.230.145"])
    api.option('ip_admin_eth0.ip_admin_eth0').value.set(["192.168.230.145"])
    api.option('ip_admin_eth0.ip_admin_eth0').value.set(["192.168.230.145", "192.168.230.145"])
    api.option('ip_admin_eth0.netmask_admin_eth0', 0).value.set('255.255.255.0')
    api.option('ip_admin_eth0.netmask_admin_eth0', 1).value.set('255.255.255.0')
    raises(LeadershipError, "api.option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.230.145'])")
    assert api.option('ip_admin_eth0.netmask_admin_eth0', 0).value.get() == '255.255.255.0'
    assert api.option('ip_admin_eth0.netmask_admin_eth0', 1).value.get() == '255.255.255.0'
    api.option('ip_admin_eth0.ip_admin_eth0').value.pop(1)
    assert api.option('ip_admin_eth0.ip_admin_eth0').value.get() == ["192.168.230.145"]
    assert api.option('ip_admin_eth0.netmask_admin_eth0', 0).value.get() == '255.255.255.0'
    api.option('ip_admin_eth0.ip_admin_eth0').value.reset()
    assert api.option('ip_admin_eth0.ip_admin_eth0').value.get() == []


def test_values_with_leader_and_followers_leader_pop():
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé", multi=True)
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "masque du sous-réseau", multi=True)
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    maconfig = OptionDescription('toto', '', [interface1])
    api = Config(maconfig)
    api.property.read_write()
    api.option('ip_admin_eth0.ip_admin_eth0').value.set(["192.168.230.145", "192.168.230.146"])
    api.option('ip_admin_eth0.netmask_admin_eth0', 1).value.set('255.255.0.0')
    assert api.option('ip_admin_eth0.ip_admin_eth0').value.get() == ["192.168.230.145", "192.168.230.146"]
    assert api.option('ip_admin_eth0.netmask_admin_eth0', 0).value.get() == None
    assert api.option('ip_admin_eth0.netmask_admin_eth0', 1).value.get() == '255.255.0.0'
    compare(api.value.exportation(), (('ip_admin_eth0.ip_admin_eth0', 'ip_admin_eth0.netmask_admin_eth0'), (None, (1,)), (('192.168.230.145', '192.168.230.146'), ('255.255.0.0',)), ('user', ('user',))))
    api.option('ip_admin_eth0.ip_admin_eth0').value.pop(0)
    compare(api.value.exportation(), (('ip_admin_eth0.ip_admin_eth0', 'ip_admin_eth0.netmask_admin_eth0'), (None, (0,)), (('192.168.230.146',), ('255.255.0.0',)), ('user', ('user',))))
    assert api.option('ip_admin_eth0.ip_admin_eth0').value.get() == ["192.168.230.146"]
    assert api.option('ip_admin_eth0.netmask_admin_eth0', 0).value.get() == '255.255.0.0'
    api.option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.230.146', "192.168.230.145", "192.168.230.146", "192.168.230.147", "192.168.230.148", "192.168.230.149"])
    api.option('ip_admin_eth0.netmask_admin_eth0', 3).value.set('255.255.0.0')
    api.option('ip_admin_eth0.netmask_admin_eth0', 4).value.set('255.255.0.0')
    compare(api.value.exportation(), (('ip_admin_eth0.ip_admin_eth0', 'ip_admin_eth0.netmask_admin_eth0'), (None, (0, 3, 4)), (('192.168.230.146', "192.168.230.145", "192.168.230.146", "192.168.230.147", "192.168.230.148", "192.168.230.149"), ('255.255.0.0', '255.255.0.0', '255.255.0.0')), ('user', ('user', 'user', 'user'))))
    api.option('ip_admin_eth0.ip_admin_eth0').value.pop(5)
    compare(api.value.exportation(), (('ip_admin_eth0.ip_admin_eth0', 'ip_admin_eth0.netmask_admin_eth0'), (None, (0, 3, 4)), (('192.168.230.146', "192.168.230.145", "192.168.230.146", "192.168.230.147", "192.168.230.148"), ('255.255.0.0', '255.255.0.0', '255.255.0.0')), ('user', ('user', 'user', 'user'))))
    api.option('ip_admin_eth0.ip_admin_eth0').value.pop(2)
    compare(api.value.exportation(), (('ip_admin_eth0.ip_admin_eth0', 'ip_admin_eth0.netmask_admin_eth0'), (None, (0, 2, 3)), (('192.168.230.146', "192.168.230.145", "192.168.230.147", "192.168.230.148"), ('255.255.0.0', '255.255.0.0', '255.255.0.0')), ('user', ('user', 'user', 'user'))))
    api.option('ip_admin_eth0.ip_admin_eth0').value.pop(2)
    compare(api.value.exportation(), (('ip_admin_eth0.ip_admin_eth0', 'ip_admin_eth0.netmask_admin_eth0'), (None, (0, 2)), (('192.168.230.146', "192.168.230.145", "192.168.230.148"), ('255.255.0.0', '255.255.0.0')), ('user', ('user', 'user'))))
    api.option('ip_admin_eth0.ip_admin_eth0').value.pop(2)
    compare(api.value.exportation(), (('ip_admin_eth0.ip_admin_eth0', 'ip_admin_eth0.netmask_admin_eth0'), (None, (0,)), (('192.168.230.146', "192.168.230.145"), ('255.255.0.0',)), ('user', ('user',))))


def test_values_with_leader_owner():
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé", multi=True)
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "masque du sous-réseau", multi=True)
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    maconfig = OptionDescription('toto', '', [interface1])
    api = Config(maconfig)
    api.property.read_write()
    owner = api.owner.get()
    assert api.option('ip_admin_eth0.ip_admin_eth0').owner.isdefault()
    api.option('ip_admin_eth0.ip_admin_eth0').value.set(["192.168.230.145"])
    assert api.option('ip_admin_eth0.ip_admin_eth0').owner.get() == owner
    assert api.option('ip_admin_eth0.netmask_admin_eth0', 0).owner.isdefault()
    api.option('ip_admin_eth0.ip_admin_eth0').value.pop(0)
    assert api.option('ip_admin_eth0.ip_admin_eth0').owner.get() == owner


def test_values_with_leader_disabled():
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé", multi=True)
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "masque du sous-réseau", multi=True)
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    maconfig = OptionDescription('toto', '', [interface1])
    api = Config(maconfig)
    api.property.read_write()
    api.option('ip_admin_eth0.ip_admin_eth0').value.set(["192.168.230.145"])
    api.option('ip_admin_eth0.ip_admin_eth0').value.pop(0)
    api.option('ip_admin_eth0.ip_admin_eth0').value.set(["192.168.230.145"])
    api.option('ip_admin_eth0.netmask_admin_eth0', 0).value.set("192.168.230.145")
    api.option('ip_admin_eth0.ip_admin_eth0').value.pop(0)
    api.option('ip_admin_eth0.netmask_admin_eth0').property.add('disabled')
    api.option('ip_admin_eth0.ip_admin_eth0').value.set(["192.168.230.145"])
    api.option('ip_admin_eth0.ip_admin_eth0').value.pop(0)

    #delete with value in disabled var
    api.unrestraint.option('ip_admin_eth0.netmask_admin_eth0').property.pop('disabled')
    api.option('ip_admin_eth0.ip_admin_eth0').value.set(["192.168.230.145"])
    api.option('ip_admin_eth0.netmask_admin_eth0', 0).value.set("192.168.230.145")
    api.unrestraint.option('ip_admin_eth0.netmask_admin_eth0').property.add('disabled')
    api.option('ip_admin_eth0.ip_admin_eth0').value.pop(0)

    #append with value in disabled var
    api.unrestraint.option('ip_admin_eth0.netmask_admin_eth0').property.pop('disabled')
    api.option('ip_admin_eth0.ip_admin_eth0').value.set(["192.168.230.145"])
    api.option('ip_admin_eth0.netmask_admin_eth0', 0).value.set("192.168.230.145")
    api.unrestraint.option('ip_admin_eth0.netmask_admin_eth0').property.add('disabled')
    api.option('ip_admin_eth0.ip_admin_eth0').value.set(["192.168.230.145", '192.168.230.43'])


def test_multi_non_valid_value():
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé", multi=True)
    maconfig = OptionDescription('toto', '', [ip_admin_eth0])
    api = Config(maconfig)
    api.property.read_write()
    api.option('ip_admin_eth0').value.set(['a'])
    raises(ValueError, "api.option('ip_admin_eth0').value.set([1])")


def test_multi_leader_default_follower():
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé", multi=True)
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "masque du sous-réseau", default_multi="255.255.255.0", multi=True)
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    maconfig = OptionDescription('toto', '', [interface1])
    api = Config(maconfig)
    api.property.read_write()
    api.option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.1.1'])
    assert api.option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.1']


def test_groups_with_leader_get_modified_value():
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé", multi=True)
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "masque du sous-réseau", multi=True)
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    maconfig = OptionDescription('toto', '', [interface1])
    api = Config(maconfig)
    api.property.read_write()
    compare(api.value.exportation(), ((), (), (), ()))
    api.option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.1.1'])
    compare(api.value.exportation(), (('ip_admin_eth0.ip_admin_eth0',), (None,), (('192.168.1.1',),), ('user',)))
    api.option('ip_admin_eth0.netmask_admin_eth0', 0).value.set('255.255.255.255')
    compare(api.value.exportation(), (('ip_admin_eth0.ip_admin_eth0', 'ip_admin_eth0.netmask_admin_eth0',), (None, (0,)), (('192.168.1.1',), ('255.255.255.255',)), ('user', ('user',))))
    api.option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.1.1', '192.168.1.1'])
    api.option('ip_admin_eth0.netmask_admin_eth0', 1).value.set('255.255.255.255')
    compare(api.value.exportation(), (('ip_admin_eth0.ip_admin_eth0', 'ip_admin_eth0.netmask_admin_eth0',), (None, (0, 1)), (('192.168.1.1', '192.168.1.1'), ('255.255.255.255', '255.255.255.255')), ('user', ('user', 'user'))))


def test_groups_with_leader_importation():
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé", multi=True)
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "masque du sous-réseau", multi=True)
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    maconfig = OptionDescription('toto', '', [interface1])
    api = Config(maconfig)
    api.property.read_write()
    api.value.importation((('ip_admin_eth0.ip_admin_eth0', 'ip_admin_eth0.netmask_admin_eth0',), (None, (0, 1)), (('192.168.1.1', '192.168.1.0'), ('255.255.255.255', '255.255.255.0')), ('user', ('user', 'user'))))
    api.option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.1', '192.168.1.0']
    api.option('ip_admin_eth0.netmask_admin_eth0', 0).value.get() == '255.255.255.255'
    api.option('ip_admin_eth0.netmask_admin_eth0', 1).value.get() == '255.255.255.0'
    api.option('ip_admin_eth0.ip_admin_eth0').owner.get() == 'user'
    api.option('ip_admin_eth0.netmask_admin_eth0', 0).owner.get() == 'user'
    api.option('ip_admin_eth0.netmask_admin_eth0', 1).owner.get() == 'user'


def test_wrong_index():
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé", multi=True, default=['1.1.1.1'])
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "masque du sous-réseau", multi=True)
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    od1 = OptionDescription('od', '', [interface1])
    maconfig = OptionDescription('toto', '', [od1])
    api = Config(maconfig)
    api.property.read_write()
    assert api.option('od.ip_admin_eth0.ip_admin_eth0').option.get()
    raises(APIError, "api.option('od.ip_admin_eth0.ip_admin_eth0', 0).option.get()")
    assert api.option('od.ip_admin_eth0.netmask_admin_eth0', 0).option.get()
    assert api.option('od.ip_admin_eth0').option.get()
    raises(APIError, "api.option('od.ip_admin_eth0', 0).option.get()")
    assert api.option('od').option.get()
    raises(APIError, "api.option('od', 0).option.get()")


def test_without_leader_or_follower():
    raises(ValueError, "Leadership('ip_admin_eth0', '', [])")
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé", multi=True, default=['1.1.1.1'])
    raises(ValueError, "Leadership('ip_admin_eth0', '', [ip_admin_eth0])")
    #empty optiondescription is allowed
    OptionDescription('ip_admin_eth0', '', [])


def test_leader_not_multi():
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé")
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "masque du sous-réseau", multi=True)
    raises(ValueError, "Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])")


def test_follower_not_multi():
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé", multi=True)
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "masque du sous-réseau")
    raises(ValueError, "Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])")


def test_follower_not_same():
    ip_admin_eth0 = IPOption('ip_admin_eth0', "ip réseau autorisé", multi=True, default=['1.1.1.1'])
    netmask_admin_eth0 = NetmaskOption('netmask_admin_eth0', "masque du sous-réseau", multi=True)
    interface0 = Leadership('interface0', '', [ip_admin_eth0, netmask_admin_eth0])
    ip_admin_eth1 = IPOption('ip_admin_eth1', "ip réseau autorisé", multi=True, default=['1.1.1.1'])
    netmask_admin_eth1 = NetmaskOption('netmask_admin_eth1', "masque du sous-réseau", multi=True)
    netmask_admin_eth1.impl_add_consistency('ip_netmask', ip_admin_eth0)
    interface1 = Leadership('interface1', '', [ip_admin_eth1, netmask_admin_eth1])
    od1 = OptionDescription('od', '', [interface0, interface1])
    maconfig = OptionDescription('toto', '', [od1])
    raises(ConfigError, "Config(maconfig)")


def test_follower_not_same_not_equal():
    ip_admin_eth0 = IPOption('ip_admin_eth0', "ip réseau autorisé", multi=True, default=['1.1.1.1'])
    netmask_admin_eth0 = NetmaskOption('netmask_admin_eth0', "masque du sous-réseau", multi=True)
    interface0 = Leadership('interface0', '', [ip_admin_eth0, netmask_admin_eth0])
    ip_admin_eth1 = IPOption('ip_admin_eth1', "ip réseau autorisé", multi=True, default=['1.1.1.1'])
    netmask_admin_eth1 = NetmaskOption('netmask_admin_eth1', "masque du sous-réseau", multi=True)
    netmask_admin_eth1.impl_add_consistency('not_equal', netmask_admin_eth0)
    interface1 = Leadership('interface1', '', [ip_admin_eth1, netmask_admin_eth1])
    od1 = OptionDescription('od', '', [interface0, interface1])
    maconfig = OptionDescription('toto', '', [od1])
    api = Config(maconfig)
    api.property.read_write()


def test_follower_force_store_value():
    ip_admin_eth0 = IPOption('ip_admin_eth0', "ip réseau autorisé", multi=True, default=['1.1.1.1'])
    netmask_admin_eth0 = NetmaskOption('netmask_admin_eth0', "masque du sous-réseau", multi=True, properties=('force_store_value',))
    interface0 = Leadership('interface0', '', [ip_admin_eth0, netmask_admin_eth0])
    od1 = OptionDescription('od', '', [interface0])
    maconfig = OptionDescription('toto', '', [od1])
    raises(ConfigError, "Config(maconfig)")
