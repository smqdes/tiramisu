"config.set() or config.setoption() or option.setoption()"
from .autopath import do_autopath
do_autopath()

from py.test import raises

from tiramisu.i18n import _
from tiramisu.error import display_list, ConfigError
from tiramisu.setting import owners, groups
from tiramisu import ChoiceOption, BoolOption, IntOption, FloatOption, \
    StrOption, OptionDescription, Leadership, Config, undefined
from tiramisu.error import PropertiesOptionError
from tiramisu.storage import list_sessions


def teardown_function(function):
    assert list_sessions() == [], 'session list is not empty when leaving "{}"'.format(function.__name__)


def make_description():
    gcoption = ChoiceOption('name', 'GC name', ('ref', 'framework'), 'ref')
    gcdummy = BoolOption('dummy', 'dummy', default=False)
    objspaceoption = ChoiceOption('objspace', 'Object space',
                                  ('std', 'thunk'), 'std')
    booloption = BoolOption('bool', 'Test boolean option', default=True)
    intoption = IntOption('int', 'Test int option', default=0)
    floatoption = FloatOption('float', 'Test float option', default=2.3)
    stroption = StrOption('str', 'Test string option', default="abc")
    boolop = BoolOption('boolop', 'Test boolean option op', default=True)
    wantref_option = BoolOption('wantref', 'Test requires', default=False)
    wantframework_option = BoolOption('wantframework', 'Test requires',
                                      default=False)
    gcgroup = OptionDescription('gc', '', [gcoption, gcdummy, floatoption])
    descr = OptionDescription('tiramisu', '', [gcgroup, booloption, objspaceoption,
                                               wantref_option, stroption,
                                               wantframework_option,
                                               intoption, boolop])
    return descr


#____________________________________________________________
# change with __setattr__
def test_attribute_access():
    "Once set, option values can't be changed again by attribute access"
    s = StrOption("string", "", default="string")
    descr = OptionDescription("options", "", [s])
    api = Config(descr)
    # let's try to change it again
    api.option('string').value.set('foo')
    assert api.option('string').value.get() == 'foo'


def test_mod_read_only_write():
    "default with multi is a list"
    s = StrOption("string", "", default=[], default_multi="string", multi=True)
    descr = OptionDescription("options", "", [s])
    config = Config(descr)
    config2 = Config(descr)
    assert config.property.getdefault() == {'cache', 'validator', 'warnings'}
    assert config.property.getdefault('read_only', 'append') == {'frozen',
                                                                 'disabled',
                                                                 'validator',
                                                                 'everything_frozen',
                                                                 'mandatory',
                                                                 'empty',
                                                                 'force_store_value'}
    assert config.property.getdefault('read_only', 'remove') == {'permissive',
                                                                 'hidden'}
    assert config.property.getdefault('read_write', 'append') == {'frozen',
                                                                  'disabled',
                                                                  'validator',
                                                                  'hidden',
                                                                  'force_store_value'}
    assert config.property.getdefault('read_write', 'remove') == {'permissive',
                                                                  'everything_frozen',
                                                                  'mandatory',
                                                                  'empty'}
    #
    config.property.setdefault({'cache'})
    config.property.setdefault(type='read_only', when='append', properties={'disabled'})
    config.property.setdefault(type='read_only', when='remove', properties={'hidden'})
    config.property.setdefault(type='read_write', when='append', properties={'disabled',
                                                                             'hidden'})
    config.property.setdefault(type='read_write', when='remove', properties=set([]))
    raises(ValueError, "config.property.setdefault(type='unknown', when='append', properties={'disabled'})")
    raises(ValueError, "config.property.setdefault(type='read_only', when='unknown', properties={'disabled'})")
    raises(TypeError, "config.property.setdefault(type='read_only', when='append', properties=['disabled'])")

    assert config.property.getdefault() == {'cache'}
    assert config.property.getdefault('read_only', 'append') == {'disabled'}
    assert config.property.getdefault('read_only', 'remove') == {'hidden'}
    assert config.property.getdefault('read_write', 'append') == {'disabled',
                                                                  'hidden'}
    assert config.property.getdefault('read_write', 'remove') == set([])
    #
    config.property.read_only()
    assert config.property.get() == {'cache', 'disabled'}
    config.property.read_write()
    assert config.property.get() == {'cache', 'disabled', 'hidden'}
    config.property.read_only()
    assert config.property.get() == {'cache', 'disabled'}
    #
    assert config2.property.getdefault() == {'cache', 'validator', 'warnings'}
    assert config2.property.getdefault('read_only', 'append') == {'frozen',
                                                                  'disabled',
                                                                  'validator',
                                                                  'everything_frozen',
                                                                  'mandatory',
                                                                  'empty',
                                                                  'force_store_value'}
    assert config2.property.getdefault('read_only', 'remove') == {'permissive',
                                                                  'hidden'}
    assert config2.property.getdefault('read_write', 'append') == {'frozen',
                                                                   'disabled',
                                                                   'validator',
                                                                   'hidden',
                                                                   'force_store_value'}
    assert config2.property.getdefault('read_write', 'remove') == {'permissive',
                                                                   'everything_frozen',
                                                                   'mandatory',
                                                                   'empty'}
    raises(ValueError, "config2.property.getdefault('unknown', 'remove')")
    raises(ValueError, "config2.property.getdefault('read_write', 'unknown')")


def test_setitem():
    s = StrOption("string", "", default=["string", "sdfsdf"], default_multi="prout", multi=True)
    descr = OptionDescription("options", "", [s])
    api = Config(descr)
    api.option('string').value.set([undefined, 'foo'])
    assert api.option('string').value.get() == ['string', 'foo']


def test_reset():
    "if value is None, resets to default owner"
    s = StrOption("string", "", default="string")
    descr = OptionDescription("options", "", [s])
    api = Config(descr)
    api.option('string').value.set('foo')
    assert api.option('string').value.get() == "foo"
    assert api.option('string').owner.get() ==owners.user
    api.option('string').value.reset()
    assert api.option('string').value.get() == 'string'
    assert api.option('string').owner.get() ==owners.default


def test_reset_with_multi():
    s = StrOption("string", "", default=["string"], default_multi="string", multi=True)
    descr = OptionDescription("options", "", [s])
    api = Config(descr)
#    api.option('string').value.set([])
    api.option('string').value.reset()
    assert api.option('string').value.get() == ["string"]
    assert api.option('string').owner.get() =='default'
    api.option('string').value.set(["eggs", "spam", "foo"])
    assert api.option('string').owner.get() =='user'
    api.option('string').value.set([])
    api.option('string').value.reset()
#    assert api.option('string').value.get() == ["string"]
    assert api.option('string').owner.get() =='default'
    raises(ValueError, "api.option('string').value.set(None)")


def test_property_only_raises():
    s = StrOption("string", "", default=["string"], default_multi="string", multi=True)
    intoption = IntOption('int', 'Test int option', default=0)
    stroption = StrOption('str', 'Test string option', default=["abc"], default_multi="abc",
                          requires=[{'option': intoption, 'expected': 1, 'action': 'hidden'}], multi=True)
    descr = OptionDescription("options", "", [s, intoption, stroption])
    api = Config(descr)
    api.property.read_write()
    assert api.option('str').property.get() == {'empty'}
    assert api.option('str').property.get(only_raises=True) == set()


def test_default_with_multi():
    "default with multi is a list"
    s = StrOption("string", "", default=[], default_multi="string", multi=True)
    descr = OptionDescription("options", "", [s])
    api = Config(descr)
    assert api.option('string').value.get() == []
    s = StrOption("string", "", default=None, default_multi="string", multi=True)
    descr = OptionDescription("options", "", [s])
    api = Config(descr)
    assert api.option('string').value.get() == []


def test_idontexist():
    descr = make_description()
    api = Config(descr)
    api.value.dict()
    raises(AttributeError, "api.option('idontexist').value.get()")


# ____________________________________________________________
def test_attribute_access_with_multi():
    s = StrOption("string", "", default=["string"], default_multi="string", multi=True)
    descr = OptionDescription("options", "", [s])
    api = Config(descr)
    api.option('string').value.set(["foo", "bar"])
    assert api.option('string').value.get() == ["foo", "bar"]


def test_item_access_with_multi():
    s = StrOption("string", "", default=["string"], multi=True)
    descr = OptionDescription("options", "", [s])
    api = Config(descr)
    api.option('string').value.set(["foo", "bar"])
    assert api.option('string').value.get() == ["foo", "bar"]
    api.option('string').value.set(["changetest", "bar"])
    assert api.option('string').value.get() == ["changetest", "bar"]


def test_access_with_multi_default():
    s = StrOption("string", "", default=["string"], multi=True)
    descr = OptionDescription("options", "", [s])
    api = Config(descr)
    assert api.option('string').owner.get() =='default'
    api.option('string').value.set(["foo", "bar"])
    assert api.option('string').value.get() == ["foo", "bar"]
    assert api.option('string').owner.get() =='user'


def test_multi_with_requires():
    s = StrOption("string", "", default=["string"], default_multi="string", multi=True)
    intoption = IntOption('int', 'Test int option', default=0)
    stroption = StrOption('str', 'Test string option', default=["abc"], default_multi="abc",
                          requires=[{'option': intoption, 'expected': 1, 'action': 'hidden'}], multi=True)
    descr = OptionDescription("options", "", [s, intoption, stroption])
    api = Config(descr)
    api.property.read_write()
    assert not 'hidden' in api.option('str').property.get()
    api.option('int').value.set(1)
    raises(PropertiesOptionError, "api.option('str').value.set(['a', 'b'])")
    assert 'hidden' in api.forcepermissive.option('str').property.get()


def test__requires_with_inverted():
    s = StrOption("string", "", default=["string"], multi=True)
    intoption = IntOption('int', 'Test int option', default=0)
    stroption = StrOption('str', 'Test string option', default=["abc"], default_multi="abc",
                          requires=[{'option': intoption, 'expected': 1, 'action': 'hide', 'inverse': True}], multi=True)
    descr = OptionDescription("options", "", [s, intoption, stroption])
    api = Config(descr)
    assert not 'hidden' in api.option('str').property.get()
    api.option('int').value.set(1)
    assert not 'hidden' in api.option('str').property.get()


def test_multi_with_requires_in_another_group():
    s = StrOption("string", "", default=["string"], multi=True)
    intoption = IntOption('int', 'Test int option', default=0)
    stroption = StrOption('str', 'Test string option', default=["abc"],
                          requires=[{'option': intoption, 'expected': 1, 'action': 'hidden'}], multi=True)
    descr = OptionDescription("opt", "", [stroption])
    descr2 = OptionDescription("opt2", "", [intoption, s, descr])
    api = Config(descr2)
    api.property.read_write()
    assert not 'hidden' in api.option('opt.str').property.get()
    api.option('int').value.set(1)
    raises(PropertiesOptionError,  "api.option('opt.str').value.set(['a', 'b'])")
    assert 'hidden' in api.forcepermissive.option('opt.str').property.get()


def test_multi_with_requires_in_another_group_inverse():
    s = StrOption("string", "", default=["string"], multi=True)
    intoption = IntOption('int', 'Test int option', default=0)
    stroption = StrOption('str', 'Test string option', default=["abc"],
            requires=[{'option': intoption, 'expected': 0, 'action': 'hidden', 'inverse': True}], multi=True)
    descr = OptionDescription("opt", "", [stroption])
    descr2 = OptionDescription("opt2", "", [intoption, s, descr])
    api = Config(descr2)
    api.property.read_write()
    assert not 'hidden' in api.option('opt.str').property.get()
    api.option('int').value.set(1)
    raises(PropertiesOptionError,  "api.option('opt.str').value.set(['a', 'b'])")
    assert 'hidden' in api.forcepermissive.option('opt.str').property.get()


def test_apply_requires_from_config():
    s = StrOption("string", "", default=["string"], multi=True)
    intoption = IntOption('int', 'Test int option', default=0)
    stroption = StrOption('str', 'Test string option', default=["abc"],
                          requires=[{'option': intoption, 'expected': 1, 'action': 'hidden'}], multi=True)
    descr = OptionDescription("opt", "", [stroption])
    descr2 = OptionDescription("opt2", "", [intoption, s, descr])
    api = Config(descr2)
    api.property.read_write()
    assert not 'hidden' in api.option('opt.str').property.get()
    api.option('int').value.set(1)
    raises(PropertiesOptionError, "api.option('opt.str').value.get()")
    assert 'hidden' in api.forcepermissive.option('opt.str').property.get()
    assert 'hidden' not in api.forcepermissive.option('opt.str').option.properties()
    assert 'hidden' not in api.forcepermissive.option('opt.str').option.properties(only_raises=True)


def test_apply_requires_with_disabled():
    s = StrOption("string", "", default=["string"], multi=True)
    intoption = IntOption('int', 'Test int option', default=0)
    stroption = StrOption('str', 'Test string option', default=["abc"],
                          requires=[{'option': intoption, 'expected': 1, 'action': 'disabled'}], multi=True)
    descr = OptionDescription("opt", "", [stroption])
    descr2 = OptionDescription("opt2", "", [intoption, s, descr])
    api = Config(descr2)
    api.property.read_write()
    assert not 'disabled' in api.option('opt.str').property.get()
    api.option('int').value.set(1)
    raises(PropertiesOptionError, "api.option('opt.str').value.get()")
    assert 'disabled' not in api.unrestraint.option('opt.str').option.properties()
    assert 'disabled' not in api.unrestraint.option('opt.str').option.properties(only_raises=True)
    assert 'disabled' in api.unrestraint.option('opt.str').property.get()


def test_multi_with_requires_with_disabled_in_another_group():
    s = StrOption("string", "", default=["string"], multi=True)
    intoption = IntOption('int', 'Test int option', default=0)
    stroption = StrOption('str', 'Test string option', default=["abc"],
                          requires=[{'option': intoption, 'expected': 1, 'action': 'disabled'}], multi=True)
    descr = OptionDescription("opt", "", [stroption])
    descr2 = OptionDescription("opt2", "", [intoption, s, descr])
    api = Config(descr2)
    api.property.read_write()
    assert not 'disabled' in api.option('opt.str').property.get()
    api.option('int').value.set(1)
    raises(PropertiesOptionError,  "api.option('opt.str').value.set(['a', 'b'])")
    assert 'disabled' in api.unrestraint.option('opt.str').property.get()


def test_multi_with_requires_that_is_multi():
    b = IntOption('int', 'Test int option', default=[0], multi=True)
    c = StrOption('str', 'Test string option', default=['abc'], requires=[{'option': b, 'expected': 1, 'action': 'hidden'}], multi=True)
    descr = OptionDescription("opt", "", [b, c])
    descr
    raises(ValueError, "Config(descr)")


def test_multi_with_requires_that_is_multi_inverse():
    b = IntOption('int', 'Test int option', default=[0], multi=True)
    c = StrOption('str', 'Test string option', default=['abc'], requires=[{'option': b, 'expected': 0, 'action': 'hidden', 'inverse': True}], multi=True)
    descr = OptionDescription("opt", "", [b, c])
    descr
    raises(ValueError, "Config(descr)")


def test_multi_with_requires_that_is_leadership():
    b = IntOption('int', 'Test int option', default=[0], multi=True)
    c = StrOption('str', 'Test string option', requires=[{'option': b, 'expected': 1, 'action': 'hidden'}], multi=True)
    descr = Leadership("int", "", [b, c])
    od = OptionDescription('root', '', [descr])
    Config(od)


def test_multi_with_requires_that_is_leadership_leader():
    b = IntOption('int', 'Test int option', multi=True)
    c = StrOption('str', 'Test string option', requires=[{'option': b, 'expected': 1, 'action': 'hidden'}], multi=True)
    raises(ValueError, "Leadership('str', '', [c, b])")


def test_multi_with_requires_that_is_leadership_follower():
    b = IntOption('int', 'Test int option', default=[0], multi=True)
    c = StrOption('str', 'Test string option', multi=True)
    d = StrOption('str1', 'Test string option', requires=[{'option': c, 'expected': '1', 'action': 'hidden'}], multi=True)
    descr = Leadership("int", "", [b, c, d])
    descr2 = OptionDescription('od', '', [descr])
    api = Config(descr2)
    api.property.read_write()
    assert api.option('int.int').value.get() == [0]
    assert api.option('int.str', 0).value.get() == None
    assert api.option('int.str1', 0).value.get() == None
    api.option('int.int').value.set([0, 1])
    assert api.option('int.int').value.get() == [0, 1]
    assert api.option('int.str', 0).value.get() == None
    assert api.option('int.str', 1).value.get() == None
    assert api.option('int.str1', 0).value.get() == None
    assert api.option('int.str1', 1).value.get() == None
    api.option('int.str', 1).value.set('1')
    api.property.read_only()
    assert api.option('int.str1', 0).value.get() == None
    assert api.option('int.str1', 1).value.get() == None
    api.property.read_write()
    assert api.option('int.str1', 0).value.get() == None
    raises(PropertiesOptionError, "api.option('int.str1', 1).value.get()")


def test_multi_with_requires_that_is_leadership_follower_inverse():
    b = IntOption('int', 'Test int option', default=[0], multi=True)
    c = StrOption('str', 'Test string option', multi=True)
    d = StrOption('str1', 'Test string option', requires=[{'option': c, 'expected': None, 'action': 'hidden', 'inverse': True}], multi=True)
    descr = Leadership("int", "", [b, c, d])
    descr2 = OptionDescription('od', '', [descr])
    api = Config(descr2)
    api.property.read_write()
    assert api.option('int.int').value.get() == [0]
    assert api.option('int.str', 0).value.get() is None
    assert api.option('int.str1', 0).value.get() is None
    api.option('int.int').value.set([0, 1])
    assert api.option('int.int').value.get() == [0, 1]
    assert api.option('int.str', 0).value.get() is None
    assert api.option('int.str', 1).value.get() is None
    assert api.option('int.str1', 0).value.get() is None
    assert api.option('int.str1', 1).value.get() is None
    api.option('int.str', 1).value.set('1')
    api.property.read_only()
    assert api.option('int.str1', 0).value.get() is None
    assert api.option('int.str1', 1).value.get() is None
    api.property.read_write()
    assert api.option('int.str1', 0).value.get() is None
    raises(PropertiesOptionError, "api.option('int.str1', 1).value.get()")


def test_multi_with_requires_that_is_not_same_leadership():
    b = IntOption('int', 'Test int option', default=[0], multi=True)
    c = StrOption('str', 'Test string option', requires=[{'option': b, 'expected': 1, 'action': 'hidden'}], multi=True)
    descr1 = Leadership("int", "", [b, c])
    d = IntOption('int1', 'Test int option', default=[0], multi=True)
    e = StrOption('str', 'Test string option', requires=[{'option': b, 'expected': 1, 'action': 'hidden'}], multi=True)
    descr2 = Leadership("int1", "", [d, e])
    descr3 = OptionDescription('val', '', [descr1, descr2])
    descr3
    raises(ValueError, "Config(descr3)")


def test_multi_with_bool():
    s = BoolOption("bool", "", default=[False], multi=True)
    descr = OptionDescription("options", "", [s])
    api = Config(descr)
    api.option('bool').value.set([True, False])
    assert api.option('bool').value.get() == [True, False]


def test_choice_access_with_multi():
    ch = ChoiceOption("t1", "", ("a", "b"), default=["a"], multi=True)
    descr = OptionDescription("options", "", [ch])
    api = Config(descr)
    api.option('t1').value.set(["a", "b", "a", "b"]) 
    assert api.option('t1').value.get() == ["a", "b", "a", "b"]


#____________________________________________________________
def test_accepts_multiple_changes_from_option():
    s = StrOption("string", "", default="string")
    descr = OptionDescription("options", "", [s])
    api = Config(descr)
    api.option('string').value.set("egg")
    assert api.option('string').option.default() == "string"
    assert api.option('string').value.get() == "egg"
    api.option('string').value.set('blah')
    assert api.option('string').option.default() == "string"
    assert api.option('string').value.get() == "blah"
    api.option('string').value.set('bol')
    assert api.option('string').value.get() == 'bol'


def test_allow_multiple_changes_from_config():
    """
    a `setoption` from the config object is much like the attribute access,
    except the fact that value owner can bet set
    """
    s = StrOption("string", "", default="string")
    s2 = StrOption("string2", "", default="string")
    suboption = OptionDescription("bip", "", [s2])
    descr = OptionDescription("options", "", [s, suboption])
    api = Config(descr)
    api.option('string').value.set("oh")
    assert api.option('string').value.get() == "oh"
    api.option('string').value.set("blah")
    assert api.option('string').value.get() == "blah"


# ____________________________________________________________
# accessing a value by the get method
def test_access_by_get():
    descr = make_description()
    api = Config(descr)
    raises(AttributeError, "list(api.option.find('idontexist'))")
    assert api.option.find('wantref', first=True).value.get() is False
    assert api.option.find('dummy', first=True).value.get() is False


def test_access_by_get_whith_hide():
    b1 = BoolOption("b1", "", properties=(('hidden'),))
    descr = OptionDescription("opt", "",
                              [OptionDescription("sub", "",
                                                 [b1, ChoiceOption("c1", "", ('a', 'b', 'c'), 'a'),
                                                  BoolOption("d1", "")]),
                               BoolOption("b2", ""),
                               BoolOption("d1", "")])
    api = Config(descr)
    api.property.read_write()
    raises(AttributeError, "api.option.find('b1').value.get()")


def test_append_properties():
    descr = make_description()
    api = Config(descr)
    assert api.option('gc.dummy').property.get() == set()
    api.option('gc.dummy').property.add('test')
    assert api.option('gc.dummy').property.get() == {'test'}
    raises(ConfigError, "api.option('gc.dummy').property.add('force_store_value')")
    assert api.option('gc.dummy').property.get() == {'test'}


def test_reset_properties():
    descr = make_description()
    api = Config(descr)
    assert api.option('gc.dummy').property.get() == set()
    api.option('gc.dummy').property.add('frozen')
    assert api.option('gc.dummy').property.get() == {'frozen'}
    api.option('gc.dummy').property.reset()
    assert api.option('gc.dummy').property.get() == set()


def test_properties_cached():
    b1 = BoolOption("b1", "", properties=('test',))
    descr = OptionDescription("opt", "", [OptionDescription("sub", "", [b1])])
    api = Config(descr)
    api.property.read_write()
    assert api.option('sub.b1').property.get() == {'test'}


def test_append_properties_force_store_value():
    gcdummy = BoolOption('dummy', 'dummy', default=False, properties=('force_store_value',))
    gcgroup = OptionDescription('gc', '', [gcdummy])
    descr = OptionDescription('tiramisu', '', [gcgroup])
    api = Config(descr)
    assert api.option('gc.dummy').property.get() == {'force_store_value'}
    api.option('gc.dummy').property.add('test')
    assert api.option('gc.dummy').property.get() == {'force_store_value', 'test'}


def test_reset_properties_force_store_value():
    gcdummy = BoolOption('dummy', 'dummy', default=False, properties=('force_store_value',))
    gcgroup = OptionDescription('gc', '', [gcdummy])
    descr = OptionDescription('tiramisu', '', [gcgroup])
    api = Config(descr)
    assert api.property.exportation() == {}
    api.property.add('frozen')
    assert api.property.exportation() == \
            {None: set(('frozen', 'cache', 'validator', 'warnings'))}
    api.property.reset()
    assert api.property.exportation() == {}
    api.option('gc.dummy').property.add('test')
    assert api.property.exportation() == {'gc.dummy': set(('test', 'force_store_value'))}
    api.property.reset()
    assert api.property.exportation() == {'gc.dummy': set(('test', 'force_store_value'))}
    api.property.add('frozen')
    assert api.property.exportation() == \
            {None: set(('frozen', 'validator', 'cache', 'warnings')),
             'gc.dummy': set(('test', 'force_store_value'))}
    api.property.add('frozen')
    assert api.property.exportation() == \
            {None: set(('frozen', 'validator', 'cache', 'warnings')),
             'gc.dummy': set(('test', 'force_store_value'))}
    api.option('gc.dummy').property.add('test')
    assert api.property.exportation() == \
            {None: set(('frozen', 'validator', 'cache', 'warnings')),
             'gc.dummy': set(('test', 'force_store_value'))}


def test_importation_force_store_value():
    gcdummy = BoolOption('dummy', 'dummy', default=False,
                         properties=('force_store_value',))
    gcgroup = OptionDescription('gc', '', [gcdummy])
    descr = OptionDescription('tiramisu', '', [gcgroup])
    config1 = Config(descr)
    assert config1.value.exportation() == [[], [], [], []]
    config1.property.add('frozen')
    assert config1.value.exportation() == [[], [], [], []]
    config1.property.add('force_store_value')
    assert config1.value.exportation() == [['gc.dummy'], [None], [False], ['forced']]
    exportation = config1.property.exportation()
    config2 = Config(descr)
    assert config2.value.exportation() == [[], [], [], []]
    config2.property.importation(exportation)
    assert config2.value.exportation() == [['gc.dummy'], [None], [False], ['forced']]
    config2.property.importation(exportation)
    assert config2.value.exportation() == [['gc.dummy'], [None], [False], ['forced']]


def test_set_modified_value():
    gcdummy = BoolOption('dummy', 'dummy', default=False, properties=('force_store_value',))
    gcgroup = OptionDescription('gc', '', [gcdummy])
    descr = OptionDescription('tiramisu', '', [gcgroup])
    api = Config(descr)
    assert api.property.exportation() == {}
    api.property.importation({None: set(('frozen', 'cache', 'validator', 'warnings'))})
    assert api.property.exportation() == \
            {None: set(('frozen', 'cache', 'validator', 'warnings'))}


def test_pprint():
    msg_error = _("cannot access to {0} \"{1}\" because has {2} {3}")
    msg_is_not = _('the value of "{0}" is not {1}')
    msg_is = _('the value of "{0}" is "{1}"')
    properties = _('properties')
    prop = _('property')

    s = StrOption("string", "", default=["string"], default_multi="string", multi=True, properties=('hidden', 'disabled'))
    s2 = StrOption("string2", "", default="string")
    s3 = StrOption("string3", "", default=["string"], default_multi="string", multi=True, properties=('hidden',))
    intoption = IntOption('int', 'Test int option', default=0)
    stroption = StrOption('str', 'Test string option', default="abc",
                          requires=[{'option': intoption, 'expected': 2, 'action': 'hidden', 'inverse': True},
                    {'option': intoption, 'expected': 3, 'action': 'hidden', 'inverse': True},
                    {'option': intoption, 'expected': 4, 'action': 'hidden', 'inverse': True},
                    {'option': intoption, 'expected': 1, 'action': 'disabled'},
                    {'option': s2, 'expected': 'string', 'action': 'disabled'}])

    val2 = StrOption('val2', "")
    descr2 = OptionDescription("options", "", [val2], requires=[{'option': intoption, 'expected': 1, 'action': 'hidden'}])

    val3 = StrOption('val3', "", requires=[{'option': stroption, 'expected': '2', 'action': 'hidden', 'inverse': True}])

    descr = OptionDescription("options", "", [s, s2, s3, intoption, stroption, descr2, val3])
    api = Config(descr)
    api.property.read_write()
    api.option('int').value.set(1)
    err = None
    try:
        api.option('str').value.get()
    except PropertiesOptionError as error:
        err = error

    list_disabled = '"disabled" (' + display_list([msg_is.format('Test int option', '1'), msg_is.format('string2', 'string')]) + ')'
    list_hidden = '"hidden" (' + msg_is_not.format('Test int option', display_list([2, 3, 4], 'or', add_quote=True)) + ')'
    assert str(err) == _(msg_error.format('option', 'Test string option', properties, display_list([list_disabled, list_hidden])))
    del err

    err = None
    try:
        api.option('options.val2').value.get()
    except PropertiesOptionError as error:
        err = error

    assert str(err) == msg_error.format('optiondescription', 'options', prop, '"hidden" (' + msg_is.format('Test int option', 1) + ')')

    #err = None
    #try:
    #    api.option('val3').value.get()
    #except PropertiesOptionError as error:
    #    err = error

    #msg_1 = msg_is.format('string2', 'string')
    #msg_2 = msg_is.format('Test int option', 1)
    #msg_3 = msg_is_not.format('Test int option', display_list([2, 3, 4], 'or', add_quote=True))
    #list_hidden = '"hidden" (' + display_list([msg_2, msg_3, msg_1]) + ')'

    #assert str(err) == msg_error.format('option', 'val3', prop, list_hidden)

    err = None
    try:
        api.option('string').value.get()
    except Exception as error:
        err = error

    assert str(err) == msg_error.format('option', 'string', properties, display_list(['disabled', 'hidden'], add_quote=True))
    del err

    err = None
    try:
        api.option('string3').value.get()
    except Exception as error:
        err = error

    assert str(err) == msg_error.format('option', 'string3', prop, '"hidden"')
    del err
