from .autopath import do_autopath
do_autopath()

from py.test import raises

from tiramisu.setting import groups, owners
from tiramisu import IntOption, StrOption, NetworkOption, NetmaskOption, \
                     OptionDescription, Leadership, Config, GroupConfig, MixConfig, \
                     Params, ParamOption, ParamValue
from tiramisu.error import ConfigError, ConflictError, PropertiesOptionError, LeadershipError, APIError
from tiramisu.storage import list_sessions

owners.addowner('mix1')
owners.addowner('mix2')


def teardown_function(function):
    assert list_sessions() == [], 'session list is not empty when leaving "{}"'.format(function.__name__)


def return_value(value=None):
    return value


def raise_exception():
    raise Exception('test')


def make_description():
    i1 = IntOption('i1', '')
    i2 = IntOption('i2', '', default=1)
    i3 = IntOption('i3', '')
    i4 = IntOption('i4', '', default=2)
    i5 = IntOption('i5', '', default=[2], multi=True)
    i6 = IntOption('i6', '', properties=('disabled',))
    od1 = OptionDescription('od1', '', [i1, i2, i3, i4, i5, i6])
    od2 = OptionDescription('od2', '', [od1])
    return od2


def make_description1():
    i1 = IntOption('i1', '')
    i2 = IntOption('i2', '', default=1)
    i3 = IntOption('i3', '')
    i4 = IntOption('i4', '', default=2)
    i5 = IntOption('i5', '', default=[2], multi=True)
    i6 = IntOption('i6', '', properties=('disabled',))
    od1 = OptionDescription('od1', '', [i1, i2, i3, i4, i5, i6])
    od2 = OptionDescription('od2', '', [od1])
    return od2


def make_description2():
    i1 = IntOption('i1', '')
    i2 = IntOption('i2', '', default=1)
    i3 = IntOption('i3', '')
    i4 = IntOption('i4', '', default=2)
    i5 = IntOption('i5', '', default=[2], multi=True)
    i6 = IntOption('i6', '', properties=('disabled',))
    od1 = OptionDescription('od1', '', [i1, i2, i3, i4, i5, i6])
    od2 = OptionDescription('od2', '', [od1])
    return od2


def make_description3():
    i1 = IntOption('i1', '')
    i2 = IntOption('i2', '', default=1)
    i3 = IntOption('i3', '')
    i4 = IntOption('i4', '', default=2)
    i5 = IntOption('i5', '', default=[2], multi=True)
    i6 = IntOption('i6', '', properties=('disabled',))
    od1 = OptionDescription('od1', '', [i1, i2, i3, i4, i5, i6])
    od2 = OptionDescription('od2', '', [od1])
    return od2


def make_mixconfig(double=False):
    od1 = make_description()
    od2 = make_description1()
    od3 = make_description2()
    conf1 = Config(od1, session_id='conf1')
    conf2 = Config(od2, session_id='conf2')
    mix = MixConfig(od3, [conf1, conf2], session_id='mix')
    if double:
        od4 = make_description3()
        mix.owner.set(owners.mix2)
        mix = MixConfig(od4, [mix], session_id='doublemix')
    mix.property.read_write()
    mix.owner.set(owners.mix1)
    return mix


def test_mix_name():
    mix = make_mixconfig(True)
    assert mix.config.path() == 'doublemix'
    assert mix.config('mix').config.path() == 'doublemix.mix'
    assert mix.config('mix.conf1').config.path() == 'doublemix.mix.conf1'
    assert mix.config('mix.conf2').config.path() == 'doublemix.mix.conf2'


def test_mix_not_group():
    i1 = IntOption('i1', '')
    od1 = OptionDescription('od1', '', [i1])
    od2 = OptionDescription('od2', '', [od1])
    conf1 = Config(od2, session_id='conf1')
    grp = GroupConfig([conf1])
    raises(TypeError, "MixConfig(od2, [grp])")


def test_unknown_config():
    mix = make_mixconfig()
    raises(ConfigError, "mix.config('unknown')")


def test_none():
    mix = make_mixconfig()
    assert mix.option('od1.i3').value.get() is mix.config('conf1').option('od1.i3').value.get() is mix.config('conf2').option('od1.i3').value.get() is None
    assert mix.option('od1.i3').owner.get() is mix.config('conf1').option('od1.i3').owner.get() is mix.config('conf2').option('od1.i3').owner.get() is owners.default
    #
    mix.option('od1.i3').value.set(3)
    assert mix.option('od1.i3').value.get() == mix.config('conf1').option('od1.i3').value.get() == mix.config('conf2').option('od1.i3').value.get() == 3
    assert mix.option('od1.i3').owner.get() is mix.config('conf1').option('od1.i3').owner.get() is mix.config('conf2').option('od1.i3').owner.get() is owners.mix1
    #
    mix.config('conf1').option('od1.i3').value.set(2)
    assert mix.option('od1.i3').value.get() == mix.config('conf2').option('od1.i3').value.get() == 3
    assert mix.config('conf1').option('od1.i3').value.get() == 2
    assert mix.option('od1.i3').owner.get() is mix.config('conf2').option('od1.i3').owner.get() is owners.mix1
    assert mix.config('conf1').option('od1.i3').owner.get() is owners.user
    #
    mix.option('od1.i3').value.set(4)
    assert mix.option('od1.i3').value.get() == mix.config('conf2').option('od1.i3').value.get() == 4
    assert mix.config('conf1').option('od1.i3').value.get() == 2
    assert mix.option('od1.i3').owner.get() is mix.config('conf2').option('od1.i3').owner.get() is owners.mix1
    assert mix.config('conf1').option('od1.i3').owner.get() is owners.user
    #
    mix.option('od1.i3').value.reset()
    assert mix.option('od1.i3').value.get() is mix.config('conf2').option('od1.i3').value.get() is None
    assert mix.config('conf1').option('od1.i3').value.get() == 2
    assert mix.option('od1.i3').owner.get() is mix.config('conf2').option('od1.i3').owner.get() is owners.default
    assert mix.config('conf1').option('od1.i3').owner.get() is owners.user
    #
    mix.config('conf1').option('od1.i3').value.reset()
    assert mix.option('od1.i3').value.get() is mix.config('conf1').option('od1.i3').value.get() is mix.config('conf2').option('od1.i3').value.get() is None
    assert mix.option('od1.i3').owner.get() is mix.config('conf1').option('od1.i3').owner.get() is mix.config('conf2').option('od1.i3').owner.get() is owners.default
    #
    assert mix.config(None).config.name() == mix.config.name()


def test_reset():
    mix = make_mixconfig()
    assert mix.option('od1.i2').value.get() == 1
    mix.option('od1.i2').value.set(2)
    mix.config('conf1').option('od1.i2').value.set(3)
    assert mix.option('od1.i2').value.get() == 2
    assert mix.config('conf1').option('od1.i2').value.get() == 3
    assert mix.config('conf2').option('od1.i2').value.get() == 2
    mix.config.reset()
    assert mix.option('od1.i2').value.get() == 1
    assert mix.config('conf1').option('od1.i2').value.get() == 3
    assert mix.config('conf2').option('od1.i2').value.get() == 1


def test_default():
    mix = make_mixconfig()
    assert mix.option('od1.i2').value.get() == mix.config('conf1').option('od1.i2').value.get() == mix.config('conf2').option('od1.i2').value.get() == 1
    assert mix.option('od1.i2').owner.get() is mix.config('conf1').option('od1.i2').owner.get() is mix.config('conf2').option('od1.i2').owner.get() is owners.default
    #
    mix.option('od1.i2').value.set(3)
    assert mix.option('od1.i2').value.get() == mix.config('conf1').option('od1.i2').value.get() == mix.config('conf2').option('od1.i2').value.get() == 3
    assert mix.option('od1.i2').owner.get() is mix.config('conf1').option('od1.i2').owner.get() is mix.config('conf2').option('od1.i2').owner.get() is owners.mix1
    #
    mix.config('conf1').option('od1.i2').value.set(2)
    assert mix.option('od1.i2').value.get() == mix.config('conf2').option('od1.i2').value.get() == 3
    assert mix.config('conf1').option('od1.i2').value.get() == 2
    assert mix.option('od1.i2').owner.get() is mix.config('conf2').option('od1.i2').owner.get() is owners.mix1
    assert mix.config('conf1').option('od1.i2').owner.get() is owners.user
    #
    mix.option('od1.i2').value.set(4)
    assert mix.option('od1.i2').value.get() == mix.config('conf2').option('od1.i2').value.get() == 4
    assert mix.config('conf1').option('od1.i2').value.get() == 2
    assert mix.option('od1.i2').owner.get() is mix.config('conf2').option('od1.i2').owner.get() is owners.mix1
    assert mix.config('conf1').option('od1.i2').owner.get() is owners.user
    #
    mix.option('od1.i2').value.reset()
    assert mix.option('od1.i2').value.get() == mix.config('conf2').option('od1.i2').value.get() == 1
    assert mix.config('conf1').option('od1.i2').value.get() == 2
    assert mix.option('od1.i2').owner.get() is mix.config('conf2').option('od1.i2').owner.get() is owners.default
    assert mix.config('conf1').option('od1.i2').owner.get() is owners.user
    #
    mix.config('conf1').option('od1.i2').value.reset()
    assert mix.option('od1.i2').value.get() == mix.config('conf1').option('od1.i2').value.get() == mix.config('conf2').option('od1.i2').value.get() == 1
    assert mix.option('od1.i2').owner.get() is mix.config('conf1').option('od1.i2').owner.get() is mix.config('conf2').option('od1.i2').owner.get() is owners.default


def test_contexts():
    mix = make_mixconfig()
    errors = mix.value.set('od1.i2', 6, only_config=True)
    assert mix.option('od1.i2').value.get() == 1
    assert mix.option('od1.i2').owner.get() == owners.default
    assert mix.config('conf1').option('od1.i2').value.get() == mix.config('conf1').option('od1.i2').value.get() == 6
    assert mix.config('conf1').option('od1.i2').owner.get() == mix.config('conf1').option('od1.i2').owner.get() is owners.user
    assert len(errors) == 0


def test_find():
    mix = make_mixconfig()
    ret = list(mix.option.find('i2'))
    assert len(ret) == 1
    assert 1 == ret[0].value.get()
    assert 1 == mix.option.find('i2', first=True).value.get()
    assert mix.value.dict() == {'od1.i4': 2, 'od1.i1': None, 'od1.i3': None,
                                 'od1.i2': 1, 'od1.i5': [2]}


def test_mix_mix():
    mix = make_mixconfig(double=True)
    assert mix.option('od1.i2').value.get() == mix.config('mix').option('od1.i2').value.get() == mix.config('mix.conf1').option('od1.i2').value.get() == mix.config('mix.conf2').option('od1.i2').value.get() == 1
    assert mix.option('od1.i2').owner.get() is mix.config('mix').option('od1.i2').owner.get() is mix.config('mix.conf1').option('od1.i2').owner.get() is mix.config('mix.conf2').option('od1.i2').owner.get() is owners.default
    #
    mix.option('od1.i2').value.set(3)
    assert mix.option('od1.i2').value.get() == mix.config('mix').option('od1.i2').value.get() == mix.config('mix.conf1').option('od1.i2').value.get() == mix.config('mix.conf2').option('od1.i2').value.get() == 3
    assert mix.option('od1.i2').owner.get() is mix.config('mix').option('od1.i2').owner.get() is mix.config('mix.conf1').option('od1.i2').owner.get() is mix.config('mix.conf2').option('od1.i2').owner.get() is owners.mix1
    #
    mix.config('mix.conf1').option('od1.i2').value.set(2)
    assert mix.option('od1.i2').value.get() == mix.config('mix').option('od1.i2').value.get() == mix.config('mix.conf2').option('od1.i2').value.get() == 3
    assert mix.config('mix.conf1').option('od1.i2').value.get() == 2
    assert mix.option('od1.i2').owner.get() is mix.config('mix').option('od1.i2').owner.get() is mix.config('mix.conf2').option('od1.i2').owner.get() is owners.mix1
    assert mix.config('mix.conf1').option('od1.i2').owner.get() is owners.user
    #
    mix.config('mix').option('od1.i2').value.set(4)
    assert mix.option('od1.i2').value.get() == 3
    assert mix.config('mix').option('od1.i2').value.get() == mix.config('mix.conf2').option('od1.i2').value.get() == 4
    assert mix.config('mix.conf1').option('od1.i2').value.get() == 2
    assert mix.option('od1.i2').owner.get() is owners.mix1
    assert mix.config('mix').option('od1.i2').owner.get() is mix.config('mix.conf2').option('od1.i2').owner.get() is owners.mix2
    assert mix.config('mix.conf1').option('od1.i2').owner.get() is owners.user
    #
    mix.config('mix').option('od1.i2').value.reset()
    assert mix.option('od1.i2').value.get() == mix.config('mix').option('od1.i2').value.get() == mix.config('mix.conf2').option('od1.i2').value.get() == 3
    assert mix.config('mix.conf1').option('od1.i2').value.get() == 2
    assert mix.option('od1.i2').owner.get() is mix.config('mix').option('od1.i2').owner.get() is mix.config('mix.conf2').option('od1.i2').owner.get() is owners.mix1
    assert mix.config('mix.conf1').option('od1.i2').owner.get() is owners.user
    #
    mix.option('od1.i2').value.reset()
    assert mix.option('od1.i2').value.get() == mix.config('mix').option('od1.i2').value.get() == mix.config('mix.conf2').option('od1.i2').value.get() == 1
    assert mix.config('mix.conf1').option('od1.i2').value.get() == 2
    assert mix.option('od1.i2').owner.get() is mix.config('mix').option('od1.i2').owner.get() is mix.config('mix.conf2').option('od1.i2').owner.get() is owners.default
    assert mix.config('mix.conf1').option('od1.i2').owner.get() is owners.user
    #
    mix.config('mix.conf1').option('od1.i2').value.reset()
    assert mix.option('od1.i2').value.get() == mix.config('mix').option('od1.i2').value.get() == mix.config('mix.conf1').option('od1.i2').value.get() == mix.config('mix.conf2').option('od1.i2').value.get() == 1
    assert mix.option('od1.i2').owner.get() is mix.config('mix').option('od1.i2').owner.get() is mix.config('mix.conf1').option('od1.i2').owner.get() is mix.config('mix.conf2').option('od1.i2').owner.get() is owners.default


def test_mix_mix_set():
    mix = make_mixconfig(double=True)
    errors1 = mix.value.set('od1.i1', 7, only_config=True)
    errors2 = mix.value.set('od1.i6', 7, only_config=True)
    assert len(errors1) == 0
    assert len(errors2) == 2
    conf1 = mix.config('mix.conf1')._config_bag.context
    conf2 = mix.config('mix.conf2')._config_bag.context
    assert mix.config('mix.conf1').option('od1.i1').value.get() == mix.config('mix.conf2').option('od1.i1').value.get() == 7
    #
    dconfigs = []
    for conf in mix.config.find('i1', value=7).config.list():
        dconfigs.append(conf._config_bag.context)
    assert [conf1, conf2] == dconfigs
    mix.config('mix.conf1').option('od1.i1').value.set(8)
    #
    dconfigs = []
    for conf in mix.config.find('i1').config.list():
        dconfigs.append(conf._config_bag.context)
    assert [conf1, conf2] == dconfigs
    assert conf2 == list(mix.config.find('i1', value=7).config.list())[0]._config_bag.context
    assert conf1 == list(mix.config.find('i1', value=8).config.list())[0]._config_bag.context
    #
    dconfigs = []
    for conf in mix.config.find('i5', value=2).config.list():
        dconfigs.append(conf._config_bag.context)
    assert [conf1, conf2] == dconfigs
    #
    raises(AttributeError, "mix.config.find('i1', value=10)")
    raises(AttributeError, "mix.config.find('not', value=10)")
    raises(AttributeError, "mix.config.find('i6')")
    raises(ValueError, "mix.value.set('od1.i6', 7, only_config=True, force_default=True)")
    raises(ValueError, "mix.value.set('od1.i6', 7, only_config=True, force_default_if_same=True)")
    raises(ValueError, "mix.value.set('od1.i6', 7, only_config=True, force_dont_change_value=True)")


def test_mix_unconsistent():
    i1 = IntOption('i1', '')
    i2 = IntOption('i2', '', default=1)
    i3 = IntOption('i3', '')
    i4 = IntOption('i4', '', default=2)
    od1 = OptionDescription('od1', '', [i1, i2, i3, i4])
    od2 = OptionDescription('od2', '', [od1])
    od3 = OptionDescription('od3', '', [od1])
    conf1 = Config(od2, session_id='conf1')
    conf2 = Config(od2, session_id='conf2')
    conf3 = Config(od2, session_id='conf3')
    i5 = IntOption('i5', '')
    od4 = OptionDescription('od4', '', [i5])
    conf4 = Config(od4, session_id='conf4')
    mix = MixConfig(od2, [conf1, conf2])
    mix.owner.set(owners.mix1)
    raises(TypeError, 'MixConfig(od2, "string")')
    # same descr but conf1 already in mix
    raises(ValueError, "MixConfig(od2, [conf1, conf3])")
    # not same descr
    MixConfig(od2, [conf3, conf4])


def test_mix_leadership():
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip", multi=True)
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "mask", multi=True, properties=('hidden',))
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    od = OptionDescription('root', '', [interface1])
    conf1 = Config(od, session_id='conf1')
    conf2 = Config(od, session_id='conf2')
    mix = MixConfig(od, [conf1, conf2])
    mix.property.read_only()
    itr = mix.config.find('ip_admin_eth0').config.list()
    assert conf1._config_bag.context == next(itr)._config_bag.context
    assert conf2._config_bag.context == next(itr)._config_bag.context
    itr = mix.config.find('netmask_admin_eth0').config.list()
    assert conf1._config_bag.context == next(itr)._config_bag.context
    assert conf2._config_bag.context == next(itr)._config_bag.context
    mix.property.read_write()
    raises(AttributeError, "mix.config.find('netmask_admin_eth0')")
    itr = mix.unrestraint.config.find('netmask_admin_eth0').config.list()
    assert conf1._config_bag.context == next(itr)._config_bag.context
    assert conf2._config_bag.context == next(itr)._config_bag.context
    mix.property.read_only()
    itr = mix.config.find('netmask_admin_eth0').config.list()
    assert conf1._config_bag.context == next(itr)._config_bag.context
    assert conf2._config_bag.context == next(itr)._config_bag.context


def test_mix_leadership_value2():
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip", multi=True)
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "mask", multi=True, properties=('hidden',))
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    od = OptionDescription('root', '', [interface1])
    conf1 = Config(od, session_id='conf1')
    conf2 = Config(od, session_id='conf2')
    mix = MixConfig(od, [conf1, conf2], session_id="mix")
    mix.config('conf1').option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.1.8'])
    assert mix.config('conf1').option('ip_admin_eth0.netmask_admin_eth0', 0).value.get() == None
    #FIXME devrait raise ! assert mix.config('conf1').option('ip_admin_eth0.ip_admin_eth0', 0).value.get() == None
    #
    mix.config('conf1').option('ip_admin_eth0.ip_admin_eth0').value.reset()
    #
    mix.option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.1.1'])
    assert mix.config('conf1').option('ip_admin_eth0.netmask_admin_eth0', 0).value.get() == None
    mix.option('ip_admin_eth0.netmask_admin_eth0', 0).value.set('255.255.255.0')
    assert mix.config('conf1').option('ip_admin_eth0.netmask_admin_eth0', 0).value.get() == '255.255.255.0'
    mix.option('ip_admin_eth0.netmask_admin_eth0', 0).value.set('255.255.0.0')
    assert mix.config('conf1').option('ip_admin_eth0.netmask_admin_eth0', 0).value.get() == '255.255.0.0'
    #
    mix.config('conf1').option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.1.1'])
    assert mix.config('conf1').option('ip_admin_eth0.netmask_admin_eth0', 0).value.get() == None


def test_mix_leadership_value_default():
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip", multi=True, default=['192.168.1.1'])
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "mask", multi=True)
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    od = OptionDescription('root', '', [interface1])
    conf1 = Config(od, session_id='conf1')
    conf2 = Config(od, session_id='conf2')
    mix = MixConfig(od, [conf1, conf2])
    assert mix.config('conf1').option('ip_admin_eth0.netmask_admin_eth0', 0).value.get() == None
    #
    mix.option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.1.1'])
    assert mix.config('conf1').option('ip_admin_eth0.netmask_admin_eth0', 0).value.get() == None
    #
    mix.option('ip_admin_eth0.netmask_admin_eth0', 0).value.set('255.255.255.0')
    assert mix.config('conf1').option('ip_admin_eth0.netmask_admin_eth0', 0).value.get() == '255.255.255.0'
    #
    mix.option('ip_admin_eth0.netmask_admin_eth0', 0).value.set('255.255.0.0')
    assert mix.config('conf1').option('ip_admin_eth0.netmask_admin_eth0', 0).value.get() == '255.255.0.0'
    #
    mix.config('conf1').option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.1.1'])
    assert mix.config('conf1').option('ip_admin_eth0.netmask_admin_eth0', 0).value.get() == None


def test_mix_leadership_owners():
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip", multi=True)
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "mask", multi=True, properties=('hidden',))
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    od = OptionDescription('root', '', [interface1])
    conf1 = Config(od, session_id='conf1')
    conf2 = Config(od, session_id='conf2')
    mix = MixConfig(od, [conf1, conf2])
    mix.owner.set(owners.mix1)
    assert mix.config('conf1').option('ip_admin_eth0.ip_admin_eth0').owner.isdefault()
    raises(LeadershipError, "mix.config('conf1').option('ip_admin_eth0.netmask_admin_eth0', 0).owner.isdefault()")
    #
    mix.config('conf1').option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.1.1'])
    assert mix.config('conf1').option('ip_admin_eth0.ip_admin_eth0').owner.get() == owners.user
    assert mix.config('conf1').option('ip_admin_eth0.netmask_admin_eth0', 0).owner.isdefault()
    #
    mix.config('conf1').option('ip_admin_eth0.ip_admin_eth0').value.reset()
    assert mix.config('conf1').option('ip_admin_eth0.ip_admin_eth0').owner.isdefault()
    #
    mix.option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.1.1'])
    assert mix.config('conf1').option('ip_admin_eth0.ip_admin_eth0').owner.get() == owners.mix1
    assert mix.config('conf1').option('ip_admin_eth0.netmask_admin_eth0', 0).owner.isdefault()
    #
    mix.option('ip_admin_eth0.netmask_admin_eth0', 0).value.set('255.255.255.0')
    assert mix.config('conf1').option('ip_admin_eth0.ip_admin_eth0').owner.get() == owners.mix1
    assert mix.config('conf1').option('ip_admin_eth0.netmask_admin_eth0', 0).owner.get() == owners.mix1
    #
    mix.option('ip_admin_eth0.netmask_admin_eth0', 0).value.set('255.255.0.0')
    assert mix.config('conf1').option('ip_admin_eth0.ip_admin_eth0').owner.get() == owners.mix1
    assert mix.config('conf1').option('ip_admin_eth0.netmask_admin_eth0', 0).owner.get() == owners.mix1
    #
    mix.config('conf1').option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.1.1'])
    assert mix.config('conf1').option('ip_admin_eth0.ip_admin_eth0').owner.get() == owners.user
    assert mix.config('conf1').option('ip_admin_eth0.netmask_admin_eth0', 0).owner.get() == owners.default


def test_mix_force_default():
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip", multi=True)
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "mask", multi=True, properties=('hidden',))
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    od = OptionDescription('root', '', [interface1])
    conf1 = Config(od, session_id='conf1')
    conf2 = Config(od, session_id='conf2')
    mix = MixConfig(od, [conf1, conf2])
    mix.property.read_write()
    mix.owner.set('mix1')
    assert mix.option('ip_admin_eth0.ip_admin_eth0').value.get() == []
    assert mix.config('conf1').option('ip_admin_eth0.ip_admin_eth0').value.get() == []
    assert mix.config('conf2').option('ip_admin_eth0.ip_admin_eth0').value.get() == []
    #
    errors = mix.value.set('ip_admin_eth0.ip_admin_eth0', ['192.168.1.1'])
    assert len(errors) == 0
    assert mix.option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.1']
    assert mix.config('conf1').option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.1']
    assert mix.config('conf2').option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.1']
    #
    mix.config('conf1').option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.1.2'])
    assert mix.option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.1']
    assert mix.config('conf1').option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.2']
    assert mix.config('conf2').option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.1']
    #
    errors = mix.value.set('ip_admin_eth0.ip_admin_eth0', ['192.168.1.3'])
    assert len(errors) == 0
    assert mix.option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.3']
    assert mix.config('conf1').option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.2']
    assert mix.config('conf2').option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.3']
    #
    errors = mix.value.set('ip_admin_eth0.ip_admin_eth0', ['192.168.1.4'], force_default=True)
    assert len(errors) == 0
    assert mix.option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.4']
    assert mix.config('conf1').option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.4']
    assert mix.config('conf2').option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.4']


def test_mix_force_dont_change_value():
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip", multi=True)
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "mask", multi=True, properties=('hidden',))
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    od = OptionDescription('root', '', [interface1])
    conf1 = Config(od, session_id='conf1')
    conf2 = Config(od, session_id='conf2')
    mix = MixConfig(od, [conf1, conf2])
    mix.property.read_write()
    mix.owner.set('mix1')
    assert mix.option('ip_admin_eth0.ip_admin_eth0').value.get() == []
    assert mix.config('conf1').option('ip_admin_eth0.ip_admin_eth0').value.get() == []
    assert mix.config('conf2').option('ip_admin_eth0.ip_admin_eth0').value.get() == []
    mix.config('conf1').option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.1.4'])
    assert mix.config('conf1').option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.4']
    assert mix.config('conf2').option('ip_admin_eth0.ip_admin_eth0').value.get() == []
    assert mix.config('conf1').option('ip_admin_eth0.ip_admin_eth0').owner.get() is owners.user
    assert mix.config('conf2').option('ip_admin_eth0.ip_admin_eth0').owner.isdefault()
    errors = mix.value.set('ip_admin_eth0.ip_admin_eth0', ['192.168.1.4'], force_dont_change_value=True)
    assert len(errors) == 0
    assert mix.option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.4']
    assert mix.config('conf1').option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.4']
    assert mix.config('conf2').option('ip_admin_eth0.ip_admin_eth0').value.get() == []
    assert mix.config('conf1').option('ip_admin_eth0.ip_admin_eth0').owner.get() is owners.user
    assert mix.config('conf2').option('ip_admin_eth0.ip_admin_eth0').owner.get() is owners.user


def test_mix_force_default_if_same():
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip", multi=True)
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "mask", multi=True, properties=('hidden',))
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    od = OptionDescription('root', '', [interface1])
    conf1 = Config(od, session_id='conf1')
    conf2 = Config(od, session_id='conf2')
    mix = MixConfig(od, [conf1, conf2])
    mix.property.read_write()
    mix.owner.set('mix1')
    #
    assert mix.option('ip_admin_eth0.ip_admin_eth0').value.get() == []
    assert mix.config('conf1').option('ip_admin_eth0.ip_admin_eth0').value.get() == []
    assert mix.config('conf2').option('ip_admin_eth0.ip_admin_eth0').value.get() == []
    #
    mix.config('conf1').option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.1.4'])
    assert mix.config('conf1').option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.4']
    assert mix.config('conf2').option('ip_admin_eth0.ip_admin_eth0').value.get() == []
    assert mix.config('conf1').option('ip_admin_eth0.ip_admin_eth0').owner.get() is owners.user
    assert mix.config('conf2').option('ip_admin_eth0.ip_admin_eth0').owner.isdefault()
    errors = mix.value.set('ip_admin_eth0.ip_admin_eth0', ['192.168.1.4'], force_default_if_same=True)
    assert len(errors) == 0
    assert mix.option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.4']
    assert mix.config('conf1').option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.4']
    assert mix.config('conf2').option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.4']
    assert mix.config('conf1').option('ip_admin_eth0.ip_admin_eth0').owner.get() is owners.mix1
    assert mix.config('conf2').option('ip_admin_eth0.ip_admin_eth0').owner.get() is owners.mix1
    #
    mix.config('conf1').option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.1.3'])
    assert mix.config('conf1').option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.3']
    assert mix.config('conf2').option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.4']
    assert mix.config('conf1').option('ip_admin_eth0.ip_admin_eth0').owner.get() is owners.user
    assert mix.config('conf2').option('ip_admin_eth0.ip_admin_eth0').owner.get() is owners.mix1
    errors = mix.value.set('ip_admin_eth0.ip_admin_eth0', ['192.168.1.5'], force_default_if_same=True)
    assert len(errors) == 0
    assert mix.option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.5']
    assert mix.config('conf1').option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.3']
    assert mix.config('conf2').option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.5']
    assert mix.config('conf1').option('ip_admin_eth0.ip_admin_eth0').owner.get() is owners.user
    assert mix.config('conf2').option('ip_admin_eth0.ip_admin_eth0').owner.get() is owners.mix1


def test_mix_force_default_if_same_and_dont_change():
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip", multi=True)
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "mask", multi=True, properties=('hidden',))
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    od = OptionDescription('root', '', [interface1])
    conf1 = Config(od, session_id='conf1')
    conf2 = Config(od, session_id='conf2')
    mix = MixConfig(od, [conf1, conf2])
    mix.property.read_write()
    mix.owner.set('mix1')
    #
    assert mix.option('ip_admin_eth0.ip_admin_eth0').value.get() == []
    assert mix.config('conf1').option('ip_admin_eth0.ip_admin_eth0').value.get() == []
    assert mix.config('conf2').option('ip_admin_eth0.ip_admin_eth0').value.get() == []
    #
    mix.config('conf1').option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.1.4'])
    assert mix.config('conf1').option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.4']
    assert mix.config('conf2').option('ip_admin_eth0.ip_admin_eth0').value.get() == []
    assert mix.config('conf1').option('ip_admin_eth0.ip_admin_eth0').owner.get() is owners.user
    assert mix.config('conf2').option('ip_admin_eth0.ip_admin_eth0').owner.isdefault()
    errors = mix.value.set('ip_admin_eth0.ip_admin_eth0', ['192.168.1.4'], force_default_if_same=True, force_dont_change_value=True)
    assert len(errors) == 0
    assert mix.option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.4']
    assert mix.config('conf1').option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.4']
    assert mix.config('conf2').option('ip_admin_eth0.ip_admin_eth0').value.get() == []
    assert mix.config('conf1').option('ip_admin_eth0.ip_admin_eth0').owner.get() is owners.mix1
    assert mix.config('conf2').option('ip_admin_eth0.ip_admin_eth0').owner.get() is owners.user
    #
    mix.config('conf1').option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.1.3'])
    assert mix.config('conf1').option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.3']
    assert mix.config('conf2').option('ip_admin_eth0.ip_admin_eth0').value.get() == []
    assert mix.config('conf1').option('ip_admin_eth0.ip_admin_eth0').owner.get() is owners.user
    assert mix.config('conf2').option('ip_admin_eth0.ip_admin_eth0').owner.get() is owners.user
    errors = mix.value.set('ip_admin_eth0.ip_admin_eth0', ['192.168.1.5'], force_default_if_same=True, force_dont_change_value=True)
    assert len(errors) == 0
    assert mix.option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.5']
    assert mix.config('conf1').option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.3']
    assert mix.config('conf2').option('ip_admin_eth0.ip_admin_eth0').value.get() == []
    assert mix.config('conf1').option('ip_admin_eth0.ip_admin_eth0').owner.get() is owners.user
    assert mix.config('conf2').option('ip_admin_eth0.ip_admin_eth0').owner.get() is owners.user


def test_mix_force_default_and_dont_change():
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip", multi=True)
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "mask", multi=True, properties=('hidden',))
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    od = OptionDescription('root', '', [interface1])
    conf1 = Config(od, session_id='rconf1')
    conf2 = Config(od, session_id='rconf2')
    mix = MixConfig(od, [conf1, conf2])
    mix.property.read_write()
    mix.owner.set('mix1')
    raises(ValueError, "mix.value.set('ip_admin_eth0.ip_admin_eth0', ['192.168.1.4'], force_default=True, force_dont_change_value=True)")


def test_mix_properties_mix():
    ip_admin_eth0 = NetworkOption('ip_admin_eth0', "ip", multi=True, default=['192.168.1.1'])
    netmask_admin_eth0 = NetmaskOption('netmask_admin_eth0', "mask", multi=True, properties=('disabled',))
    netmask_admin_eth0.impl_add_consistency('network_netmask', ip_admin_eth0)
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    od = OptionDescription('root', '', [interface1])
    conf1 = Config(od, session_id='conf1')
    conf2 = Config(od, session_id='conf2')
    conf1.property.read_write()
    conf2.property.read_write()
    mix = MixConfig(od, [conf1, conf2])
    mix.property.read_write()
    assert mix.config('conf1').value.dict() == {}


def test_mix_exception_mix():
    ip_admin_eth0 = NetworkOption('ip_admin_eth0', "ip", multi=True, default=['192.168.1.1'])
    netmask_admin_eth0 = NetmaskOption('netmask_admin_eth0', "mask", multi=True, callback=raise_exception)
    netmask_admin_eth0.impl_add_consistency('network_netmask', ip_admin_eth0)
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    od = OptionDescription('root', '', [interface1])
    conf1 = Config(od, session_id='conf1')
    conf2 = Config(od, session_id='conf2')
    mix = MixConfig(od, [conf1, conf2])
    mix.property.read_write()
    raises(Exception, "conf1.make_dict()")


def test_mix_callback():
    val1 = StrOption('val1', "", 'val')
    val2 = StrOption('val2', "", callback=return_value, callback_params=Params(ParamOption(val1)))
    val3 = StrOption('val3', "", callback=return_value, callback_params=Params(ParamValue('yes')))
    val4 = StrOption('val4', "", callback=return_value, callback_params=Params(kwargs={'value': ParamOption(val1)}))
    val5 = StrOption('val5', "", callback=return_value, callback_params=Params(kwargs={'value': ParamValue('yes')}))
    maconfig = OptionDescription('rootconfig', '', [val1, val2, val3, val4, val5])
    cfg = Config(maconfig, session_id='cfg')
    mix = MixConfig(maconfig, [cfg])
    mix.property.read_write()
    assert mix.config('cfg').value.dict() == {'val3': 'yes', 'val2': 'val', 'val1': 'val', 'val5': 'yes', 'val4': 'val'}
    mix.config('cfg').option('val1').value.set('new')
    assert mix.config('cfg').value.dict() == {'val3': 'yes', 'val2': 'new', 'val1': 'new', 'val5': 'yes', 'val4': 'new'}
    mix.config('cfg').option('val1').value.reset()
    mix.option('val1').value.set('new')
    assert mix.config('cfg').value.dict() == {'val3': 'yes', 'val2': 'new', 'val1': 'new', 'val5': 'yes', 'val4': 'new'}
    mix.config('cfg').option('val4').value.set('new1')
    assert mix.config('cfg').value.dict() == {'val3': 'yes', 'val2': 'new', 'val1': 'new', 'val5': 'yes', 'val4': 'new1'}
    mix.config('cfg').option('val4').value.reset()
    mix.option('val4').value.set('new1')
    assert mix.config('cfg').value.dict() == {'val3': 'yes', 'val2': 'new', 'val1': 'new', 'val5': 'yes', 'val4': 'new1'}
    mix.option('val4').value.reset()


def test_mix_callback_follower():
    val = StrOption('val', "", default='val')
    val1 = StrOption('val1', "", multi=True, callback=return_value, callback_params=Params(ParamOption(val)))
    val3 = StrOption('val2', "", multi=True, callback=return_value, callback_params=Params(ParamOption(val1)))
    val4 = StrOption('val3', "", multi=True, callback=return_value, callback_params=Params(ParamOption(val1)))
    interface1 = Leadership('val1', '', [val1, val3, val4])
    od = OptionDescription('root', '', [interface1])
    maconfig = OptionDescription('rootconfig', '', [val, interface1])
    cfg = Config(maconfig, session_id='cfg1')
    mix = MixConfig(maconfig, [cfg])
    mix.property.read_write()
    assert mix.config('cfg1').value.dict() == {'val1.val2': ['val'], 'val1.val1': ['val'], 'val1.val3': ['val'], 'val': 'val'}
    mix.config('cfg1').option('val').value.set('val1')
    assert mix.config('cfg1').value.dict() == {'val1.val2': ['val1'], 'val1.val1': ['val1'], 'val1.val3': ['val1'], 'val': 'val1'}
    mix.config('cfg1').option('val').value.reset()
    mix.option('val').value.set('val1')
    assert mix.config('cfg1').value.dict() == {'val1.val2': ['val1'], 'val1.val1': ['val1'], 'val1.val3': ['val1'], 'val': 'val1'}
    mix.option('val').value.reset()
    mix.config('cfg1').option('val1.val2', 0).value.set('val2')
    assert mix.config('cfg1').value.dict() == {'val1.val2': ['val2'], 'val1.val1': ['val'], 'val1.val3': ['val'], 'val': 'val'}
    mix.config('cfg1').option('val1.val2', 0).value.reset()
    assert mix.config('cfg1').value.dict() == {'val1.val2': ['val'], 'val1.val1': ['val'], 'val1.val3': ['val'], 'val': 'val'}
    mix.option('val1.val2', 0).value.set('val2')
    assert mix.config('cfg1').value.dict() == {'val1.val2': ['val2'], 'val1.val1': ['val'], 'val1.val3': ['val'], 'val': 'val'}
    mix.config('cfg1').option('val1.val3', 0).value.set('val6')
    assert mix.config('cfg1').value.dict() == {'val1.val2': ['val2'], 'val1.val1': ['val'], 'val1.val3': ['val6'], 'val': 'val'}
    mix.option('val1.val2', 0).value.reset()
    mix.config('cfg1').option('val1.val3', 0).value.reset()
    mix.config('cfg1').option('val1.val1').value.set(['val3'])
    assert mix.config('cfg1').value.dict() == {'val1.val2': ['val3'], 'val1.val1': ['val3'], 'val1.val3': ['val3'], 'val': 'val'}
    mix.config('cfg1').option('val1.val1').value.reset()
    assert mix.config('cfg1').value.dict() == {'val1.val2': ['val'], 'val1.val1': ['val'], 'val1.val3': ['val'], 'val': 'val'}
    mix.option('val1.val1').value.set(['val3'])
    assert mix.config('cfg1').value.dict() == {'val1.val2': ['val3'], 'val1.val1': ['val3'], 'val1.val3': ['val3'], 'val': 'val'}
    mix.config('cfg1').option('val1.val2', 0).value.set('val2')
    assert mix.config('cfg1').value.dict() == {'val1.val2': ['val2'], 'val1.val1': ['val3'], 'val1.val3': ['val3'], 'val': 'val'}
    mix.option('val1.val1').value.set(['val3', 'rah'])
    assert mix.config('cfg1').value.dict() == {'val1.val2': ['val2', 'rah'], 'val1.val1': ['val3', 'rah'], 'val1.val3': ['val3', 'rah'], 'val': 'val'}
    mix.option('val1.val1').value.pop(1)
    mix.option('val1.val1').value.set(['val4'])
    assert mix.config('cfg1').value.dict() == {'val1.val2': ['val2'], 'val1.val1': ['val4'], 'val1.val3': ['val4'], 'val': 'val'}


def test_meta_reset():
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip", multi=True)
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "mask", multi=True, properties=('hidden',))
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    od0 = OptionDescription('root', '', [interface1])
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip", multi=True)
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "mask", multi=True, properties=('hidden',))
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    od1 = OptionDescription('root', '', [interface1])
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip", multi=True)
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "mask", multi=True, properties=('hidden',))
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    od2 = OptionDescription('root', '', [interface1])
    conf1 = Config(od0, session_id='conf1')
    conf2 = Config(od1, session_id='conf2')
    meta = MixConfig(od2, [conf1, conf2])
    meta.property.read_write()
    meta.owner.set('mix1')
    assert meta.option('ip_admin_eth0.ip_admin_eth0').value.get() == []
    assert meta.config('conf1').option('ip_admin_eth0.ip_admin_eth0').value.get() == []
    assert meta.config('conf2').option('ip_admin_eth0.ip_admin_eth0').value.get() == []
    errors = meta.value.set('ip_admin_eth0.ip_admin_eth0', ['192.168.1.1'])
    assert len(errors) == 0
    assert meta.option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.1']
    assert meta.config('conf1').option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.1']
    assert meta.config('conf2').option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.1']
    meta.config('conf1').option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.1.2'])
    assert meta.option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.1']
    assert meta.config('conf1').option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.2']
    assert meta.config('conf2').option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.1']
    meta.value.reset('ip_admin_eth0.ip_admin_eth0')
    assert meta.option('ip_admin_eth0.ip_admin_eth0').value.get() == []
    assert meta.config('conf1').option('ip_admin_eth0.ip_admin_eth0').value.get() == []
    assert meta.config('conf2').option('ip_admin_eth0.ip_admin_eth0').value.get() == []


def test_mix_properties_mix_copy():
    ip_admin_eth0 = NetworkOption('ip_admin_eth0', "ip", multi=True, default=['192.168.1.1'])
    netmask_admin_eth0 = NetmaskOption('netmask_admin_eth0', "mask", multi=True, properties=('disabled',))
    interface0 = OptionDescription('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    ip_admin_eth0 = NetworkOption('ip_admin_eth0', "ip", multi=True, default=['192.168.1.1'])
    netmask_admin_eth0 = NetmaskOption('netmask_admin_eth0', "mask", multi=True, properties=('disabled',))
    interface1 = OptionDescription('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    ip_admin_eth0 = NetworkOption('ip_admin_eth0', "ip", multi=True, default=['192.168.1.1'])
    netmask_admin_eth0 = NetmaskOption('netmask_admin_eth0', "mask", multi=True, properties=('disabled',))
    interface2 = OptionDescription('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    conf1 = Config(interface0, session_id='conf1')
    conf2 = Config(interface1, session_id='conf2')
    conf1.property.read_write()
    conf2.property.read_write()
    mix = MixConfig(interface2, [conf1, conf2], session_id='mix1')
    mix.property.read_write()

    conf3 = mix.config('conf1').config.copy(session_id='conf3')
    mix2 = conf3.config.metaconfig()
    assert mix.config.name() == mix2.config.name()

    assert mix.config('conf1').value.dict() == {'ip_admin_eth0': ['192.168.1.1']}
    assert mix.config('conf2').value.dict() == {'ip_admin_eth0': ['192.168.1.1']}
    assert mix.config('conf3').value.dict() == {'ip_admin_eth0': ['192.168.1.1']}
    mix.option('ip_admin_eth0').value.set(['192.168.1.2'])
    assert mix.config('conf1').value.dict() == {'ip_admin_eth0': ['192.168.1.2']}
    assert mix.config('conf2').value.dict() == {'ip_admin_eth0': ['192.168.1.2']}
    assert mix.config('conf3').value.dict() == {'ip_admin_eth0': ['192.168.1.2']}
    ret = mix.value.set('ip_admin_eth0', ['192.168.1.3'], force_default_if_same=True)
    assert mix.config('conf1').value.dict() == {'ip_admin_eth0': ['192.168.1.3']}
    assert mix.config('conf2').value.dict() == {'ip_admin_eth0': ['192.168.1.3']}
    assert mix.config('conf3').value.dict() == {'ip_admin_eth0': ['192.168.1.3']}


def test_mix_properties_mix_deepcopy():
    ip_admin_eth0 = NetworkOption('ip_admin_eth0', "ip", multi=True, default=['192.168.1.1'])
    netmask_admin_eth0 = NetmaskOption('netmask_admin_eth0', "mask", multi=True,
                                       properties=('disabled',))
    interface0 = OptionDescription('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    ip_admin_eth0 = NetworkOption('ip_admin_eth0', "ip", multi=True, default=['192.168.1.1'])
    netmask_admin_eth0 = NetmaskOption('netmask_admin_eth0', "mask", multi=True,
                                       properties=('disabled',))
    interface1 = OptionDescription('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    ip_admin_eth0 = NetworkOption('ip_admin_eth0', "ip", multi=True, default=['192.168.1.1'])
    netmask_admin_eth0 = NetmaskOption('netmask_admin_eth0', "mask", multi=True,
                                       properties=('disabled',))
    interface2 = OptionDescription('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    conf1 = Config(interface0, session_id='conf1')
    conf2 = Config(interface1, session_id='conf2')
    conf1.property.read_write()
    conf2.property.read_write()
    mix = MixConfig(interface2, [conf1, conf2])
    mix.permissive.set(frozenset({'hidden'}))
    mix.property.read_write()

    mix2 = mix.config('conf1').config.deepcopy(session_id='conf3')
    assert mix != mix2
    assert mix.permissive.get() == mix2.permissive.get()

    assert mix.config('conf1').value.dict() == {'ip_admin_eth0': ['192.168.1.1']}
    assert mix.config('conf2').value.dict() == {'ip_admin_eth0': ['192.168.1.1']}
    assert mix2.config('conf3').value.dict() == {'ip_admin_eth0': ['192.168.1.1']}
    mix.option('ip_admin_eth0').value.set(['192.168.1.2'])
    assert mix.config('conf1').value.dict() == {'ip_admin_eth0': ['192.168.1.2']}
    assert mix.config('conf2').value.dict() == {'ip_admin_eth0': ['192.168.1.2']}
    assert mix2.config('conf3').value.dict() == {'ip_admin_eth0': ['192.168.1.1']}
    mix.value.set('ip_admin_eth0', ['192.168.1.3'], force_default_if_same=True)
    assert mix.config('conf1').value.dict() == {'ip_admin_eth0': ['192.168.1.3']}
    assert mix.config('conf2').value.dict() == {'ip_admin_eth0': ['192.168.1.3']}
    assert mix2.config('conf3').value.dict() == {'ip_admin_eth0': ['192.168.1.1']}


def test_mix_properties_submix_deepcopy():
    ip_admin_eth0 = NetworkOption('ip_admin_eth0', "ip", multi=True, default=['192.168.1.1'])
    netmask_admin_eth0 = NetmaskOption('netmask_admin_eth0', "mask", multi=True,
                                       properties=('disabled',))
    interface0 = OptionDescription('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    ip_admin_eth0 = NetworkOption('ip_admin_eth0', "ip", multi=True, default=['192.168.1.1'])
    netmask_admin_eth0 = NetmaskOption('netmask_admin_eth0', "mask", multi=True,
                                       properties=('disabled',))
    interface1 = OptionDescription('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    ip_admin_eth0 = NetworkOption('ip_admin_eth0', "ip", multi=True, default=['192.168.1.1'])
    netmask_admin_eth0 = NetmaskOption('netmask_admin_eth0', "mask", multi=True,
                                       properties=('disabled',))
    interface2 = OptionDescription('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    conf1 = Config(interface0, session_id='conf1')
    conf1.property.read_write()
    mix1 = MixConfig(interface1, [conf1], session_id='mix1')
    mix2 = MixConfig(interface2, [mix1], session_id='mix2')
    mix_copy = conf1.config.deepcopy(session_id='conf2',
                                      metaconfig_prefix='copy_')
    assert mix_copy.config.name() == 'copy_mix2'
    assert mix_copy.config('copy_mix1').config.name() == 'copy_mix1'
    assert mix_copy.config('copy_mix1').config('conf2').config.name() == 'conf2'


def test_mix_properties_submix_deepcopy_owner():
    ip_admin_eth0 = NetworkOption('ip_admin_eth0', "ip")
    netmask_admin_eth0 = NetmaskOption('netmask_admin_eth1', "mask")
    interface0 = OptionDescription('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    ip_admin_eth0 = NetworkOption('ip_admin_eth1', "ip")
    netmask_admin_eth0 = NetmaskOption('netmask_admin_eth0', "mask")
    interface1 = OptionDescription('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    ip_admin_eth0 = NetworkOption('ip_admin_eth0', "ip")
    netmask_admin_eth0 = NetmaskOption('netmask_admin_eth0', "mask")
    interface2 = OptionDescription('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    conf1 = Config(interface0, session_id='conf1')
    conf1.owner.set('conf1_user')
    conf1.property.read_write()
    mix1 = MixConfig(interface1, [conf1], session_id='mix1')
    mix1.owner.set('mix1_user')
    mix2 = MixConfig(interface2, [mix1], session_id='mix2')
    mix2.owner.set('mix2_user')
    #
    conf1.option('ip_admin_eth0').value.set('192.168.0.1')
    assert conf1.option('ip_admin_eth0').owner.get() == 'conf1_user'
    mix2.option('ip_admin_eth0').value.set('192.168.0.3')
    assert mix2.option('ip_admin_eth0').owner.get() == 'mix2_user'
    #
    mix2_copy = conf1.config.deepcopy(session_id='conf2',
                                       metaconfig_prefix='copy_')
    mix2_copy.option('netmask_admin_eth0').value.set('255.255.255.255')
    assert mix2_copy.option('ip_admin_eth0').value.get() == '192.168.0.3'
    assert mix2_copy.option('ip_admin_eth0').owner.get() == 'mix2_user'
    assert mix2_copy.option('netmask_admin_eth0').owner.get() == 'mix2_user'
    #
    mix1_copy = mix2_copy.config('copy_mix1')
    mix1_copy.option('netmask_admin_eth0').value.set('255.255.255.255')
    #
    conf2 = mix1_copy.config('conf2')
    conf2.owner.set('conf2_user')
    conf2.option('netmask_admin_eth1').value.set('255.255.255.255')
    assert conf2.option('netmask_admin_eth1').owner.get() == 'conf2_user'
    assert conf2.option('ip_admin_eth0').value.get() == '192.168.0.1'
    assert conf2.option('ip_admin_eth0').owner.get() == 'conf1_user'


def test_mix_properties_mix_set_value():
    ip_admin_eth0 = NetworkOption('ip_admin_eth1', "ip", multi=True, default=['192.168.1.1'])
    netmask_admin_eth0 = NetmaskOption('netmask_admin_eth0', "mask", multi=True, properties=('disabled',))
    interface0 = OptionDescription('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    ip_admin_eth0 = NetworkOption('ip_admin_eth0', "ip", multi=True, default=['192.168.1.1'])
    netmask_admin_eth0 = NetmaskOption('netmask_admin_eth1', "mask", multi=True, properties=('disabled',))
    interface1 = OptionDescription('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    ip_admin_eth0 = NetworkOption('ip_admin_eth0', "ip", multi=True, default=['192.168.1.1'])
    netmask_admin_eth0 = NetmaskOption('netmask_admin_eth0', "mask", multi=True, properties=('disabled',))
    interface2 = OptionDescription('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    conf1 = Config(interface0, session_id='conf1')
    conf2 = Config(interface1, session_id='conf2')
    conf1.property.read_write()
    conf2.property.read_write()
    mix = MixConfig(interface2, [conf1, conf2])
    mix.property.read_write()
    assert mix.config('conf2').value.dict() == {'ip_admin_eth0': ['192.168.1.1']}
    ret = mix.value.set('netmask_admin_eth0', ['255.255.255.255'], only_config=True)
    assert len(ret) == 2
    assert isinstance(ret[0], PropertiesOptionError)
    assert isinstance(ret[1], AttributeError)
    del ret[1]
    del ret[0]
    del ret
    ret = mix.value.set('netmask_admin_eth0', ['255.255.255.255'], force_default=True)
    assert len(ret) == 2
    assert isinstance(ret[0], AttributeError)
    assert isinstance(ret[1], PropertiesOptionError)
    del ret[1]
    del ret[0]
    del ret
    ret = mix.value.set('netmask_admin_eth0', ['255.255.255.255'], force_dont_change_value=True)
    assert len(ret) == 3
    assert isinstance(ret[0], PropertiesOptionError)
    assert isinstance(ret[1], AttributeError)
    assert isinstance(ret[2], PropertiesOptionError)
    del ret[2]
    del ret[1]
    del ret[0]
    del ret
    ret = mix.value.set('netmask_admin_eth0', ['255.255.255.255'], force_default_if_same=True)
    assert len(ret) == 2
    assert isinstance(ret[0], AttributeError)
    assert isinstance(ret[1], PropertiesOptionError)
    del ret[1]
    del ret[0]
    del ret
    ret = mix.value.set('ip_admin_eth0', '255.255.255.255', only_config=True)
    assert len(ret) == 2
    assert isinstance(ret[0], AttributeError)
    assert isinstance(ret[1], ValueError)
    del ret[1]
    del ret[0]
    del ret
    ret = mix.value.set('ip_admin_eth0', '255.255.255.255', force_default=True)
    assert len(ret) == 2
    assert isinstance(ret[0], AttributeError)
    assert isinstance(ret[1], ValueError)
    del ret[1]
    del ret[0]
    del ret
    ret = mix.value.set('ip_admin_eth0', '255.255.255.255', force_dont_change_value=True)
    assert len(ret) == 2
    assert isinstance(ret[0], AttributeError)
    assert isinstance(ret[1], ValueError)
    del ret[1]
    del ret[0]
    del ret
    ret = mix.value.set('ip_admin_eth0', '255.255.255.255', force_default_if_same=True)
    assert len(ret) == 2
    assert isinstance(ret[0], AttributeError)
    assert isinstance(ret[1], ValueError)
    del ret[1]
    del ret[0]
    del ret


def test_mix_different_default():
    ip_admin_eth0 = NetworkOption('ip_admin_eth0', "ip", multi=True, default=['192.168.1.1'])
    interface0 = OptionDescription('ip_admin_eth0', '', [ip_admin_eth0])
    ip_admin_eth0 = NetworkOption('ip_admin_eth1', "ip", multi=True, default=['192.168.1.2'])
    interface1 = OptionDescription('ip_admin_eth0', '', [ip_admin_eth0])
    ip_admin_eth0 = NetworkOption('ip_admin_eth1', "ip", multi=True, default=['192.168.1.3'])
    interface2 = OptionDescription('ip_admin_eth0', '', [ip_admin_eth0])
    ip_admin_eth0 = NetworkOption('ip_admin_eth0', "ip", multi=True, default=['192.168.1.4'])
    ip_admin_eth1 = NetworkOption('ip_admin_eth1', "ip", multi=True, default=['192.168.1.5'])
    interface3 = OptionDescription('ip_admin_eth0', '', [ip_admin_eth0, ip_admin_eth1])
    ip_admin_eth0 = NetworkOption('ip_admin_eth0', "ip", multi=True, default=['192.168.1.6'])
    interface4 = OptionDescription('ip_admin_eth0', '', [ip_admin_eth0])
    conf1 = Config(interface0, session_id='conf1')
    conf1.property.read_write()
    conf2 = Config(interface1, session_id='conf2')
    conf2.property.read_write()
    mix = MixConfig(interface2, [conf1, conf2], session_id='submix1')
    mix = MixConfig(interface3, [mix], session_id='submix2')
    mix = MixConfig(interface4, [mix])
    mix.property.read_write()
    #
    assert mix.value.dict() == {'ip_admin_eth0': ['192.168.1.6']}
    assert mix.config('submix2').value.dict() == {'ip_admin_eth0': ['192.168.1.4'], 'ip_admin_eth1': ['192.168.1.5']}
    assert mix.config('submix2.submix1').value.dict() == {'ip_admin_eth1': ['192.168.1.3']}
    assert mix.config('submix2.submix1.conf2').value.dict() == {'ip_admin_eth1': ['192.168.1.2']}
    assert mix.config('submix2.submix1.conf1').value.dict() == {'ip_admin_eth0': ['192.168.1.1']}
    #
    mix.option('ip_admin_eth0').value.set(['192.168.1.7'])
    assert mix.value.dict() == {'ip_admin_eth0': ['192.168.1.7']}
    assert mix.config('submix2').value.dict() == {'ip_admin_eth0': ['192.168.1.7'], 'ip_admin_eth1': ['192.168.1.5']}
    assert mix.config('submix2.submix1').value.dict() == {'ip_admin_eth1': ['192.168.1.3']}
    assert mix.config('submix2.submix1.conf2').value.dict() == {'ip_admin_eth1': ['192.168.1.2']}
    assert mix.config('submix2.submix1.conf1').value.dict() == {'ip_admin_eth0': ['192.168.1.7']}
    #
    mix.config('submix2').option('ip_admin_eth0').value.set(['192.168.1.8'])
    assert mix.value.dict() == {'ip_admin_eth0': ['192.168.1.7']}
    assert mix.config('submix2').value.dict() == {'ip_admin_eth0': ['192.168.1.8'], 'ip_admin_eth1': ['192.168.1.5']}
    assert mix.config('submix2.submix1').value.dict() == {'ip_admin_eth1': ['192.168.1.3']}
    assert mix.config('submix2.submix1.conf2').value.dict() == {'ip_admin_eth1': ['192.168.1.2']}
    assert mix.config('submix2.submix1.conf1').value.dict() == {'ip_admin_eth0': ['192.168.1.8']}
    #
    raises(AttributeError, "mix.config('submix2.submix1').option('ip_admin_eth0').value.set(['192.168.1.9'])")
    assert mix.value.dict() == {'ip_admin_eth0': ['192.168.1.7']}
    assert mix.config('submix2').value.dict() == {'ip_admin_eth0': ['192.168.1.8'], 'ip_admin_eth1': ['192.168.1.5']}
    assert mix.config('submix2.submix1').value.dict() == {'ip_admin_eth1': ['192.168.1.3']}
    assert mix.config('submix2.submix1.conf2').value.dict() == {'ip_admin_eth1': ['192.168.1.2']}
    assert mix.config('submix2.submix1.conf1').value.dict() == {'ip_admin_eth0': ['192.168.1.8']}
    #
    raises(AttributeError, "mix.config('submix2.submix1.conf2').option('ip_admin_eth0').value.set(['192.168.1.9'])")
    assert mix.value.dict() == {'ip_admin_eth0': ['192.168.1.7']}
    assert mix.config('submix2').value.dict() == {'ip_admin_eth0': ['192.168.1.8'], 'ip_admin_eth1': ['192.168.1.5']}
    assert mix.config('submix2.submix1').value.dict() == {'ip_admin_eth1': ['192.168.1.3']}
    assert mix.config('submix2.submix1.conf2').value.dict() == {'ip_admin_eth1': ['192.168.1.2']}
    assert mix.config('submix2.submix1.conf1').value.dict() == {'ip_admin_eth0': ['192.168.1.8']}
    #
    mix.config('submix2.submix1.conf1').option('ip_admin_eth0').value.set(['192.168.1.9'])
    assert mix.value.dict() == {'ip_admin_eth0': ['192.168.1.7']}
    assert mix.config('submix2').value.dict() == {'ip_admin_eth0': ['192.168.1.8'], 'ip_admin_eth1': ['192.168.1.5']}
    assert mix.config('submix2.submix1').value.dict() == {'ip_admin_eth1': ['192.168.1.3']}
    assert mix.config('submix2.submix1.conf2').value.dict() == {'ip_admin_eth1': ['192.168.1.2']}
    assert mix.config('submix2.submix1.conf1').value.dict() == {'ip_admin_eth0': ['192.168.1.9']}
    #
    raises(AttributeError, "mix.option('ip_admin_eth1').value.set(['192.168.1.10'])")
    assert mix.value.dict() == {'ip_admin_eth0': ['192.168.1.7']}
    assert mix.config('submix2').value.dict() == {'ip_admin_eth0': ['192.168.1.8'], 'ip_admin_eth1': ['192.168.1.5']}
    assert mix.config('submix2.submix1').value.dict() == {'ip_admin_eth1': ['192.168.1.3']}
    assert mix.config('submix2.submix1.conf2').value.dict() == {'ip_admin_eth1': ['192.168.1.2']}
    assert mix.config('submix2.submix1.conf1').value.dict() == {'ip_admin_eth0': ['192.168.1.9']}
    #
    mix.config('submix2').option('ip_admin_eth1').value.set(['192.168.1.10'])
    assert mix.value.dict() == {'ip_admin_eth0': ['192.168.1.7']}
    assert mix.config('submix2').value.dict() == {'ip_admin_eth0': ['192.168.1.8'], 'ip_admin_eth1': ['192.168.1.10']}
    assert mix.config('submix2.submix1').value.dict() == {'ip_admin_eth1': ['192.168.1.10']}
    assert mix.config('submix2.submix1.conf2').value.dict() == {'ip_admin_eth1': ['192.168.1.10']}
    assert mix.config('submix2.submix1.conf1').value.dict() == {'ip_admin_eth0': ['192.168.1.9']}
    #
    mix.config('submix2.submix1').option('ip_admin_eth1').value.set(['192.168.1.11'])
    assert mix.value.dict() == {'ip_admin_eth0': ['192.168.1.7']}
    assert mix.config('submix2').value.dict() == {'ip_admin_eth0': ['192.168.1.8'], 'ip_admin_eth1': ['192.168.1.10']}
    assert mix.config('submix2.submix1').value.dict() == {'ip_admin_eth1': ['192.168.1.11']}
    assert mix.config('submix2.submix1.conf2').value.dict() == {'ip_admin_eth1': ['192.168.1.11']}
    assert mix.config('submix2.submix1.conf1').value.dict() == {'ip_admin_eth0': ['192.168.1.9']}
    #
    mix.config('submix2.submix1.conf2').option('ip_admin_eth1').value.set(['192.168.1.12'])
    assert mix.value.dict() == {'ip_admin_eth0': ['192.168.1.7']}
    assert mix.config('submix2').value.dict() == {'ip_admin_eth0': ['192.168.1.8'], 'ip_admin_eth1': ['192.168.1.10']}
    assert mix.config('submix2.submix1').value.dict() == {'ip_admin_eth1': ['192.168.1.11']}
    assert mix.config('submix2.submix1.conf2').value.dict() == {'ip_admin_eth1': ['192.168.1.12']}
    assert mix.config('submix2.submix1.conf1').value.dict() == {'ip_admin_eth0': ['192.168.1.9']}
    #
    raises(AttributeError, "mix.config('submix2.submix1.conf1').option('ip_admin_eth1').value.set(['192.168.1.13'])")
    assert mix.value.dict() == {'ip_admin_eth0': ['192.168.1.7']}
    assert mix.config('submix2').value.dict() == {'ip_admin_eth0': ['192.168.1.8'], 'ip_admin_eth1': ['192.168.1.10']}
    assert mix.config('submix2.submix1').value.dict() == {'ip_admin_eth1': ['192.168.1.11']}
    assert mix.config('submix2.submix1.conf2').value.dict() == {'ip_admin_eth1': ['192.168.1.12']}
    assert mix.config('submix2.submix1.conf1').value.dict() == {'ip_admin_eth0': ['192.168.1.9']}


def test_mix_different_default_reset():
    ip_admin_eth0 = NetworkOption('ip_admin_eth0', "ip", multi=True, default=['192.168.1.1'])
    interface0 = OptionDescription('ip_admin_eth0', '', [ip_admin_eth0])
    ip_admin_eth0 = NetworkOption('ip_admin_eth1', "ip", multi=True, default=['192.168.1.2'])
    interface1 = OptionDescription('ip_admin_eth0', '', [ip_admin_eth0])
    ip_admin_eth0 = NetworkOption('ip_admin_eth1', "ip", multi=True, default=['192.168.1.3'])
    interface2 = OptionDescription('ip_admin_eth0', '', [ip_admin_eth0])
    ip_admin_eth0 = NetworkOption('ip_admin_eth0', "ip", multi=True, default=['192.168.1.4'])
    ip_admin_eth1 = NetworkOption('ip_admin_eth1', "ip", multi=True, default=['192.168.1.5'])
    interface3 = OptionDescription('ip_admin_eth0', '', [ip_admin_eth0, ip_admin_eth1])
    ip_admin_eth0 = NetworkOption('ip_admin_eth0', "ip", multi=True, default=['192.168.1.6'])
    interface4 = OptionDescription('ip_admin_eth0', '', [ip_admin_eth0])
    conf1 = Config(interface0, session_id='conf1')
    conf1.property.read_write()
    conf2 = Config(interface1, session_id='conf2')
    conf2.property.read_write()
    mix = MixConfig(interface2, [conf1, conf2], session_id='submix1')
    mix = MixConfig(interface3, [mix], session_id='submix2')
    mix = MixConfig(interface4, [mix])
    mix.property.read_write()
    #
    mix.option('ip_admin_eth0').value.set(['192.168.1.7'])
    mix.config('submix2').option('ip_admin_eth0').value.set(['192.168.1.8'])
    mix.config('submix2').option('ip_admin_eth1').value.set(['192.168.1.10'])
    mix.config('submix2.submix1').option('ip_admin_eth1').value.set(['192.168.1.11'])
    mix.config('submix2.submix1.conf2').option('ip_admin_eth1').value.set(['192.168.1.12'])
    mix.config('submix2.submix1.conf1').option('ip_admin_eth0').value.set(['192.168.1.9'])
    assert mix.value.dict() == {'ip_admin_eth0': ['192.168.1.7']}
    assert mix.config('submix2').value.dict() == {'ip_admin_eth0': ['192.168.1.8'], 'ip_admin_eth1': ['192.168.1.10']}
    assert mix.config('submix2.submix1').value.dict() == {'ip_admin_eth1': ['192.168.1.11']}
    assert mix.config('submix2.submix1.conf2').value.dict() == {'ip_admin_eth1': ['192.168.1.12']}
    assert mix.config('submix2.submix1.conf1').value.dict() == {'ip_admin_eth0': ['192.168.1.9']}
    #
    mix.value.reset('ip_admin_eth0')
    assert mix.value.dict() == {'ip_admin_eth0': ['192.168.1.6']}
    assert mix.config('submix2').value.dict() == {'ip_admin_eth0': ['192.168.1.4'], 'ip_admin_eth1': ['192.168.1.10']}
    assert mix.config('submix2.submix1').value.dict() == {'ip_admin_eth1': ['192.168.1.11']}
    assert mix.config('submix2.submix1.conf2').value.dict() == {'ip_admin_eth1': ['192.168.1.12']}
    assert mix.config('submix2.submix1.conf1').value.dict() == {'ip_admin_eth0': ['192.168.1.1']}
    #
    mix.value.reset('ip_admin_eth1')
    assert mix.value.dict() == {'ip_admin_eth0': ['192.168.1.6']}
    assert mix.config('submix2').value.dict() == {'ip_admin_eth0': ['192.168.1.4'], 'ip_admin_eth1': ['192.168.1.5']}
    assert mix.config('submix2.submix1').value.dict() == {'ip_admin_eth1': ['192.168.1.3']}
    assert mix.config('submix2.submix1.conf2').value.dict() == {'ip_admin_eth1': ['192.168.1.2']}
    assert mix.config('submix2.submix1.conf1').value.dict() == {'ip_admin_eth0': ['192.168.1.1']}
