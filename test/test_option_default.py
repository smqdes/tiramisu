"test all types of option default values for options, add new option in a descr"
from .autopath import do_autopath
do_autopath()

from py.test import raises

from tiramisu.setting import owners
from tiramisu.error import PropertiesOptionError, ConfigError
from tiramisu import IntOption, FloatOption, StrOption, ChoiceOption, \
    BoolOption, OptionDescription, Leadership, Config, undefined
from tiramisu.storage import list_sessions


def teardown_function(function):
    assert list_sessions() == [], 'session list is not empty when leaving "{}"'.format(function.__name__)


def make_description():
    gcoption = ChoiceOption('name', 'GC name', ['ref', 'framework'], 'ref')
    gcdummy = BoolOption('dummy', 'dummy', default=False)
    objspaceoption = ChoiceOption('objspace', 'Object space',
                                  ['std', 'thunk'], 'std')
    booloption = BoolOption('bool', 'Test boolean option', default=True)
    intoption = IntOption('int', 'Test int option', default=0)
    floatoption = FloatOption('float', 'Test float option', default=2.3)
    stroption = StrOption('str', 'Test string option', default="abc")
    boolop = BoolOption('boolop', 'Test boolean option op', default=True)
    wantref_option = BoolOption('wantref', 'Test requires', default=False,
                                requires=['boolop'])
    wantframework_option = BoolOption('wantframework', 'Test requires',
                                      default=False,
                                      requires=['boolop'])

    gcgroup = OptionDescription('gc', '', [gcoption, gcdummy, floatoption])
    descr = OptionDescription('tiramisu', '', [gcgroup, booloption, objspaceoption,
                                               wantref_option, stroption,
                                               wantframework_option,
                                               intoption, boolop])
    return descr


#____________________________________________________________
# default values
def test_default_is_none():
    """
    Most constructors take a ``default`` argument that specifies the default
    value of the option. If this argument is not supplied the default value is
    assumed to be ``None``.
    """
    dummy1 = BoolOption('dummy1', 'doc dummy')
    dummy2 = BoolOption('dummy2', 'doc dummy')
    group = OptionDescription('group', '', [dummy1, dummy2])
    api = Config(group)
    # so when the default value is not set, there is actually a default value
    assert api.option('dummy1').value.get() is None
    assert api.option('dummy2').value.get() is None


def test_set_defaut_value_from_option_object():
    """Options have an available default setting and can give it back"""
    b = BoolOption("boolean", "", default=False)
    assert b.impl_getdefault() is False


def test_force_default_on_freeze():
    "a frozen option wich is forced returns his default"
    dummy1 = BoolOption('dummy1', 'doc dummy', default=False, properties=('force_default_on_freeze',))
    dummy2 = BoolOption('dummy2', 'doc dummy', default=True)
    group = OptionDescription('group', '', [dummy1, dummy2])
    api = Config(group)
    api.property.read_write()
    owner = api.owner.get()
    api.option('dummy1').value.set(True)
    api.option('dummy2').value.set(False)
    assert api.option('dummy1').owner.get() == owner
    assert api.option('dummy2').owner.get() == owner
    api.option('dummy1').property.add('frozen')
    api.option('dummy2').property.add('frozen')
    assert api.option('dummy1').value.get() is False
    assert api.option('dummy2').value.get() is False
    assert api.option('dummy1').owner.isdefault()
    assert api.option('dummy2').owner.get() == owner
    raises(PropertiesOptionError, "api.option('dummy2').owner.set('frozen')")
    raises(PropertiesOptionError, "api.option('dummy1').value.reset()")
    api.option('dummy1').property.pop('frozen')
    api.option('dummy1').value.reset()
    api.option('dummy1').property.add('frozen')
    raises(PropertiesOptionError, "api.option('dummy2').owner.set('frozen')")


def test_force_default_on_freeze_multi():
    dummy1 = BoolOption('dummy1', 'doc dummy', default=[False], properties=('force_default_on_freeze',), multi=True)
    dummy2 = BoolOption('dummy2', 'doc dummy', default=[True], multi=True)
    group = OptionDescription('group', '', [dummy1, dummy2])
    api = Config(group)
    api.property.read_write()
    api.option('dummy1').value.set([undefined, True])
    api.option('dummy2').value.set([undefined, False])
    owner = api.owner.get()
    assert api.option('dummy1').owner.get() == owner
    assert api.option('dummy2').owner.get() == owner
    api.option('dummy1').property.add('frozen')
    api.option('dummy2').property.add('frozen')
    assert api.option('dummy1').value.get() == [False]
    assert api.option('dummy2').value.get() == [True, False]
    assert api.option('dummy1').owner.isdefault()
    assert api.option('dummy2').owner.get() == owner
    raises(PropertiesOptionError, "api.option('dummy2').owner.set('owner')")
    raises(PropertiesOptionError, "api.option('dummy2').value.reset()")
    api.option('dummy1').property.pop('frozen')
    api.option('dummy1').value.reset()
    api.option('dummy1').property.add('frozen')


def test_force_default_on_freeze_leader():
    dummy1 = BoolOption('dummy1', 'Test int option', multi=True, properties=('force_default_on_freeze',))
    dummy2 = BoolOption('dummy2', 'Test string option', multi=True)
    descr = Leadership("dummy1", "", [dummy1, dummy2])
    descr = OptionDescription("root", "", [descr])
    raises(ConfigError, "Config(descr)")


def test_force_metaconfig_on_freeze_leader():
    dummy1 = BoolOption('dummy1', 'Test int option', multi=True, properties=('force_metaconfig_on_freeze',))
    dummy2 = BoolOption('dummy2', 'Test string option', multi=True)
    descr = Leadership("dummy1", "", [dummy1, dummy2])
    descr = OptionDescription("root", "", [descr])
    raises(ConfigError, "Config(descr)")


def test_force_default_on_freeze_leader_frozen():
    dummy1 = BoolOption('dummy1', 'Test int option', multi=True, properties=('force_default_on_freeze', 'frozen'))
    dummy2 = BoolOption('dummy2', 'Test string option', multi=True)
    descr = Leadership("dummy1", "", [dummy1, dummy2])
    descr = OptionDescription("root", "", [descr])
    api = Config(descr)
    raises(ConfigError, "api.option('dummy1.dummy1').property.pop('frozen')")


def test_force_metaconfig_on_freeze_leader_frozen():
    dummy1 = BoolOption('dummy1', 'Test int option', multi=True, properties=('force_metaconfig_on_freeze', 'frozen'))
    dummy2 = BoolOption('dummy2', 'Test string option', multi=True)
    descr = Leadership("dummy1", "", [dummy1, dummy2])
    descr = OptionDescription("root", "", [descr])
    api = Config(descr)
    raises(ConfigError, "api.option('dummy1.dummy1').property.pop('frozen')")


def test_force_default_on_freeze_follower():
    dummy1 = BoolOption('dummy1', 'Test int option', multi=True)
    dummy2 = BoolOption('dummy2', 'Test string option', multi=True, properties=('force_default_on_freeze',))
    descr = Leadership("dummy1", "", [dummy1, dummy2])
    descr = OptionDescription("root", "", [descr])
    api = Config(descr)
    api.property.read_write()
    owners.addowner("frozenmultifollower2")
    api.option('dummy1.dummy1').value.set([True])
    api.option('dummy1.dummy2', 0).value.set(False)
    assert api.option('dummy1.dummy1').value.get() == [True]
    assert api.option('dummy1.dummy2', 0).value.get() == False
    assert api.option('dummy1.dummy1').owner.get() == 'user'
    assert api.option('dummy1.dummy2', 0).owner.get() == 'user'
    #
    api.option('dummy1.dummy2').property.add('frozen')
    assert api.option('dummy1.dummy1').value.get() == [True]
    assert api.option('dummy1.dummy2', 0).value.get() == None
    assert api.option('dummy1.dummy1').owner.get() == 'user'
    assert api.option('dummy1.dummy2', 0).owner.isdefault()
    raises(PropertiesOptionError, "api.option('dummy1.dummy2', 0).owner.set('frozenmultifollower2')")
    #
    api.option('dummy1.dummy2').property.pop('frozen')
    api.option('dummy1.dummy1').value.set([True, True])
    api.option('dummy1.dummy2', 1).value.set(False)
    assert api.option('dummy1.dummy1').value.get() == [True, True]
    assert api.option('dummy1.dummy2', 0).value.get() == False
    assert api.option('dummy1.dummy2', 1).value.get() == False
    #
    api.option('dummy1.dummy2').property.add('frozen')
    assert api.option('dummy1.dummy1').value.get() == [True, True]
    assert api.option('dummy1.dummy2', 0).value.get() == None
    assert api.option('dummy1.dummy2', 1).value.get() == None
    #
    api.option('dummy1.dummy1').value.pop(1)
    assert api.option('dummy1.dummy1').value.get() == [True]
    assert api.option('dummy1.dummy2', 0).value.get() == None
    #
    api.option('dummy1.dummy2').property.pop('frozen')
    assert api.option('dummy1.dummy1').value.get() == [True]
    assert api.option('dummy1.dummy2', 0).value.get() == False
    #
    api.option('dummy1.dummy1').value.set([True, True])
    assert api.option('dummy1.dummy2', 0).value.get() == False
    assert api.option('dummy1.dummy2', 1).value.get() == None


def test_overrides_changes_option_value():
    "with config.override(), the default is changed and the value is changed"
    descr = OptionDescription("test", "", [
        BoolOption("b", "", default=False)])
    api = Config(descr)
    api.option('b').value.set(True)


def test_choice_with_no_default():
    descr = OptionDescription("test", "", [
        ChoiceOption("backend", "", ("c", "cli"))])
    api = Config(descr)
    assert api.option('backend').value.get() is None
    api.option('backend').value.set('c')


def test_choice_with_default():
    descr = OptionDescription("test", "", [
        ChoiceOption("backend", "", ("c", "cli"), default="cli")])
    api = Config(descr)
    assert api.option('backend').value.get() == 'cli'
