## coding: utf-8
from .autopath import do_autopath
do_autopath()

from py.test import raises

try:
    from tiramisu.setting import OptionBag, ConfigBag
    tiramisu_version = 3
except:
    tiramisu_version = 2
from tiramisu import Config
from tiramisu.config import SubConfig
from tiramisu.option import ChoiceOption, BoolOption, IntOption, FloatOption,\
    StrOption, SymLinkOption, UnicodeOption, IPOption, OptionDescription, \
    PortOption, NetworkOption, NetmaskOption, DomainnameOption, EmailOption, \
    URLOption, FilenameOption
from tiramisu.storage import list_sessions, delete_session


def teardown_function(function):
    assert list_sessions() == [], 'session list is not empty when leaving "{}"'.format(function.__name__)


def test_slots_option():
    c = ChoiceOption('a', '', ('a',))
    raises(AttributeError, "c.x = 1")
    del c
    c = BoolOption('a', '')
    raises(AttributeError, "c.x = 1")
    del c
    c = IntOption('a', '')
    raises(AttributeError, "c.x = 1")
    del c
    c = FloatOption('a', '')
    raises(AttributeError, "c.x = 1")
    del c
    c = StrOption('a', '')
    raises(AttributeError, "c.x = 1")
    c = SymLinkOption('b', c)
    raises(AttributeError, "c.x = 1")
    del c
    c = UnicodeOption('a', '')
    raises(AttributeError, "c.x = 1")
    del c
    c = IPOption('a', '')
    raises(AttributeError, "c.x = 1")
    del c
    c = OptionDescription('a', '', [])
    raises(AttributeError, "c.x = 1")
    del c
    c = PortOption('a', '')
    raises(AttributeError, "c.x = 1")
    del c
    c = NetworkOption('a', '')
    raises(AttributeError, "c.x = 1")
    del c
    c = NetmaskOption('a', '')
    raises(AttributeError, "c.x = 1")
    del c
    c = DomainnameOption('a', '')
    raises(AttributeError, "c.x = 1")
    del c
    c = EmailOption('a', '')
    raises(AttributeError, "c.x = 1")
    del c
    c = URLOption('a', '')
    raises(AttributeError, "c.x = 1")
    del c
    c = FilenameOption('a', '')
    raises(AttributeError, "c.x = 1")
    del c


def test_slots_option_readonly():
    a = ChoiceOption('a', '', ('a',))
    b = BoolOption('b', '')
    c = IntOption('c', '')
    d = FloatOption('d', '')
    e = StrOption('e', '')
    g = UnicodeOption('g', '')
    h = IPOption('h', '')
    i = PortOption('i', '')
    j = NetworkOption('j', '')
    k = NetmaskOption('k', '')
    l = DomainnameOption('l', '')
    o = EmailOption('o', '')
    p = URLOption('p', '')
    q = FilenameOption('q', '')
    m = OptionDescription('m', '', [a, b, c, d, e, g, h, i, j, k, l, o, p, q])
    a._requires = (((a,),),)
    b._requires = (((a,),),)
    c._requires = (((a,),),)
    d._requires = (((a,),),)
    e._requires = (((a,),),)
    g._requires = (((a,),),)
    h._requires = (((a,),),)
    i._requires = (((a,),),)
    j._requires = (((a,),),)
    k._requires = (((a,),),)
    l._requires = (((a,),),)
    m._requires = (((a,),),)
    o._requires = (((a,),),)
    p._requires = (((a,),),)
    q._requires = (((a,),),)
    Config(m)
    raises(AttributeError, "a._requires = 'a'")
    raises(AttributeError, "b._requires = 'b'")
    raises(AttributeError, "c._requires = 'c'")
    raises(AttributeError, "d._requires = 'd'")
    raises(AttributeError, "e._requires = 'e'")
    raises(AttributeError, "g._requires = 'g'")
    raises(AttributeError, "h._requires = 'h'")
    raises(AttributeError, "i._requires = 'i'")
    raises(AttributeError, "j._requires = 'j'")
    raises(AttributeError, "k._requires = 'k'")
    raises(AttributeError, "l._requires = 'l'")
    raises(AttributeError, "m._requires = 'm'")
    raises(AttributeError, "o._requires = 'o'")
    raises(AttributeError, "p._requires = 'p'")
    raises(AttributeError, "q._requires = 'q'")


#def test_slots_description():
#    # __slots__ for OptionDescription should be complete for __getattr__
#    slots = set()
#    for subclass in OptionDescription.__mro__:
#        if subclass is not object:
#            slots.update(subclass.__slots__)
#    assert slots == set(OptionDescription.__slots__)


def test_slots_config():
    od1 = OptionDescription('a', '', [])
    od2 = OptionDescription('a', '', [od1])
    c = Config(od2)
    raises(AttributeError, "c._config_bag.context.x = 1")
    raises(AttributeError, "c._config_bag.context.cfgimpl_x = 1")
    option_bag = OptionBag()
    option_bag.set_option(od2,
                          'a',
                          None,
                          ConfigBag(c._config_bag.context))
    sc = c._config_bag.context.get_subconfig(option_bag)
    assert isinstance(sc, SubConfig)
    raises(AttributeError, "sc.x = 1")
    raises(AttributeError, "sc.cfgimpl_x = 1")


def test_slots_setting():
    od1 = OptionDescription('a', '', [])
    od2 = OptionDescription('a', '', [od1])
    c = Config(od2)
    s = c._config_bag.context.cfgimpl_get_settings()
    s
    raises(AttributeError, "s.x = 1")


def test_slots_value():
    od1 = OptionDescription('a', '', [])
    od2 = OptionDescription('a', '', [od1])
    c = Config(od2)
    v = c._config_bag.context.cfgimpl_get_values()
    v
    raises(AttributeError, "v.x = 1")
