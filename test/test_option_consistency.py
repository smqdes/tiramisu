from .autopath import do_autopath
do_autopath()

from py.test import raises

from tiramisu.setting import owners, groups
from tiramisu import IPOption, NetworkOption, NetmaskOption, IntOption,\
    BroadcastOption, StrOption, SymLinkOption, OptionDescription, submulti, Leadership,\
    Config, undefined, Params, ParamOption
from tiramisu.error import ConfigError, ValueWarning, PropertiesOptionError
from tiramisu.api import TIRAMISU_VERSION
import warnings
from tiramisu.storage import list_sessions


def teardown_function(function):
    assert list_sessions() == [], 'session list is not empty when leaving "{}"'.format(function.__name__)


def test_consistency():
    a = IntOption('a', '')
    b = IntOption('b', '')
    a.impl_add_consistency('not_equal', b)
    #consistency to itself
    raises(ConfigError, "a.impl_add_consistency('not_equal', a)")


def test_consistency_not_exists():
    a = IntOption('a', '')
    b = IntOption('b', '')
    a, b
    raises(ConfigError, "a.impl_add_consistency('not_exists', b)")


def test_consistency_unknown_params():
    a = IntOption('a', '')
    b = IntOption('b', '')
    a, b
    raises(ValueError, "a.impl_add_consistency('not_equal', b, unknown=False)")


def test_consistency_warnings_only_default():
    a = IntOption('a', '', 1)
    b = IntOption('b', '', 1)
    warnings.simplefilter("always", ValueWarning)
    with warnings.catch_warnings(record=True) as w:
        a.impl_add_consistency('not_equal', b, warnings_only=True)
    assert w != []


def test_consistency_warnings_only():
    a = IntOption('a', '')
    b = IntOption('b', '')
    c = IntOption('c', '')
    od = OptionDescription('od', '', [a, b, c])
    a.impl_add_consistency('not_equal', b, warnings_only=True)
    api = Config(od)
    assert api.option('a').option.consistencies()
    assert not api.option('b').option.consistencies()
    assert not api.option('c').option.consistencies()
    api.option('a').value.set(1)
    warnings.simplefilter("always", ValueWarning)
    with warnings.catch_warnings(record=True) as w:
        api.option('b').value.set(1)
    assert w != []


def test_consistency_warnings_only_more_option():
    a = IntOption('a', '')
    b = IntOption('b', '')
    d = IntOption('d', '')
    od = OptionDescription('od', '', [a, b, d])
    a.impl_add_consistency('not_equal', b, d, warnings_only=True)
    api = Config(od)
    api.option('a').value.set(1)
    warnings.simplefilter("always", ValueWarning)
    with warnings.catch_warnings(record=True) as w:
        api.option('b').value.set(1)
    assert w != []
    assert len(w) == 1
    with warnings.catch_warnings(record=True) as w:
        api.option('d').value.get()
    assert w != []
    assert len(w) == 1
    with warnings.catch_warnings(record=True) as w:
        api.option('d').value.set(1)
    assert w != []
    assert len(w) == 1


def test_consistency_error_prefix():
    a = IntOption('a', '')
    b = IntOption('b', '')
    od = OptionDescription('od', '', [a, b])
    a.impl_add_consistency('not_equal', b)
    api = Config(od)
    api.option('a').value.set(1)
    try:
        api.option('b').value.set(1)
    except Exception as err:
        assert str(err) == '"1" is an invalid integer for "b", must be different from the value of "a"'
    try:
        api.option('b').value.set(1)
    except Exception as err:
        err.prefix = ''
        assert str(err) == 'must be different from the value of "a"'


def test_consistency_warnings_only_option():
    a = IntOption('a', '')
    b = IntOption('b', '', warnings_only=True)
    od = OptionDescription('od', '', [a, b])
    a.impl_add_consistency('not_equal', b)
    api = Config(od)
    api.option('a').value.set(1)
    warnings.simplefilter("always", ValueWarning)
    with warnings.catch_warnings(record=True) as w:
        api.option('b').value.set(1)
    assert w != []
    api.option('a').value.reset()
    api.option('b').value.set(1)
    raises(ValueError, "api.option('a').value.set(1)")
    #
    api.property.add('demoting_error_warning')
    warnings.simplefilter("always", ValueWarning)
    with warnings.catch_warnings(record=True) as w:
        api.option('a').value.set(1)
    assert len(w) == 1


def test_consistency_not_equal():
    a = IntOption('a', '')
    b = IntOption('b', '')
    od = OptionDescription('od', '', [a, b])
    a.impl_add_consistency('not_equal', b)
    api = Config(od)
    assert api.option('a').value.get() is None
    assert api.option('b').value.get() is None
    api.option('a').value.set(1)
    api.option('a').value.reset()
    api.option('a').value.set(1)
    raises(ValueError, "api.option('b').value.set(1)")
    api.option('b').value.set(2)
    #
    api.property.add('demoting_error_warning')
    warnings.simplefilter("always", ValueWarning)
    with warnings.catch_warnings(record=True) as w:
        api.option('b').value.set(1)
    assert len(w) == 1


def test_consistency_not_equal_many_opts():
    a = IntOption('a', '')
    b = IntOption('b', '')
    c = IntOption('c', '')
    d = IntOption('d', '')
    e = IntOption('e', '')
    f = IntOption('f', '')
    od = OptionDescription('od', '', [a, b, c, d, e, f])
    a.impl_add_consistency('not_equal', b, c, d, e, f)
    api = Config(od)
    assert api.option('a').value.get() is None
    assert api.option('b').value.get() is None
    #
    api.option('a').value.set(1)
    api.option('a').value.reset()
    #
    api.option('a').value.set(1)
    raises(ValueError, "api.option('b').value.set(1)")
    #
    api.option('b').value.set(2)
    raises(ValueError, "api.option('f').value.set(2)")
    raises(ValueError, "api.option('f').value.set(1)")
    #
    api.option('d').value.set(3)
    raises(ValueError, "api.option('f').value.set(3)")
    raises(ValueError, "api.option('a').value.set(3)")
    api.option('d').value.set(3)
    raises(ValueError, "api.option('c').value.set(3)")
    raises(ValueError, "api.option('e').value.set(3)")
    #
    api.property.add('demoting_error_warning')
    warnings.simplefilter("always", ValueWarning)
    with warnings.catch_warnings(record=True) as w:
        api.option('c').value.set(3)
    assert len(w) == 1
    with warnings.catch_warnings(record=True) as w:
        api.option('e').value.set(3)
    assert len(w) == 1


def test_consistency_not_equal_many_opts_one_disabled():
    a = IntOption('a', '')
    b = IntOption('b', '')
    c = IntOption('c', '')
    d = IntOption('d', '')
    e = IntOption('e', '')
    f = IntOption('f', '')
    g = IntOption('g', '', properties=('disabled',))
    od = OptionDescription('od', '', [a, b, c, d, e, f, g])
    a.impl_add_consistency('not_equal', b, c, d, e, f, g, transitive=False)
    api = Config(od)
    api.property.read_write()
    assert api.option('a').value.get() is None
    assert api.option('b').value.get() is None
    #
    api.option('a').value.set(1)
    api.option('a').value.reset()
    #
    api.option('a').value.set(1)
    raises(ValueError, "api.option('b').value.set(1)")
    #
    api.option('b').value.set(2)
    raises(ValueError, "api.option('f').value.set(2)")
    raises(ValueError, "api.option('f').value.set(1)")
    #
    api.option('d').value.set(3)
    raises(ValueError, "api.option('f').value.set(3)")
    raises(ValueError, "api.option('a').value.set(3)")
    raises(ValueError, "api.option('c').value.set(3)")
    raises(ValueError, "api.option('e').value.set(3)")
    #
    api.property.add('demoting_error_warning')
    with warnings.catch_warnings(record=True) as w:
        api.option('c').value.set(3)
    assert len(w) == 1


def test_consistency_not_in_config_1():
    a = IntOption('a', '')
    b = IntOption('b', '')
    a.impl_add_consistency('not_equal', b)
    od1 = OptionDescription('od1', '', [a])
    od = OptionDescription('root', '', [od1])
    od
    raises(ConfigError, "Config(od)")


def test_consistency_not_in_config_2():
    a = IntOption('a', '')
    b = IntOption('b', '')
    a.impl_add_consistency('not_equal', b)
    od1 = OptionDescription('od1', '', [a])
    od2 = OptionDescription('od2', '', [b])
    od = OptionDescription('root', '', [od1, od2])
    Config(od)


def test_consistency_not_in_config_3():
    a = IntOption('a', '')
    b = IntOption('b', '')
    a.impl_add_consistency('not_equal', b)
    od1 = OptionDescription('od1', '', [a])
    od2 = OptionDescription('od2', '', [b])
    od = OptionDescription('root', '', [od1, od2])
    od
    #with subconfig
    raises(ConfigError, "Config(od1)")


def test_consistency_after_config():
    a = IntOption('a', '')
    b = IntOption('b', '')
    od1 = OptionDescription('od1', '', [a])
    od2 = OptionDescription('od2', '', [b])
    od = OptionDescription('root', '', [od1, od2])
    Config(od)
    raises(AttributeError, "a.impl_add_consistency('not_equal', b)")


def test_consistency_not_equal_symlink():
    a = IntOption('a', '')
    b = IntOption('b', '')
    c = SymLinkOption('c', a)
    od = OptionDescription('od', '', [a, b, c])
    a.impl_add_consistency('not_equal', b)
    api = Config(od)
    assert set(od._cache_consistencies.keys()) == set([a, b])


def test_consistency_mix():
    b = IntOption('b', '', multi=True)
    c = IntOption('c', '', multi=True)
    d = IntOption('d', '', multi=True)
    od = Leadership('c', '', [c, d])
    od2 = OptionDescription('a', '', [b, od])
    c.impl_add_consistency('not_equal', b, d)
    cfg = Config(od2)
    cfg.option('b').value.set([1, 2, 3])
    cfg.option('c.c').value.set([4, 5])
    raises(ValueError, "cfg.option('c.c').value.set([1, 2])")
    raises(ValueError, "cfg.option('c.d', 0).value.set(1)")
    raises(ValueError, "cfg.option('c.d', 1).value.set(4)")
    #
    cfg.property.add('demoting_error_warning')
    with warnings.catch_warnings(record=True) as w:
        cfg.option('c.d', 1).value.set(4)
    assert len(w) == 1


def test_consistency_not_equal_submulti():
    a = IntOption('a', '', multi=submulti)
    b = IntOption('b', '', multi=submulti)
    od = OptionDescription('a', '', [a, b])
    raises(ConfigError, 'a.impl_add_consistency("not_equal", b)')


def test_consistency_not_equal_default_submulti():
    a = IntOption('a', '', [[1, 2]], multi=submulti)
    b = IntOption('b', '', [[1]], multi=submulti)
    od = OptionDescription('od', '', [a, b])
    od
    raises(ConfigError, "a.impl_add_consistency('not_equal', b)")


def test_consistency_not_equal_leadership():
    a = IntOption('a', '', multi=True)
    b = IntOption('b', '', multi=True)
    od = Leadership('a', '', [a, b])
    od2 = OptionDescription('b', '', [od])
    a.impl_add_consistency('not_equal', b)
    api = Config(od2)
    assert api.option('a.a').value.get() == []
    api.option('a.a').value.set([1])
    api.option('a.a').value.reset()
    api.option('a.a').value.set([1])
    raises(ValueError, "api.option('a.b', 0).value.set(1)")
    api.option('a.b', 0).value.set(2)
    api.option('a.a').value.reset()
    api.option('a.a').value.set([1])
    api.value.dict()


def test_consistency_not_equal_leadership_error_multi1():
    a = IPOption('a', '', multi=True)
    b = NetmaskOption('b', '', multi=True)
    c = NetmaskOption('c', '', multi=True)
    od = Leadership('a', '', [a, b])
    od2 = OptionDescription('b', '', [od, c])
    c.impl_add_consistency('ip_netmask', a)
    raises(ConfigError, "Config(od2)")


def test_consistency_not_equal_leadership_error_multi2():
    a = IPOption('a', '', multi=True)
    b = NetmaskOption('b', '', multi=True)
    c = IPOption('c', '', multi=True)
    od = Leadership('a', '', [a, b])
    od2 = OptionDescription('b', '', [od, c])
    b.impl_add_consistency('ip_netmask', c)
    raises(ConfigError, "Config(od2)")


def test_consistency_ip_netmask_leadership_error_not_leader():
    a = IPOption('a', '', multi=True)
    b = NetmaskOption('b', '', multi=True)
    od = OptionDescription('a', '', [a, b])
    od2 = OptionDescription('b', '', [od])
    b.impl_add_consistency('ip_netmask', a)
    raises(ConfigError, "Config(od2)")


def test_consistency_ip_netmask_leadership_error_leader_and_not():
    a = IPOption('a', '', multi=True)
    b = NetmaskOption('b', '', multi=True)
    c = IPOption('c', '', multi=True)
    d = NetmaskOption('d', '', multi=True)
    od = Leadership('a', '', [a, b])
    od2 = OptionDescription('c', '', [c, d])
    od3 = OptionDescription('b', '', [od, od2])
    d.impl_add_consistency('ip_netmask', a)
    raises(ConfigError, "Config(od3)")


def test_consistency_ip_netmask_leadership_error_otherleader():
    a = IPOption('a', '', multi=True)
    b = NetmaskOption('b', '', multi=True)
    c = IPOption('c', '', multi=True)
    d = NetmaskOption('d', '', multi=True)
    od = Leadership('a', '', [a, b])
    od2 = Leadership('c', '', [c, d])
    od3 = OptionDescription('b', '', [od, od2])
    d.impl_add_consistency('ip_netmask', a)
    raises(ConfigError, "Config(od2)")


def test_consistency_not_equal_leadership_default():
    a = IntOption('a', '', multi=True)
    b = IntOption('b', '', multi=True, default_multi=1)
    od = Leadership('a', '', [a, b])
    od2 = OptionDescription('a', '', [od])
    a.impl_add_consistency('not_equal', b)
    api = Config(od2)
    assert api.option('a.a').value.get() == []
    raises(ValueError, "api.option('a.a').value.set([1])")
    api.option('a.a').value.set([2])
    api.option('a.a').value.reset()
    #
    api.property.add('demoting_error_warning')
    with warnings.catch_warnings(record=True) as w:
        api.option('a.a').value.set([1])
    assert len(w) == 1


def test_consistency_not_equal_multi():
    a = IntOption('a', '', multi=True)
    b = IntOption('b', '', multi=True)
    od = OptionDescription('a', '', [a, b])
    a.impl_add_consistency('not_equal', b)
    api = Config(od)
    assert api.option('a').value.get() == []
    assert api.option('b').value.get() == []
    api.option('a').value.set([1])
    api.option('a').value.reset()
    api.option('a').value.set([1])
    raises(ValueError, "api.option('b').value.set([1])")
    api.option('a').value.set([2])
    raises(ValueError, "api.option('b').value.set([2, 1])")
    api.option('a').value.set([2, 3])
    raises(ValueError, "api.option('a').value.set([2, 3, 3])")
    raises(ValueError, "api.option('b').value.set([2, 3])")
    #
    api.property.add('demoting_error_warning')
    with warnings.catch_warnings(record=True) as w:
        api.option('b').value.set([2, 3])
    assert len(w) == 1


def test_consistency_not_equal_multi_default1():
    a = IntOption('a', '', multi=True, default=[1])
    b = IntOption('b', '', multi=True, default=[3, 1])
    od = OptionDescription('a', '', [a, b])
    raises(ValueError, "b.impl_add_consistency('not_equal', a)")


def test_consistency_not_equal_multi_default2():
    a = IntOption('a', '', multi=True, default=[1])
    b = IntOption('b', '', multi=True, default_multi=1)
    od = OptionDescription('a', '', [a, b])
    #default_multi not tested now
    a.impl_add_consistency('not_equal', b)


def test_consistency_not_equal_leader_default():
    a = IntOption('a', '', multi=True, default=[2, 1])
    b = IntOption('b', '', multi=True, default_multi=1)
    od = Leadership('a', '', [a, b])
    a.impl_add_consistency('not_equal', b)
    od2 = OptionDescription('a', '', [od])
    api = Config(od2)
    # default_multi not tested
    raises(ValueError, "api.option('a.b', 0).value.get()")
    api.option('a.b', 0).value.set(3)
    api.option('a.b', 1).value.set(3)
    assert api.option('a.b', 1).value.get() == 3


def test_consistency_not_equal_multi_default_modif():
    a = IntOption('a', '', multi=True)
    b = IntOption('b', '', multi=True, default=[1, 2])
    od = OptionDescription('a', '', [a, b])
    a.impl_add_consistency('not_equal', b)
    api = Config(od)
    assert api.option('a').value.get() == []
    assert api.option('b').value.get() == [1, 2]
    raises(ValueError, "api.option('a').value.set([1])")
    raises(ValueError, "api.option('b').value.set([1, 2, 1])")
    #
    api.property.add('demoting_error_warning')
    with warnings.catch_warnings(record=True) as w:
        api.option('b').value.set([1, 2, 1])
    assert len(w) == 1


def test_consistency_default():
    a = IntOption('a', '', 1)
    b = IntOption('b', '', 1)
    a, b
    raises(ValueError, "a.impl_add_consistency('not_equal', b)")


def test_consistency_default_multi():
    a = IntOption('a', '', [2, 1], multi=True)
    b = IntOption('b', '', [1, 1], multi=True)
    c = IntOption('c', '', [1, 2], multi=True)
    b
    raises(ValueError, "a.impl_add_consistency('not_equal', b)")
    if TIRAMISU_VERSION != 2:
        raises(ValueError, "a.impl_add_consistency('not_equal', c)")


def test_consistency_default_diff():
    a = IntOption('a', '', 3)
    b = IntOption('b', '', 1)
    od = OptionDescription('od', '', [a, b])
    a.impl_add_consistency('not_equal', b)
    api = Config(od)
    raises(ValueError, "api.option('a').value.set(1)")
    api.option('a').value.set(2)
    api.option('b').value.set(3)
    owner = api.owner.get()
    assert api.option('a').owner.get() == owner
    raises(ValueError, "api.option('a').value.reset()")
    assert api.option('a').owner.get() == owner
    #
    api.property.add('demoting_error_warning')
    with warnings.catch_warnings(record=True) as w:
        api.option('a').value.reset()
    assert len(w) == 1


def test_consistency_ip_netmask():
    a = IPOption('a', '')
    b = NetmaskOption('b', '')
    od = OptionDescription('od', '', [a, b])
    b.impl_add_consistency('ip_netmask', a)
    api = Config(od)
    api.option('a').value.set('192.168.1.1')
    api.option('b').value.set('255.255.255.0')
    api.option('a').value.set('192.168.1.2')
    api.option('b').value.set('255.255.255.128')
    api.option('b').value.set('255.255.255.0')
    raises(ValueError, "api.option('a').value.set('192.168.1.0')")
    raises(ValueError, "api.option('a').value.set('192.168.1.255')")
    api.option('a').value.reset()
    api.option('b').value.reset()
    api.option('a').value.set('192.168.1.255')
    raises(ValueError, "api.option('b').value.set('255.255.255.0')")
    #
    api.property.add('demoting_error_warning')
    with warnings.catch_warnings(record=True) as w:
        api.option('b').value.set('255.255.255.0')
    assert len(w) == 1


def test_consistency_ip_netmask_invalid():
    b = NetmaskOption('b', '')
    od = OptionDescription('od', '', [b])
    raises(ConfigError, "b.impl_add_consistency('ip_netmask')")


def test_consistency_network_netmask():
    a = NetworkOption('a', '')
    b = NetmaskOption('b', '')
    od = OptionDescription('od', '', [a, b])
    b.impl_add_consistency('network_netmask', a)
    api = Config(od)
    api.option('a').value.set('192.168.1.1')
    api.option('b').value.set('255.255.255.255')
    api.option('b').value.reset()
    api.option('a').value.set('192.168.1.0')
    api.option('b').value.set('255.255.255.0')
    raises(ValueError, "api.option('a').value.set('192.168.1.1')")
    #
    api.property.add('demoting_error_warning')
    with warnings.catch_warnings(record=True) as w:
        api.option('a').value.set('192.168.1.1')
    assert len(w) == 1


def test_consistency_network_netmask_invalid():
    b = NetmaskOption('b', '')
    od = OptionDescription('od', '', [b])
    raises(ConfigError, "b.impl_add_consistency('network_netmask')")


def test_consistency_ip_in_network():
    a = NetworkOption('a', '')
    b = NetmaskOption('b', '')
    c = IPOption('c', '')
    d = IPOption('d', '')
    od = OptionDescription('od', '', [a, b, c, d])
    c.impl_add_consistency('in_network', a, b)
    d.impl_add_consistency('in_network', a, b, warnings_only=True)
    warnings.simplefilter("always", ValueWarning)
    api = Config(od)
    api.option('a').value.set('192.168.1.0')
    api.option('b').value.set('255.255.255.0')
    api.option('c').value.set('192.168.1.1')
    raises(ValueError, "api.option('c').value.set('192.168.2.1')")
    raises(ValueError, "api.option('c').value.set('192.168.1.0')")
    raises(ValueError, "api.option('c').value.set('192.168.1.255')")
    with warnings.catch_warnings(record=True) as w:
        api.option('d').value.set('192.168.2.1')
    assert len(w) == 1


def test_consistency_ip_in_network_cidr():
    a = NetworkOption('a', '', cidr=True)
    c = IPOption('c', '')
    d = IPOption('d', '')
    od = OptionDescription('od', '', [a, c, d])
    c.impl_add_consistency('in_network', a)
    d.impl_add_consistency('in_network', a, warnings_only=True)
    warnings.simplefilter("always", ValueWarning)
    api = Config(od)
    api.option('a').value.set('192.168.1.0/24')
    api.option('c').value.set('192.168.1.1')
    raises(ValueError, "api.option('c').value.set('192.168.2.1')")
    raises(ValueError, "api.option('c').value.set('192.168.1.0')")
    raises(ValueError, "api.option('c').value.set('192.168.1.255')")
    with warnings.catch_warnings(record=True) as w:
        api.option('d').value.set('192.168.2.1')
    assert len(w) == 1


def test_consistency_ip_in_network_invalid():
    a = NetworkOption('a', '')
    b = NetmaskOption('b', '')
    c = IPOption('c', '')
    d = IPOption('d', '')
    od = OptionDescription('od', '', [a, b, c, d])
    raises(ConfigError, "c.impl_add_consistency('in_network', a)")


def test_consistency_ip_netmask_error_multi():
    a = IPOption('a', '', multi=True)
    b = NetmaskOption('b', '')
    OptionDescription('od', '', [a, b])
    raises(ConfigError, "b.impl_add_consistency('ip_netmask', a)")


def test_consistency_ip_netmask_multi():
    a = IPOption('a', '', multi=True)
    b = NetmaskOption('b', '', multi=True)
    od = Leadership('a', '', [a, b])
    b.impl_add_consistency('ip_netmask', a)
    od2 = OptionDescription('od2', '', [od])
    api = Config(od2)
    api.option('a.a').value.set(['192.168.1.1'])
    api.option('a.b', 0).value.set('255.255.255.0')
    api.option('a.a').value.set(['192.168.1.2'])
    api.option('a.b', 0).value.set('255.255.255.128')
    api.option('a.b', 0).value.set('255.255.255.0')
    raises(ValueError, "api.option('a.a').value.set(['192.168.1.0'])")
    #
    api.property.add('demoting_error_warning')
    with warnings.catch_warnings(record=True) as w:
        api.option('a.a').value.set(['192.168.1.0'])
    assert len(w) == 1


def test_consistency_network_netmask_multi():
    a = NetworkOption('a', '', multi=True)
    b = NetmaskOption('b', '', multi=True)
    od = Leadership('a', '', [a, b])
    b.impl_add_consistency('network_netmask', a)
    od2 = OptionDescription('od', '', [od])
    api = Config(od2)
    api.option('a.a').value.set(['192.168.1.1'])
    api.option('a.b', 0).value.set('255.255.255.255')
    api.option('a.b', 0).value.reset()
    api.option('a.a').value.set(['192.168.1.0'])
    api.option('a.b', 0).value.set('255.255.255.0')
    raises(ValueError, "api.option('a.a').value.set(['192.168.1.1'])")


def test_consistency_network_netmask_multi_follower_default_multi():
    a = NetworkOption('a', '', default_multi=u'192.168.1.0', multi=True, properties=('mandatory',))
    b = NetmaskOption('b', '', default_multi=u'255.255.255.0', multi=True, properties=('mandatory',))
    od = Leadership('a', '', [a, b])
    od2 = OptionDescription('od2', '', [od])
    b.impl_add_consistency('network_netmask', a)
    api = Config(od2)
    api.property.read_write()
    api.option('a.a').value.set([undefined])
    assert api.option('a.a').value.get() == ['192.168.1.0']
    assert api.option('a.b', 0).value.get() == '255.255.255.0'


def test_consistency_network_netmask_multi_follower_default():
    a = NetworkOption('a', '', multi=True, properties=('mandatory',))
    b = NetmaskOption('b', '', default_multi=u'255.255.255.0', multi=True, properties=('mandatory',))
    od = Leadership('a', '', [a, b])
    b.impl_add_consistency('network_netmask', a)
    od2 = OptionDescription('od2', '', [od])
    api = Config(od2)
    api.property.read_write()
    api.property.pop('cache')
    assert api.option('a.a').value.get() == []
    api.option('a.a').value.set(['192.168.1.0'])
    api.property.read_only()
    assert api.option('a.a').value.get() == [u'192.168.1.0']
    assert api.option('a.b', 0).value.get() == u'255.255.255.0'
    api.property.read_write()
    raises(ValueError, "api.option('a.a').value.set([u'192.168.1.0', u'192.168.1.1'])")
    api.option('a.a').value.set(['192.168.1.0', undefined])
    api.option('a.b', 0).value.set('255.255.255.0')
    api.option('a.b', 1).value.set('255.255.255.255')
    api.option('a.a').value.set([u'192.168.1.0', u'192.168.1.1'])


def return_netmask(*args, **kwargs):
    return u'255.255.255.0'


def return_netmask2(leader):
    if leader is not None:
        if leader.endswith('2.1'):
            return u'255.255.255.0'
        if not leader.endswith('.0'):
            return u'255.255.255.255'
    return u'255.255.255.0'


def test_consistency_network_netmask_multi_follower_callback():
    a = NetworkOption('a', '', multi=True, properties=('mandatory',))
    b = NetmaskOption('b', '', callback=return_netmask, multi=True, properties=('mandatory',))
    od = Leadership('a', '', [a, b])
    b.impl_add_consistency('network_netmask', a)
    od2 = OptionDescription('od2', '', [od])
    api = Config(od2)
    api.property.read_write()
    api.property.pop('cache')
    assert api.option('a.a').value.get() == []
    api.option('a.a').value.set(['192.168.1.0'])
    api.property.read_only()
    assert api.option('a.a').value.get() == [u'192.168.1.0']
    assert api.option('a.b', 0).value.get() == '255.255.255.0'
    api.property.read_write()
    raises(ValueError, "assert api.option('a.a').value.set([u'192.168.1.0', u'192.168.1.1'])")
    api.option('a.a').value.set(['192.168.1.0', undefined])
    api.option('a.b', 0).value.set('255.255.255.0')
    api.option('a.b', 1).value.set('255.255.255.255')
    api.option('a.a').value.set(['192.168.1.0', '192.168.1.1'])


def test_consistency_network_netmask_multi_follower_callback_value():
    a = NetworkOption('a', '', multi=True, properties=('mandatory',))
    b = NetmaskOption('b', '', callback=return_netmask2, callback_params=Params(ParamOption(a)), multi=True, properties=('mandatory',))
    od = Leadership('a', '', [a, b])
    b.impl_add_consistency('network_netmask', a)
    od2 = OptionDescription('od2', '', [od])
    api = Config(od2)
    api.property.read_write()
    api.property.pop('cache')
    assert api.option('a.a').value.get() == []
    api.option('a.a').value.set(['192.168.1.0'])
    assert api.option('a.a').value.get() == ['192.168.1.0']
    assert api.option('a.b', 0).value.get() == '255.255.255.0'
    raises(ValueError, "api.option('a.a').value.set(['192.168.1.0', '192.168.2.1'])")
    assert api.option('a.a').value.get() == [u'192.168.1.0']
    assert api.option('a.b', 0).value.get() == '255.255.255.0'
    raises(ValueError, "api.option('a.a').value.set(['192.168.2.1'])")
    assert api.option('a.a').value.get() == [u'192.168.1.0']
    assert api.option('a.b', 0).value.get() == '255.255.255.0'
    api.option('a.a').value.set(['192.168.1.0', '192.168.1.1'])
    api.option('a.b', 0).value.set('255.255.255.0')
    api.option('a.b', 1).value.set('255.255.255.255')


def test_consistency_ip_netmask_multi_leader():
    a = IPOption('a', '', multi=True)
    b = NetmaskOption('b', '', multi=True)
    od = Leadership('a', '', [a, b])
    b.impl_add_consistency('ip_netmask', a)
    od2 = OptionDescription('od2', '', [od])
    api = Config(od2)
    api.option('a.a').value.set(['192.168.1.1'])
    api.option('a.b', 0).value.set('255.255.255.0')
    api.option('a.a').value.set(['192.168.1.2'])
    api.option('a.b', 0).value.set('255.255.255.128')
    api.option('a.b', 0).value.set('255.255.255.0')
    raises(ValueError, "api.option('a.a').value.set(['192.168.1.0'])")
    api.option('a.a').value.set(['192.168.1.128'])
    raises(ValueError, "api.option('a.b', 0).value.set('255.255.255.128')")
    api.option('a.a').value.set(['192.168.1.2', '192.168.1.3'])


def test_consistency_network_netmask_multi_leader():
    a = NetworkOption('a', '', multi=True)
    b = NetmaskOption('b', '', multi=True)
    od = Leadership('a', '', [a, b])
    b.impl_add_consistency('network_netmask', a)
    od2 = OptionDescription('od2', '', [od])
    api = Config(od2)
    api.option('a.a').value.set(['192.168.1.1'])
    api.option('a.b', 0).value.set('255.255.255.255')
    api.option('a.b', 0).value.reset()
    api.option('a.a').value.set(['192.168.1.0'])
    api.option('a.b', 0).value.set('255.255.255.0')
    raises(ValueError, "api.option('a.a').value.set(['192.168.1.1'])")


def test_consistency_broadcast():
    a = NetworkOption('a', '', multi=True)
    b = NetmaskOption('b', '', multi=True)
    c = BroadcastOption('c', '', multi=True)
    od = Leadership('a', '', [a, b, c])
    b.impl_add_consistency('network_netmask', a)
    c.impl_add_consistency('broadcast', a, b)
    od2 = OptionDescription('od2', '', [od])
    api = Config(od2)
    #first, test network_netmask
    api.option('a.a').value.set(['192.168.1.128'])
    raises(ValueError, "api.option('a.a').value.set(['255.255.255.0'])")
    #
    api.option('a.a').value.set(['192.168.1.0'])
    api.option('a.b', 0).value.set('255.255.255.0')
    api.option('a.c', 0).value.set('192.168.1.255')
    raises(ValueError, "api.option('a.a').value.set(['192.168.1.1'])")
    #
    api.option('a.a').value.set(['192.168.1.0', '192.168.2.128'])
    api.option('a.b', 0).value.set('255.255.255.0')
    api.option('a.b', 1).value.set('255.255.255.128')
    api.option('a.c', 0).value.set('192.168.1.255')
    api.option('a.c', 1).value.set('192.168.2.255')
    raises(ValueError, "api.option('a.c', 1).value.set('192.168.2.128')")
    api.option('a.c', 1).value.set('192.168.2.255')


def test_consistency_broadcast_error():
    a = NetworkOption('a', '', multi=True)
    b = NetmaskOption('b', '', multi=True)
    c = BroadcastOption('c', '', multi=True)
    od = Leadership('a', '', [a, b, c])
    od2 = OptionDescription('od2', '', [od])
    b.impl_add_consistency('network_netmask', a)
    c.impl_add_consistency('broadcast', a)
    api = Config(od2)
    raises(ConfigError, "api.option('a.a').value.set(['192.168.1.0'])")


def test_consistency_broadcast_warnings():
    warnings.simplefilter("always", ValueWarning)
    a = NetworkOption('a', '', properties=('mandatory', 'disabled'))
    b = NetmaskOption('b', '', properties=('mandatory', 'disabled'))
    c = NetmaskOption('c', '', properties=('mandatory', 'disabled'))
    od = OptionDescription('a', '', [a, b, c])
    b.impl_add_consistency('network_netmask', a, warnings_only=True)
    api = Config(od)
    with warnings.catch_warnings(record=True) as w:
        api.option('a').value.set('192.168.1.4')
        api.option('b').value.set('255.255.255.0')
    assert len(w) == 1
    api.property.read_write()
    with warnings.catch_warnings(record=True) as w:
        list(api.value.mandatory())
    assert len(w) == 0


def test_consistency_broadcast_default_1():
    a = NetworkOption('a', '', '192.168.1.0')
    b = NetmaskOption('b', '', '255.255.255.128')
    c = BroadcastOption('c', '', '192.168.2.127')
    od = OptionDescription('a', '', [a, b, c])
    od
    raises(ValueError, "c.impl_add_consistency('broadcast', a, b)")


def test_consistency_broadcast_default_2():
    a = NetworkOption('a', '', '192.168.1.0')
    b = NetmaskOption('b', '', '255.255.255.128')
    d = BroadcastOption('d', '', '192.168.1.127')
    od2 = OptionDescription('a', '', [a, b, d])
    od2
    d.impl_add_consistency('broadcast', a, b)


def test_consistency_not_all():
    #_cache_consistencies is not None by not options has consistencies
    a = NetworkOption('a', '', multi=True)
    b = NetmaskOption('b', '', multi=True)
    c = BroadcastOption('c', '', multi=True)
    od = Leadership('a', '', [a, b, c])
    b.impl_add_consistency('network_netmask', a)
    od2 = OptionDescription('od2', '', [od])
    api = Config(od2)
    api.option('a.a').value.set(['192.168.1.0'])
    api.option('a.b', 0).value.set('255.255.255.0')
    api.option('a.c', 0).value.set('192.168.1.255')


def test_consistency_permissive():
    a = IntOption('a', '', 1)
    b = IntOption('b', '', 2, properties=('hidden',))
    od = OptionDescription('od', '', [a, b])
    a.impl_add_consistency('not_equal', b)
    api = Config(od)
    api.property.read_write()
    api.permissive.set(frozenset(['hidden']))
    api.option('a').value.set(1)


def test_consistency_disabled():
    a = IntOption('a', '')
    b = IntOption('b', '', properties=('disabled',))
    od = OptionDescription('od', '', [a, b])
    a.impl_add_consistency('not_equal', b)
    api = Config(od)
    api.property.read_write()
    raises(PropertiesOptionError, "api.option('a').value.set(1)")


def test_consistency_disabled_transitive():
    a = IntOption('a', '')
    b = IntOption('b', '', properties=('disabled',))
    od = OptionDescription('od', '', [a, b])
    a.impl_add_consistency('not_equal', b, transitive=False)
    api = Config(od)
    api.property.read_write()
    api.option('a').value.set(1)


def test_consistency_disabled_transitive_2():
    a = IPOption('a', '')
    b = IPOption('b', '')
    c = NetworkOption('c', '', default='192.168.1.0')
    d = NetmaskOption('d', '', default='255.255.255.0', properties=('disabled',))
    od = OptionDescription('od', '', [a, b, c, d])
    a.impl_add_consistency('not_equal', b)
    a.impl_add_consistency('in_network', c, d, transitive=False)
    api = Config(od)
    api.property.read_write()
    api.option('a').value.set('192.168.1.1')
    raises(ValueError, "api.option('b').value.set('192.168.1.1')")
    api.option('a').value.set('192.168.2.1')
    #
    api.option('a').value.set('192.168.1.1')
    api.property.pop('disabled')
    raises(ValueError, "api.option('a').value.set('192.168.2.1')")
    #
    api.property.add('demoting_error_warning')
    with warnings.catch_warnings(record=True) as w:
        api.option('a').value.set('192.168.2.1')
    assert len(w) == 1


def return_val(*args, **kwargs):
    return '192.168.1.1'


def test_consistency_with_callback():
    a = NetworkOption('a', '', default='192.168.1.0')
    b = NetmaskOption('b', '', default='255.255.255.0')
    c = IPOption('c', '', callback=return_val, callback_params=Params(ParamOption(a)))
    od = OptionDescription('od', '', [a, b, c])
    c.impl_add_consistency('in_network', a, b)
    api = Config(od)
    api.option('c').value.get()


def test_consistency_warnings_only_options():
    a = IPOption('a', '', warnings_only=True)
    b = IPOption('b', '')
    c = NetworkOption('c', '', default='192.168.1.0')
    d = NetmaskOption('d', '', default='255.255.255.0', properties=('disabled',))
    od = OptionDescription('od', '', [a, b, c, d])
    a.impl_add_consistency('not_equal', b)
    a.impl_add_consistency('in_network', c, d, transitive=False)
    api = Config(od)
    api.property.read_write()
    api.option('a').value.set('192.168.1.1')
    raises(ValueError, "api.option('b').value.set('192.168.1.1')")
    api.option('a').value.set('192.168.2.1')
    #
    api.option('a').value.set('192.168.1.1')
    api.property.pop('disabled')
    with warnings.catch_warnings(record=True) as w:
        api.option('a').value.set('192.168.2.1')
    assert len(w) == 1


def test_consistency_warnings_only_options_callback():
    a = IPOption('a', '', warnings_only=True)
    b = IPOption('b', '')
    c = NetworkOption('c', '', default='192.168.1.0')
    d = NetmaskOption('d', '', callback=return_netmask2, callback_params=Params(ParamOption(c)))
    od = OptionDescription('od', '', [a, b, c, d])
    a.impl_add_consistency('not_equal', b)
    a.impl_add_consistency('in_network', c, d, transitive=False)
    api = Config(od)
    api.property.read_write()
    api.option('a').value.set('192.168.1.1')
    raises(ValueError, "api.option('b').value.set('192.168.1.1')")
    with warnings.catch_warnings(record=True) as w:
        api.option('a').value.set('192.168.2.1')
    assert len(w) == 1
    #
    api.option('a').value.set('192.168.1.1')
    api.property.pop('disabled')
    with warnings.catch_warnings(record=True) as w:
        api.option('a').value.set('192.168.2.1')
    assert len(w) == 1


def test_consistency_double_warnings():
    a = IntOption('a', '')
    b = IntOption('b', '', 1)
    c = IntOption('c', '', 1)
    od = OptionDescription('od', '', [a, b, c])
    warnings.simplefilter("always", ValueWarning)
    a.impl_add_consistency('not_equal', b, warnings_only=True)
    a.impl_add_consistency('not_equal', c, warnings_only=True)
    od2 = OptionDescription('od2', '', [od])
    api = Config(od2)
    with warnings.catch_warnings(record=True) as w:
        api.option('od.a').value.set(1)
    assert w != []
    assert len(w) == 2
    with warnings.catch_warnings(record=True) as w:
        api.option('od.c').value.set(2)
    assert len(w) == 0
    with warnings.catch_warnings(record=True) as w:
        api.option('od.a').value.set(2)
    assert w != []
    assert len(w) == 1
    api.property.pop('warnings')
    with warnings.catch_warnings(record=True) as w:
        api.option('od.a').value.set(1)
    assert w == []


def test_consistency_warnings_error():
    a = IntOption('a', '')
    b = IntOption('b', '', 1)
    c = IntOption('c', '', 1)
    od = OptionDescription('od', '', [a, b, c])
    warnings.simplefilter("always", ValueWarning)
    a.impl_add_consistency('not_equal', b, warnings_only=True)
    a.impl_add_consistency('not_equal', c)
    api = Config(od)
    with warnings.catch_warnings(record=True) as w:
        raises(ValueError, "api.option('a').value.set(1)")
    assert w == []


def test_consistency_network_netmask_mandatory():
    a = NetworkOption('a', '', multi=True, properties=('mandatory',), default=[u'0.0.0.0'])
    b = NetmaskOption('b', '', multi=True, properties=('mandatory',), default_multi=u'0.0.0.0')
    od = Leadership('a', '', [a, b])
    b.impl_add_consistency('network_netmask', a)
    od2 = OptionDescription('od2', '', [od])
    api = Config(od2)
    api.property.read_only()
    api.property.pop('mandatory')
    api.value.dict()


def test_consistency_has_dependency():
    a = IPOption('a', '')
    b = NetmaskOption('b', '')
    od = OptionDescription('od', '', [a, b])
    b.impl_add_consistency('ip_netmask', a)
    api = Config(od)
    assert api.option('a').option.has_dependency() is True
    assert api.option('b').option.has_dependency() is True
    assert api.option('a').option.has_dependency(False) is True
    assert api.option('b').option.has_dependency(False) is True


def test_consistency_not_equal_has_dependency():
    a = IntOption('a', '')
    b = IntOption('b', '')
    od = OptionDescription('od', '', [a, b])
    a.impl_add_consistency('not_equal', b)
    api = Config(od)
    assert api.option('a').option.has_dependency() is False
    assert api.option('b').option.has_dependency() is False
    assert api.option('a').option.has_dependency(False) is True
    assert api.option('b').option.has_dependency(False) is True
