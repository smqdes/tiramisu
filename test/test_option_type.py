# coding: utf-8
"frozen and hidden values"
from .autopath import do_autopath
do_autopath()

from py.test import raises

from tiramisu import ChoiceOption, BoolOption, IntOption, FloatOption, \
    PasswordOption, StrOption, DateOption, OptionDescription, Config
from tiramisu.error import PropertiesOptionError
from tiramisu.storage import list_sessions


def teardown_function(function):
    assert list_sessions() == [], 'session list is not empty when leaving "{}"'.format(function.__name__)


def make_description():
    gcoption = ChoiceOption('name', 'GC name', ('ref', 'framework'), 'ref')
    gcdummy = BoolOption('dummy', 'dummy', default=False, properties=(('hidden'),))
    objspaceoption = ChoiceOption('objspace', 'Object space',
                                  ('std', 'thunk'), ['std'], multi=True)
    booloption = BoolOption('bool', 'Test boolean option', default=True)
    intoption = IntOption('int', 'Test int option', default=0)
    floatoption = FloatOption('float', 'Test float option', default=2.3)
    stroption = StrOption('str', 'Test string option', default="abc")

    wantref_option = BoolOption('wantref', 'Test requires', default=False,
                                requires=({'option': gcoption, 'expected': 'ref', 'action': 'hidden'},))
    wantframework_option = BoolOption('wantframework', 'Test requires',
                                      default=False,
                                      requires=({'option': gcoption, 'expected': 'framework', 'action': 'hidden'},))

    # ____________________________________________________________
    booloptiontwo = BoolOption('booltwo', 'Test boolean option two', default=False)
    subgroup = OptionDescription('subgroup', '', [booloptiontwo])
    # ____________________________________________________________

    gcgroup = OptionDescription('gc', '', [subgroup, gcoption, gcdummy, floatoption])
    descr = OptionDescription('trs', '', [gcgroup, booloption, objspaceoption,
                                          wantref_option, stroption,
                                          wantframework_option,
                                          intoption])
    return descr


# ____________________________________________________________
def test_is_hidden():
    descr = make_description()
    api = Config(descr)
    api.property.read_write()
    assert not 'frozen' in api.forcepermissive.option('gc.dummy').property.get()
    # setattr
    raises(PropertiesOptionError, "api.option('gc.dummy').value.get() == False")
    # getattr
    raises(PropertiesOptionError, "api.option('gc.dummy').value.get()")


def test_group_is_hidden():
    descr = make_description()
    api = Config(descr)
    api.property.read_write()
    api.option('gc').property.add('hidden')
    raises(PropertiesOptionError, "api.option('gc.dummy').value.get()")
    assert 'hidden' in api.forcepermissive.option('gc').property.get()
    raises(PropertiesOptionError, "api.option('gc.float').value.get()")
    # manually set the subconfigs to "show"
    api.forcepermissive.option('gc').property.pop('hidden')
    assert not 'hidden' in api.option('gc').property.get()
    assert api.option('gc.float').value.get() == 2.3
    #dummy est en hide
    prop = []
    try:
        api.option('gc.dummy').value.set(False)
    except PropertiesOptionError as err:
        prop = err.proptype
    assert 'hidden' in prop


def test_group_is_hidden_multi():
    descr = make_description()
    api = Config(descr)
    api.property.read_write()
    api.option('objspace').property.add('hidden')
    raises(PropertiesOptionError, "api.option('objspace').value.get()")
    assert 'hidden' in api.forcepermissive.option('objspace').property.get()
    prop = []
    try:
        api.option('objspace').value.set(['std'])
    except PropertiesOptionError as err:
        prop = err.proptype
    assert 'hidden' in prop
    api.forcepermissive.option('objspace').property.pop('hidden')
    assert not 'hidden' in api.option('objspace').property.get()
    api.option('objspace').value.set(['std', 'std'])


def test_global_show():
    descr = make_description()
    api = Config(descr)
    api.property.read_write()
    api.forcepermissive.option('gc.dummy').property.add('hidden')
    assert 'hidden' in api.forcepermissive.option('gc.dummy').property.get()
    raises(PropertiesOptionError, "api.option('gc.dummy').value.get() == False")


def test_with_many_subgroups():
    descr = make_description()
    api = Config(descr)
    #booltwo = config.unwrap_from_path('gc.subgroup.booltwo')
    #setting = config.cfgimpl_get_settings()
    assert not 'hidden' in api.option('gc.subgroup.booltwo').property.get()
    assert api.option('gc.subgroup.booltwo').value.get() is False
    api.option('gc.subgroup.booltwo').property.add('hidden')


def test_password_option():
    o = PasswordOption('o', '')
    d = OptionDescription('d', '', [o])
    api = Config(d)

    api.option('o').value.set('a_valid_password')
    raises(ValueError, "api.option('o').value.set(1)")


def test_date_option():
    o = DateOption('o', '')
    d = OptionDescription('d', '', [o])
    api = Config(d)

    api.option('o').value.set('2017-02-04')
    api.option('o').value.set('2017-2-4')
    raises(ValueError, "api.option('o').value.set(1)")
    raises(ValueError, "api.option('o').value.set('2017-13-20')")
    raises(ValueError, "api.option('o').value.set('2017-11-31')")
    raises(ValueError, "api.option('o').value.set('2017-12-32')")
    raises(ValueError, "api.option('o').value.set('2017-2-29')")
    raises(ValueError, "api.option('o').value.set('2-2-2017')")
    raises(ValueError, "api.option('o').value.set('2017/2/2')")
