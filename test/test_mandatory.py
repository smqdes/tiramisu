# coding: utf-8
from .autopath import do_autopath
do_autopath()

from py.test import raises
from tiramisu.api import TIRAMISU_VERSION
from tiramisu import Config
from tiramisu import IntOption, StrOption, UnicodeOption, OptionDescription, \
                     SymLinkOption, Leadership, undefined, Params, ParamOption
from tiramisu.error import PropertiesOptionError, ConfigError
from tiramisu.setting import groups
from tiramisu.storage import list_sessions


def teardown_function(function):
    assert list_sessions() == [], 'session list is not empty when leaving "{}"'.format(function.__name__)


def make_description():
    stro = StrOption('str', '')
    subdescr = OptionDescription('sub', '', [stro], properties=('disabled',))
    stroption = StrOption('str', 'Test string option', default="abc",
                          properties=('mandatory', ))
    stroption1 = StrOption('str1', 'Test string option',
                           properties=('mandatory', ))
    stroption2 = UnicodeOption('unicode2', 'Test string option',
                               properties=('mandatory', ))
    stroption3 = StrOption('str3', 'Test string option', multi=True,
                           properties=('mandatory', ))
    stroption4 = StrOption('str4', 'Test string option', multi=True,
                           properties=('mandatory', ), allow_empty_list=True)
    descr = OptionDescription('tiram', '', [subdescr, stroption, stroption1, stroption2, stroption3, stroption4])
    return descr


def return_value(value):
    return value


def make_description2():
    stroption = StrOption('str', 'Test string option', default="abc",
                          properties=('mandatory', ))
    stroption1 = StrOption('str1', 'Test string option',
                           properties=('mandatory', ))
    stroption2 = SymLinkOption('unicode2', stroption1)
    stroption3 = StrOption('str3', 'Test string option', multi=True,
                           properties=('mandatory', ))
    unicode1 = UnicodeOption('unicode1', 'Test string option', callback=return_value, callback_params=Params(ParamOption(stroption)),  properties=('mandatory', ))
    descr = OptionDescription('tiram', '', [stroption, stroption1, stroption2, stroption3, unicode1])
    return descr


def make_description_sym():
    stroption = StrOption('str', 'Test string option', default="abc",
                          properties=('mandatory', ))
    stroption1 = StrOption('str1', 'Test string option',
                           properties=('mandatory', ))
    stroption2 = SymLinkOption('unicode2', stroption1)
    stroption3 = StrOption('str3', 'Test string option', multi=True,
                           properties=('mandatory', ))
    descr = OptionDescription('tiram', '', [stroption, stroption1, stroption2, stroption3])
    return descr


def make_description3():
    stroption = StrOption('str', 'Test string option', default="abc",
                          properties=('mandatory', ))
    stroption1 = StrOption('str1', 'Test string option',
                           properties=('mandatory', ))
    stroption2 = SymLinkOption('unicode2', stroption1)
    stroption3 = StrOption('str3', 'Test string option', multi=True,
                           properties=('mandatory', ))
    unicode1 = UnicodeOption('unicode1', 'Test string option', callback=return_value, callback_params=Params(ParamOption(stroption)),  properties=('mandatory', ))
    int1 = IntOption('int1', '', callback=return_value, callback_params=Params(ParamOption(stroption)),  properties=('mandatory', ))
    descr = OptionDescription('tiram', '', [stroption, stroption1, stroption2, stroption3, unicode1, int1])
    return descr


def test_mandatory_ro():
    descr = make_description()
    api = Config(descr)
    api.property.read_only()
    prop = []
    try:
        api.option('str1').value.get()
    except PropertiesOptionError as err:
        prop = err.proptype
    assert 'mandatory' in prop
    api.property.read_write()
    api.option('str1').value.set('yes')
    api.property.read_only()
    assert api.option('str1').value.get() == 'yes'


def test_mandatory_ro_dict():
    descr = make_description()
    api = Config(descr)
    api.property.read_only()
    prop = []
    try:
        api.value.dict()
    except PropertiesOptionError as err:
        prop = err.proptype
    assert 'mandatory' in prop
    api.property.read_write()
    api.option('str1').value.set('yes')
    api.option('unicode2').value.set('yes')
    api.property.read_only()
    try:
        api.value.dict()
    except PropertiesOptionError as err:
        prop = err.proptype
    assert 'mandatory' in prop
    api.property.read_write()
    api.option('str3').value.set(['yes'])
    api.property.read_only()
    assert api.value.dict() == {'str': 'abc', 'str1': 'yes', 'str3': ['yes'], 'str4': [], 'unicode2': 'yes'}


def test_mandatory_rw():
    descr = make_description()
    api = Config(descr)
    api.property.read_write()
    # not mandatory in rw
    api.option('str1').value.get()
    api.option('str1').value.set('yes')
    assert api.option('str1').value.get() == 'yes'


def test_mandatory_default():
    descr = make_description()
    api = Config(descr)
    api.property.read_only()
    #not mandatory in rw
    api.option('str').value.get()
    api.property.read_write()
    api.option('str').value.set('yes')
    api.property.read_only()
    api.option('str').value.get()
    api.property.read_write()
    api.option('str').value.set(None)
    api.property.read_only()
    prop = []
    try:
        api.option('str').value.get()
    except PropertiesOptionError as err:
        prop = err.proptype
    assert 'mandatory' in prop


def test_mandatory_delete():
    descr = make_description()
    api = Config(descr)
    api.property.read_only()
    api.option('str').value.get()
    try:
        api.option('str1').value.get()
    except PropertiesOptionError as err:
        prop = err.proptype
    assert 'mandatory' in prop
    api.property.read_write()
    api.option('str1').value.set('yes')
    api.property.read_only()
    assert api.option('str1').value.get() == 'yes'
    api.property.pop('everything_frozen')
    prop = []
    try:
        api.option('str1').value.reset()
    except PropertiesOptionError as err:
        prop = err.proptype
    assert 'mandatory' in prop
    api.option('str').value.reset()

    assert api.option('str1').value.get() == 'yes'


#valeur vide : None, '', u'', ...
def test_mandatory_none():
    descr = make_description()
    api = Config(descr)
    api.option('str1').value.set(None)
    assert api.option('str1').owner.get() == 'user'
    api.property.read_only()
    prop = []
    try:
        api.option('str1').value.get()
    except PropertiesOptionError as err:
        prop = err.proptype
    assert 'mandatory' in prop


def test_mandatory_empty():
    descr = make_description()
    api = Config(descr)
    api.option('str1').value.set('')
    assert api.option('str1').owner.get() == 'user'
    api.property.read_only()
    prop = []
    try:
        api.option('str1').value.get()
    except PropertiesOptionError as err:
        prop = err.proptype
    assert 'mandatory' in prop


def test_mandatory_multi_none():
    descr = make_description()
    api = Config(descr)
    api.option('str3').value.set([None])
    assert api.option('str3').owner.get() == 'user'
    api.property.read_only()
    prop = []
    try:
        api.option('str3').value.get()
    except PropertiesOptionError as err:
        prop = err.proptype
    assert 'mandatory' in prop
    api.property.read_write()
    api.option('str3').value.set(['yes', None])
    assert api.option('str3').owner.get() == 'user'
    api.property.read_only()
    prop = []
    try:
        api.option('str3').value.get()
    except PropertiesOptionError as err:
        prop = err.proptype
    assert 'mandatory' in prop


def test_mandatory_multi_empty():
    descr = make_description()
    api = Config(descr)
    api.option('str3').value.set([])
    assert api.option('str3').owner.get() == 'user'
    api.property.read_only()
    prop = []
    try:
        api.option('str3').value.get()
    except PropertiesOptionError as err:
        prop = err.proptype
    assert 'mandatory' in prop
    #
    api.property.read_write()
    api.option('str3').value.set([''])
    assert api.option('str3').owner.get() == 'user'
    api.property.read_only()
    prop = []
    try:
        api.option('str3').value.get()
    except PropertiesOptionError as err:
        prop = err.proptype
    assert 'mandatory' in prop
    #
    api.property.read_write()
    api.option('str3').value.set(['yes', ''])
    assert api.option('str3').owner.get() == 'user'
    api.property.read_only()
    prop = []
    try:
        api.option('str3').value.get()
    except PropertiesOptionError as err:
        prop = err.proptype
    assert 'mandatory' in prop


def test_mandatory_multi_empty_allow_empty_list():
    descr = make_description()
    api = Config(descr)
    api.option('str4').value.set([])
    assert api.option('str4').owner.get() == 'user'
    api.property.read_only()
    prop = []
    api.option('str4').value.get()
    #
    api.property.read_write()
    api.option('str4').value.set([''])
    assert api.option('str4').owner.get() == 'user'
    api.property.read_only()
    prop = []
    try:
        api.option('str4').value.get()
    except PropertiesOptionError as err:
        prop = err.proptype
    assert 'mandatory' in prop
    #
    api.property.read_write()
    api.option('str4').value.set(['yes', ''])
    assert api.option('str4').owner.get() == 'user'
    api.property.read_only()
    prop = []
    try:
        api.option('str4').value.get()
    except PropertiesOptionError as err:
        prop = err.proptype
    assert 'mandatory' in prop


def test_mandatory_multi_append():
    descr = make_description()
    api = Config(descr)
    api.option('str3').value.set(['yes'])
    api.property.read_write()
    api.option('str3').value.get().append(None)


def test_mandatory_disabled():
    descr = make_description()
    api = Config(descr)
    api.option('str1').value.get()
    api.option('str1').property.add('disabled')
    api.property.read_only()
    pop = []
    try:
        api.option('str1').value.get()
    except PropertiesOptionError as err:
        prop = err.proptype
    if TIRAMISU_VERSION == 2:
        search_prop = {'disabled', 'mandatory'}
    else:
        search_prop = {'disabled'}
    assert set(prop) == search_prop


def test_mandatory_unicode():
    descr = make_description()
    api = Config(descr)
    api.option('unicode2').value.get()
    api.property.read_only()
    prop = []
    try:
        api.option('unicode2').value.get()
    except PropertiesOptionError as err:
        prop = err.proptype
    assert 'mandatory' in prop
    api.property.read_write()
    api.option('unicode2').value.set(u'')
    api.property.read_only()
    prop = []
    try:
        api.option('unicode2').value.get()
    except PropertiesOptionError as err:
        prop = err.proptype
    assert 'mandatory' in prop


def test_mandatory_warnings_ro():
    descr = make_description()
    api = Config(descr)
    api.option('str').value.set('')
    api.property.read_only()
    proc = []
    try:
        api.option('str').value.get()
    except PropertiesOptionError as err:
        prop = err.proptype
    assert 'mandatory' in prop
    assert list(api.value.mandatory()) == ['str', 'str1', 'unicode2', 'str3']
    api.property.read_write()
    api.option('str').value.set('a')
    api.property.read_only()
    assert list(api.value.mandatory()) == ['str1', 'unicode2', 'str3']


def test_mandatory_warnings_rw():
    descr = make_description()
    api = Config(descr)
    api.option('str').value.set('')
    api.property.read_write()
    api.option('str').value.get()
    assert list(api.value.mandatory()) == ['str', 'str1', 'unicode2', 'str3']
    api.option('str').value.set('a')
    assert list(api.value.mandatory()) == ['str1', 'unicode2', 'str3']


def test_mandatory_warnings_disabled():
    descr = make_description()
    api = Config(descr)
    api.option('str').value.set('')
    api.property.read_write()
    api.option('str').value.get()
    assert set(api.value.mandatory()) == {'str', 'str1', 'unicode2', 'str3'}
    api.option('str').property.add('disabled')
    assert set(api.value.mandatory()) == {'str1', 'unicode2', 'str3'}


def test_mandatory_warnings_hidden():
    descr = make_description()
    api = Config(descr)
    api.option('str').value.set('')
    api.property.read_write()
    api.permissive.set(frozenset(['hidden']))
    api.option('str').value.get()
    assert set(api.value.mandatory()) == {'str', 'str1', 'unicode2', 'str3'}
    api.option('str').property.add('hidden')
    assert set(api.value.mandatory()) == {'str', 'str1', 'unicode2', 'str3'}


def test_mandatory_warnings_frozen():
    descr = make_description()
    api = Config(descr)
    api.option('str').value.set('')
    api.property.read_write()
    api.option('str').value.get()
    assert set(api.value.mandatory()) == {'str', 'str1', 'unicode2', 'str3'}
    api.option('str').property.add('frozen')
    api.property.read_only()
    assert set(api.value.mandatory()) == {'str', 'str1', 'unicode2', 'str3'}


def test_mandatory_leader():
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé", multi=True,
                              properties=('mandatory', ))
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "masque du sous-réseau",
                                   multi=True)
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    descr = OptionDescription('o', '', [interface1])
    api = Config(descr)
    api.property.read_only()
    raises(PropertiesOptionError, "api.option('ip_admin_eth0.ip_admin_eth0').value.get()")
    raises(PropertiesOptionError, "api.value.dict()")


def test_mandatory_warnings_leader():
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé", multi=True,
                              properties=('mandatory', ))
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "masque du sous-réseau",
                                   multi=True)
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    descr = OptionDescription('o', '', [interface1])
    api = Config(descr)
    assert list(api.value.mandatory()) == ['ip_admin_eth0.ip_admin_eth0']


def test_mandatory_leader_empty():
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé", multi=True)
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "masque du sous-réseau",
                                   multi=True)
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    descr = OptionDescription('o', '', [interface1])
    api = Config(descr)
    api.property.read_write()
    assert api.option('ip_admin_eth0.ip_admin_eth0').value.get() == []
    #
    api.option('ip_admin_eth0.ip_admin_eth0').value.set([undefined])
    assert api.option('ip_admin_eth0.ip_admin_eth0').value.get() == [None]
    assert api.option('ip_admin_eth0.netmask_admin_eth0', 0).value.get() == None
    api.property.read_only()
    raises(PropertiesOptionError, "api.option('ip_admin_eth0.ip_admin_eth0').value.get()")
    raises(PropertiesOptionError, "api.option('ip_admin_eth0.netmask_admin_eth0', 0).value.get()")
    api.property.read_write()
    api.option('ip_admin_eth0.ip_admin_eth0').value.reset()
    assert api.option('ip_admin_eth0.ip_admin_eth0').value.get() == []
    #
    api.option('ip_admin_eth0.ip_admin_eth0').value.set([''])
    assert api.option('ip_admin_eth0.ip_admin_eth0').value.get() == ['']
    assert api.option('ip_admin_eth0.netmask_admin_eth0', 0).value.get() == None
    api.property.read_only()
    raises(PropertiesOptionError, "api.option('ip_admin_eth0.ip_admin_eth0').value.get()")
    raises(PropertiesOptionError, "api.option('ip_admin_eth0.netmask_admin_eth0', 0).value.get()")
    api.property.read_write()
    #
    api.property.read_write()
    api.option('ip_admin_eth0.ip_admin_eth0').value.set(['ip'])
    api.property.read_only()
    assert api.option('ip_admin_eth0.ip_admin_eth0').value.get() == ['ip']
    assert api.option('ip_admin_eth0.netmask_admin_eth0', 0).value.get() == None
    #
    api.property.read_write()
    api.option('ip_admin_eth0.ip_admin_eth0').value.set(['ip2'])
    api.property.read_only()
    raises(PropertiesOptionError, "api.option('ip_admin_eth0.ip_admin_eth0').value.reset()")
    api.property.read_write()
    api.option('ip_admin_eth0.ip_admin_eth0').value.reset()


def test_mandatory_warnings_leader_empty():
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé", multi=True)
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "masque du sous-réseau",
                                   multi=True)
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    descr = OptionDescription('o', '', [interface1])
    api = Config(descr)
    api.property.read_write()
    api.option('ip_admin_eth0.ip_admin_eth0').value.set([undefined])
    assert api.option('ip_admin_eth0.ip_admin_eth0').value.get() == [None]
    assert api.option('ip_admin_eth0.netmask_admin_eth0', 0).value.get() == None
    assert list(api.value.mandatory()) == ['ip_admin_eth0.ip_admin_eth0']
    api.option('ip_admin_eth0.ip_admin_eth0').value.reset()
    #
    api.option('ip_admin_eth0.ip_admin_eth0').value.set([''])
    assert api.option('ip_admin_eth0.ip_admin_eth0').value.get() == ['']
    assert api.option('ip_admin_eth0.netmask_admin_eth0', 0).value.get() == None
    assert list(api.value.mandatory()) == ['ip_admin_eth0.ip_admin_eth0']
    #
    api.property.read_write()
    api.option('ip_admin_eth0.ip_admin_eth0').value.set(['ip'])
    assert list(api.value.mandatory()) == []


def test_mandatory_follower():
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé", multi=True)
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "masque du sous-réseau",
                                   multi=True, properties=('mandatory', ))
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    descr = OptionDescription('o', '', [interface1])
    api = Config(descr)
    api.property.read_only()
    assert api.option('ip_admin_eth0.ip_admin_eth0').value.get() == []
    #
    api.property.read_write()
    api.option('ip_admin_eth0.ip_admin_eth0').value.set(['ip'])
    api.property.read_only()
    assert api.option('ip_admin_eth0.ip_admin_eth0').value.get() == ['ip']
    raises(PropertiesOptionError, "api.option('ip_admin_eth0.netmask_admin_eth0', 0).value.get()")
    #
    api.property.read_write()
    api.option('ip_admin_eth0.netmask_admin_eth0', 0).value.set('')
    api.property.read_only()
    assert api.option('ip_admin_eth0.ip_admin_eth0').value.get() == ['ip']
    raises(PropertiesOptionError, "api.option('ip_admin_eth0.netmask_admin_eth0', 0).value.get()")
    #
    api.property.read_write()
    api.option('ip_admin_eth0.netmask_admin_eth0', 0).value.set('ip')
    api.property.read_only()
    assert api.option('ip_admin_eth0.ip_admin_eth0').value.get() == ['ip']
    assert api.option('ip_admin_eth0.netmask_admin_eth0', 0).value.get() == 'ip'


def test_mandatory_warnings_follower():
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé", multi=True)
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "masque du sous-réseau",
                                   multi=True, properties=('mandatory', ))
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    descr = OptionDescription('o', '', [interface1])
    api = Config(descr)
    api.property.read_only()
    assert api.option('ip_admin_eth0.ip_admin_eth0').value.get() == []
    #
    api.property.read_write()
    assert list(api.value.mandatory()) == []
    api.option('ip_admin_eth0.ip_admin_eth0').value.set(['ip'])
    assert list(api.value.mandatory()) == ['ip_admin_eth0.netmask_admin_eth0']


def test_mandatory_warnings_symlink():
    descr = make_description_sym()
    api = Config(descr)
    api.option('str').value.set('')
    api.property.read_write()
    api.option('str').value.get()
    assert list(api.value.mandatory()) == ['str', 'str1', 'str3']
    api.option('str').property.add('frozen')
    api.property.read_only()
    assert list(api.value.mandatory()) == ['str', 'str1', 'str3']


#def test_mandatory_warnings_validate():
#    descr = make_description3()
#    api = Config(descr)
#    api.option('str').value.set('')
#    raises(ValueError, "list(api.value.mandatory())")
#    api.option('str').value.set('test')
#    raises(ValueError, "list(api.value.mandatory())")


def test_mandatory_warnings_validate_empty():
    descr = make_description2()
    api = Config(descr)
    api.option('str').value.set('')
    api.property.read_only()
    assert list(api.value.mandatory()) == ['str', 'str1', 'str3', 'unicode1']


def test_mandatory_warnings_requires():
    stroption = StrOption('str', 'Test string option', default="abc",
                          properties=('mandatory', ))
    stroption1 = StrOption('str1', 'Test string option',
                           properties=('mandatory', ))
    stroption2 = UnicodeOption('unicode2', 'Test string option',
                               properties=('mandatory', ))
    stroption3 = StrOption('str3', 'Test string option', multi=True, requires=[{'option': stroption, 'expected': 'yes', 'action': 'mandatory', 'transitive': False}])
    descr = OptionDescription('tiram', '', [stroption, stroption1, stroption2, stroption3])
    api = Config(descr)
    api.option('str').value.set('')
    api.property.read_write()
    api.option('str').value.get()
    assert list(api.value.mandatory()) == ['str', 'str1', 'unicode2']
    api.property.read_only()
    assert list(api.value.mandatory()) == ['str', 'str1', 'unicode2']
    api.property.read_write()
    api.option('str').value.set('yes')
    assert list(api.value.mandatory()) == ['str1', 'unicode2', 'str3']


def test_mandatory_warnings_requires_leadership():
    stroption = StrOption('str', 'Test string option', default="abc",
                          properties=('mandatory', ))
    stroption1 = StrOption('str1', 'Test string option', multi=True)
    stroption2 = StrOption('str2', 'Test string option', multi=True, requires=[{'option': stroption, 'expected': 'yes', 'action': 'mandatory', 'transitive': False}])
    leadership = Leadership('leader', 'leadership', [stroption1, stroption2])
    descr = OptionDescription('tiram', '', [stroption, leadership])
    api = Config(descr)
    api.option('str').value.set('')
    api.option('leader.str1').value.set(['str'])
    assert list(api.value.mandatory()) == ['str']
    api.option('str').value.set('yes')
    assert list(api.value.mandatory()) == ['leader.str2']


def test_mandatory_warnings_requires_leadership_follower():
    stroption = StrOption('str', 'Test string option', multi=True)
    stroption1 = StrOption('str1', 'Test string option', multi=True)
    stroption2 = StrOption('str2', 'Test string option', multi=True, requires=[{'option': stroption1, 'expected': 'yes', 'action': 'mandatory', 'transitive': False}])
    leadership = Leadership('leader', 'leadership', [stroption, stroption1, stroption2])
    descr = OptionDescription('tiram', '', [leadership])
    api = Config(descr)
    api.option('leader.str').value.set(['str'])
    assert list(api.value.mandatory()) == []
    api.option('leader.str1', 0).value.set('yes')
    assert list(api.value.mandatory()) == ['leader.str2']


def test_mandatory_od_disabled():
    descr = make_description()
    descr = OptionDescription('od', '', [descr])
    api = Config(descr)
    api.property.read_only()
    assert list(api.value.mandatory()) == ['tiram.str1', 'tiram.unicode2', 'tiram.str3']
    api.option('tiram').property.add('disabled')
    assert list(api.value.mandatory()) == []
